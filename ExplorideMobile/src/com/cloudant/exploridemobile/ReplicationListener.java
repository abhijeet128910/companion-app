package com.cloudant.exploridemobile;

/**
 * Created by pranav.r on 10/25/16.
 */

public interface ReplicationListener {
    public void replicationComplete();
    public void replicationError();
}
