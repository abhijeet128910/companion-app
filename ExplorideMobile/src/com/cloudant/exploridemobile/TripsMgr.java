package com.cloudant.exploridemobile;

import android.util.Log;

public class TripsMgr implements ReplicationListener {

    public static final String TAG = TripsMgr.class.getSimpleName();
    private static TripsMgr mTripsMgr;
    private ExpDocumentsModel mTripsModel;
    private static Object mTripsMgrObj = new Object();

    public void init() {
        mTripsModel = Helper.getTheDocumentManager().getDocumentsModel(ExpDocumentManager.DOC_TYPE_EXPTRIPS);
    }

    /**
     * get the trip manager
     *
     * @return
     */
    public static TripsMgr getTripsMgr() {
        if (mTripsMgr == null)
            synchronized (mTripsMgrObj) {
                if (mTripsMgr == null)
                    mTripsMgr = new TripsMgr();
            }
        return mTripsMgr;
    }

    public void replicationComplete() {

    }

    public void replicationError() {
        Log.e(TAG, "error()");
    }

    /**
     * When a journey is started this is called to add trip.
     * @param origin
     * @param destination
     * @param date
     * @return
     */
    public String addTrip(String origin, String destination, String date) {
        String tripId = "";
        try {
            ExpTrip mExpTrip = new ExpTrip(origin, destination, date);
            ExpDocument expDocument = mTripsModel.createDocument(mExpTrip);
            tripId = expDocument.getDocumentRevision().getId();
        } catch (Exception ce) {
            Log.e(TAG, ce.getMessage());
        }
        return tripId;
    }

}
