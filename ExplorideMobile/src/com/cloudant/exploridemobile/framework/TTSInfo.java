package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abhijeet on 8/5/17.
 */

public class TTSInfo {

    // TTS COMMANDS
    public static final int PLAY = 0;
    public static final int PAUSE = 1;
    public static final int STOP = 2;

    @SerializedName("tid")
    private int mId = 0;

    @SerializedName("tcmd")
    private int mCommand;

    @SerializedName("ttxt")
    private String mText;

    public int getId() {
        return mId;
    }

    public void setId(int ttsId) {
        mId = ttsId;
    }

    public int getCommand() {
        return mCommand;
    }

    public void setCommand(int command) {
        this.mCommand = command;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        this.mText = text;
    }
}