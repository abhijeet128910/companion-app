package com.cloudant.exploridemobile.framework;

import java.util.HashMap;
import java.util.List;

/**
 * Created on 28/06/17.
 */

public abstract class StoreInfoProvider {

    private String keyName = getKeyName();

    abstract String getKeyName();
}
