package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Model class to hold Selected Route Information
 * <p>
 * Author Mayank
 */

public class Navigation {


    @SerializedName("G")
    private ArrayList<GeoCoordinate> geoCoordinateList;

//    @SerializedName ("location")
//    private GeoCoordinate geoCoordinate;

    @SerializedName("S")
    private Status status;

    @SerializedName("DRcd")
    private DestinationReached destinationReached;

    public ArrayList<GeoCoordinate> getGeoCoordinateList() {
        return geoCoordinateList;
    }

    public void setGeoCoordinateList(ArrayList<GeoCoordinate> geoCoordinateList) {
        this.geoCoordinateList = geoCoordinateList;
    }
   /* public GeoCoordinate getGeoCoordinate() {
        return geoCoordinate;
    }

    public void setGeoCoordinate(GeoCoordinate geoCoordinate) {
        this.geoCoordinate = geoCoordinate;
    }*/

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public class GeoCoordinate {

        @SerializedName("lat")
        private double latitude;

        @SerializedName("lon")
        private double longitude;

        public GeoCoordinate(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }
    }

    public class Status {

        @SerializedName("exit")
        private boolean isExitNavigation;

        @SerializedName("dest")
        private String destination;

        @SerializedName("src")
        public String source;

        @SerializedName("desc")
        private String description;

        @SerializedName("sLat")
        private double srcLat;

        @SerializedName("sLng")
        private double srcLng;

        @SerializedName("DLat")
        private double destLat;

        @SerializedName("DLng")
        private double destLng;

        @SerializedName("DRch")
        private boolean destinationReached;

//        @SerializedName ("del")
//        private String delimiter;

        public boolean isExitNavigation() {
            return isExitNavigation;
        }

        public void setExitNavigation(boolean exitNavigation) {
            isExitNavigation = exitNavigation;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public double getSourceLat() {
            return srcLat;
        }

        public void setSourceLat(double sourceLat) {
            this.srcLat = sourceLat;
        }

        public double getSourceLng() {
            return srcLng;
        }

        public void setSourceLng(double sourceLng) {
            this.srcLng = sourceLng;
        }

        public double getDestLat() {
            return destLat;
        }

        public void setDestLat(double destLat) {
            this.destLat = destLat;
        }

        public double getDestLng() {
            return destLng;
        }

        public void setDestLng(double destLng) {
            this.destLng = destLng;
        }

        /*public String getDelimiter() {
            return delimiter;
        }

        public void setDelimiter(String delimiter) {
            this.delimiter = delimiter;
        }*/

        public boolean isDestinationReached() {
            return destinationReached;
        }

        public void setDestinationReached(boolean destinationReached) {
            this.destinationReached = destinationReached;
        }
    }

    public DestinationReached getDestinationReached() {
        return destinationReached;
    }

    public void setDestinationReached(DestinationReached destinationReached) {
        this.destinationReached = destinationReached;
    }

}
