package com.cloudant.exploridemobile.framework;

/**
 * Created on 15/11/16.
 */

public class ActionConstants {

    public static class ActionCode{

        public static final int WifiPWD = 1,WifiError=2, Restart = 3, ReqCoordinates = 4, EmailLogs = 5;

        public static final int WifiScanRequest = 6, WifiStopScan = 7, CheckInternet = 9, WifiConnectivityChanged = 10;
        public static final int InternetAvailable = 11, InternetNotAvailable = 12, WiFiConnectionCompleted = 13;

        public static final int RequestUpdatesCheck = 14, UpdatesAvailable = 15, UpdatesNotAvailable = 16, UpdatesDownloadFailed = 17
                , UpdatesDownloadNow = 18, UpdatesDownloadFinished = 19;

        //codes from 20-30 will be used for Download Maps
        public static final int MapPackageError = 20;
        public static final int MapInstallSuccess = 21;
        public static final int MapInstallError = 22;
        public static final int MapUninstallSuccess = 23;
        public static final int MapUninstallError = 24;
        public static final int CancelMapDownload = 25;
        public static final int MapEngineInitializationError = 26;

        public static final int VIDEO_START = 27, VIDEO_EXIT = 28, VIDEO_NEXT = 29, VIDEO_PREV = 30,
                VIDEO_PLAY = 31, VIDEO_PAUSE = 32;

        public static final int SPEECH_TXT = 33;
        public static final int CALL_ACTIONS = 34;

        public static final int HUD_APP_VERSION = 35;
        public static final int CONNECT_OBD = 36;

        public static final int MAP_INIT_FAILED = 37;
        public static final int TCP_IN = 40, TCP_DS = 38, TCP_EN = 39;

        public static final int IMAGE_SHOW = 41, IMAGE_HIDE = 42, IMAGE_NEXT = 43, IMAGE_PREV = 44;

        public static final int CHECK_NAVIGATION_STATE = 45;
        public static final int INACTIVE_NAVIGATION = 46;

        public static final int VERIFY_TEXT_SIZES_SHOW = 47, VERIFY_TEXT_SIZES_HIDE = 48;
        public static final int TEXT_UTILISATION_SHOW = 49, TEXT_UTILISATION_HIDE = 50;
        public static final int NAVIGATION_PAUSE = 51, NAVIGATION_RESUME = 52;

        public static final int GET_BT_NAME = 53;

        public static final int MUSIC_PLAY = 54, MUSIC_PAUSE = 55, MUSIC_STOP = 56, MUSIC_NEXT = 57,
                MUSIC_PREV = 58, TOGGLE_PLAY_PAUSE = 59, VOL_INCREASE = 60, VOL_DECREASE = 61;

        public static final int MAP_ENGINE_OFFLINE_ERROR = 62;

        public static final int OBD_CONN_CHN = 63;
        public static final int OBD_CONN_STATE = 64, OBD_CONN_STATE_RESP = 65, SC_NAME = 66;
        public static final int SEARCH_CONTACT_TERM = 67, SEARCH_CONTACT_RESP = 68, CONTACT_LIST_RESP = 69;
        public static final int LOCATION_SRC_REQ = 70, LOCATION_SRC_RESP = 71;
    }
}


