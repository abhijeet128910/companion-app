package com.cloudant.exploridemobile.framework;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pranav.r on 10/20/16.
 */
public class Credential implements Comparable<Credential> {
    @SerializedName("un")
    public String userName;
    @SerializedName("st")
    public String status ;
    @SerializedName("ke")
    public String apiKey ;
    @SerializedName("pwd")
    public String apiPassword ;
    @SerializedName("dbn")
    public String documentDbName ;
    @SerializedName("dbh")
    public String documentDbHost;
    @SerializedName("npwd")
    private String password;

    public String getUserName(){
        return userName;
    }
    public String getApiKey(){
        return apiKey;
    }
    public String getPassword(){
        return password;
    }
    public String getApiPassword(){
        return apiPassword;
    }
    public String getDocumentDbHost(){
        return documentDbHost;
    }
    public Credential(String userName, String password, String apiKey, String apiPassword, String documentDbHost){
        this.userName = userName;
        this.password = password;
        this.apiKey = apiKey;
        this.apiPassword = apiPassword;
        this.documentDbHost = documentDbHost;
    }

    @Override
    public int compareTo(@NonNull Credential credential) {
        if(credential.getUserName().equals(this.getUserName()))
            return 0;
        return -1;
    }
}
