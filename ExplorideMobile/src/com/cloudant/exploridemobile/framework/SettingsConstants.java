package com.cloudant.exploridemobile.framework;

/**
 * Created by peeyushupadhyay on 28/06/17.
 */

public class SettingsConstants {


    public static final String KEY_PREFERENCE_FILE = "com.exploride.hud.PREFERENCE_FILE_KEY";
    public static final String KEY_BASE = "com.exploride.hud.";
    public static final String KEY_TRANSFORMATION = "transform.";
    public static final String KEY_SOURCE_LOCATION = "source_location";
    public static final String KEY_DESTINATION_LOCATION = "destination_location";
    public static final String KEY_CURRENT_SONG_INDEX = "current_song_index";
    public static final String KEY_MUSIC_STATUS_PLAYING = "music_status_playing";
    public static final String KEY_NAVIGATION_STARTED = "navigation_started";
    public static final String KEY_ENABLE_DEBUG_MODE = "enable_debug_mode";
    public static final String KEY_TOP_NAVIGATION_VIEW_HUD = "top_navigation_view";
    public static final String KEY_ENABLE_OFFLINE_NAVIGATION = "offline_navigation";
    public static final String KEY_ENABLE_GESTURES = "gestures_status";
    public static final String KEY_ALS = "automatic_brightness";
    public static final String KEY_BRIGHTNESS_VALUE = "brightness_value";
    public static final String QUALIFIER_OBD = "obd";
    public static final String QUALIFIER_SC = "bluclik";
    public static final String KEY_LANGUAGE_US_ID = "language_us_id";
    public static final String KEY_MOTORWAYS_ENABLED = "motorways_enabled";
    public static final String KEY_MOTORWAYS_Test = "motorways_test";
    public static final String KEY_TOLL_ROADS_ENABLED = "toll_roads_enabled";
    public static final String KEY_FERRIES_ENABLED = "ferries_enabled";
    public static final String KEY_LANDMARKS_ENABLED = "landmarks_enabled";
    public static final String KEY_SPEED_ALERT_ENABLED = "speed_alert_enabled";
    public static final String KEY_LIVE_TRAFFIC_ENABLED = "live_traffic_enabled";
    public static final String KEY_TRACKING_ENABLED = "tracking_enabled";
    public static final String KEY_LOCALE_UNIT_KMS = "locale_unit_kms";
    public static final String KEY_VOICE_PREF = "tracking_enabled";
    public static final String KEY_NAVIGATION = "navigation";
    public static final String KEY_SHOW_ROUTE_ONLY = "show_route_only";

    public static final String KEY_DEVOPTIONS_ZOOM_LEVEL = "zoom-level";
    public static final String KEY_DEVOPTIONS_TILT_LEVEL = "tilt-level";
}
