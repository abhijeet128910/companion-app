package com.cloudant.exploridemobile.framework;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Coordinate implements Parcelable, Comparable {
    @SerializedName("lat")
    public double mLat;
    @SerializedName("lon")
    public double mLon;
    @SerializedName("loc")
    public Location mLoc;
    @SerializedName("sId")
    public int mSerialId;
    public Coordinate(Double lat, Double lon){
        if (lat != 0 && lon != 0) {
            mLat = lat;
            mLon = lon;
        }
    }
    public Coordinate(Location loc){
        if (loc != null && loc.getLatitude() != 0 && loc.getLongitude() != 0) {
            mLoc = loc;
            mLat = loc.getLatitude();
            mLon = loc.getLongitude();
        }
    }

    protected Coordinate(Parcel in) {
        double[] data = new double[2];
        in.readDoubleArray(data);
        this.mLat = data[0];
        this.mLon = data[1];
        this.mLoc = in.readParcelable(null);
        String a= "";
    }
    public void setSerialId(int serialId){
        mSerialId = serialId;
    }
    public static final Creator<Coordinate> CREATOR = new Creator<Coordinate>() {
        @Override
        public Coordinate createFromParcel(Parcel in) {
            return new Coordinate(in);
        }

        @Override
        public Coordinate[] newArray(int size) {
            return new Coordinate[size];
        }
    };

    @Override
    public String toString() {
        return mLat+","+mLon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDoubleArray(new double[] { this.mLat, this.mLon });
        dest.writeParcelable(mLoc, 0);
    }
    @Override
    public int compareTo(Object o) {
        if((o == null || ((Coordinate)o).mLoc == null|| this.mLoc == null))
            return 0;
        return this.mSerialId<((Coordinate)o).mSerialId?-1:1;
    }
    public double getLatitude(){
        return mLat;
    }
    public double getLongitude(){
        return mLon;
    }
}
