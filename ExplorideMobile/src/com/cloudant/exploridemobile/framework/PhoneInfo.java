package com.cloudant.exploridemobile.framework;

/**
 * Created by abhijeet on 13/7/17.
 */

public class PhoneInfo {
    private String btName;

    public String getBtName() {
        return btName;
    }

    public void setBtName(String btName) {
        this.btName = btName;
    }
}