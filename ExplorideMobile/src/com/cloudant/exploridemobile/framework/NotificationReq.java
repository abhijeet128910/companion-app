package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

public class NotificationReq {
    @SerializedName("rId")
    private int mReqId;
    @SerializedName("pr1")
    private int mProg1;
    @SerializedName("pr2")
    private int mProg2;
    @SerializedName("sub")
    private String mDownloadSubject;

    public NotificationReq(int reqId, int prog1, int prog2, String subject){
        mReqId = reqId;
        mProg1 = prog1;
        mProg2 = prog2;
        mDownloadSubject = subject;
    }

    public String getDownloadSubject() {
        return mDownloadSubject;
    }

    public void setDownloadSubject(String mDownloadSubject) {
        this.mDownloadSubject = mDownloadSubject;
    }

    public int getProgress1() {
        return mProg1;
    }

    public void setProgress1(int mProg1) {
        this.mProg1 = mProg1;
    }

    public int getProgress2() {
        return mProg2;
    }

    public void setProgress2(int mProg2) {
        this.mProg2 = mProg2;
    }

    public int getReqId() {
        return mReqId;
    }
}
