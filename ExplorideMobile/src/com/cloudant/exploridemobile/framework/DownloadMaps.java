package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Model class to hold download maps information
 * Author Mayank
 */

public class DownloadMaps {

    @SerializedName("id")
    private int mapPackageId;

    @SerializedName ("installed")
    private boolean installedMapPackageId;

    @SerializedName ("id_list")
    private List<Integer> mapPackageIdList;

    @SerializedName ("install")
    private boolean install;

    @SerializedName ("uninstall")
    private boolean uninstall;

    @SerializedName("pro")
    private int progress;

    @SerializedName("engine_onl")
    private boolean mapEngineOnline;

    public boolean isMapEngineOnline() {
        return mapEngineOnline;
    }

    public void setMapEngineOnline(boolean enableMapEngineOnline) {
        this.mapEngineOnline = enableMapEngineOnline;
    }

    public int getMapPackageId() {
        return mapPackageId;
    }

    public void setMapPackageId(int mapPackageId) {
        this.mapPackageId = mapPackageId;
    }

    public boolean isInstalledMapPackageId() {
        return installedMapPackageId;
    }

    public void setInstalledMapPackageId(boolean installedMapPackageId) {
        this.installedMapPackageId = installedMapPackageId;
    }

    public List<Integer> getMapPackageIdList() {
        return mapPackageIdList;
    }

    public void setMapPackageIdList(List<Integer> mapPackageIdList) {
        this.mapPackageIdList = mapPackageIdList;
    }

    public boolean isInstall() {
        return install;
    }

    public void setInstall(boolean install) {
        this.install = install;
    }

    public boolean isUninstall() {
        return uninstall;
    }

    public void setUninstall(boolean uninstall) {
        this.uninstall = uninstall;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }
}

