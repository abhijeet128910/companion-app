package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Class to hold weather information
 */

public class WeatherInfo {
    @SerializedName("WDesc")
    private String weatherDescription;

    @SerializedName("Temp")
    private int temperature;

    @SerializedName("Icon")
    private String iconName;

    @SerializedName("Humid")
    private int humidity;

    @SerializedName("PlcNm")
    private String placeName;

    public WeatherInfo() {
    }

    public WeatherInfo(int temperature, String weatherDescription, int humidity, String placeName) {
        this.temperature = temperature;
        this.weatherDescription = weatherDescription;
        this.humidity = humidity;
        this.placeName = placeName;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }
}
