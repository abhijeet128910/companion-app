package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 15/11/16.
 */

public class CannedMsg {
    @SerializedName("nu")
    String mNumber;
    @SerializedName("msg")
    String mMessage;
    @SerializedName("msts")
    int status;

    public static class SendMessageStatus {
        public static final int MESSAGE_SENT = 1;
        public static final int MESSAGE_NOT_SENT = 2;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}