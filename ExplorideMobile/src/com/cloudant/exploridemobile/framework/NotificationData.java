package com.cloudant.exploridemobile.framework;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by peeyushupadhyay on 30/03/17.
 */

/**
 * REFERENCE:
 * Missed Call:
 Extras: Bundle[{android.title=Missed call, android.subText=null, android.showChronometer=false, android.icon=17301631, android.text=Exploride Coolpad, android.progress=0, android.progressMax=0, android.showWhen=true, android.rebuild.applicationInfo=ApplicationInfo{80cd1bf com.android.server.telecom}, android.infoText=null, android.originatingUserId=0, android.progressIndeterminate=false}]

 ticker text null, vis 0, prio =0, number = 0, flags = 17
 sbn->pkg com.android.server.telecom


 Message:
 extras: Bundle[{android.title=3 new messages, android.textLines=[Ljava.lang.CharSequence;@ef30511, android.subText=null, android.template=android.app.Notification$InboxStyle, android.showChronometer=false, android.icon=2130837863, android.text=Exploride Coolpad, AM-TEST, android.progress=0, android.progressMax=0, android.showWhen=true, android.rebuild.applicationInfo=ApplicationInfo{52a8676 com.google.android.apps.messaging}, android.infoText=null, android.wearable.EXTENSIONS=Bundle[mParcelledData.dataSize=28], android.originatingUserId=0, android.progressIndeterminate=false}]

 sbn.getNotification().extras.get("android.textLines")
 [0]Exploride Coolpad  Hello, how are you doing test today for a message tomorrow test
 [1]TEST  Hi, your Order 21126121655 on 3rd March, 2017 Please check your Email ID upadhyay.peeyush9@gmail.com for more details!

 extras->mMap
 value[0] 3 new messages
 value[1]
 [0]Exploride Coolpad  Hello, how are you doing test today for a message tomorrow test
 [1]AM-TEST  Hi, your Order 21126121655 on 3rd March, 2017 Please check your Email ID upadhyay.peeyush9@gmail.com for more details!
 value[6]Exploride Coolpad, AM-TEST

 ticker text : Exploride Coolpad: Hello, how are you doing test today for a message tomorrow test
 sbn->pkg: com.google.android.apps.messaging
 Category: “msg”
 pr 1; vis 0


 Bundle[{android.title=Exploride Coolpad, android.subText=null, android.template=android.app.Notification$BigTextStyle, android.showChronometer=false, android.icon=2130837902, android.text=Hello, how are you doing test today for a message tomorrow test, android.progress=0, android.progressMax=0, android.selfDisplayName=You, android.messages=[Landroid.os.Parcelable;@d7b513b, android.showWhen=true, android.rebuild.applicationInfo=ApplicationInfo{ee3c058 com.google.android.apps.messaging}, android.people=[Ljava.lang.String;@7a3e5b1, android.largeIcon=android.graphics.Bitmap@8aaea96, android.bigText=Hello, how are you doing test today for a message tomorrow test, android.infoText=null, android.wearable.EXTENSIONS=Bundle[mParcelledData.dataSize=684], android.originatingUserId=0, android.progressIndeterminate=false}]


 Email:
 Bundle[{android.title=Peeyush Upadhyay, android.subText=upadhyay.peeyush9@gmail.com, android.template=android.app.Notification$BigTextStyle, android.showChronometer=false, android.icon=2130837893, android.text=test email, android.progress=0, android.progressMax=0, android.showWhen=true, android.rebuild.applicationInfo=ApplicationInfo{b26c88f com.google.android.gm}, android.people=[Ljava.lang.String;@e39271c, android.largeIcon=android.graphics.Bitmap@f615eb5, android.bigText=test email
 Hello,
 this is a day after a holiday and a day before weekend. So keep working!!! :-)
 Regards,
 Peeyush Upadhyay
 Software Lead
 Cell: +91 8123614346
 Exploride Technologies
 Drive Safe, Drive Connected!
 peeyush.u@exploride.com
 Disclaimer: The information in this email and any files transmitted with it is intended only for the addressee and may contain confidential and privileged material. Access to this email by anyone else is unauthorized. If you receive this email in error, please immediately delete the material from your computer. If you are not the intended recipient, any disclosure, copying, distribution or any action taken or omitted to be taken in reliance on it, is strictly prohibited. Statement and opinions expressed in this e-mail are those of the sender and do not necessarily reflect those of Exploride Inc. Exploride Inc. and its shareholders, subsidiaries and its employees accept no liability for any damage caused by any virus/error transmitted by this email.
 , android.infoText=null, android.wearable.EXTENSIONS=Bundle[mParcelledData.dataSize=1088], android.originatingUserId=0, android.progressIndeterminate=false}]


 ticker text: Peeyush Upadhyay
 Category: email
 pr, vis 0
 sbn->pkg: com.google.android.gm


 Missed call:
 Bundle[{android.title=Missed call, android.subText=null, android.showChronometer=false, android.icon=17301631, android.text=Exploride Coolpad, android.progress=0, android.progressMax=0, android.showWhen=true, android.rebuild.applicationInfo=ApplicationInfo{1db2e6b com.android.server.telecom}, android.infoText=null, android.originatingUserId=0, android.progressIndeterminate=false}]

 Category: null
 vis, pr 0
 sbn->pkg: com.android.server.telecom


 */
public class NotificationData implements Parcelable, Comparable {


    String ticker_text="";
    String category=""; // "email" for email, null for missed call, “msg” for message
    String pkg="", title="", subText="", text="", bigText="";
    String templateStyle=""; //0 for small text, 1 for big.
    String[] textLines;
    int id=0, textLinesLen = 0;
    long time=0;
    String key="";
    String name, contactNumber, contactEmail;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String[] getTextLines() {
        return textLines;
    }

    public void setTextLines(String[] textLines) {
        this.textLines = textLines;
    }


    public String getTicker_text() {
        return ticker_text;
    }

    public NotificationData setTicker_text(String ticker_text) {
        this.ticker_text = ticker_text; return this;
    }

    public String getCategory() {
        return category;
    }

    public NotificationData setCategory(String category) {
        this.category = category; return this;
    }

    public String getPkg() {
        return pkg;
    }

    public NotificationData setPkg(String pkg) {
        this.pkg = pkg; return this;
    }

    public String getTitle() {
        return title;
    }

    public NotificationData setTitle(String title) {
        this.title = title; return this;
    }

    public String getSubText() {
        return subText;
    }

    public NotificationData setSubText(String subText) {
        this.subText = subText; return this;
    }

    public String getText() {
        return text;
    }

    public NotificationData setText(String text) {
        this.text = text; return this;
    }

    public String getBigText() {
        return bigText;
    }

    public NotificationData setBigText(String bigText) {
        this.bigText = bigText; return this;
    }

    public String getTemplateStyle() {
        return templateStyle;
    }

    public NotificationData setTemplateStyle(String templateStyle) {
        this.templateStyle = templateStyle; return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public NotificationData(){

    }
    public NotificationData(int id, String key, String ticker_text, String category, String pkg,
                            String title, String subText, String text, String bigText, String templateStyle,
                            String[] textLines, long time){

        this.id = id;
        this.key = key;
        this.ticker_text=ticker_text;
        this.category=category;
        this.pkg=pkg;
        this.title=title;
        this.subText=subText;
        this.text=text;
        this.bigText=bigText;
        this.templateStyle=templateStyle;
        this.textLines = textLines;
        this.time = time;
    }

    public NotificationData(Parcel in) {
        try {
            ticker_text = in.readString();
            category = in.readString();
            pkg = in.readString();
            title = in.readString();
            subText = in.readString();
            text = in.readString();
            bigText = in.readString();
            templateStyle = in.readString();
            textLinesLen = in.readInt();
            textLines = new String[textLinesLen];
            in.readStringArray(textLines);
            id = in.readInt();
            key = in.readString();
            time = in.readLong();
            name = in.readString();
            contactNumber = in.readString();
            contactEmail = in.readString();
        }catch (Exception e){
            String a = "";
        }
        String val = "";
    }

    public static final Creator<NotificationData> CREATOR = new Creator<NotificationData>() {
        @Override
        public NotificationData createFromParcel(Parcel in) {
            return new NotificationData(in);
        }

        @Override
        public NotificationData[] newArray(int size) {
            return new NotificationData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ticker_text);
        dest.writeString(category);
        dest.writeString(pkg);
        dest.writeString(title);
        dest.writeString(subText);
        dest.writeString(text);
        dest.writeString(bigText);
        dest.writeString(templateStyle);
        dest.writeInt(textLines==null?0:textLines.length);
        dest.writeStringArray(textLines==null?new String[0]:textLines);
        dest.writeInt(id);
        dest.writeString(key);
        dest.writeLong(time);
        dest.writeString(name);
        dest.writeString(contactNumber);
        dest.writeString(contactEmail);
    }
    @Override
    public String toString(){
        return
                "id= "+ this.id +
                        "key= "+this.key +
                        "ticker_text= "+this.ticker_text+
                        "category= "+this.category+
                        "pkg= "+this.pkg+
                        "title= "+this.title+
                        "subText= "+this.subText+
                        "text= "+this.text+
                        "bigText= "+this.bigText+
                        "templateStyle= "+this.templateStyle+
                        "textLines= "+(this.textLines==null?"":this.textLines)+//expand this too //TODO
                        "time= "+this.time+
                        "name= "+this.name +
                        "contactNumber= "+this.contactNumber+
                        "contactEmail= "+this.contactEmail;
    }

    @Override
    public int compareTo(Object o) {
        if(o == null)
            return 0;
        return this.getTime()>((NotificationData)o).getTime()?-1:1;
    }
}