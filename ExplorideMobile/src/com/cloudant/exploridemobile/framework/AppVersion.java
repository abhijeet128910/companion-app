package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayank on 1/24/2017.
 */

public class AppVersion {
    @SerializedName ("VName")
    private String versionName;

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}
