package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhijeet on 18/1/17.
 */

public class TimeZone {
    @SerializedName("TzId")
    private String TimeZoneId;

    @SerializedName("TMil")
    private String timeMillis;

    public String getTimeZoneId() {
        return TimeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        TimeZoneId = timeZoneId;
    }

    public String getTimeMillis() {
        return timeMillis;
    }

    public void setTimeMillis(String timeMillis) {
        this.timeMillis = timeMillis;
    }
}