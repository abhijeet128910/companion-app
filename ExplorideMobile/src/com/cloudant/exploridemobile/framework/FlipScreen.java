package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhijeet on 1/4/2017.
 */

public class FlipScreen {
    @SerializedName("FlpVl")
    private boolean flipScreenXValue = false;

    public boolean getFlipScreenXValue() {
        return flipScreenXValue;
    }

    public void setFlipScreenXValue(boolean flipScreenXValue) {
        this.flipScreenXValue = flipScreenXValue;
    }
}