package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 15/11/16.
 */

public class WifiConnect {
    @SerializedName("")
    String mSSID;
    String mPwd;

    public String getSSID(){
        return mSSID;
    }

    public void setSSID(String ssid){
        mSSID = ssid;
    }

    public String getPwd(){
        return mPwd;
    }

    public void setPwd(String pwd){
        mPwd = pwd;
    }
}
