package com.cloudant.exploridemobile.framework;

/**
 * Created by abhijeet on 24/7/17.
 */

public class MusicInfo {
    private String mTrack, mAlbum, mArtist, mGenre, mTitle, mSongPath, mAlbumArt;
    private int mPlayingState, mProgress = -1, mIndex = -1;
    private long mDuration;

    public static int NONE = 0;
    public static int PLAYING = 1;
    public static int PAUSED = 2;
    public static int STOPPED = 3;

    public String getTrack() {
        return mTrack;
    }

    public void setTrack(String track) {
        mTrack = track;
    }

    public String getAlbum() {
        return mAlbum;
    }

    public void setAlbum(String album) {
        mAlbum = album;
    }

    public String getArtist() {
        return mArtist;
    }

    public void setArtist(String artist) {
        mArtist = artist;
    }

    public int getPlayingState() {
        return mPlayingState;
    }

    public void setPlayingState(int playingState) {
        mPlayingState = playingState;
    }

    public long getDuration() {
        return mDuration;
    }

    public void setDuration(long Duration) {
        mDuration = Duration;
    }

    public int getProgress() {
        return mProgress;
    }

    public void setProgress(int progress) {
        mProgress = progress;
    }

    public String getGenre() {
        return mGenre;
    }

    public void setGenre(String genre) {
        mGenre = genre;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getSongPath() {
        return mSongPath;
    }

    public void setSongPath(String songPath) {
        this.mSongPath = songPath;
    }

    public String getAlbumArt() {
        return mAlbumArt;
    }

    public void setAlbumArt(String albumArt) {
        mAlbumArt = albumArt;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }
}