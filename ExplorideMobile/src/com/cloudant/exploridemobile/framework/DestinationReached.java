package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhijeet on 11/14/2016.
 */

public class DestinationReached {

    @SerializedName("DeIm")
    public String destinationImageUrl;

    @SerializedName("DeNm")
    public String destinationName;

    @SerializedName("DeD")
    public String destinationDistance;

    @SerializedName("DeT")
    public String reachingTime;

    @SerializedName("DeS")
    public String averageSpeed;

    @SerializedName("DeLat")
    public String lat;

    @SerializedName("DeLng")
    public String lng;

    public String getDestinationImageUrl() {
        return destinationImageUrl;
    }

    public void setDestinationImageUrl(String destinationImageUrl) {
        this.destinationImageUrl = destinationImageUrl;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getDestinationDistance() {
        return destinationDistance;
    }

    public void setDestinationDistance(String destinationDistance) {
        this.destinationDistance = destinationDistance;
    }

    public String getReachingTime() {
        return reachingTime;
    }

    public void setReachingTime(String reachingTime) {
        this.reachingTime = reachingTime;
    }

    public String getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(String averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}