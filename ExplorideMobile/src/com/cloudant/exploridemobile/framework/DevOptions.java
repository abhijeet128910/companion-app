package com.cloudant.exploridemobile.framework;

import android.util.Log;

import com.cloudant.exploridemobile.framework.SyncPreferences.StoreType;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

/**
 * Created by Abhijeet on 12/2/2016.
 */

public class DevOptions extends StoreInfoProvider {

    public static final String TAG = DevOptions.class.getSimpleName();
    @SerializedName("DDt")
    private Destination destination;

    @SerializedName("Zm")
    private double zoomLevel;

    @SerializedName("Tlt")
    private float tiltLevel;

    @SerializedName("locSr")
    private LocationSource locationSource;

    @SerializedName("Tv")
    private TopMapView topView;

    @SerializedName("MoL")
    private AllowMockLocation allowMockLocations;

    @SerializedName("MIn")
    private float mockInterval;

    @SerializedName("DMod")
    private DebugMode debugMode;

    @SerializedName("RMod")
    private RouteOnlyMode routeOnlyMode;

    @SerializedName("ScTyp")
    private SCType scType;

    public TopMapView getTopView() {
        return topView;
    }

    public void setTopView(TopMapView topView) {
        this.topView = topView;
    }

    public double getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(double zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    public LocationSource getLocationSource() {
        return locationSource;
    }

    public void setLocationSource(LocationSource locationSource) {
        this.locationSource = locationSource;
    }

    public float getTiltLevel() {
        return tiltLevel;
    }

    public void setTiltLevel(float tiltLevel) {
        this.tiltLevel = tiltLevel;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public AllowMockLocation getAllowMockLocations() {
        return allowMockLocations;
    }

    public void setAllowMockLocations(AllowMockLocation allowMockLocations) {
        this.allowMockLocations = allowMockLocations;
    }

    public float getMockInterval() {
        return mockInterval;
    }

    public void setMockInterval(float mockInterval) {
        this.mockInterval = mockInterval;
    }

    public DebugMode getDebugMode() {
        return debugMode;
    }

    public void setDebugMode(DebugMode debugMode) {
        this.debugMode = debugMode;
    }

    public RouteOnlyMode isRouteOnlyMode() {
        return routeOnlyMode;
    }

    public void setRouteOnlyMode(RouteOnlyMode immersiveMode) {
        this.routeOnlyMode = immersiveMode;
    }

    public SCType getScType() {
        return scType;
    }

    public void setScType(SCType scType) {
        this.scType = scType;
    }

    @Override
    String getKeyName() {
        return null;
    }

    public HashMap<String, Object> getStoreData(int storetype) {

        StoreType[] storeTypeVal = StoreType.values();
        StoreType storeTypeEnum = StoreType.DEFAULT;
        for(StoreType c : StoreType.values()) {
            if (c.getValue() == storetype) {
                storeTypeEnum = c;
                break;
            }
        }
        switch (storeTypeEnum) {
            case NAVIG:
                return getNavigationData();
            case MAPSETTINGS:
                return getMapSettings();

        }
        return null;
    }

    public HashMap<String, Object> getMapSettings(){
        Log.i(TAG, "getMapSettings() for "+this.getClass().getSimpleName());
        HashMap<String, Object> storeData = new HashMap<>();

        if (routeOnlyMode != null)
            storeData.put(routeOnlyMode.getKeyName(), routeOnlyMode);
        if(topView!=null)
            storeData.put(topView.getKeyName(), topView);
        if (zoomLevel > 0)
            storeData.put(SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_DEVOPTIONS_ZOOM_LEVEL, zoomLevel);
        if (tiltLevel > 0)
            storeData.put(SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_DEVOPTIONS_TILT_LEVEL, tiltLevel);

        return storeData;
    }

    public HashMap<String, Object> getNavigationData(){
        Log.i(TAG, "getNavigationData() for "+this.getClass().getSimpleName());
        HashMap<String, Object> storeData = new HashMap<>();

        if(destination!=null)
            storeData.put(destination.getKeyName(), destination);

        return storeData;
    }

    public class LocationSource{

        @SerializedName("locP")
        private boolean isLocationFromPhone = true;
        public boolean getIsLocationFromPhone() {
            return isLocationFromPhone;
        }

        public void setIsLocationFromPhone(boolean isLocationFromPhone) {
            this.isLocationFromPhone = isLocationFromPhone;
        }

    }

    public class Destination extends StoreInfoProvider {
        @SerializedName("DPt")
        private String destination;

        @SerializedName("DLat")
        private Double destLat;

        @SerializedName("DLng")
        private Double destLon;

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public Double getDestLat() {
            return destLat;
        }

        public void setDestLat(Double destLat) {
            this.destLat = destLat;
        }

        public Double getDestLon() {
            return destLon;
        }

        public void setDestLon(Double destLon) {
            this.destLon = destLon;
        }

        public String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_DESTINATION_LOCATION;
        }
    }

    public class DebugMode{
        @SerializedName("En")
        private boolean enable;

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }
    }

    public class TopMapView extends StoreInfoProvider {
        @SerializedName("TvEn")
        private boolean enable;

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_TOP_NAVIGATION_VIEW_HUD;
        }
    }

    public class RouteOnlyMode extends StoreInfoProvider {
        @SerializedName("ROM")
        private boolean mIsRouteOnlyModeEnabled;

        public boolean isRouteOnlyModeEnabled() {
            return mIsRouteOnlyModeEnabled;
        }

        public void setRouteOnlyModeEnabled(boolean routeOnlyModeEnabled) {
            this.mIsRouteOnlyModeEnabled = routeOnlyModeEnabled;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_SHOW_ROUTE_ONLY;
        }

    }

    public class AllowMockLocation{
        @SerializedName("MoEn")
        private boolean enable;

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }
    }

    public class SCType {

        @SerializedName("Typ")
        private boolean bluClick;

        public boolean isBluClick() {
            return bluClick;
        }

        public void setBluClick(boolean bluClick) {
            this.bluClick = bluClick;
        }
    }
}