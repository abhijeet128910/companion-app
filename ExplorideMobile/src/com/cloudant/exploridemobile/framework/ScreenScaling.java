package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhijeet on 12/31/2016.
 */

public class ScreenScaling {

    @SerializedName("Ssr")
    private String scaleRate;

    @SerializedName("SOfx")
    private String offsetX;

    @SerializedName("SOfy")
    private String offsetY;

    public String getScaleRate() {
        return scaleRate;
    }

    public void setScaleRate(String scaleRate) {
        this.scaleRate = scaleRate;
    }

    public String getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(String offsetX) {
        this.offsetX = offsetX;
    }

    public String getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(String offsetY) {
        this.offsetY = offsetY;
    }
}