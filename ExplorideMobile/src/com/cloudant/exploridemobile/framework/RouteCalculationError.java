package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Model class to hold Route Calculation error
 * <p>
 * Author Mayank
 */

public class RouteCalculationError {

    @SerializedName("Err")
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
