package com.cloudant.exploridemobile.framework;

/**
 * Created by peeyushupadhyay on 31/10/17.
 */

public class Utils {

    public enum TaskPriority{
        Low(1),
        Medium(2),
        High(3);

        int mVal;
        TaskPriority(int value) {
            mVal = value;
        }
        public int getVal(){
            return mVal;
        }
    }
    public static TaskPriority getResolvedTaskPriority(int val){
        if(TaskPriority.High.getVal() == val)
            return TaskPriority.High;
        else if(TaskPriority.Medium.getVal() == val)
            return TaskPriority.Medium;
        else if(TaskPriority.Low.getVal() == val)
            return TaskPriority.Low;
        return TaskPriority.Medium;
    }
}
