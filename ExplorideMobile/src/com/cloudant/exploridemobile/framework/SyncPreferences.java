package com.cloudant.exploridemobile.framework;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

import static com.cloudant.exploridemobile.framework.SyncPreferences.StoreType.MAPSETTINGS;
import static com.cloudant.exploridemobile.framework.SyncPreferences.StoreType.NAVPREFS;

/**
 * Class for preferences that are set by user through app/HUD/web.
 * you should instantiate only the required parameters that are to be
 * synced to the other endpoints.
 */
public class SyncPreferences extends Pref {

    public static final String TAG = SyncPreferences.class.getSimpleName();
    @SerializedName ("H")
    //instance of Hear class for Hear preferences
    Hear mHear;
    @SerializedName ("V")
    //instance of Voice class for Voice preferences
    VoicePref mVoicePref;
    @SerializedName ("L")
    //instance of Language class for Language preferences
    Language mLanguage;
    @SerializedName ("N")
    //instance of Navigation class for Navigation preferences
    Navigation mNav;

    @SerializedName ("NMo")
    //instance of Navigation class for Navigation preferences
    Motorway motorway;

    @SerializedName("NTo")
    //instance of Navigation class for Navigation preferences
    Tollroad tollroad;

    @SerializedName("NFe")
    //instance of Navigation class for Navigation preferences
    Ferries ferries;

    @SerializedName("NLd")
    //instance of Navigation class for Navigation preferences
    Landmarks landmarks;

    @SerializedName("NLiTr")
    //instance of Navigation class for Navigation preferences
    LiveTraffic liveTraffic;

    @SerializedName("NSpa")
    //instance of Navigation class for Navigation preferences
    SpeedAlert speedAlert;

    @SerializedName("NTr")
    //instance of Navigation class for Navigation preferences
    AllowTracking allowTracking;

    @SerializedName("Br")
    //instance of Brightness class for HUD shouldGetBrightness preferences
    Brightness brightness;

    @SerializedName("Lo")
    //instance of Locale class
            Locale locale;

    @SerializedName("OflnNv")
    //instance of OfflineNavigation class
    OfflineNavigation offlineNavigation;

    @SerializedName("Ges")
    //instance of Gestures class
    private Gestures gestures;

    public SyncPreferences(){

    }

    public void init(){

        Hear hear = new Hear();
        setHear(hear);

        VoicePref voicePref = new VoicePref();
        setVoicePref(voicePref);

        Language language = new Language();
        setLanguage(language);

        Navigation navigation = new Navigation();
        setNavigation(navigation);

        Motorway motorway = new Motorway();
        setMotorway(motorway);

        Tollroad tollroad = new Tollroad();
        setTollroad(tollroad);

        Ferries ferries = new Ferries();
        setFerries(ferries);

        Landmarks landmarks = new Landmarks();
        setLandmarks(landmarks);

        SpeedAlert speedAlert = new SpeedAlert();
        setSpeedAlert(speedAlert);

        LiveTraffic liveTraffic = new LiveTraffic();
        setLiveTraffic(liveTraffic);

        AllowTracking allowTracking = new AllowTracking();
        setAllowTracking(allowTracking);

        Brightness brightness = new Brightness();
        setBrightness(brightness);

        Locale locale = new Locale();
        setLocale(locale);

        OfflineNavigation offlineNavigation = new OfflineNavigation();
        setOfflineNavigation(offlineNavigation);

        Gestures gestures = new Gestures();
        setGestures(gestures);
    }
    /**
     * gets the HEAR instance with this class
     * @return HEAR instance.
     */
    public Hear getHear(){
        return mHear;
    }

    /**
     * sets the HEAR instance. call this setter before serializing it
     * @param hear HEAR instance to set
     */
    public void setHear(Hear hear){
        mHear = hear;
    }


    public VoicePref getVoicePref() {
        return mVoicePref;
    }

    public void setVoicePref(VoicePref voicePref) {
        this.mVoicePref = voicePref;
    }

    /**
     * gets the Language instance with this class
     * @return Language instance.
     */
    public Language getLanguage(){
        return mLanguage;
    }

    /**
     * sets the Language instance. call this setter before serializing it
     * @param language HEAR instance to set
     */
    public void setLanguage(Language language){
        mLanguage = language;
    }
    /**
     * gets the Navigation instance with this class
     * @return Navigation instance.
     */
    public Navigation getNavigation(){
        return mNav;
    }

    /**
     * sets the Navigation instance. call this setter before serializing it
     * @param navigation Navigation instance to set
     */
    public void setNavigation(Navigation navigation){
        mNav = navigation;
    }

    public Motorway getMotorway() {
        return motorway;
    }

    public void setMotorway(Motorway motorway) {
        this.motorway = motorway;
    }

    public Tollroad getTollroad() {
        return tollroad;
    }

    public void setTollroad(Tollroad tollroad) {
        this.tollroad = tollroad;
    }

    public Ferries getFerries() {
        return ferries;
    }

    public void setFerries(Ferries ferries) {
        this.ferries = ferries;
    }

    public Landmarks getLandmarks() {
        return landmarks;
    }

    public void setLandmarks(Landmarks landmarks) {
        this.landmarks = landmarks;
    }

    public LiveTraffic getLiveTraffic() {
        return liveTraffic;
    }

    public void setLiveTraffic(LiveTraffic liveTraffic) {
        this.liveTraffic = liveTraffic;
    }

    public SpeedAlert getSpeedAlert() {
        return speedAlert;
    }

    public void setSpeedAlert(SpeedAlert speedAlert) {
        this.speedAlert = speedAlert;
    }

    public AllowTracking getAllowTracking() {
        return allowTracking;
    }

    public void setAllowTracking(AllowTracking allowTracking) {
        this.allowTracking = allowTracking;
    }

    /**
     * gets {@link Brightness} instance that is to be set on HUD
     * @return int value of shouldGetBrightness (range 0-255)
     */
    public Brightness getBrightness() {
        return brightness;
    }

    /**
     * sets {@link Brightness} instance to be sent to the HUD
     * @param brightness: int value of shouldGetBrightness (range 0-255)
     */
    public void setBrightness(Brightness brightness) {
        this.brightness = brightness;
    }

    /**
     * gets {@link Locale}instance with this class
     * @return Locale class instance
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * sets {@link Locale} instance. call this setter before serializing it
     * @param locale: {@link Locale} instance
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public OfflineNavigation getOfflineNavigation() {
        return offlineNavigation;
    }

    public void setOfflineNavigation(OfflineNavigation offlineNavigation) {
        this.offlineNavigation = offlineNavigation;
    }

    public Gestures getGestures() {
        return gestures;
    }

    public void setGestures(Gestures gestures) {
        this.gestures = gestures;
    }

    @Override
    String getKeyName() {
        return null;
    }

    public enum StoreType{
        DEFAULT(0),
        NAVPREFS(1),
        VOICE(2),
        NAVIG(3),
        MAPSETTINGS(4);

        int value;

        StoreType(int value){
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public HashMap<String, Object> getStoreData(int storetype) {

        StoreType[] storeTypeVal = StoreType.values();
        StoreType storeTypeEnum = StoreType.DEFAULT;
        for(StoreType c : StoreType.values()) {
            if (c.getValue() == storetype) {
                storeTypeEnum = c;
                break;
            }
        }
        switch (storeTypeEnum) {
            case NAVPREFS:
                return getNavigationPrefs();
            case MAPSETTINGS:
                return getMapSettings();

        }
        return null;
    }
    public HashMap<String, Object> getNavigationPrefs(){
        Log.i(TAG, "getNavigationPrefs() for "+this.getClass().getSimpleName());
        HashMap<String, Object> storeData = new HashMap<>();
        int storeType = NAVPREFS.value;
        if(mHear!=null){
            storeData.put(mHear.getKeyName(), mHear);
        }
        if (motorway != null)
            storeData.put(motorway.getKeyName(), motorway);

        if (tollroad != null)
            storeData.put(tollroad.getKeyName(), tollroad);

        if (ferries != null)
            storeData.put(ferries.getKeyName(), ferries);

        if (landmarks != null)
            storeData.put(landmarks.getKeyName(), landmarks);

        if (speedAlert != null)
            storeData.put(speedAlert.getKeyName(), speedAlert);

        if (liveTraffic != null)
            storeData.put(liveTraffic.getKeyName(), liveTraffic);

        if (allowTracking != null)
            storeData.put(allowTracking.getKeyName(), allowTracking);

        return storeData;
    }

    public HashMap<String, Object> getMapSettings(){
        Log.i(TAG, "getMapSettings() for "+this.getClass().getSimpleName());
        HashMap<String, Object> storeData = new HashMap<>();
        int storeType = MAPSETTINGS.value;
        if(locale!=null){
            storeData.put(locale.getKeyName(), locale);
        }
        if (offlineNavigation != null)
            storeData.put(offlineNavigation.getKeyName(), offlineNavigation);

        return storeData;
    }

    /**
     * Class for HEAR preferences
     */
    public class Hear extends Pref {
        @SerializedName ("Ml")
        public boolean mMale = false;

        public void setIsMaleVoice(boolean val){
            mMale = val;
        }

        @Override
        String getKeyName() {
            return null;
        }
    }
    /**
     * Class for Voice preferences
     */
    public class VoicePref extends Pref {
        @SerializedName ("Vo")
        public boolean isOnline = false;

        public void setIsVoiceOnline(boolean val){
            isOnline = val;
        }

        public boolean isOnline() {
            return isOnline;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_VOICE_PREF;
        }
    }

    /**
     * Class for Language preferences
     */
    public class Language extends Pref {
        @SerializedName ("La")
        public Languages mLang = Languages.ENG_US;


        public void setLang(Languages lang){
            mLang = lang;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_LANGUAGE_US_ID;
        }
    }
    public enum Languages { ENG_US, ENG_UK};

    /**
     * Class for Navigation preferences
     */
    public class Navigation extends Pref {
        @SerializedName ("De")
        private String mDest = "";

        public void setDest(String dest){
            mDest = dest;
        }
        public String getDest(){
            return mDest;
        }

        @Override
        public String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_NAVIGATION;
        }
    }

    /**
     * Class for Navigation Option: Motorways
     */
    public class Motorway extends Pref {
        @SerializedName ("Mw")
        private Boolean enableMotorway = true;

        public Boolean isEnableMotorway() {
            return enableMotorway;
        }

        public void setEnableMotorway(Boolean enableMotorway) {
            this.enableMotorway = enableMotorway;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_MOTORWAYS_ENABLED;
        }
    }

    /**
     * Class for Navigation Option: Tollroads
     */
    public class Tollroad extends Pref {
        @SerializedName ("Etr")
        private boolean enableTollroad;

        public boolean isEnableTollroad() {
            return enableTollroad;
        }

        public void setEnableTollroad(boolean enableTollroad) {
            this.enableTollroad = enableTollroad;
        }

        @Override
        public String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_TOLL_ROADS_ENABLED;
        }
    }

    /**
     * Class for Navigation Option: Ferries
     */
    public class Ferries extends Pref {
        @SerializedName("Ef")
        private boolean enableFerries;

        public boolean isEnableFerries() {
            return enableFerries;
        }

        public void setEnableFerries(boolean enableFerries) {
            this.enableFerries = enableFerries;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_FERRIES_ENABLED;
        }
    }

    /**
     * Class for Navigation Option: Landmarks
     */
    public class Landmarks extends Pref {
        @SerializedName("EL")
        private boolean enableLandmarks;

        public boolean isEnableLandmarks() {
            return enableLandmarks;
        }

        public void setEnableLandmarks(boolean enableLandmarks) {
            this.enableLandmarks = enableLandmarks;
        }
        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_LANDMARKS_ENABLED;
        }
    }

    /**
     * Class for Navigation Option: SpeedAlert
     */
    public class SpeedAlert extends Pref {
        @SerializedName("ESp")
        private boolean enableSpeedAlert;

        public boolean isEnableSpeedAlert() {
            return enableSpeedAlert;
        }

        public void setEnableSpeedAlert(boolean enableSpeedAlert) {
            this.enableSpeedAlert = enableSpeedAlert;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_SPEED_ALERT_ENABLED;
        }
    }

    /**
     * Class for Navigation Option: Live traffic
     */
    public class LiveTraffic extends Pref {
        @SerializedName("ETrf")
        private boolean enableLiveTraffic;

        public boolean isEnableLiveTraffic() {
            return enableLiveTraffic;
        }

        public void setEnableLiveTraffic(boolean enableLiveTraffic) {
            this.enableLiveTraffic = enableLiveTraffic;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_LIVE_TRAFFIC_ENABLED;
        }
    }

    /**
     * Class for Navigation Option: Allow tracking
     */
    public class AllowTracking extends Pref {
        @SerializedName("ETr")
        private boolean enableTracking;

        public boolean isEnableTracking() {
            return enableTracking;
        }

        public void setEnableTracking(boolean enableTracking) {
            this.enableTracking = enableTracking;
        }

        @Override
        String getKeyName() {
            return "";
        }
    }

    /**
     * Class for Navigation Option: Offline Navigation
     */
    public class OfflineNavigation extends Pref {
        @SerializedName("Ofln")
        private boolean isOffline;

        public boolean isOffline() {
            return isOffline;
        }

        public void setOffline(boolean offline) {
            isOffline = offline;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_ENABLE_OFFLINE_NAVIGATION;
        }
    }

    /**
     * Class for Navigation Option: Enable Gestures
     */
    public class Gestures extends Pref {
        @SerializedName("GesEn")
        private boolean isEnabled;

        public boolean isEnabled() {
            return isEnabled;
        }

        public void setEnabled(boolean enabled) {
            isEnabled = enabled;
        }

        @Override
        String getKeyName() {
            return "";
        }
    }

    /**
     * Class for HUD Brightness preferences
     */
        public class Brightness extends Pref {
        @SerializedName("val")
        private int value;

        @SerializedName("br")
        private boolean shouldGetBrightness;

        @SerializedName("auto")
        private boolean isAutomaticBrightnessOn;

        //cannot use isAutomaticBrightnessOn
        @SerializedName("als")
        private boolean isALS = true;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public boolean isShouldGetBrightness() {
            return shouldGetBrightness;
        }

        public void setShouldGetBrightness(boolean shouldGetBrightness) {
            this.shouldGetBrightness = shouldGetBrightness;
        }

//        public boolean isAutomaticBrightnessOn() {
//            return isAutomaticBrightnessOn;
//        }
//
//        public void setAutomaticBrightnessOn(boolean automaticBrightnessOn) {
//            isAutomaticBrightnessOn = automaticBrightnessOn;
//        }

        public void setALS(boolean isAls) {
            this.isALS = isAls;
        }

        public boolean isALS() {
            return this.isALS;
        }

        @Override
        String getKeyName() {
            return "";
        }
    }


    /**
     * Class for Locale preferences
     */
    public class Locale extends Pref {
        @SerializedName ("Ml")
        public boolean isLocaleUnitKms = false;

        public void setIsLocaleUnitKms(boolean val){
            isLocaleUnitKms = val;
        }

        @Override
        String getKeyName() {
            return SettingsConstants.KEY_TRANSFORMATION + SettingsConstants.KEY_LOCALE_UNIT_KMS;
        }

    }
}

abstract class Pref extends StoreInfoProvider {

}