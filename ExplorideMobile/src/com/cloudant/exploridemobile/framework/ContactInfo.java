package com.cloudant.exploridemobile.framework;

import com.cloudant.exploridemobile.framework.extn.VoiceData;

/**
 * @author Abhijeet
 */

public class ContactInfo extends VoiceData {
    private String id, name, number, type;

    public ContactInfo(String id, String name, String number) {
        super(VoiceDataType.CONTACT, id);
        this.id = id;
        this.name = name;
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return name + ":" + number;
    }

    public void setName(String name) {
        this.name = name;
    }
}