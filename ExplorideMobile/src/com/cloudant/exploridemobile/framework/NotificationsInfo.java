package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class NotificationsInfo {
    @SerializedName("ND")
    private ArrayList<NotificationData> notificationData;
    int notificationType; //freshlist = 1, new:2, del = 3

    public ArrayList<NotificationData> getNotificationData() {
        return notificationData;
    }

    public void setNotificationData(ArrayList<NotificationData> notificationData) {
        this.notificationData = notificationData;
    }

    public int getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(int notification_type) {
        notificationType = notification_type;
    }
}
