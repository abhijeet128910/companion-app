package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created on 15/11/16.
 */

public class CallInfo {
    @SerializedName("nu")
    String mNumber;
    @SerializedName("nm")
    String mName;
    @SerializedName("st")
    Date mStart;
    @SerializedName("en")
    Date mEnd;
    @SerializedName("code")
    int mCallCode;
    @SerializedName("ac")
    int mAction;
    @SerializedName("pic")
    String mPhoto;

    public static class CallConstants{
        public static final int IncomingCallReceived = 1;
        public static final int IncomingCallAnswered = 2;
        public static final int IncomingCallEnded = 3;
        public static final int OutgoingCallStarted = 4;
        public static final int OutgoingCallEnded = 5;
        public static final int MissedCall = 6;
        public static final int CallNotification = 7;
        public static final int ContactDetails = 8;
    }

    public static class CallActions{
        public static final int AcceptCall = 51;
        public static final int RejectCall = 52;
        public static final int EndCall = 53;
        public static final int Call = 54;
    }

    public CallInfo(){

    }

    public CallInfo(String name, String number, Date start, Date end, int callCode, String photo){
        setName(name);
        setNumber(number);
        setStart(start);
        setEnd(end);
        setCallCode(callCode);
        setPhoto(photo);
    }
    public CallInfo(String number, Date start, Date end, int callCode){
        setNumber(number);
        setStart(start);
        setEnd(end);
        setCallCode(callCode);
    }
    public String getNumber(){
        return mNumber;
    }
    public String getName() {
        return mName;
    }
    public Date getStart(){
        return mStart;
    }
    public Date getEnd(){
        return mEnd;
    }
    public int getCallCode(){
        return mCallCode;
    }
    public int getCallAction(){
        return mAction;
    }

    public void setNumber(String number){
        mNumber = number;
    }
    public void setName(String mName) {
        this.mName = mName;
    }
    public void setStart(Date start){
        mStart = start;
    }
    public void setEnd(Date end){
        mEnd = end;
    }
    public void setCallCode(int callCode){
        mCallCode = callCode;
    }
    public void setCallAction(int callAction){
        mAction = callAction;
    }
    public String getPhoto() {
        return mPhoto;
    }
    public void setPhoto(String photo) {
        this.mPhoto = photo;
    }
}
