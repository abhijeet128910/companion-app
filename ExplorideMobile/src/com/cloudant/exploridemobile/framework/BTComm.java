package com.cloudant.exploridemobile.framework;


import com.cloudant.exploridemobile.Helper;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Locale;

/**
 * copy of this class to be maintained in the other project HUD/Mobile
 */
public class BTComm {

    @SerializedName("AcCo")//ActionCode
            int mActionCode;//
    @SerializedName("Guid")
    String mGuid;
    @SerializedName("SyP")
    SyncPreferences mSyncPreferences;
    @SerializedName("Cr")
    Credential mCredential;
    @SerializedName("Nav")
    Navigation mNavigation;
    //    @SerializedName("DRcd")
//    DestinationReached destinationReached;
    @SerializedName("Wi")
    WifiConnect mWifiConnect;

    @SerializedName("WiSc")
    List<WifiScanResult> mScanResults;

    @SerializedName("Cor")
    Coordinate mCoordinate;

    @SerializedName("BTS")
    BTSpam mBtSpam;

    @SerializedName("NfR")
    NotificationReq mNotificationReq;

    @SerializedName("SFlp")
    FlipScreen flipScreen;

    @SerializedName("data")
    String mData;

    @SerializedName("TTS")
    TTSInfo mTTSInfo;

    @SerializedName("DevOps")
    private DevOptions devOptions;

    @SerializedName("DwnMp")
    private DownloadMaps downloadMaps;


    @SerializedName("NI")
    private NotificationsInfo mNotificationsInfo;

    @SerializedName("Sscl")
    private ScreenScaling screenScaling;

    @SerializedName("CalIn")
    private CallInfo mCallInfo;

    @SerializedName("Tz")
    private TimeZone timeZone;

    @SerializedName("RouteCalErr")
    private RouteCalculationError routeCalculationError;

    @SerializedName("NavOptn")
    private NavigationOption navigationOption;

    @SerializedName("AppVer")
    private AppVersion appVersion;

    @SerializedName("WInfo")
    private WeatherInfo weatherInfo;

    @SerializedName("RDOf")
    private RequestDataOffline requestDataOffline;

    @SerializedName("dl")
    private Locale devLocale;

    @SerializedName("CanMsg")
    private CannedMsg cannedMsg;

    @SerializedName("Cntct")
    private List<ContactInfo> mContactsList;

    @SerializedName("Music")
    private MusicInfo mMusicInfo;

    @SerializedName("PhnInfo")
    private PhoneInfo phoneInfo;

    @SerializedName("Pr")
    private int priority;

    public ScreenScaling getScreenScaling() {
        return screenScaling;
    }

    public void setScreenScaling(ScreenScaling screenScaling) {
        this.screenScaling = screenScaling;
    }

    public BTComm() {
        mGuid = Helper.genRandom32Hex();
    }

    public String getData()
    {
        return mData;
    }

    public void setData(String data){
        mData = data;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
    public int getPriority() {
        return this.priority;
    }

    public TTSInfo getTTSInfo() {
        return mTTSInfo;
    }

    public void setTTSInfo(TTSInfo ttsInfo) {
        this.mTTSInfo= ttsInfo;
    }

    public NotificationsInfo getNotificationsInfo()
    {
        return mNotificationsInfo;
    }

    public void setNotificationsInfo(NotificationsInfo notificationsInfo){
        mNotificationsInfo = notificationsInfo;
    }
    public Locale getDevLocale()
    {
        return devLocale;
    }

    public void setDevLocale(Locale devLocale){
        this.devLocale = devLocale;
    }

    public CallInfo getCallInfo()
    {
        return mCallInfo;
    }

    public void setCallInfo(CallInfo callInfo){
        mCallInfo = callInfo;
    }


    public int getActionCode()
    {
        return mActionCode;
    }

    public void setActionCode(int actionCode){
        mActionCode = actionCode;
    }


    public List<WifiScanResult> getWifiScanResult()
    {
        return mScanResults;
    }

    public void setWifiScanResult(List<WifiScanResult> scanResults){
        mScanResults = scanResults;
    }

    public SyncPreferences getSyncPreferences()
    {
        return mSyncPreferences;
    }
    public void setSyncPreferences(SyncPreferences syncPreferences){
        mSyncPreferences = syncPreferences;
    }

    public NotificationReq getNotificationReq()
    {
        return mNotificationReq;
    }
    public void setNotificationReq(NotificationReq notificationReq){
        mNotificationReq = notificationReq;
    }


    public BTSpam getBTSpam()
    {
        return mBtSpam;
    }
    public void setBTSpam(BTSpam coordinate){
        mBtSpam = coordinate;
    }

    public Coordinate getCoordinate()
    {
        return mCoordinate;
    }
    public void setCoordinate(Coordinate coordinate){
        mCoordinate = coordinate;
    }

    public Credential getCredentials()
    {
        return mCredential;
    }
    public void setCredentials(Credential credential){
        mCredential = credential;
    }

//    public DestinationReached getDestinationReached() {
//        return destinationReached;
//    }
//
//    public void setDestinationReached(DestinationReached destinationReached) {
//        this.destinationReached = destinationReached;
//    }

    public Navigation getNavigation() {
        return mNavigation;
    }

    public void setNavigation(Navigation mNavigation) {
        this.mNavigation = mNavigation;
    }

    public WifiConnect getWifiConnect()
    {
        return mWifiConnect;
    }

    public void setWifiConnect(WifiConnect wifiConnect){
        mWifiConnect = wifiConnect;
    }

    public DevOptions getDevOptions() {
        return devOptions;
    }

    public void setDevOptions(DevOptions devOptions) {
        this.devOptions = devOptions;
    }

    /**
     * sets the DownloadMap instance. call this setter before serializing it
     * @param downloadMaps Download instance to set
     */
    public void setDownloadMaps(DownloadMaps downloadMaps) {
        this.downloadMaps = downloadMaps;
    }

    /**
     * gets DownloadMaps instance with this class
     * @return DownloadMaps instance
     */
    public DownloadMaps getDownloadMaps() {
        return downloadMaps;
    }

    public FlipScreen getFlipScreen() {
        return flipScreen;
    }

    public void setFlipScreen(FlipScreen flipScreen) {
        this.flipScreen = flipScreen;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public RouteCalculationError getRouteCalculationError() {
        return routeCalculationError;
    }

    public void setRouteCalculationError(RouteCalculationError routeCalculationError) {
        this.routeCalculationError = routeCalculationError;
    }

    public NavigationOption getNavigationOption() {
        return navigationOption;
    }

    public void setNavigationOption(NavigationOption navigationOption) {
        this.navigationOption = navigationOption;
    }

    public AppVersion getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(AppVersion appVersion) {
        this.appVersion = appVersion;
    }

    public WeatherInfo getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(WeatherInfo weatherInfo) {
        this.weatherInfo = weatherInfo;
    }

    public RequestDataOffline getRequestDataOffline() {
        return requestDataOffline;
    }

    public void setRequestDataOffline(RequestDataOffline requestDataOffline) {
        this.requestDataOffline = requestDataOffline;
    }

    public CannedMsg getCannedMsg() {
        return cannedMsg;
    }

    public void setCannedMsg(CannedMsg cannedMsg) {
        this.cannedMsg = cannedMsg;
    }

    public List<ContactInfo> getContactList() {
        return mContactsList;
    }

    public void setContactList(List<ContactInfo> contactsList) {
        this.mContactsList = contactsList;
    }

    public MusicInfo getMusicInfo() {
        return mMusicInfo;
    }

    public void setMusicInfo(MusicInfo musicInfo) {
        mMusicInfo = musicInfo;
    }

    public PhoneInfo getPhoneInfo() {
        return phoneInfo;
    }

    public void setPhoneInfo(PhoneInfo phoneInfo) {
        this.phoneInfo = phoneInfo;
    }
}
