package com.cloudant.exploridemobile.framework.extn;

/**
 * Created by peeyushupadhyay on 20/07/17.
 */

public class VoiceData {
    VoiceDataType mVoiceDataType;
    String mUuid;

    protected VoiceData(VoiceDataType voiceDataType, String uuid){
        mVoiceDataType = voiceDataType;
        mUuid = uuid;
    }

    public enum VoiceDataType{
        DEFAULT(0),
        PLACE(1),
        CONTACT(2);

        int value;

        VoiceDataType(int value){
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    public VoiceDataType getVoiceDataType(){
        return mVoiceDataType;
    }
    public String getUUid(){
        return mUuid;
    }
}

