package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Model class to hold Navigation Options error
 *
 * Author: Mayank(6/16/2017).
 */

public class NavigationOption {

    @SerializedName("Optn")
    private String option;

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }
}
