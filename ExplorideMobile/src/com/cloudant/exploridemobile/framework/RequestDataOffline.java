package com.cloudant.exploridemobile.framework;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhijeet on 10/2/17.
 */

public class RequestDataOffline {
    @SerializedName("OW")
    boolean requestWeather;

    @SerializedName("OTm")
    boolean requestTime;

    @SerializedName("OLc")
    boolean requestLocation;

    public boolean isRequestWeather() {
        return requestWeather;
    }

    public void setRequestWeather(boolean requestWeather) {
        this.requestWeather = requestWeather;
    }

    public boolean isRequestTime() {
        return requestTime;
    }

    public void setRequestTime(boolean requestTime) {
        this.requestTime = requestTime;
    }

    public boolean isRequestLocation() {
        return requestLocation;
    }

    public void setRequestLocation(boolean requestLocation) {
        this.requestLocation = requestLocation;
    }
}