package com.cloudant.exploridemobile.framework;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class BTSpam {
    @SerializedName("uuid")
    public int mUuid;
    @SerializedName("data")
    public String mData;
    public BTSpam(int uuid, String data){
        mUuid = uuid;
        mData = data;
    }

}
