package com.cloudant.exploridemobile.framework;

/**
 * Created by peeyushupadhyay on 28/12/16.
 */

public class WifiScanResult {

    /**
     * The network name.
     */
    public String SSID;

    /**
     * The address of the access point.
     */
    public String BSSID;
    public boolean isConnected;

    public WifiScanResult(String SSID, String BSSID){
        this.SSID = SSID;
        this.BSSID = BSSID;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }
}
