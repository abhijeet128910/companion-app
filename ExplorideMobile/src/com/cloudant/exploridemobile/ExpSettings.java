package com.cloudant.exploridemobile;

import com.cloudant.exploridemobile.framework.SyncPreferences;
import com.cloudant.sync.datastore.DocumentRevision;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

public class ExpSettings extends ExpDocument {
    SyncPreferences mSyncPreferences;

    public ExpSettings(){
        this.mType =   ExpDocumentManager.DOC_TYPE_EXPSETTINGS ;
        if(mSyncPreferences==null){
            mSyncPreferences = new SyncPreferences();
            mSyncPreferences.init();
        }
    }
    public SyncPreferences getSyncPreferences(){
        return mSyncPreferences;
    }
    @Override
    public String toString() {
       return "";
    }

    @Override
    public ExpDocument fromRevision(DocumentRevision rev) {
        mRev = rev;
        Map<String, Object> map = rev.asMap();
        if(map.containsKey("type") && map.get("type").equals(this.mType)) {
            setType((String) map.get("type"));
        }
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(map);
        mSyncPreferences = gson.fromJson(json, SyncPreferences.class);
        if(mSyncPreferences==null){
            mSyncPreferences = new SyncPreferences();
            mSyncPreferences.init();
        }
        return this;
    }

    @Override
    public Map<String, Object> asMap() {

        Gson gson = new Gson();
        String str_syncPreferences = gson.toJson(mSyncPreferences);
        Map<String, Object> retMap = gson.fromJson(str_syncPreferences,
                new TypeToken<HashMap<String, Object>>() {}.getType());
        System.out.println(retMap);
        return retMap;
    }
}

