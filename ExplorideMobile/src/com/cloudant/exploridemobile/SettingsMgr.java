package com.cloudant.exploridemobile;

import android.util.Log;

import com.cloudant.exploridemobile.framework.SyncPreferences;

public class SettingsMgr implements ReplicationListener {

    public static final String TAG = SettingsMgr.class.getSimpleName();
    private static SettingsMgr mSettingsMgr;
    private ExpSettings mExpSettings;
    private ExpDocumentsModel mSettingsModel;
    private SettingsUpdateListener settingsUpdateListener;
    private static Object mSettingsMgrLockObj = new Object();

    public void init(SettingsUpdateListener settingsUpdateListener){

        try {
            this.settingsUpdateListener = settingsUpdateListener;
            mSettingsModel = Helper.getTheDocumentManager().getDocumentsModel(ExpDocumentManager.DOC_TYPE_EXPSETTINGS);
            mSettingsModel.setReplicationListener(this);
            if (mSettingsModel.allDocuments().size() == 0) {
                mExpSettings = new ExpSettings();
                mSettingsModel.createDocument(mExpSettings);
                //handle the case where the settings sync is not done
                //Lets make sure that settings are loaded first time thru HUD
                //TODO:: show HUD pairing screen / exit/ load defaults
                return;
            }

            mExpSettings = (ExpSettings)mSettingsModel.get(0);
            settingsUpdateListener.onSettingsUpdated(mExpSettings.getSyncPreferences());
        }catch (Exception e){
            String a = "";
        }
    }

    /**
     * get the settings
     * @return
     */
    public static SettingsMgr getSettings(){
        if (mSettingsMgr == null)
            synchronized (mSettingsMgrLockObj) {
                if (mSettingsMgr == null)
                    mSettingsMgr = new SettingsMgr();
            }
        return mSettingsMgr;
    }
    public ExpSettings getExpSettings(){
        return mExpSettings;
    }

    public void replicationComplete() {
        if (mExpSettings != null && mExpSettings.getSyncPreferences() != null)
            settingsUpdateListener.onSettingsUpdated(mExpSettings.getSyncPreferences());
    }

    public void replicationError() {
        Log.e(TAG, "error()");
    }

    private void setGetUpdatedDoc(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{

                    mExpSettings = (ExpSettings) (mSettingsModel.updateDocument(mExpSettings));

                }catch(Exception ce){
                    Log.e(TAG,ce.getMessage());
                }
            }
        }).start();

    }
    //TODO: use default instead in case of null preference
    public void setIsMaleVoice(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getHear() ==null) {
                mExpSettings.getSyncPreferences().setHear(mExpSettings.getSyncPreferences(). new Hear());
            }
            mExpSettings.getSyncPreferences().getHear().setIsMaleVoice(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    public void setIsVoiceOnline(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getVoicePref() ==null) {
                mExpSettings.getSyncPreferences().setVoicePref(mExpSettings.getSyncPreferences(). new VoicePref());
            }
            mExpSettings.getSyncPreferences().getVoicePref().setIsVoiceOnline(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    public void setLang(SyncPreferences.Languages lang){
        try{

            if(mExpSettings.getSyncPreferences().getLanguage() ==null) {
                mExpSettings.getSyncPreferences().setLanguage(mExpSettings.getSyncPreferences(). new Language());
            }
            mExpSettings.getSyncPreferences().getLanguage().setLang(lang);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }
    public void setDest(String val){

            if(mExpSettings.getSyncPreferences().getNavigation() ==null) {
                mExpSettings.getSyncPreferences().setNavigation(mExpSettings.getSyncPreferences(). new Navigation());
            }
            mExpSettings.getSyncPreferences().getNavigation().setDest(val);
            setGetUpdatedDoc();

    }
    public void setEnableMotorway(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getMotorway() ==null) {
                mExpSettings.getSyncPreferences().setMotorway(mExpSettings.getSyncPreferences(). new Motorway());
            }
            mExpSettings.getSyncPreferences().getMotorway().setEnableMotorway(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }
    public void setEnableTollroad(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getTollroad() ==null) {
                mExpSettings.getSyncPreferences().setTollroad(mExpSettings.getSyncPreferences(). new Tollroad());
            }
            mExpSettings.getSyncPreferences().getTollroad().setEnableTollroad(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }
    public void setEnableFerries(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getFerries() ==null) {
                mExpSettings.getSyncPreferences().setFerries(mExpSettings.getSyncPreferences(). new Ferries());
            }
            mExpSettings.getSyncPreferences().getFerries().setEnableFerries(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }
    public void setEnableLandmarks(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getLandmarks() ==null) {
                mExpSettings.getSyncPreferences().setLandmarks(mExpSettings.getSyncPreferences(). new Landmarks());
            }
            mExpSettings.getSyncPreferences().getLandmarks().setEnableLandmarks(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    public void setEnableLiveTraffic(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getLiveTraffic() ==null) {
                mExpSettings.getSyncPreferences().setLiveTraffic(mExpSettings.getSyncPreferences(). new LiveTraffic());
            }
            mExpSettings.getSyncPreferences().getLiveTraffic().setEnableLiveTraffic(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    public void setEnableSpeedAlert(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getSpeedAlert() ==null) {
                mExpSettings.getSyncPreferences().setSpeedAlert(mExpSettings.getSyncPreferences(). new SpeedAlert());
            }
            mExpSettings.getSyncPreferences().getSpeedAlert().setEnableSpeedAlert(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }
    public void setEnableTracking(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getAllowTracking() == null) {
                mExpSettings.getSyncPreferences().setAllowTracking(mExpSettings.getSyncPreferences(). new AllowTracking());
            }
            mExpSettings.getSyncPreferences().getAllowTracking().setEnableTracking(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    public void setEnableOfflineNavigation(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getOfflineNavigation() == null) {
                mExpSettings.getSyncPreferences().setOfflineNavigation(mExpSettings.getSyncPreferences(). new OfflineNavigation());
            }
            mExpSettings.getSyncPreferences().getOfflineNavigation().setOffline(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    public void setEnableGestures(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getGestures() == null) {
                mExpSettings.getSyncPreferences().setGestures(mExpSettings.getSyncPreferences(). new Gestures());
            }
            mExpSettings.getSyncPreferences().getGestures().setEnabled(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    public void setIsLocaleUnitKms(boolean val){
        try{

            if(mExpSettings.getSyncPreferences().getLocale() ==null) {
                mExpSettings.getSyncPreferences().setLocale(mExpSettings.getSyncPreferences(). new Locale());
            }
            mExpSettings.getSyncPreferences().getLocale().setIsLocaleUnitKms(val);
            setGetUpdatedDoc();

        }catch(Exception ce){
            Log.e(TAG,ce.getMessage());
        }
    }

    /**
     * Use as callback in MainActivity when replication is completed.
     */
    public interface SettingsUpdateListener{
        void onSettingsUpdated(SyncPreferences syncPreferences);
    }
}
