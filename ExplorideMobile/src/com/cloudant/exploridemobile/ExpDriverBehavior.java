package com.cloudant.exploridemobile;

import android.content.Context;
import android.util.Log;

import com.cloudant.exploridemobile.ConnectedDriverAPI.API;

import java.util.Arrays;

/**
 * Created by pranav.r on 12/8/17.
 */

public class ExpDriverBehavior {
    private static final String LOG_TAG = "ExpDriverBehavior";

    private static Object mInstanceLockObj = new Object();
    private ExpAnalyzeDriving analyzeDriving=null;
    public void initialize(final Context ctx){
        try{
            synchronized (mInstanceLockObj) {
                Log.d(LOG_TAG, "initialize called");

                new API(ctx);

                API.doInitialize(ctx.getString(R.string.db_base_url));

                analyzeDriving = new ExpAnalyzeDriving();
                //ExpAnalyzeDriving.behaviorDemo = true;
                //ExpAnalyzeDriving.needCredentials = true;
                //String mobileAppDeviceId = "d" + API.getUUID().substring(0, 30);
                String mobileAppDeviceId = "d" + API.getUUID();

                analyzeDriving.doInitialize(ctx, Helper.getMainActivity(),mobileAppDeviceId);
                if (Helper.AppType.HUDApp == Helper.getsAppType()) {
                    analyzeDriving.createDeviceClient();
                } else {

                }

            }
        }
        catch(Exception ex){
            Log.e(LOG_TAG,ex.getMessage());
        }
        finally {

        }

    }

    public void startNavigation(){
        try{
            synchronized (mInstanceLockObj) {
                Log.d(LOG_TAG, "startNavigation called");

                if (analyzeDriving.needCredentials) {
                    analyzeDriving.createDeviceClient();
                }
                analyzeDriving.startDrive();
            }
        }
        catch(Exception ex){
            Log.e(LOG_TAG,ex.getMessage());
        }
        finally {

        }
    }

    public void endNavigation(){
        try{
            synchronized (mInstanceLockObj) {
                Log.d(LOG_TAG, "endNavigation called");

                analyzeDriving.endDrive();
            }
        }
        catch(Exception ex){
            Log.e(LOG_TAG,ex.getMessage());
        }
        finally {

        }
    }

    public void updateLocation(android.location.Location location){
        try{
            synchronized (mInstanceLockObj) {
                Log.d(LOG_TAG, "updateLocation called");

                analyzeDriving.updateLocation(location,Helper.getMainActivity());
            }
        }
        catch(Exception ex){
            Log.e(LOG_TAG,ex.getMessage());
        }
        finally {

        }
    }

    public void uninitialize(){
        try{
            synchronized (mInstanceLockObj) {
                Log.d(LOG_TAG, "uninitialize called");

                analyzeDriving = null;
                if (Helper.AppType.HUDApp == Helper.getsAppType()) {
                } else {

                }

            }
        }
        catch(Exception ex){
            Log.e(LOG_TAG,ex.getMessage());
        }
        finally {

        }
    }
}
