package com.cloudant.exploridemobile;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by pranav.r on 10/26/16.
 */

public class ExpLoginManager {
    private String mbaseURL;
    private Context mContext;
    public ExpLoginManager(String baseURL, Context context){
        mbaseURL = baseURL;
        mContext = context;
    }
    private void doOperation(String strUrl, String method, JSONObject jsonObject, StringBuilder responseOutput) throws MalformedURLException, ProtocolException, IOException, JSONException {
        URL url = new URL(strUrl);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestMethod(method);
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.connect();

        DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
        wr.writeBytes(jsonObject.toString());
        wr.flush();
        wr.close();
        int responseCode = httpURLConnection.getResponseCode();
        BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
        String line = "";
        while ((line = br.readLine()) != null) {
            responseOutput.append(line);
        }
        br.close();
    }

    public  void doLogin(String userName, String password) throws MalformedURLException, ProtocolException, IOException, JSONException,FileNotFoundException {
        String strUrl =  mbaseURL+ "/api/login";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", Helper.simplifyName(userName));
        jsonObject.put("password", password);
        StringBuilder responseOutput = new StringBuilder();
        doOperation(strUrl, "POST", jsonObject, responseOutput);

        String jsonString = responseOutput.toString();
        Helper.setCredential(mContext,userName,password,jsonString);
        Helper.getTheDocumentManager().initialize(mContext,true);
        Helper.getDriverBehavior().initialize(mContext);

    }




    public void doRegister(String userName, String password) throws MalformedURLException, ProtocolException, IOException, JSONException {
        String strUrl = mbaseURL + "/api/users/"+ Helper.simplifyName(userName);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", Helper.simplifyName(userName));
        jsonObject.put("password", password);
        jsonObject.put("email", userName);
        jsonObject.put("mType", "user");
        jsonObject.put("_id", Helper.simplifyName(userName));
        StringBuilder responseOutput = new StringBuilder();
        doOperation(strUrl, "PUT", jsonObject, responseOutput);
        String jsonString = responseOutput.toString();
        JSONObject jsonResponse = new JSONObject(jsonString);
        String status = jsonResponse.getString("ok");
        if (status.equalsIgnoreCase("true"))
            doLogin(userName,password);

    }
    public void doLogout(){

    }


}
