package com.cloudant.exploridemobile;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.cloudant.exploridemobile.ConnectedDriverAPI.API;
import com.cloudant.exploridemobile.framework.Credential;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.SecureRandom;

/**
 * Created by pranav.r on 10/20/16.
 */

public class Helper {

    private static final String TAG = Helper.class.toString();
    public enum AppType{MobileApp,HUDApp};

    private static volatile SecureRandom sSecureRandom = null;
    private static final long MSB = 0x8000000000000000L;
    private static final String LOG_TAG = "Helper";
    private static AppType sAppType;
    private static Activity sMainActivity;
    private static Credential sCredential=null;
    private static ExpLoginManager sLM =null;
    private static ExpDriverBehavior sDriverBehavior = null;
    private static SharedPreferences sPreference = null;
    public static AppType getsAppType(){
        return sAppType;
    }
    public static ExpLoginManager getTheLoginManager(){
        return sLM;
    }
    public static void logout(){
        try {
            sLM.doLogout();
            uninitialize();
        }catch(Exception ex){
            Log.e(LOG_TAG, ex.getMessage());
        }
    }

    public static String genRandom32Hex() {
        SecureRandom sr = sSecureRandom;
        if (sr == null) {
            sSecureRandom = sr = new SecureRandom();
        }

        return Long.toHexString(MSB | sr.nextLong()) + Long.toHexString(MSB | sr.nextLong());
    }

    public static void uninitialize(){
        sLM.doLogout();;
        sLM = null;
        sCredential=null;
        if(null != sDriverBehavior){
            sDriverBehavior.uninitialize();
        }
        sDriverBehavior = null;
        sPreference=null;
    }
    public static void initialize(String url, Activity activity, String appName, AppType appType){
        sAppType = appType;
        sMainActivity = activity;
        sLM = new ExpLoginManager(url,activity.getApplicationContext());
        sPreference = activity.getApplicationContext().getSharedPreferences(
                activity.getString(R.string.pref_name), Context.MODE_PRIVATE);
      }
    public static Activity getMainActivity(){
        return sMainActivity;
    }

    public static ExpDriverBehavior getDriverBehavior(){
         if (sDriverBehavior == null)
            synchronized (mInstanceLockObj){
                if (sDriverBehavior == null) {
                   sDriverBehavior = new ExpDriverBehavior();
                }
            }
            return sDriverBehavior;
    }
    public static void setCredential(String userName, String apiKey, String apiPassword, String documentDbName, String documentDbHost){
    sCredential = new Credential(userName,apiKey,apiPassword,documentDbName,documentDbHost);
    }
    public static void setCredential(Context context, String userName, String password, String jsonString) throws JSONException {
        JSONObject jsonResponse = new JSONObject(jsonString);

        String status = jsonResponse.getString("ok");
        String apiKey = jsonResponse.getString("api_key");
        String apiPassword = jsonResponse.getString("api_password");
        String locationDbHost = jsonResponse.getString("location_db_host");
        sCredential = new Credential(userName,password,apiKey,apiPassword,locationDbHost);
        storeCredential(context,sCredential);
    }

    public static void setCredential(Credential credential){
        sCredential = credential;
    }
    public static String getLocalDBName(String type){

        return "local_" + getDBName(type);
    }

    public static String getDBName(String type){
        String name = "exp_unkown";
        if(type.equalsIgnoreCase(ExpDocumentManager.DOC_TYPE_EXPTRIPLOCATION))
        {
            name = "exp_stream";
        }
        else
        if(type.equalsIgnoreCase(ExpDocumentManager.DOC_TYPE_LOCATION))
        {
            name = "exp_locations";
        }
        else
        if(type.equalsIgnoreCase(ExpDocumentManager.DOC_TYPE_EXPSETTINGS))
        {
            name = "exp_settings";
        }
        if (getCredential() == null)
            return "";

        return name + "_user_" + simplifyName(getCredential().getUserName());
    }

    public static Credential getCredential(){
        return sCredential;
    }

    public static String simplifyName(String str) {
        try {
            if (str == null || str.isEmpty()) {
                return "";
            }
            str = str.toLowerCase();

            str = str.replaceAll("[.]", "_");
            str = str.replace("@", "(at)");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }
    private static Object mInstanceLockObj = new Object();
    private static ExpDocumentManager sDocMgr=null;
    public static ExpDocumentManager getTheDocumentManager(){
        if (sDocMgr == null)
            synchronized (mInstanceLockObj){
                if (sDocMgr == null) {
                    // Model needs to stay in existence for lifetime of app.
                    sDocMgr = new ExpDocumentManager();
                }
            }
        if(!sDocMgr.isInitialized()){

            Log.e(TAG, "!sDocMgr.isInitialized()");
        }
        return sDocMgr;
    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    /*
    private static String getDataForKey(Context context, String key)throws Exception
    {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);



            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(key, null);


            // Encrypt the text
            String filesDirectory = context.getFilesDir().getAbsolutePath();

            String encryptedDataFilePath = filesDirectory + File.separator + key;




            Cipher outCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            outCipher.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());


            CipherInputStream cipherInputStream =
                    new CipherInputStream(new FileInputStream(encryptedDataFilePath),
                            outCipher);
            byte[] roundTrippedBytes = new byte[1000]; // TODO: dynamically resize as we get more data

            int index = 0;
            int nextByte;
            while ((nextByte = cipherInputStream.read()) != -1) {
                roundTrippedBytes[index] = (byte) nextByte;
                index++;
            }
            String roundTrippedString = new String(roundTrippedBytes, 0, index, "UTF-8");
            return roundTrippedString;

        } catch (NoSuchAlgorithmException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        } catch (KeyStoreException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        }  catch (IOException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        } catch (UnrecoverableEntryException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        } catch (NoSuchPaddingException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        } catch (InvalidKeyException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        } catch (UnsupportedOperationException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        }
        catch(Exception e){
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        }

    }
    private static void storeDataForKey(Context context, String key, String value )
    {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);


            int nBefore = keyStore.size();

            // Create the keys if necessary
            if (!keyStore.containsAlias(key)) {

                Calendar notBefore = Calendar.getInstance();
                Calendar notAfter = Calendar.getInstance();
                notAfter.add(Calendar.YEAR, 1);
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                        .setAlias(key)
                        .setKeyType("RSA")
                        .setKeySize(1024)
                        .setSubject(new X500Principal("CN=test"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(notBefore.getTime())
                        .setEndDate(notAfter.getTime())
                        .build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
                generator.initialize(spec);

                KeyPair keyPair = generator.generateKeyPair();
            }
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(key, null);


            // Encrypt the text
            String filesDirectory = context.getFilesDir().getAbsolutePath();

            String encryptedDataFilePath = filesDirectory + File.separator + key;


            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
            inCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());


            CipherOutputStream cipherOutputStream =
                    new CipherOutputStream(
                            new FileOutputStream(encryptedDataFilePath), inCipher);
            cipherOutputStream.write(value.getBytes("UTF-8"));
            cipherOutputStream.close();

        } catch (NoSuchAlgorithmException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        } catch (NoSuchProviderException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        } catch (InvalidAlgorithmParameterException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        } catch (KeyStoreException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        }  catch (IOException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        } catch (UnrecoverableEntryException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        } catch (NoSuchPaddingException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        } catch (InvalidKeyException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        } catch (UnsupportedOperationException e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
        }
        catch(Exception e){
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        }
    }

   */
    private static String getDataForKey(Context context, String key)throws Exception
    {
        try {

            String value  = sPreference.getString(key, null);
            return value;
        }
        catch(Exception e){
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw e;
        }

    }
    private static void storeDataForKey(Context context, String key, String value )
    {
        try {

            SharedPreferences.Editor editor = sPreference.edit();
            editor.putString(key, value);
            editor.commit();

        }
        catch(Exception e){
            Log.e(LOG_TAG, Log.getStackTraceString(e));

        }
    }
    private static void storeCredential(Context context, Credential credential) {

        storeDataForKey(context,"exp_un",credential.getUserName());
        storeDataForKey(context,"exp_up",credential.getPassword());
        storeDataForKey(context,"exp_ak",credential.getApiKey());
        storeDataForKey(context,"exp_ap",credential.getApiPassword());
        storeDataForKey(context,"exp_dh",credential.getDocumentDbHost());
    }
    public static void  loadCredential(Context context) throws Exception {
        String userName = getDataForKey(context,"exp_un");
        String password = getDataForKey(context,"exp_up");
        String apiKey = getDataForKey(context,"exp_ak");
        String apiPassword = getDataForKey(context,"exp_ap");
        String documentHost = getDataForKey(context,"exp_dh");

        sCredential = new Credential(userName,password,apiKey,apiPassword,documentHost);

    }

}
