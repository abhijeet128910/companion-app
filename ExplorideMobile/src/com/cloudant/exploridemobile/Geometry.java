package com.cloudant.exploridemobile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pranav.r on 10/19/16.
 */

public class Geometry {
    double mLatitude = 0;
    double mLongitude = 0;
    private Geometry(){

    }
    public  Geometry(double latitude, double longitude){

       this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    public void setLatitude(double latitude){
        this.mLatitude = latitude;
    }
    public double getLatitude(){return this.mLatitude;}
    public void setLongitude(double longitude){
        this.mLongitude = longitude;
    }
    public double getLongitude(){return this.mLongitude;}
    @Override
    public String toString() {
        return "{ lat: " + Double.toString(mLatitude) + ", long: " + Double.toString(mLongitude )+ "}";
    }


    public static Geometry fromRevision(HashMap<String, Object> map){
        Geometry g = new Geometry();
        if(null != map) {
            ArrayList<Number> coordinates = (ArrayList<Number>)map.get("coordinates");

            if (null != coordinates && coordinates.size() == 2) {
                Number n = coordinates.get(1);
                if (null != n)
                    g.setLatitude(n.doubleValue());
                n = coordinates.get(0);
                if (null != n)
                    g.setLongitude(n.doubleValue());
            }
        }
        return g;
    }
    public Map<String, Object> asMap() {
        // this could also be done by a fancy object mapper
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", "Point");
        double[] coordinates = {this.mLongitude, this.mLatitude};
        map.put("coordinates",coordinates);
        //map.put("latitude", latitude);
        //map.put("longitude", longitude);

        return map;
    }

}
