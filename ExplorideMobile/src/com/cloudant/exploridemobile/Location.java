package com.cloudant.exploridemobile;

import com.cloudant.sync.datastore.DocumentRevision;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pranav.r on 10/19/16.
 */

public class Location extends ExpDocument {
    // this is the revision in the database representing this task

    //placeid is the id returned by google api as google_place_id
    private String mPlaceId;
    private String mDescription, mDate, mTripId, mSpeed;
    private boolean mFavorite;
    private Geometry mGeometry;
    public Geometry getGeometry(){return mGeometry;}
    public void setGeometry(Geometry geometry){this.mGeometry =geometry;}
    public String getDescription() {
        if(null == mDescription)
            return "";
        return this.mDescription;
    }
    public void setDescription(String desc) {
        this.mDescription = desc;
    }
    public void setFavorite(boolean favorite){
        mFavorite =favorite;
    }
    public boolean getIsFavorite(){
        return mFavorite;
    }
    private String mName;
    public String getName(){
        if(null == mName)
            return "";
        return mName;
    }
    public void setName(String name){
        this.mName = name;
    }

    public void setPlaceId(String id){this.mPlaceId = id;}
    public String getPlaceId(){return this.mPlaceId;}
    public String getDate() {
        return mDate;
    }

    public void setDate(String mDate) {
        this.mDate = mDate;
    }

    public String getTripId() {
        return mTripId;
    }

    public void setTripId(String mTripId) {
        this.mTripId = mTripId;
    }

    @Override
    public String toString() {
        if(null != mGeometry) {
            return "{ mName: " + getName()
                    + " placeId: " + getPlaceId()
                    + " desc: " + getDescription()
                    + ", mFavorite: " + mFavorite
                    + ", mDate: " + mDate
                    + ", mGeometry: "
                    +  mGeometry.toString()
                    + ", mTripId: "
                    +  mTripId
                    + "}";
        }
        else
            return "{ mName: " + getName() +" desc: " + getDescription()+ ", mFavorite: " + mFavorite
                    + "}";
    }

    public String getSpeed() {
        return mSpeed;
    }

    public void setSpeed(String mSpeed) {
        this.mSpeed = mSpeed;
    }

    public Location(double latitude, double longitude,String desc){
        this.mType =   ExpDocumentManager.DOC_TYPE_LOCATION ;
        mGeometry = new Geometry(latitude,longitude);
        this.mDescription = desc;

    }

    public Location(){
        this.mType =   ExpDocumentManager.DOC_TYPE_LOCATION ;
    }

    @Override
    public ExpDocument fromRevision(DocumentRevision rev) {
        Location l = new Location();
        l.mRev = rev;
        // this could also be done by a fancy object mapper
        Map<String, Object> map = rev.asMap();
        if(map.containsKey("type") && map.get("type").equals(this.mType)) {
            l.setType((String) map.get("type"));
            if(map.containsKey("name"))
                l.setName((String)map.get("name"));
            if(map.containsKey("placeid"))
                l.setPlaceId((String)map.get("placeid"));
            l.setDescription((String) map.get("description"));
            if (map.containsKey("tripid"))
                l.setTripId((String) map.get("tripid"));
            if(map.containsKey("favorite"))
                l.setFavorite((boolean)map.get("favorite"));
            if(map.containsKey("date"))
                l.setDate((String) map.get("date"));

            l.setGeometry(Geometry.fromRevision((HashMap<String, Object>)map.get("geometry")));
            if(map.containsKey("speed"))
                l.setSpeed((String) map.get("speed"));
            return l;
        }
        return null;
    }

    @Override
    public Map<String, Object> asMap() {
        // this could also be done by a fancy object mapper
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("type", mType);
        map.put("name",getName());
        map.put("placeid",mPlaceId);
        map.put("description", mDescription);
        map.put("tripid", mTripId);
        map.put("favorite", mFavorite);
        map.put("date", mDate);
        map.put("geometry", mGeometry.asMap());
        map.put("speed", mSpeed);
        return map;
    }

}

class TripLocation extends Location {
    public TripLocation(double latitude, double longitude,String desc){
        super(latitude,longitude,desc);
        this.mType =   ExpDocumentManager.DOC_TYPE_EXPTRIPLOCATION ;


    }

    public TripLocation(){
        super();
        this.mType =   ExpDocumentManager.DOC_TYPE_EXPTRIPLOCATION ;
    }

}