package com.cloudant.exploridemobile;

import android.content.Context;
import android.util.Log;

import java.util.Arrays;

/**
 *
 * Created by pranav.r on 10/25/16.
 */

public class ExpDocumentManager {
    public static final String DOC_TYPE_LOCATION = "com.exploride.location";
    public static final String DOC_TYPE_EXPSETTINGS = "com.exploride.settings";
    public static final String DOC_TYPE_EXPTRIPS = "com.exploride.trip";
    public static final String DOC_TYPE_EXPTRIPLOCATION="com.exploride.triplocation";
    private static final String LOG_TAG = "ExpDocumentManager";
    ExpDocumentsModel mLocationsModel = null;
    ExpDocumentsModel mSettingsModel = null;
    ExpDocumentsModel mTripLocationsModel=null;
    public static final int sThreadPoolCount =2;
    public static final int sSyncInterval = 30;
    private boolean initialized;

    public void initialize(final Context context, final Boolean firstLogin){
            close();//close existing models first
            if(Helper.AppType.HUDApp == Helper.getsAppType()) {
                mLocationsModel = new ExpDocumentsModel(context, DOC_TYPE_LOCATION, sThreadPoolCount, sSyncInterval, ExpDocumentsModel.ReplicationDirection.Both);
                mLocationsModel.ensureIndexed(Arrays.<Object>asList("name", "favorite"), "basic");
                mSettingsModel = new ExpDocumentsModel(context, DOC_TYPE_EXPSETTINGS, sThreadPoolCount, sSyncInterval, ExpDocumentsModel.ReplicationDirection.Both);
                mTripLocationsModel = new ExpDocumentsModel(context, DOC_TYPE_EXPTRIPLOCATION,sThreadPoolCount,sSyncInterval, ExpDocumentsModel.ReplicationDirection.PushRepl);
            }
            else
            {
                mLocationsModel = new ExpDocumentsModel(context, DOC_TYPE_LOCATION, sThreadPoolCount, sSyncInterval, ExpDocumentsModel.ReplicationDirection.Both);
                mLocationsModel.ensureIndexed(Arrays.<Object>asList("name", "favorite"), "basic");
                mSettingsModel = new ExpDocumentsModel(context, DOC_TYPE_EXPSETTINGS, sThreadPoolCount, sSyncInterval, ExpDocumentsModel.ReplicationDirection.Both);
            }
            if (firstLogin && null != mSettingsModel) {
                try {
                    mSettingsModel.reloadReplicationSettings();
                    mSettingsModel.startPullReplication();
                } catch (Exception e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
                //TODO - what should happen if settings is not able to get the default record
            }
            initialized = true;
    }

    public void close(){
        try {
            initialized = false;
            if (null != mLocationsModel) {
                mLocationsModel.close();
                mLocationsModel = null;
            }
            if (null != mSettingsModel) {
                mSettingsModel.close();
                mSettingsModel = null;
            }
            if(null != mTripLocationsModel){
                mTripLocationsModel.close();
                mTripLocationsModel = null;
            }
        }catch(Exception ex)
        {
            Log.e(LOG_TAG,ex.getMessage());
        }
    }
    public ExpDocumentsModel getDocumentsModel(String type){
        if(type.equalsIgnoreCase(DOC_TYPE_LOCATION)){
            return mLocationsModel;
        }
        if(type.equalsIgnoreCase(DOC_TYPE_EXPTRIPS)){
            return mLocationsModel;
        }
        if(type.equalsIgnoreCase(DOC_TYPE_EXPSETTINGS)){
            return mSettingsModel;
        }
        if(type.equalsIgnoreCase(DOC_TYPE_EXPTRIPLOCATION)){
            return mTripLocationsModel;
        }
        return null;
    }

    public boolean isInitialized() {
        return initialized;
    }
}
