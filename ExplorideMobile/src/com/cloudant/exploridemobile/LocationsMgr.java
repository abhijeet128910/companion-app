package com.cloudant.exploridemobile;

import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationsMgr implements ReplicationListener {

    public static final String TAG = LocationsMgr.class.getSimpleName();
    private static LocationsMgr mLocationsMgr;
    private static Object mLocMgrLockObj = new Object();
    private ExpDocumentsModel mLocationsModel;
    private ExpDocumentsModel mTripLocationsModel;
    private LocationsUpdateListener mLocationsUpdateListener;

    public void getInstance(LocationsUpdateListener locationsUpdateListener) {
        mLocationsUpdateListener = locationsUpdateListener;
        if (mLocationsModel == null) {
            mLocationsModel = Helper.getTheDocumentManager().getDocumentsModel(ExpDocumentManager.DOC_TYPE_LOCATION);
            if (mLocationsModel != null)
                mLocationsModel.setReplicationListener(this);
        }
        if (mTripLocationsModel == null)
            mTripLocationsModel = Helper.getTheDocumentManager().getDocumentsModel(ExpDocumentManager.DOC_TYPE_EXPTRIPLOCATION);
    }

    public List<ExpDocument> reloadLocationsFromModel() {
        Map<String, Object> query = new HashMap<String, Object>();
        Map<String, Object> eqFavourite = new HashMap<String, Object>();
        eqFavourite.put("$eq", true);
        Map<String, Object> clause = new HashMap<String, Object>();
        clause.put("favorite", eqFavourite);
        query.put("$and", Arrays.<Object>asList(clause));

        if (mLocationsModel != null)
            return mLocationsModel.allDocumentsForQuery(query, 0, 0, null, null);
        else
            return null;
    }

    /**
     * get LocationsMgr
     * @return
     */
    public static LocationsMgr getLocationsMgr() {
        if (mLocationsMgr == null)
            synchronized (mLocMgrLockObj) {
                if (mLocationsMgr == null)
                    mLocationsMgr = new LocationsMgr();
            }
        return mLocationsMgr;
    }

    public void addTripLocation(double lat, double lng, String tripId, String speed) {
        try {
            TripLocation location = new TripLocation(lat, lng,"");
            location.setTripId(tripId);
            location.setFavorite(false);
            location.setSpeed(speed);
            if(null != mTripLocationsModel)
                mTripLocationsModel.createDocument(location);
            else
                Log.i(TAG, "addTripLocation mTripLocationsModel is null");

        } catch (Exception e) {
            Log.i(TAG, "addTripLocation exception:" + e.getMessage());
        }
    }

    public void addFavouriteLocation(String placeID, String name, String desc, String lat, String lng) {
        Double dblLat=0D;
        Double dblLng=0D;
        if(!lat.isEmpty())
            dblLat = Double.parseDouble(lat);
        if(!lng.isEmpty())
            dblLng = Double.parseDouble(lng);
        Location location = new Location(dblLat, dblLng, desc);
        location.setPlaceId(placeID);
        location.setName(name);
        location.setFavorite(true);
        String date = "";//new Date().to();
        location.setDate(date);
        if(null != mLocationsModel)
            mLocationsModel.createDocument(location);
        else{
            Log.e(TAG, "addFavouriteLocation: Failed to add location to favorite as LocationsModel is null");
        }
    }

    public Location getLocation(String placeId) {

        Map<String, Object> query = new HashMap<String, Object>();
        Map<String, Object> eqPlace = new HashMap<String, Object>();
        eqPlace.put("$eq", placeId);
        Map<String, Object> clause = new HashMap<String, Object>();
        clause.put("placeid", eqPlace);

        query.put("$and", Arrays.<Object>asList(clause));
        List<ExpDocument> documents = null;
        if (mLocationsModel != null)
            documents =  mLocationsModel.allDocumentsForQuery(query, 0, 0, null, null);
        if(null != documents && documents.size()>0)
        {
            return (Location)documents.get(0);
        }

       /* List<ExpDocument> locations = reloadLocationsFromModel();
        for (ExpDocument expDocument : locations) {
            Location location = (Location) expDocument;
            if (location.getPlaceId()!= null && (location).getPlaceId().equalsIgnoreCase(placeId)) {
                try {
                   return location;
                } catch (Exception e) {
                    Log.i(TAG, "getLocation exception:" + e.getMessage());
                    e.printStackTrace();
                }
                break;
            }
        }*/
        return null;
    }

    public boolean isFavoriteLocation(String placeId)
    {
        Location location = getLocation(placeId);
        if(null != location){
            return location.getIsFavorite();
        }
        return false;
    }

    public void deleteLocation(String placeId) {
        /*List<ExpDocument> locations = reloadLocationsFromModel();
        for (ExpDocument expDocument : locations) {
            Location location = (Location) expDocument;
            if ((location).getPlaceId().equalsIgnoreCase(placeId)) {
                try {
                    mLocationsModel.deleteDocument(location);
                } catch (Exception e) {
                    Log.i(TAG, "deleteLocation exception:" + e.getMessage());
                    e.printStackTrace();
                }
                break;
            }
        }*/

        Location location = getLocation(placeId);
        if(null != location){
            try {
                mLocationsModel.deleteDocument(location);
            } catch (Exception e) {
                Log.i(TAG, "deleteLocation exception:" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Override
    public void replicationComplete() {
        if (mLocationsUpdateListener != null)
            mLocationsUpdateListener.onLocationsUpdated(reloadLocationsFromModel());
    }

    @Override
    public void replicationError() {

    }

    public interface LocationsUpdateListener {
        void onLocationsUpdated(List<ExpDocument> locations);
    }
}