package com.cloudant.exploridemobile;

import com.cloudant.sync.datastore.DocumentRevision;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pranav.r on 10/19/16.
 */

public class ExpTrip extends ExpDocument {
    // this is the revision in the database representing this task

    private String mOrigin, mDestination, mDate;

    private String mDescription;

    public String getDescription() {
        if(null == mDescription)
            return "";
        return this.mDescription;
    }
    public void setDescription(String desc) {
        this.mDescription = desc;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    @Override
    public String toString() {
        if(null != mOrigin) {
            return "{ mOrigin: " + getOrigin() + " mDestination: " + getDestination() + ", mDescription: "
                    + mDescription+ " mDate: " + getDate() +"}";
        }
        else
            return "{ desc: " + getDescription()+ ", mDate: " + mDate
                    + "}";
    }


    public ExpTrip(String origin, String destination, String date) {
        mType = ExpDocumentManager.DOC_TYPE_EXPTRIPS;
        mOrigin = origin;
        mDestination = destination;
        mDate = date;
    }

    public ExpTrip(){
        this.mType =   ExpDocumentManager.DOC_TYPE_EXPTRIPS;
    }

    @Override
    public ExpDocument fromRevision(DocumentRevision rev) {
        ExpTrip expTrip = new ExpTrip();
        expTrip.mRev = rev;
        // this could also be done by a fancy object mapper
        Map<String, Object> map = rev.asMap();
        if(map.containsKey("type") && map.get("type").equals(this.mType)) {
            expTrip.setType((String) map.get("type"));
            if(map.containsKey("origin"))
                expTrip.setOrigin((String)map.get("origin"));

            if(map.containsKey("destination"))
                expTrip.setDestination((String)map.get("destination"));

            if(map.containsKey("description"))
                expTrip.setDescription((String)map.get("description"));

            if(map.containsKey("date"))
                expTrip.setDate((String)map.get("date"));

            return expTrip;
        }
        return null;
    }

    @Override
    public Map<String, Object> asMap() {
        // this could also be done by a fancy object mapper
        HashMap<String, Object> map = new HashMap<>();
        map.put("type", mType);
        map.put("origin",getOrigin());
        map.put("destination", getDestination());
        map.put("description", getDescription());
        map.put("date", getDate());
        return map;
    }

}
