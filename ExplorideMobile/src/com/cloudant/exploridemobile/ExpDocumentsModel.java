package com.cloudant.exploridemobile;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import com.cloudant.exploridemobile.framework.Credential;
import com.cloudant.sync.datastore.ConflictException;
import com.cloudant.sync.datastore.Datastore;
import com.cloudant.sync.datastore.DatastoreManager;
import com.cloudant.sync.datastore.DatastoreNotCreatedException;
import com.cloudant.sync.datastore.DocumentBodyFactory;
import com.cloudant.sync.datastore.DocumentException;
import com.cloudant.sync.datastore.DocumentRevision;
import com.cloudant.sync.event.Subscribe;
import com.cloudant.sync.notifications.ReplicationCompleted;
import com.cloudant.sync.notifications.ReplicationErrored;
import com.cloudant.sync.query.IndexManager;
import com.cloudant.sync.query.QueryResult;
import com.cloudant.sync.replication.Replicator;
import com.cloudant.sync.replication.ReplicatorBuilder;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by pranav.r on 10/25/16.
 */

public class ExpDocumentsModel implements Runnable {
    public enum ReplicationDirection {PushRepl,PullRepl,Both};
    private static final String LOG_TAG = "ExpDocumentsModel";



    private static final String DATASTORE_MANGER_DIR = "data";

    private Datastore mDatastore;

    private Replicator mPushReplicator;
    private Replicator mPullReplicator;

    private final Context mContext;
    private final Handler mHandler;
    private ReplicationListener mListener;
    private ScheduledExecutorService mExecutor;
    private ReplicationDirection mReplicationDir;
    private String mType;
    private IndexManager mIndexMgr;
    public ExpDocumentsModel(Context context, String type, int threadPoolCount, int interval, ReplicationDirection replicationDir){
        this.mReplicationDir = replicationDir;
        this.mContext = context;
        this.mType = type;
        // Set up our documents datastore within its own folder in the applications
        // data directory.
        File path = this.mContext.getApplicationContext().getDir(
                DATASTORE_MANGER_DIR,
                Context.MODE_PRIVATE
        );
        DatastoreManager manager = DatastoreManager.getInstance(path.getAbsolutePath());
        try {
            this.mDatastore = manager.openDatastore(Helper.getLocalDBName(mType));
            this.mIndexMgr = new IndexManager(this.mDatastore);
        } catch (DatastoreNotCreatedException dnce) {
            Log.e(LOG_TAG, "Unable to open Datastore", dnce);
        }

        Log.d(LOG_TAG, "Set up database " + Helper.getLocalDBName(mType) + " at " + path.getAbsolutePath());


        mExecutor = Executors.newScheduledThreadPool(threadPoolCount);
        if(-1 != interval) {
            ScheduledFuture<?> result = mExecutor.scheduleAtFixedRate(this, 0, interval, TimeUnit.SECONDS);
        }
        // Allow us to switch code called by the ReplicationListener into
        // the main thread so the UI can update safely.
        this.mHandler = new Handler(Looper.getMainLooper());

        Log.d(LOG_TAG, "DocumentsModel set up " + path.getAbsolutePath());
    }

    public String ensureIndexed(java.util.List<java.lang.Object> fieldNames, String indexName) {

        String name = mIndexMgr.ensureIndexed(fieldNames, indexName);
        if (name == null) {
            // there was an error creating the index
            Log.e(LOG_TAG, "Unable to create index");
        }
        return name;
    }
    /**
     * Sets the listener for replication callbacks as a weak reference.
     * @param listener  to receive callbacks.
     */
    public void setReplicationListener(ReplicationListener listener) {
        this.mListener = listener;
    }

    /**
     * Creates a document, assigning an ID.
     * @param doc document to create
     * @return new revision of the document
     */
    public ExpDocument createDocument(ExpDocument doc) {
        DocumentRevision rev = new DocumentRevision();
        rev.setBody(DocumentBodyFactory.create(doc.asMap()));
        try {
            DocumentRevision created = this.mDatastore.createDocumentFromRevision(rev);
            return ExpDocument.fromRevision(created, doc.getType());
        } catch (DocumentException de) {
            return null;
        }
    }
    /**
     * Updates a  document within the datastore.
     * @param  doc to update
     * @return the updated revision of the ExpDocument
     * @throws ConflictException if the document passed in has a mRev which doesn't
     *      match the current mRev in the datastore.
     */
    public ExpDocument updateDocument(ExpDocument doc) throws ConflictException {
        DocumentRevision rev = doc.getDocumentRevision();
        rev.setBody(DocumentBodyFactory.create(doc.asMap()));
        try {
            DocumentRevision updated = this.mDatastore.updateDocumentFromRevision(rev);
            return ExpDocument.fromRevision(updated, mType);
        } catch (DocumentException de) {
            return null;
        }
    }

    /**
     * Deletes a ExpDocument document within the datastore.
     * @param doc to delete
     * @throws ConflictException if the doc passed in has a mRev which doesn't
     *      match the current mRev in the datastore.
     */
    public void deleteDocument(ExpDocument doc) throws ConflictException {
        this.mDatastore.deleteDocumentFromRevision(doc.getDocumentRevision());
    }

    /**
     * <p>Returns all {@code document} documents in the datastore.</p>
     */
    public List<ExpDocument> allDocuments() {
        int nDocs = this.mDatastore.getDocumentCount();
        List<DocumentRevision> all = this.mDatastore.getAllDocuments(0, nDocs, true);
        List<ExpDocument> documents = new ArrayList<ExpDocument>();

        // Filter all documents down to those of mType .
        for(DocumentRevision rev : all) {
            try {
                ExpDocument d = ExpDocument.fromRevision(rev, mType);
                if (d != null) {
                    documents.add(d);
                }
            }catch (Exception e){
                String a = "";
            }
        }

        return documents;
    }
    /**
     * To get documents for saved locations
     * @param query
     * @param skip
     * @param limit
     * @param fields
     * @param sortDocument
     * @return
     */
    public List<ExpDocument> allDocumentsForQuery(Map<String, Object> query, long skip, long limit, List<String> fields, List<Map<String, String>> sortDocument) {
        List<ExpDocument> documents = new ArrayList<>();
        try {
            QueryResult queryResult = this.mIndexMgr.find(query, skip, limit, fields, sortDocument);

            if (queryResult != null) {
                for (DocumentRevision rev : queryResult) {
                    // The returned revision object contains all fields for
                    // the object. You cannot project certain fields in the
                    // current implementation.
                    try {
                        ExpDocument d = ExpDocument.fromRevision(rev, mType);
                        if (d != null) {
                            documents.add(d);
                        }
                    } catch (Exception e) {
                        String a = "";
                    }
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
        return documents;
    }

    public ExpDocument get(int position) {
        ExpDocument document = null;
        try {
            List<ExpDocument> documents = allDocuments();
            document = (ExpDocument) documents.get(position);
        }catch(Exception e){
            String a = "";
        }

        return document;
    }


    //
    // MANAGE REPLICATIONS
    //

    /**
     * <p>Stops running replications.</p>
     *
     * <p>The stop() methods stops the replications asynchronously, see the
     * replicator docs for more information.</p>
     */
    public void stopAllReplications() {
        if (this.mPullReplicator != null) {
            this.mPullReplicator.stop();
        }
        if (this.mPushReplicator != null) {
            this.mPushReplicator.stop();
        }
    }

    /**
     * <p>Starts the configured push replication.</p>
     */
    public void startPushReplication() {
        if (this.mPushReplicator != null) {
            this.mPushReplicator.start();
        } else {
            throw new RuntimeException("Push replication not set up correctly");
        }
    }

    /**
     * <p>Starts the configured pull replication.</p>
     */
    public void startPullReplication() {
        if (this.mPullReplicator != null) {
            this.mPullReplicator.start();
        } else {
            throw new RuntimeException("Push replication not set up correctly");
        }
    }

    @Override
    public void run()
    {
        try {
            // Set up the replicator objects from the app's settings.
            if(!Helper.isNetworkAvailable(mContext))
            {
                return;
            }
            try {
                if(null == this.mPullReplicator || null == this.mPushReplicator)
                    //should happen first time
                reloadReplicationSettings();
            } catch (URISyntaxException e) {
                Log.e(LOG_TAG, "Unable to construct remote URI from configuration", e);
                return;
            }

            //TODO - Check if we need to sequence this, i.e. call pull only after push is complete
            if(ReplicationDirection.Both == mReplicationDir || ReplicationDirection.PushRepl == mReplicationDir)
                startPushReplication();

            if(ReplicationDirection.Both == mReplicationDir || ReplicationDirection.PullRepl == mReplicationDir)
                startPullReplication();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * <p>Stops running replications and reloads the replication settings from
     * the app's preferences.</p>
     */
    public void reloadReplicationSettings()
            throws URISyntaxException {

        // Stop running replications before reloading the replication
        // settings.
        // The stop() method instructs the replicator to stop ongoing
        // processes, and to stop making changes to the datastore. Therefore,
        // we don't clear the listeners because their complete() methods
        // still need to be called once the replications have stopped
        // for the UI to be updated correctly with any changes made before
        // the replication was stopped.
        this.stopAllReplications();

        // Set up the new replicator objects
        if(Helper.getCredential()!=null){
            URI uri = this.createServerURI();

            mPullReplicator = ReplicatorBuilder.pull().to(mDatastore).from(uri).build();
            mPushReplicator = ReplicatorBuilder.push().from(mDatastore).to(uri).build();

            mPushReplicator.getEventBus().register(this);
            mPullReplicator.getEventBus().register(this);

            Log.d(LOG_TAG, "Set up replicators for URI:" + uri.toString());
        }

    }

    /**
     * <p>Returns the URI for the remote database, based on the app's
     * configuration.</p>
     * @return the remote database's URI
     * @throws URISyntaxException if the settings give an invalid URI
     */
    private URI createServerURI()
            throws URISyntaxException {
        // We store this in plain text for the purposes of simple demonstration,
        // you might want to use something more secure.
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        Credential credential = Helper.getCredential();

        String dbName = Helper.getDBName(this.mType);
        String apiKey = credential.getApiKey();
        String apiSecret = credential.getApiPassword();
        String host = credential.getDocumentDbHost();

        // We recommend always using HTTPS to talk to Cloudant.
        return new URI("https", apiKey + ":" + apiSecret, host, 443, "/" + dbName, null, null);
    }

    //
    // REPLICATIONLISTENER IMPLEMENTATION
    //

    /**
     * Calls the TodoActivity's replicationComplete method on the main thread,
     * as the complete() callback will probably come from a replicator worker
     * thread.
     */
    @Subscribe
    public void complete(ReplicationCompleted rc) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {

              if (mListener != null) {
                    mListener.replicationComplete();
                }
            }
        });
    }

    /**
     * Calls the TodoActivity's replicationComplete method on the main thread,
     * as the error() callback will probably come from a replicator worker
     * thread.
     */
    @Subscribe
    public void error(ReplicationErrored re) {
        Log.e(LOG_TAG, "Replication error:", re.errorInfo.getException());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
              if (mListener != null) {
                    mListener.replicationError();
                }
            }
        });
    }

    public void close(){
        try {
            mIndexMgr.close();
            mExecutor.shutdown();
            this.stopAllReplications();
            this.setReplicationListener(null);
        }catch(Exception e){

        }
    }
    protected void finalize() throws Throwable {
        try {
            close();        // close open files
        } finally {
            super.finalize();
        }
    }
}
