/**
 * Copyright 2016 IBM Corp. All Rights Reserved.
 * <p>
 * Licensed under the IBM License, a copy of which may be obtained at:
 * <p>
 * http://www14.software.ibm.com/cgi-bin/weblap/lap.pl?li_formnum=L-DDIN-AEGGZJ&popup=y&title=IBM%20IoT%20for%20Automotive%20Sample%20Starter%20Apps%20%28Android-Mobile%20and%20Server-all%29
 * <p>
 * You may not use this file except in compliance with the license.
 */
package com.cloudant.exploridemobile;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Trace;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;


import com.google.gson.JsonObject;
import com.ibm.iotf.client.app.ApplicationClient;
import com.ibm.iotf.client.device.DeviceClient;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import com.cloudant.exploridemobile.ConnectedDriverAPI.API;
import com.cloudant.exploridemobile.ConnectedDriverAPI.ReservationsData;

public class ExpAnalyzeDriving  implements  LocationListener {
    private static final String LOG_TAG = "ExpAnalyzeDriving";

    protected  boolean behaviorDemo = true;
    protected  boolean needCredentials = true;

    protected  String tripID = null;
    protected  String deviceID = null;
    protected  final String[] reservationId = {new String()}; // Needs to be final to be able to access and change it inside the API.doRequest callback function

    protected  boolean userUnlocked = false;
    protected  boolean startedDriving = false;
    protected  boolean alreadyReserved = false;

    protected  Context context;
    private int tripCount = 0;

    private DeviceClient deviceClient;

    private Activity activity = null;
    public void doInitialize(Context context,Activity activity, String deviceID){
        // Retrieve a refrence of the location manager on Android from the location service
        this.context= context;
        this.deviceID=deviceID;
        this.activity = activity;
    }

    private  String getDeviceID(){
        return this.deviceID;
    }


    public void startDrive(){


        if (!startedDriving) {


            // The UI thread solely deals with the user interface and can execute tasks on it
            API.runInAsyncUIThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        connectDeviceClient();
                        if (startDrive(deviceID)) {
                            // Keep the screen Awake (do not lock automatically or dim)
                            // https://developer.android.com/training/scheduling/wakelock.html

                            reserveCar();


                            //transmissionCount = 0;

                        } else {
                            // Show a toast to the user
                            Log.e(LOG_TAG, "startDrive returning as startDrive failed");
                        }
                    }catch(Exception ex){
                        Log.e(LOG_TAG,"startDrive Exception " + ex.toString());
                    }
                }
            }, activity);
        }
        else{
            Log.e(LOG_TAG, "startDrive returning as startedDrive is already true");
        }



    }

    public void endDrive(){
        startedDriving = false;
//Todo : Make this async
                // Complete the reservation on the server
                completeReservation(reservationId[0], false);


    }


    private boolean startDrive(final String deviceId) {
        if (deviceClient == null) {
            Log.e(LOG_TAG, "startDrive returning as deviceClient is null");
            return false;
        }
         if (reservationForMyDevice(deviceId)) {
            userUnlocked = true;

            if (tripID == null) {
                // Generate a random UUID and assign it to this trip's ID
                tripID = UUID.randomUUID().toString();
            }
        }
        return true;
    }

    public void stopDrive(final String deviceId) {
        // If the reservation belongs to the current user
        if (reservationForMyDevice(deviceId)) {
            userUnlocked = false;
        }
    }

    public void completeDrive(final String deviceId) {
        // If the reservation belongs to the current user
        if (reservationForMyDevice(deviceId)) {
            tripID = null;  // clear the tripID
        }
    }

    public  String getTripId(final String deviceId) {
        // If the reservation belongs to the current user
        if (reservationForMyDevice(deviceId)) {
            return tripID;
        }

        return null;
    }

    public  boolean reservationForMyDevice(final String deviceId) {
        return behaviorDemo && deviceId == getDeviceID();
    }







    @Override
    public void onLocationChanged(Location location) {
//        Log.i("Location Data", "New Location - " + location.getLatitude() + ", " +  location.getLongitude());
        //updateLocation(location);
    }

    public void updateLocation(final Location location, Activity activity) {

       // final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
       // final boolean gpsEnabled = true;
       // final boolean networkConnected = networkInfo != null && networkInfo.isConnected();
        final boolean networkConnected =true;//ToDo - Handle the case where there is no network and it tracks it offline
        if (Helper.isNetworkAvailable(context)) {

            /*if (!checkCurrentLocation()) {
                Log.e("Location Data", "Not Working!");
                Toast.makeText(getActivity().getApplicationContext(), "Please activate your location settings and restart the application!", Toast.LENGTH_LONG).show();
                getAccurateLocation();
                return;
            }*/

            if (startedDriving) {
                if (!behaviorDemo) {
                    // get credentials may be failed
                    startedDriving = false;
                    completeReservation(reservationId[0], false);
                    return;
                }

                tripCount += 1;
                if (tripCount % 10 == 0) {
//                renderMapMatchedLocation()
                }
            } else if (deviceClient != null) {
               // supportActionBar.setTitle("Press Start Driving when ready.");
            }
            if (behaviorDemo) {
                API.runInAsyncUIThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (deviceClient == null && needCredentials) {
                                createDeviceClient();

                            }
                            if (userUnlocked) {
                                sendLocation(location);
                            }
                        }catch (Exception ex){
                            Log.e(LOG_TAG,ex.getMessage());
                        }
                    }
                }, activity);
                /*//ToDo - Evaluate if this needs to be async
                if (deviceClient == null && needCredentials) {
                    createDeviceClient();
                    connectDeviceClient();
                }
                if (userUnlocked) {
                    sendLocation(location);
                }*/
            }
        } else {

        }
    }

    /*private boolean checkCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        final List<String> providers = locationManager.getProviders(true);
        Location finalLocation = null;

        for (final String provider : providers) {
            final Location lastKnown = locationManager.getLastKnownLocation(provider);
            if (lastKnown == null) {
                continue;
            }
            if (finalLocation == null || (lastKnown.getAccuracy() < finalLocation.getAccuracy())) {
                finalLocation = lastKnown;
            }
        }
        location = finalLocation;
        return location != null;
    }
*/
    public void createDeviceClient() {
        if (deviceClient != null) {
            // already got credentials
            return;
        }
        final String url = API.credentials + "/" + getDeviceID() + "?owneronly=true";
        try {
            API.doRequest task = new API.doRequest(new API.doRequest.TaskListener() {
                @Override
                public void postExecute(JSONArray result) throws JSONException, MqttException {
                    final JSONObject serverResponse = result.getJSONObject(result.length() - 1);
                    final int statusCode = serverResponse.getInt("statusCode");

                    result.remove(result.length() - 1);


                    if (statusCode == 200 && result.length() > 0) {
                        //Toast.makeText(context, "Start Driving when ready.", Toast.LENGTH_LONG).show();;

                        Log.d(LOG_TAG, "getDeviceID was successfull");
                    } else if (statusCode == 409) {
                        //Toast.makeText(context, "Error: vehicle already registered inconsistently. Ask admin to remove it. And try again.", Toast.LENGTH_LONG).show();
                        Log.d(LOG_TAG, "getDeviceID failed: vehicle already registered inconsistently. Ask admin to remove it. And try again");
                        behaviorDemo = false;
                        return;
                    } else {
                        //Toast.makeText(context, "Error: Failed to get credentials.", Toast.LENGTH_LONG).show();
                        Log.d(LOG_TAG, "getDeviceID failed:  Failed to get credentials.");

                        behaviorDemo = false;
                        return;
                    }

                    final JSONObject deviceCredentials = result.getJSONObject(0);
                    try {
                        final Properties options = new Properties();
                        options.setProperty("org", deviceCredentials.getString("org"));
                        options.setProperty("type", deviceCredentials.getString("deviceType"));
                        options.setProperty("id", deviceCredentials.getString("deviceId"));
                        options.setProperty("auth-method", "token");
                        options.setProperty("auth-token", deviceCredentials.getString("token"));
                        deviceClient = new DeviceClient(options);
                    } catch (MqttException me) {
                        me.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.i(LOG_TAG, "Credentials Data: "+ result.toString());
                    needCredentials = false;

                   //startDrive();



                }
            });

            task.execute(url, "GET").get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    // return true if connection exists
    private boolean connectDeviceClient() {
        if (deviceClient == null) {
            return false;
        }
        if (!deviceClient.isConnected()) {
            try {
                deviceClient.connect();
                Log.d(LOG_TAG, "MQTT: "+ "Connected");
                return true;
            } catch (MqttException e) {
                e.printStackTrace();
                Log.d(LOG_TAG, "MQTT" + "Failed to connect");
                return false;
            }
        } else {
            return true;
        }
    }

    /*public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == GPS_INTENT) {
            if (networkIntentNeeded) {
                Toast.makeText(context, "Please connect to a network", Toast.LENGTH_LONG).show();

                final Intent settingsIntent = new Intent(Settings.ACTION_SETTINGS);
                //startActivityForResult(settingsIntent, SETTINGS_INTENT);
            } else {
                getAccurateLocation();
            }
        } else if (requestCode == SETTINGS_INTENT) {
            networkIntentNeeded = false;

            getAccurateLocation();
        }
    }*/

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void sendLocation(final Location location) {
        if (connectDeviceClient()) {
            final GregorianCalendar cal = new GregorianCalendar();
            final TimeZone gmt = TimeZone.getTimeZone("GMT");
            cal.setTimeZone(gmt);
            final SimpleDateFormat formattedCal = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            formattedCal.setCalendar(cal);
            final String timestamp = formattedCal.format(cal.getTime());

            final double speed = Math.max(0.0, location.getSpeed() * 60 * 60 / 1000);
            final double longitude = location.getLongitude();
            final double latitude = location.getLatitude();
            final String mobileAppDeviceId = getDeviceID();
            final String status = tripID != null ? "Unlocked" : "Locked";

            if (tripID == null) {
                // this trip should be completed, so lock device now
                userUnlocked = false;
            }

            final JsonObject event = new JsonObject();
            final JsonObject data = new JsonObject();
            event.add("d", data);
            data.addProperty("trip_id", tripID);
            data.addProperty("speed", speed);
            data.addProperty("lng", longitude);
            data.addProperty("lat", latitude);
            data.addProperty("ts", timestamp);
            data.addProperty("id", mobileAppDeviceId);
            data.addProperty("status", status);

            if (deviceClient.publishEvent("sensorData", event, 0)) {
                Log.d(LOG_TAG, "publish event " + event.toString());
                //supportActionBar.setTitle(speedMessage + " - Data sent (" + (++transmissionCount) + ")");
            } else {
                Log.d(LOG_TAG, "ERROR in publishing event " + event.toString());
                //supportActionBar.setTitle("Data Transmission Error.");
            }
        }
    }

    public String jsonToString(ArrayList<ArrayList<String>> data) {
        String temp = "{\"d\":{";
        int accum = 0;

        for (int i = 0; i < data.size(); i++) {
            if (accum == (data.size() - 1)) {
                temp += "\"" + data.get(i).get(0) + "\": \"" + data.get(i).get(1) + "\"}}";
            } else {
                temp += "\"" + data.get(i).get(0) + "\": \"" + data.get(i).get(1) + "\", ";
            }

            accum += 1;
        }

        return temp;
    }

    public void completeReservation(final String resId, final boolean alreadyTaken) {
        final String url = API.reservation + "/" + resId;

        try {
            final API.doRequest task = new API.doRequest(new API.doRequest.TaskListener() {
                @Override
                public void postExecute(JSONArray result) throws JSONException {
                    final JSONObject serverResponse = result.getJSONObject(result.length() - 1);
                    final int statusCode = serverResponse.getInt("statusCode");

                    result.remove(result.length() - 1);

                    String title = "";
                    String message = "";

                    switch (statusCode) {
                        case 200:
                            title = "Trip completed.";
                            message = "Please allow at least 30 minutes for the driver behavior data to be analyzed";
                            reservationId[0] = null;
                            Log.i(LOG_TAG,"completeReservation:Trip completed");
                            break;
                        default:
                            title = "Something went wrong.";
                            Log.e(LOG_TAG,"completeReservation:Trip completion failed");
                    }


                    /*if (!alreadyTaken) {
                        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
                        toast.show();
                    }*/

                    Log.i(LOG_TAG, "Complete Reservation: "+ result.toString());
                }
            });

            final String trip_id = getTripId(deviceID);

            final JSONObject bodyObject = new JSONObject();
            bodyObject.put("status", "close");

            if (trip_id != null) {
                // bind this trip to this reservation
                bodyObject.put("trip_id", trip_id);
            }

            completeDrive(deviceID);

            task.execute(url, "PUT", null, bodyObject.toString()).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void cancelReservation(String resId) {
        String url = API.reservation + "/" + resId;

        try {
            API.doRequest task = new API.doRequest(new API.doRequest.TaskListener() {
                @Override
                public void postExecute(JSONArray result) throws JSONException {
                    Log.i(LOG_TAG, "Cancel Reservation:" + result.toString());
                }
            });

            task.execute(url, "DELETE").get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void reserveCar() {
// reserve my device as a car
        final String url = API.reservation;
        final GregorianCalendar temp = new GregorianCalendar();
        final long pickupTime = temp.getTimeInMillis() / 1000;
        final long dropoffTime = (temp.getTimeInMillis() / 1000) + 3600;
        final Uri.Builder builder = new Uri.Builder()
                .appendQueryParameter("carId", deviceID)
                .appendQueryParameter("pickupTime", pickupTime + "")
                .appendQueryParameter("dropOffTime", dropoffTime + "");
        final String query = builder.build().getEncodedQuery();

        try {
            final API.doRequest task = new API.doRequest(new API.doRequest.TaskListener() {
                @Override
                public void postExecute(JSONArray result) throws JSONException {
                    final JSONObject serverResponse = result.getJSONObject(result.length() - 1);
                    final int statusCode = serverResponse.getInt("statusCode");

                    result.remove(result.length() - 1);
                    switch (statusCode) {
                        case 200:
                            // start driving
                            startedDriving = true;
                            reservationId[0] = result.getJSONObject(0).getString("reservationId");
                            //Reservations.userReserved = true;
                            Log.i(LOG_TAG, "Reservation: " + "Made=" + result.toString());

                            //getAccurateLocation();

                            break;
                        case 409:
                            Log.i(LOG_TAG, "Reservation: "+ "Already Exists=" + result.toString());
                            useExistingReservation();

                            break;
                        case 404:
                            Log.i(LOG_TAG,"Reservation: "+ "Not Made" + result.toString());

                            break;
                        default:
                            Log.i(LOG_TAG,"Reservation: "+ "Error" + result.toString());
                    }
                }
            });

            alreadyReserved = true;
            task.execute(url, "POST", query).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void useExistingReservation() {
        final String url = API.reservations;

        try {
            final API.doRequest task = new API.doRequest(new API.doRequest.TaskListener() {
                @Override
                public void postExecute(JSONArray result) throws JSONException {
                    result.remove(result.length() - 1);
                    for (int i = 0; i < result.length(); i++) {
                        final ReservationsData reservationData = new ReservationsData(result.getJSONObject(i));

                        if (reservationData.carId.equals(getDeviceID())) {
                            if (reservationData.status.equals("driving")) {
                                completeReservation(reservationData._id, true);
                                reserveCar();
                            } else {
                                startedDriving = true;
                                reservationId[0] = reservationData._id;
                            }
                        }
                    }

                    Log.i(LOG_TAG,"Existing Reservation: "+ result.toString());
                }
            });

            task.execute(url, "GET").get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
