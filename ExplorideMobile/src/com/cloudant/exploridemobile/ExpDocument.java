package com.cloudant.exploridemobile;

import com.cloudant.sync.datastore.DocumentRevision;

import java.util.Map;

/**
 * Created by pranav.r on 10/25/16.
 */

public  abstract class ExpDocument {
    protected DocumentRevision mRev;
    public DocumentRevision getDocumentRevision() {
        return mRev;
    }

    protected String mType;
    public String getType() {
        return mType;
    }
    public void setType(String type) {
        this.mType = type;
    }
    public String toString() {return "";}

    public   static ExpDocument fromRevision(DocumentRevision rev, String type){
        ExpDocument doc = null;
        if(type.equalsIgnoreCase(ExpDocumentManager.DOC_TYPE_LOCATION)){
             doc = new Location();
        }
        if(type.equalsIgnoreCase(ExpDocumentManager.DOC_TYPE_EXPSETTINGS)){
            doc = new ExpSettings();
        }
        if(type.equalsIgnoreCase(ExpDocumentManager.DOC_TYPE_EXPTRIPS)){
            doc = new ExpTrip();
        }
        try {
            if (null != doc)
                return doc.fromRevision(rev);
        }catch (Exception e){
            String a = "";
        }
        return null;
    }
    public abstract ExpDocument fromRevision(DocumentRevision rev);
    public abstract Map<String, Object> asMap();


    }
