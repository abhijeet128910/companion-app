/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.peeyush.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.example.peeyush.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "dev";
  public static final int VERSION_CODE = 9;
  public static final String VERSION_NAME = "1.1.10.de0442";
  // Fields from default config.
  public static final String GIT_BRANCH = "develop";
  public static final String GIT_HASH = "de04421d5345be2989acf92a793fb69e516f8e2b";
}
