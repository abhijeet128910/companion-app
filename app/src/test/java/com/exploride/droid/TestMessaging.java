package com.exploride.droid;

import com.cloudant.exploridemobile.framework.Utils.TaskPriority;
import com.exploride.droid.handlers.HUDMessenger;
import com.exploride.droid.logger.ExpLogger;

import java.util.ArrayList;

/**
 * Created by peeyushupadhyay on 30/10/17.
 */

public class TestMessaging {

    private static String TAG = "TestMessaging";

    ArrayList<CustomRunnable> testInput;
    public TestMessaging(){

        testInput = new ArrayList<CustomRunnable>(100);

        testInput.add(getCRunnable("5 This is a String ", TaskPriority.Medium));
        testInput.add(getCRunnable("6 This is a String ", TaskPriority.Medium));
        testInput.add(getCRunnable("7 This is a String ", TaskPriority.Medium));
        testInput.add(getCRunnable("8 This is a String ", TaskPriority.Medium));
        testInput.add(getCRunnable("11 This is a String ", TaskPriority.Low));
        testInput.add(getCRunnable("12 This is a String ", TaskPriority.Low));
        testInput.add(getCRunnable("1 This is a String ", TaskPriority.High));
        testInput.add(getCRunnable("2 This is a String ", TaskPriority.High));
        testInput.add(getCRunnable("3 This is a String ", TaskPriority.High));
        testInput.add(getCRunnable("4 This is a String ", TaskPriority.High));
        testInput.add(getCRunnable("9 This is a String ", TaskPriority.Medium));
        testInput.add(getCRunnable("10 This is a String ", TaskPriority.Medium));
        testInput.add(getCRunnable("4.1 This is a String ", TaskPriority.High));

    }
    public void test(){

        HUDMessenger hudMessenger = new HUDMessenger();
        hudMessenger.init();

        for(int i =0; i<testInput.size();i++){

            CustomRunnable customRunnable = testInput.get(i);
            hudMessenger.sendMessage(customRunnable.runnable, customRunnable.priority);
        }
    }
    CustomRunnable getCRunnable(final String val, TaskPriority priority){
        Runnable r = new Runnable(){
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ExpLogger.logD(TAG, val);
            }
        };
        CustomRunnable cr = new CustomRunnable(r, priority);
        return cr;
    }
    class CustomRunnable{
        Runnable runnable;
        TaskPriority priority;

        CustomRunnable(Runnable runnable, TaskPriority priority){
            this.runnable = runnable;
            this.priority = priority;
        }
    }

}
