package com.exploride.droid.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

/**
 * Utilities and constants related to app settings_prefs.
 */
public class SettingsUtils {

    private static final String TAG = SettingsUtils.class.getSimpleName();

    private static SharedPreferences sharedPreferences;

    /**
     * Helper method to register a settings_prefs listener. This method does not automatically handle
     * {@code unregisterOnSharedPreferenceChangeListener() un-registering} the listener at the end
     * of the {@code context} lifecycle.
     *
     * @param context  Context to be used to lookup the {@link SharedPreferences}.
     * @param listener Listener to register.
     */
    public static void  registerOnSharedPreferenceChangeListener(final Context context,
                                                                SharedPreferences.OnSharedPreferenceChangeListener listener) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.registerOnSharedPreferenceChangeListener(listener);
    }

    /**
     * Helper method to un-register a settings_prefs listener typically registered with
     * {@code registerOnSharedPreferenceChangeListener()}
     *
     * @param context  Context to be used to lookup the {@link SharedPreferences}.
     * @param listener Listener to un-register.
     */
    public static void unregisterOnSharedPreferenceChangeListener(final Context context,
                                                                  SharedPreferences.OnSharedPreferenceChangeListener listener) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.unregisterOnSharedPreferenceChangeListener(listener);
    }

    /**
     * writes String value into the shared preferences
     * @param context: Context to be used to lookup the {@link SharedPreferences}.
     * @param prefsKey: the key mapped to the value
     * @param prefsValue: the value to be stored into shared preferences
     */
    public static void writePreferenceValue(Context context, String prefsKey, String prefsValue) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putString(prefsKey, prefsValue);
        editor.commit();
    }

    /**
     * reads String value into the shared preferences
     * @param context: Context to be used to lookup the {@link SharedPreferences}.
     * @param prefsKey: the key mapped to the value
     * @return String value mapped to prefsKey
     */
    public static String readPreferenceValueAsString(Context context, String prefsKey) {
        return getPreferences(context).getString(prefsKey, null);
    }


    /**
     * writes int value into the shared preferences
     * @param context: Context to be used to lookup the {@link SharedPreferences}.
     * @param prefsKey: the key mapped to the value
     * @param prefsValue: the value to be stored into shared preferences
     */
    public static void writePreferenceValue(Context context, String prefsKey, int prefsValue) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putInt(prefsKey, prefsValue);
        editor.commit();
    }

    /**
     * reads int value into the shared preferences
     * @param context: Context to be used to lookup the {@link SharedPreferences}.
     * @param prefsKey: the key mapped to the value
     * @return int value mapped to prefsKey
     */
    public static int readPreferenceValueAsInt(Context context, String prefsKey) {
        return getPreferences(context).getInt(prefsKey, -1);
    }

    /**
     * writes boolean value into the shared preferences
     * @param context: Context to be used to lookup the {@link SharedPreferences}.
     * @param prefsKey: the key mapped to the value
     * @param prefsValue: the value to be stored into shared preferences
     */
    public static void writePreferenceValue(Context context, String prefsKey, boolean prefsValue) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putBoolean(prefsKey, prefsValue);
        editor.commit();
    }
    public static Set<String> readPreferenceValueAsSet(Context context, String prefsKey) {
        return getPreferences(context).getStringSet(prefsKey, null);
    }

    public static void writePreferenceValue(Context context, String prefsKey, Set<String> prefsValue) {
        SharedPreferences.Editor editor = getPrefsEditor(context);
        editor.putStringSet(prefsKey, prefsValue);
        editor.commit();
    }
    /**
     * reads boolean value into the shared preferences
     * @param context: Context to be used to lookup the {@link SharedPreferences}.
     * @param prefsKey: the key mapped to the value
     * @return int value mapped to prefsKey
     */
    public static boolean readPreferenceValueAsBoolean(Context context, String prefsKey) {
        return getPreferences(context).getBoolean(prefsKey, false);
    }
    public static boolean readPreferenceValueAsBoolean(Context context, String prefsKey,boolean defaultVal) {
        return getPreferences(context).getBoolean(prefsKey, defaultVal);
    }

    /**
     * reads boolean value into the shared preferences
     * @param context: Context to be used to lookup the {@link SharedPreferences}.
     * @param prefsKey: the key mapped to the value
     * @return true default
     */
    public static boolean readPreferenceValueWithDefault(Context context, String prefsKey, boolean defaultValue) {
        return getPreferences(context).getBoolean(prefsKey, defaultValue);
    }

    private static SharedPreferences.Editor getPrefsEditor(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.edit();
    }

    private static SharedPreferences getPreferences(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences;
    }
}