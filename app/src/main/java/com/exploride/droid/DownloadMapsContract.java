package com.exploride.droid;

import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;
import com.here.android.mpa.odml.MapPackage;

import java.util.List;

/**
 * Author Mayank
 */

public interface DownloadMapsContract {

    /**
     * methods for Presenter. Available to View
     */
    interface DownloadMapsOps extends BasePresenter<DownloadMapsContract.DownloadMapsView> {

        void onGetMapPackagesId(List<Integer> packageIdList, boolean isInstalledMapPackageId);

        void onQueryTextChange(String query, List<MapPackage> mapPackageList);

        void onMapInstallProgress(int progress);

        void setMapPackageId(int packageId);

        int getMapPackageId();

        void switchToMapsScreen();

        int getMapInstallProgress();

        void initInstalledPackageIdList();

        void onMapPackageError();

        void onMapInstallSuccess();

        void onMapInstallError();

        void onMapUninstallSuccess();

        void onMapUninstallError();

        void onMapEngineInitializationError();

        List<Integer> getDownloadedMapPackageIdList();

    }


    /**
     * methods for View. Available to Presenter
     */

    interface DownloadMapsView extends BaseView {

        void showMapsList(MapPackage mapPackage);

        void showInstalledMapsList(List<MapPackage> downloadedMapsList);

        void onSearchCompleted(List<MapPackage> mapPackageList);

        void showMapPackageError();

        void showMapInstallProgress();

        void showMapInstallSuccess(MapPackage mapPackage);

        void showMapInstallError();

        void showMapUninstallSuccess();

        void showMapUninstallError();

        void showMapEngineInitializationError();

        void showMapEngineOfflineError();

    }

    /**
     * methods for Model. Available to Presenter
     */
    interface DownloadMapsStore {

        void setPackageIdList(List<Integer> packageIdList);

        List<Integer> getPackageIdList();

        void setMapPackageId(int mapPackageId);

        int getMapPackageId();

        void setProgress(int progress);

        int getProgress();

        void initPackageIdList();
    }

    /**
     * methods for Presenter. Available to Model
     */
    interface DownloadMapsPresenterOps {

    }
}
