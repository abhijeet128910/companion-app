package com.exploride.droid.geocoding;

import android.app.Activity;
import android.location.Location;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.cloudant.exploridemobile.LocationsMgr;
import com.exploride.droid.R;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.network.api.ApiRepo;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.SearchLocationPresenter;
import com.exploride.droid.views.fragments.MapsDestinationFragment;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

/**
 * Class to get Auto Suggest Predictions from Here
 */

class HereMaps implements GeoCodingProvider, Response.Listener<String>, Response.ErrorListener {

    private String TAG = HereMaps.class.getName();
    private String AUTOCOMPLETE_PLACES = "places_matched";
    private Activity mActivity;

    @Override
    public void getAutoSuggestPredictions(Activity activity, final String queryLocation) {

        mActivity = activity;

        Thread thread = new Thread() {
            @Override
            public void run() {

                Location currentLocation = MainPresenter.getInstance().getCurrentLocation();
                if (currentLocation != null) {
                    ApiRepo.getAutoSuggestionResponse(mActivity, HereMaps.this, HereMaps.this, currentLocation.getLatitude(), currentLocation.getLongitude(), queryLocation, AUTOCOMPLETE_PLACES);
                }

            }
        };
        thread.start();
    }

    /**
     * sets error returned while making network request
     *
     * @param error: error occurred while making a network request
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        if (error instanceof NoConnectionError) {
            ExpLogger.logE(TAG, "NoConnectionError");
            SearchLocationPresenter.getInstance().onError(mActivity.getResources().getString(R.string.no_connection_error));
        } else if (error instanceof TimeoutError) {
            ExpLogger.logE(TAG, "TimeoutError");
            SearchLocationPresenter.getInstance().onError(mActivity.getResources().getString(R.string.other_error));
        } else if (error instanceof NetworkError) {
            ExpLogger.logE(TAG, "NetworkError");
            SearchLocationPresenter.getInstance().onError(mActivity.getResources().getString(R.string.other_error));
        } else if (error instanceof ServerError) {
            ExpLogger.logE(TAG, "ServerError");
            SearchLocationPresenter.getInstance().onError(mActivity.getResources().getString(R.string.other_error));
        } else if (error instanceof AuthFailureError) {
            ExpLogger.logE(TAG, "AuthFailureError");
            SearchLocationPresenter.getInstance().onError(mActivity.getResources().getString(R.string.other_error));
        } else if (error instanceof ParseError) {
            ExpLogger.logE(TAG, "ParseError");
            SearchLocationPresenter.getInstance().onError(mActivity.getResources().getString(R.string.other_error));
        }
    }

    /**
     * parses response for the network request and passes it to @class {@link MapsDestinationFragment}
     * to be rendered on the UI
     *
     * @param response: network response
     */
    @Override
    public void onResponse(String response) {
        ExpLogger.logD("PLACES RESULT:::", response);
        Gson gson = new Gson();
        PlaceAutoComplete placeAutoComplete = gson.fromJson(response, PlaceAutoComplete.class);
        for(PlaceAutoComplete.Result result : placeAutoComplete.results){
            result.isFavorite = SearchLocationPresenter.getInstance().isFavoriteLocation(result.id);
        }
        SearchLocationPresenter.getInstance().onResponse(placeAutoComplete);
    }
}
