package com.exploride.droid.geocoding;

import android.text.TextUtils;

import com.exploride.droid.utils.Constants;

/**
 * Factory to generate object of concrete class based on given information
 */

public class GeoCodingProviderFactory {

    //use getGeoCodingProvider method to get object of type GeoCodingProvider
    public GeoCodingProvider getGeoCodingProvider(String providerType) {
        if (providerType == null) {
            return null;
        }
        if (TextUtils.equals(providerType, Constants.ProviderType.HERE_MAPS)) {
            return new HereMaps();
        } else if (TextUtils.equals(providerType, Constants.ProviderType.GOOGLE_MAPS)) {
            return new GoogleMaps();
        }

        return null;
    }
}
