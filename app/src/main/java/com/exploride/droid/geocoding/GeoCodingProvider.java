package com.exploride.droid.geocoding;

import android.app.Activity;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Interface that concrete classes will implement
 */

public interface GeoCodingProvider {
    void getAutoSuggestPredictions(Activity activity, String queryLocation);
}
