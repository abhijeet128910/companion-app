package com.exploride.droid.geocoding;

import android.app.Activity;
import android.graphics.Typeface;
import android.location.Location;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;

import com.cloudant.exploridemobile.LocationsMgr;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.SearchLocationPresenter;
import com.exploride.droid.utils.Constants;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Class to get Auto Suggest Predictions from Google
 */

public class GoogleMaps implements GeoCodingProvider {

    private String TAG = GoogleMaps.class.getName();
    private Status autoCompleteStatus;
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);

    // bound of approx. 5 mile radius
    private int distanceInMeters = 5500;

    @Override
    public void getAutoSuggestPredictions(final Activity activity, final String queryLocation) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (MainPresenter.getInstance().getGoogleApiClient() != null) {
                    // Current results returned by this adapter.
                    ArrayList<AutocompletePrediction> placeAutoSuggestions = getAutocomplete(queryLocation, MainPresenter.getInstance().getGoogleApiClient());
                    if (placeAutoSuggestions != null) {
                        ExpLogger.logE(TAG, "GeoCode Results : " + placeAutoSuggestions.size());
                        PlaceAutoComplete placeAutoComplete = new PlaceAutoComplete();
                        for (AutocompletePrediction prediction : placeAutoSuggestions) {
                            final PlaceAutoComplete.Result placeAutoCompleteResult = placeAutoComplete.new Result();
                            placeAutoCompleteResult.title = prediction.getPrimaryText(STYLE_BOLD).toString();
                            placeAutoCompleteResult.vicinity = prediction.getSecondaryText(STYLE_BOLD).toString();
                            placeAutoCompleteResult.placeId = prediction.getPlaceId();
                            placeAutoCompleteResult.isFavorite= SearchLocationPresenter.getInstance().isFavoriteLocation (prediction.getPlaceId());
//                            placeAutoCompleteResult.isFavorite=LocationsMgr.getLocationsMgr().isFavoriteLocation(prediction.getPlaceId());
                            placeAutoComplete.results.add(placeAutoCompleteResult);
                        }
                        SearchLocationPresenter.getInstance().onResponse(placeAutoComplete);
                    } else
                        SearchLocationPresenter.getInstance().onError(autoCompleteStatus.getStatusMessage());
                }

            }
        };
        thread.start();
    }

    /**
     * Submits an autocomplete query to the Places Geo Data Autocomplete API.
     * Results are returned as frozen AutocompletePrediction objects, ready to be cached.
     * objects to store the Place ID and description that the API returns.
     * Returns an empty list if no results were found.
     * Returns null if the API client is not available or the query did not complete
     * successfully.
     * This method MUST be called off the main UI thread, as it will block until data is returned
     * from the API, which may include a network request.
     *
     * @param constraint Autocomplete query string
     * @return Results from the autocomplete API or null if the query was not successful.
     * @see Places#GEO_DATA_API#getAutocomplete(CharSequence)
     * @see AutocompletePrediction#freeze()
     */
    private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint, GoogleApiClient mGoogleApiClient) {
        if (mGoogleApiClient.isConnected()) {
            ExpLogger.logI(TAG, "Starting autocomplete query for: " + constraint);

            //call to get latlng bounds around current location
            LatLngBounds latLngBounds = getBounds(MainPresenter.getInstance().getCurrentLocation());

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.
            PendingResult<AutocompletePredictionBuffer> results =
                    Places.GeoDataApi
                            .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                    latLngBounds, null);

            // This method should have been called off the main UI thread. Block and wait for at most 5 seconds for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions = results
                    .await(Constants.AUTO_COMPLETE_PLACE_REQUEST_TIMEOUT, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            autoCompleteStatus = autocompletePredictions.getStatus();
            if (!autoCompleteStatus.isSuccess()) {
                ExpLogger.logE(TAG, "Error getting autocomplete prediction API call: " + autoCompleteStatus.toString());
                autocompletePredictions.release();
                return null;
            }

            ExpLogger.logI(TAG, "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");

            // Freeze the results immutable representation that can be stored safely.
            return DataBufferUtils.freezeAndClose(autocompletePredictions);
        }
        ExpLogger.logE(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }

    /**
     * find place info for the given place id
     * @param placeId: unique id for each place
     */
    public void findCoordinatesByPlaceId(String placeId){
        //gets place information by Place Id
        Places.GeoDataApi.getPlaceById(MainPresenter.getInstance().getGoogleApiClient(), placeId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {

                    @Override
                    public void onResult(PlaceBuffer placeBuffer) {
                        if (placeBuffer.getStatus().isSuccess()) {
                            final Place mPlace = placeBuffer.get(0);
                            LatLng latLng = mPlace.getLatLng();//you can use lat with latLng.latitude and long with latLng.longitude;

                            ArrayList<Double> coordinatesList = new ArrayList<Double>();
                            coordinatesList.add(latLng.latitude);
                            coordinatesList.add(latLng.longitude);

                            SearchLocationPresenter.getInstance().setPlaceCoordinates(coordinatesList);

                        }
                        placeBuffer.release();
                    }
                });
    }

    /**
     * biasing the results to a specific area specified by latitude and longitude bounds.
     * @param location: Current Location
     * @return LatLngBounds instance for a given location
     */
    private LatLngBounds getBounds(Location location){
        double latRadian = Math.toRadians(location.getLatitude());

        double degLatKm = 110.574235;
        double degLongKm = 110.572833 * Math.cos(latRadian);
        double deltaLat = distanceInMeters / 1000.0 / degLatKm;
        double deltaLong = distanceInMeters / 1000.0 / degLongKm;

        double minLat = location.getLatitude() - deltaLat;
        double minLong = location.getLongitude() - deltaLong;
        double maxLat = location.getLatitude() + deltaLat;
        double maxLong = location.getLongitude() + deltaLong;

        ExpLogger.logD(TAG,"Min: "+Double.toString(minLat)+","+Double.toString(minLong));
        ExpLogger.logD(TAG,"Max: "+Double.toString(maxLat)+","+Double.toString(maxLong));

        return new LatLngBounds(new LatLng(minLat, minLong), new LatLng(maxLat, maxLong));
    }

}
