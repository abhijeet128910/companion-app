package com.exploride.droid.utils;

import static android.R.attr.value;

public class Constants {

    public static final int REQUEST_TIMEOUT = 15000;

    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    //auto complete place request timeout in seconds
    public static int AUTO_COMPLETE_PLACE_REQUEST_TIMEOUT = 5;

    public static final String KEY_BASE = "com.exploride.droid.";
    public static final String KEY_NAVIGATION_VIEW = "navigation_view";
    public static final String KEY_TOP_NAVIGATION_VIEW_HUD = "top_navigation_view";
    public static final String KEY_ENABLE_DEBUG_MODE = "enable_debug_mode";
    public static final String KEY_ENABLE_GESTURES = "gestures_status";
    public static final String SC_NAME = "exp_nav_";

    public static class Bluetooth {

        // Message types sent from the BluetoothClientService Handler
        public static final int MESSAGE_STATE_CHANGE = 1;
        public static final int MESSAGE_READ = 2;
        public static final int MESSAGE_WRITE = 3;
        public static final int MESSAGE_DEVICE_NAME = 4;
        public static final int MESSAGE_TOAST = 5;
        public static final int MESSAGE_CONNECTION_FAILED = 6;

        // Key names received from the BluetoothClientService Handler
        public static final String DEVICE_NAME = "device_name";
        public static final String DEVICE_ADDR = "device_addr";
        public static final String TOAST = "toast";

    }

    public static class LocationTracer {

        // Message types sent from the BluetoothClientService Handler
        public static final int MESSAGE_NEW = 1;
        public static final int MESSAGE_OPEN = 2;
        public static final int MESSAGE_WRITE = 3;
        public static final int MESSAGE_CLOSE = 4;

    }
    public static class Permissions {

        public static int CALL_PHONE_1 = 12;
    }

    public static class Fragments {

        public static final String MapsDestinationFragment = "MapsDestinationFragment";
        public static final String SearchLocationFragment = "SearchLocationFragment";
        public static final String BT_Settings = "settings";
        public static final String DownloadMapsFragment = "DownloadMapsFragment";
        public static final String SAVED_TRIPS = "SavedTrips";
        public static final String WIFI_Settings = "wifi-settings";
        public static final String ReachedLocationFragment = "ReachedLocation";
        public static final String BrightnessFragment = "brightness";
        public static final String DeveloperOptionsFragment = "DeveloperOptions";
        public static final String AppVersion = "AppVersion";
        public static final String Trips = "Trips";
        public static final String DriverProfile = "DriverProfile";


    }

    public static class SharedPrefKeys{
        public static final String KEY_BT_DEV_ADDRESS = "bt-dev-address";
        public static final String KEY_BT_DEV_NAME = "bt-dev-name";
        public static final String KEY_DEVOPTIONS_DEST_PT = "dev-options-dest";
        public static final String KEY_DEVOPTIONS_ZOOM_LEVEL = "zoom-level";
        public static final String KEY_DEVOPTIONS_TILT_LEVEL = "tilt-level";
        public static final String KEY_DEVOPTIONS_ALLOW_MOCK = "allow-mock";
        public static final String KEY_USER_COMM_CHANNEL = "user-comm-channel";
        public static final String KEY_DEVOPTIONS_MOCK_INTERVAL = "mock-interval";
        public static final String KEY_DEVOPTIONS_ALLOW_PHONE_MOCK = "allow-phone-mock";
        public static final String KEY_DEVOPTIONS_USE_GPX_FILE = "use-gpx-file";
        public static final String KEY_DEVOPTIONS_PHONE_MOCK_INTERVAL_POS = "phone-mock-interval-pos";
        public static final String KEY_DEVOPTIONS_PHONE_MOCK_REPEAT_POS = "phone-mock-repeat-pos";
        public static final String KEY_DEVOPTIONS_FLIP_SCREEN = "flip-screen";
        public static final String KEY_DEVOPTIONS_PHONE_SCREEN_SCALE = "phone-screen-scale";
        public static final String KEY_DEVOPTIONS_PHONE_X_OFFSET = "phone-x-offset";
        public static final String KEY_DEVOPTIONS_PHONE_Y_OFFSET = "phone-y-offset";;
        public static final String KEY_DEVOPTIONS_TCP_HOST = "dev-tcp-host";
        public static final String KEY_DEVOPTIONS_MOCK_MODE = "dev-mock-mode";
        public static final String KEY_RECENT_LOCATIONS = "recent-locations";
        public static final String KEY_SPLASH_SCREEN = "splash_screen";
        public static final String KEY_DEVOPTIONS_STATE_TCP = "dev-opt-st-tcp";
        public static final String KEY_DEVOPTIONS_LOC_PH = "dev-options-loc-phone";
        public static final String KEY_DEVOPTIONS_PLAY_VIDEOS_ON_START = "dev-options-play-videos-on-start";
        public static final String KEY_BRIGHTNESS_VALUE = "key_brightness_value";
        //1 for auto, 2 for manual
        public static final String KEY_BRIGHTNESS_MODE = "brightness_mode";
        public static final String KEY_ENABLE_OFFLINE_NAVIGATION = "offline_navigation";
        public static final String KEY_SHOW_ROUTE_ONLY = "show_route_only";
        public static final String KEY_SC_TYPE = "sc_type";
        public static final String KEY_DEVOPTIONS_OBD_CHN = "obd-chn";
        public static final String KEY_VOICE_ONLINE = "voice_on_phone";

    }
    public enum CHANNEL{
        BT (1),
        TCP (2);
        private final int channel;

        private CHANNEL(int channel) {
            this.channel = channel;
        }

        public int getValue() {
            return channel;
        }
    }
    public static class UrlConstants {

        public static final String BASE_URL = "https://places.api.here.com/places/v1/";
    }

    // Related to calculate and display route distance in Route summary of a route.
    public static class Distance {
        public static final String MILES = "Miles";
        public static final String KILOMETERS = "Kms";

        public static final double MILES_CONVERSION_UNIT = 1609.344;
        public static final double KMS_CONVERSION_UNIT = 1000;
    }

    public static enum SAVE_DATA_RESPONSE{
        Success,
        Exists,
        Error
    }

    public static class LocationType {

        public static final String SOURCE = "Source";
        public static final String DESTINATION = "Destination";
    }

    public static class IntentExtraKeys{
        public static final String DEST_SELECTED_ON_MAP = "dest_on_map";
    }

    //us coordinates
    public static final double latitude = 40.758194;
    public static final double longitude = -73.985388;

    public static class ProviderType {
        //geo coding providers
        public static final String HERE_MAPS = "here_maps";
        public static final String GOOGLE_MAPS = "google_maps";
    }

    /**
     * Custom turn names to display
     * Used in Maps presenter when new instruction is received
     */
    public static String[] Turns = new String[]{
            "",// UNDEFINED(0),
            "Straight",//GO_STRAIGHT(1),
            "Uturn Right", //UTURN_RIGHT(2),
            "Uturn Left",//UTURN_LEFT(3),
            "Keep Right",//KEEP_RIGHT(4),
            "Light Right",//LIGHT_RIGHT(5),
            "Turn Right",//QUITE_RIGHT(6),
            "Heavy Right",//HEAVY_RIGHT(7),
            "Keep Middle",//KEEP_MIDDLE(8),
            "Keep Left", // KEEP_LEFT(9),
            "Light Left",//LIGHT_LEFT(10),
            "Turn Left",//QUITE_LEFT(11),
            "Heavy Left",//HEAVY_LEFT(12),
            "Take Right lane",//ENTER_HIGHWAY_RIGHT_LANE(13),
            "Take Left lane",//ENTER_HIGHWAY_LEFT_LANE(14),
            "Use Right lane",//LEAVE_HIGHWAY_RIGHT_LANE(15),
            "Use Left lane",//LEAVE_HIGHWAY_LEFT_LANE(16),
            "Keep Right",//HIGHWAY_KEEP_RIGHT(17),
            "Keep Left",//HIGHWAY_KEEP_LEFT(18),
            "1st Exit",//ROUNDABOUT_1(19),
            "2nd Exit",//ROUNDABOUT_2(20),
            "3rd Exit",//ROUNDABOUT_3(21),
            "4th Exit",//ROUNDABOUT_4(22),
            "5th Exit",//ROUNDABOUT_5(23),
            "6th Exit",//ROUNDABOUT_6(24),
            "7th Exit",//ROUNDABOUT_7(25),
            "8th Exit",//ROUNDABOUT_8(26),
            "9th Exit",//ROUNDABOUT_9(27),
            "10th Exit",//ROUNDABOUT_10(28),
            "11th Exit",//ROUNDABOUT_11(29),
            "12th Exit",//ROUNDABOUT_12(30),
            "1st Left Exit",//ROUNDABOUT_1_LH(31),
            "2nd Left Exit",//ROUNDABOUT_2_LH(32),
            "3rd Left Exit",//ROUNDABOUT_3_LH(33),
            "4th Left Exit",//ROUNDABOUT_4_LH(34),
            "5th Left Exit",//ROUNDABOUT_5_LH(35),
            "6th Left Exit",//ROUNDABOUT_6_LH(36),
            "7th Left Exit",//ROUNDABOUT_7_LH(37),
            "8th Left Exit",//ROUNDABOUT_8_LH(38),
            "9th Left Exit",//ROUNDABOUT_9_LH(39),
            "10th Left Exit",//ROUNDABOUT_10_LH(40),
            "11th Left Exit",//ROUNDABOUT_11_LH(41),
            "12th Left Exit",//ROUNDABOUT_12_LH(42),
            "Your Location",//START(43),
            "Destination",//END(44),
            "Ferry",//FERRY(45),
            "Pass station",//PASS_STATION(46),
            "Head to",//HEAD_TO(47),
            "Change line",//CHANGE_LINE(48);
    };

    public static class NavigationOptions{

        public static final String MotorWays = "motorways";
        public static final String TollRoads = "tollroads";
        public static final String Ferries = "ferries";
    }

    public static final double DEFAULT_SCALE = 1;
    public static final double DEFAULT_X_OFFSET = 0;
    public static final double DEFAULT_Y_OFFSET = -70;
    public static final double DEFAULT_ZOOM = 18;
    public static final float DEFAULT_TILT = 70;
}
