package com.exploride.droid.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;

import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.CannedMsg;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.presenters.MainPresenter;

/**
 * Created by abhijeet on 31/3/17.
 */

public class SMSHelper {

    private Context context;

    public SMSHelper(Context context) {
        this.context = context;
    }

    /**
     * Sending message to the number provided
     * @param phoneNumber
     * @param message
     */
    public void sendSMS(String phoneNumber, String message) {
        if (phoneNumber == null || message == null) {
            sendMessageStatus(CannedMsg.SendMessageStatus.MESSAGE_NOT_SENT);
            return;
        }

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        final BroadcastReceiver smsSentReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        sendMessageStatus(CannedMsg.SendMessageStatus.MESSAGE_SENT);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        sendMessageStatus(CannedMsg.SendMessageStatus.MESSAGE_NOT_SENT);
                        break;
                }
                context.unregisterReceiver(this);
            }
        };
        context.registerReceiver(smsSentReceiver, new IntentFilter(SENT));

        //---when the SMS has been delivered---
//        BroadcastReceiver smsDeliveredReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(context, "SMS delivered",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(context, "SMS not delivered",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        };
//        context.registerReceiver(smsDeliveredReceiver, new IntentFilter(SENT));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    private void sendMessageStatus(int status) {
        CannedMsg cannedMsg = new CannedMsg();
        cannedMsg.setStatus(status);
        BTComm btComm = new BTComm();
        btComm.setCannedMsg(cannedMsg);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }
}