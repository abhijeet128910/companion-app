package com.exploride.droid.utils;

/**
 * Helper class to format date into different formats
 *
 * Author Mayank
 */


import android.text.format.DateUtils;

import com.exploride.droid.presenters.MainPresenter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateFormatter {

    private static String TIME_FORMAT = "h:mm a";
    private static String DATE_FORMAT = "EEE, d MMMM yyyy";
    private static String DB_DATE_FORMAT = "dd/MM/yyyy H:mm:ss a";

    /**
     *
     * @return current device time
     */
    public static String getTime() {
        Date date = new Date();
        return new SimpleDateFormat(TIME_FORMAT).format(date).toUpperCase();
    }

    /**
     *
     * @return current device date
     */
    public static String getDisplayDate() {
        Date date = new Date();
        return new SimpleDateFormat(DATE_FORMAT).format(date);
    }

    /**
     *
     * @return current device date
     */
    public static String getDate() {
        Date date = new Date();
        return new SimpleDateFormat(DB_DATE_FORMAT).format(date);
    }

    /**
     * Get duration in days, hours, mins and seconds format
     *
     * @param seconds
     * @return
     */
    public static String getDurationInText(int seconds) {
        String[] duration = DateUtils.formatElapsedTime(seconds).split(":");
        if (duration.length > 2) {

            if (Integer.parseInt(duration[0]) > 24) {
                int day = (Integer.parseInt(duration[0]) / 24);
                int hour = Integer.parseInt(duration[0]) % 24;
                return day + " Day " + hour + " Hr " + duration[1] + " Min, ";
            } else {
                return duration[0] + " Hr " + duration[1] + " Min, ";
            }
        } else {
            return duration[0] + " Min, ";
        }

    }

    /**
     * Get difference in two dates
     * @param startTimeLong
     * @param endTimeLong
     * @return
     */
    public static String dateDifference(long startTimeLong, long endTimeLong) {
        String dateDifference = "";

        long diff = endTimeLong - startTimeLong;

        long diffDays = TimeUnit.MILLISECONDS.toDays(diff);
        if (diffDays > 0) {
            if(diffDays==1)
                dateDifference += diffDays + " day, ";
            else
                dateDifference += diffDays + " days, ";
        }
        long diffHours = TimeUnit.MILLISECONDS.toHours(diff);
        if (diffHours > 0) {
            if(diffHours==1)
                dateDifference += diffHours + " hour, ";
            else
                dateDifference += diffHours + " hours ";
        }
        long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        if (diffMinutes > 0) {
            dateDifference += diffMinutes + " mins";
        }
        if (diffMinutes == 0) {
            long diffSeconds = TimeUnit.MILLISECONDS.toSeconds(diff);
            if (diffSeconds > 0) {
                dateDifference += diffSeconds + " secs";
            }
        }
        return dateDifference;
    }

    /**
     * Get actual time when destination will be reached.
     *
     * @param seconds
     * @return
     */
    public static String getReachingTime(int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, seconds);
        return new SimpleDateFormat(TIME_FORMAT).format(calendar.getTime()).toUpperCase();
    }
}
