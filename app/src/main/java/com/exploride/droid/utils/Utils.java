package com.exploride.droid.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Patterns;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.Coordinate;
import com.cloudant.exploridemobile.framework.DevOptions;
import com.cloudant.exploridemobile.framework.WeatherInfo;
import com.exploride.droid.MainApplication;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.location.CustomLocationListener;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.WeatherStore;
import com.exploride.droid.network.api.ApiRepo;
import com.exploride.droid.presenters.CallReceiver;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.settings.SettingsUtils;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;
import java.util.zip.Deflater;

public class Utils {

    private static String TAG = "Utils";

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static void toggleLocPhone(boolean val){
        CustomLocationListener customLocationListener = CustomLocationListener.getInstance();
        boolean isChecked = val;
        MainApplication.getInstance().shouldSendLocationFromPhone = isChecked;
        DevOptions devOptions = new DevOptions();
        DevOptions.LocationSource ls = devOptions.new LocationSource();
        ls.setIsLocationFromPhone(isChecked);
        devOptions.setLocationSource(ls);
        BTComm btComm = new BTComm();
        btComm.setDevOptions(devOptions);
        if(isChecked) {
            customLocationListener.startSendingLocation();
            Location location = MainPresenter.getInstance().getCurrentLocation();
            if (location != null && location.getLatitude() != 0 && location.getLongitude() != 0) {
                Coordinate coordinate = new Coordinate(location);
                btComm.setCoordinate(coordinate);
            }
        }
        else
            customLocationListener.stopSendingLocation();

        CallReceiver.addLastCallInfo(btComm);
        ConnectionHandler.getInstance().sendInfo(btComm);
        MainApplication.getInstance().shouldSendLocationFromPhone = isChecked;
        SettingsUtils.writePreferenceValue(MainApplication.getInstance().getApplicationContext(),Constants.SharedPrefKeys.KEY_DEVOPTIONS_LOC_PH,
                isChecked);
    }

    public static void getWeatherInfo(Context context, double latitude, double longitude){

        //tag for the network request
        String WEATHER_INFORMATION = "weather_info";

        //Response.Listener<String>, Response.ErrorListener

        //parses response for the network request and passes it to HUD to be rendered on the UI
        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Gson gson = new Gson();
                    WeatherStore weatherStore = gson.fromJson(response, WeatherStore.class);

                    if (weatherStore != null) {

                        WeatherInfo weatherInfo = new WeatherInfo();

                        //sets current temperature
                        if (weatherStore.main != null)
                            weatherInfo.setTemperature((int) weatherStore.main.temp);

                        if (weatherStore.weather != null && weatherStore.weather.size() > 0) {
                            weatherInfo.setHumidity(weatherStore.main.humidity);
                            weatherInfo.setPlaceName(weatherStore.name);
                            WeatherStore.Weather weather = weatherStore.weather.get(0);
                            if (weather != null) {
                                //sets weather information
                                weatherInfo.setWeatherDescription(weather.description);

                                //sets icon name for the current weather
                                weatherInfo.setIconName(weather.icon);
                            }
                        }

                        //sends weather information to HUD
                        BTComm btComm = new BTComm();
                        btComm.setWeatherInfo(weatherInfo);
                        ConnectionHandler.getInstance().sendInfo(btComm);
                    }
            }catch(Exception e){
                ExpLogger.logE(TAG, e.getMessage());
            }
            }
        };

        //sets error returned while making network request
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ExpLogger.logException(TAG, error);
            }
        };

        //call to fetch weather info to be passed to the HUD
        ApiRepo.fetchWeatherInfo(context, responseListener, errorListener, latitude, longitude,
                WEATHER_INFORMATION);
    }

    /**
     * Get contact name from number. Invoked when a call is received.
     * @param context
     * @param phoneNumber
     * @return
     */
    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    /**
     * Get contact photo from number. Invoked when a call is received.
     * @param context: {@link Context} instance
     * @param number: incoming call number
     * @return Base64-encode the profile picture for the incoming call and return a newly allocated
     * String with the result. In case of no picture found this returns null
     */
    public static String getContactPhoto(Context context, String number) {
        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        try {
            if (contactId != null) {
                InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));

                if (inputStream != null) {
                    Bitmap photo = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                    return Base64.encodeToString(convertBitmapToByteArray(photo), Base64.DEFAULT);
                }
            }
        } catch (IOException e) {
            ExpLogger.logException(TAG, e);
        }

        return null;
    }

    /**
     * converts the bitmap to byte array
     * @param bmp: {@link Bitmap} instance
     * @return byte[] array formed from given bitmap
     */
    private static byte[] convertBitmapToByteArray(Bitmap bmp) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream); //PNG format is loss less and will ignore the quality setting!
            byte[] byteArray = stream.toByteArray();
            return compress(byteArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This provides support for general purpose compression using the ZLIB compression library
     * @param data: byte array to be compressed
     * @return compressed byte array
     * @throws IOException
     */
    private static byte[] compress(byte[] data) throws IOException {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        deflater.finish();
        byte[] buffer = new byte[100];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        outputStream.close();
        return outputStream.toByteArray();
    }

    /**
     * Get contact number against contact name
     * @param context
     * @param contactName
     * @return
     */
    public static String getContactNumber(Context context, String contactName){

        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        int indexName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        cursor.moveToFirst();
        do {
            String name = cursor.getString(indexName);
            String number = cursor.getString(indexNumber);
            if (name.equalsIgnoreCase(contactName)) {
                return number.replace("-", "");
            }
        } while (cursor.moveToNext());

        return null;
    }

    /**
     * Check if Phone Number is valid
     * Escape Arabic and Western characters
     * @param text
     * @return
     */
    public static boolean isPhoneNumber(String text){
        Pattern pattern = Patterns.PHONE;
        text = text.replace("\u202D","");
        text = text.replace("\u202C","");
        return pattern.matcher(text).matches();
    }

    /**
     * Check if Email address is valid
     * @param text
     * @return
     */
    public static boolean isValidEmail(String text){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        text = text.replace("\u202D","");
        text = text.replace("\u202C","");
        return pattern.matcher(text).matches();
    }

    /**
     * Converts distance from meters to feet
     */
    public static double meterToFeet(double meter)
    {
        return 3.281 * meter;
    }

    /**
     * Converts distance from meters to miles
     */
    public static double meterToMiles(double meter)
    {
        return meter / Constants.Distance.MILES_CONVERSION_UNIT;
    }

    /**
     * Converts distance from meters to Kms
     */
    public static double meterToKms(double meter)
    {
        return meter / Constants.Distance.KMS_CONVERSION_UNIT;
    }


    /**
     * hides the soft keyboard shown
     * @param activity: {@link Activity} reference
     */
    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }catch (Exception e){
            ExpLogger.logException(TAG, e);
        }
    }
}
