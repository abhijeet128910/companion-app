package com.exploride.droid.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;

import com.cloudant.exploridemobile.framework.Coordinate;
import com.exploride.droid.MainApplication;
import com.exploride.droid.location.CustomLocationListener;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.services.MockGPSServiceFromGPX;
import com.exploride.droid.settings.SettingsUtils;

import java.util.ArrayList;

import static com.exploride.droid.services.MockGPSServiceFromGPX.SERVICE_UPDATE;
import static com.exploride.droid.views.fragments.DeveloperOptionsFragment.mMockRepeatCount;
import static com.exploride.droid.views.fragments.DeveloperOptionsFragment.mMockintervals;

/**
 * Created by peeyushupadhyay on 05/12/16.
 */

public class LocationMockerPhone {

    public static final String TAG = MainApplication.class.getSimpleName();
    LocationManager locationManager;
    private boolean allowMockPhoneLocations = false;
    private int phoneMockIntervalPos = 0;
    public static LocationMockerPhone mLocationMockerPhone = null;
    DataUpdateReceiver dataUpdateReceiver;
    public int mMockMode;//0 default, 1 gpx, 2 last trace

    private LocationMockerPhone(){

        if (dataUpdateReceiver == null)
            dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilter = new IntentFilter(MockGPSServiceFromGPX.SERVICE_UPDATE);
        MainApplication.getInstance().registerReceiver(dataUpdateReceiver, intentFilter);
        int phoneMockInterval = SettingsUtils.readPreferenceValueAsInt(MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_MOCK_INTERVAL_POS);
        if (phoneMockInterval != -1)
            setPhoneMockIntervalPos(phoneMockInterval);
        allowMockPhoneLocations = SettingsUtils.readPreferenceValueAsBoolean(MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_ALLOW_PHONE_MOCK);

        if (SettingsUtils.readPreferenceValueAsInt(MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_MOCK_MODE) != -1)
            setMockMode(SettingsUtils.readPreferenceValueAsInt(MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_MOCK_MODE));
        else {
            setMockMode(0);
        }
        int phoneMockRepeatPos = SettingsUtils.readPreferenceValueAsInt(MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_MOCK_REPEAT_POS);
        if (phoneMockRepeatPos != -1)
            setPhoneMockRepeatPos(phoneMockRepeatPos);
    }

    public static LocationMockerPhone getInstance(){
        if(mLocationMockerPhone==null)
            mLocationMockerPhone = new LocationMockerPhone();
        return mLocationMockerPhone;
    }

    public void setMockMode(int val){
        mMockMode = val;
    }

    public int getMockMode(){
        return mMockMode;
    }
    public void startPhoneMocking() {
        try {
            CustomLocationListener.getInstance().stopListeningForLocation();
            Intent serviceIntent = new Intent(MainApplication.getInstance(), MockGPSServiceFromGPX.class);
            serviceIntent.putExtra("StartNav", true);
            MainApplication.getInstance().startService(serviceIntent);
            ExpLogger.logI(TAG, "started phone mocking");
        }catch(Exception e){
            ExpLogger.logException(TAG, e);
        }
    }

    public void handlePhoneMockingStatusChange(boolean allowMock){

        if(!allowMock)
            LocationMockerPhone.getInstance().stopPhoneMocking();
        else if(MapsPresenter.getInstance().isNavigationStarted())
            LocationMockerPhone.getInstance().startPhoneMocking();
    }
    public void stopPhoneMocking(){
        try {
            CustomLocationListener.getInstance().startListeningForLocation();
            if (locationManager != null) {
                locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, false);
                locationManager.removeTestProvider(LocationManager.GPS_PROVIDER);
                locationManager = null;
            }
            Intent serviceIntent = new Intent(MainApplication.getInstance(), MockGPSServiceFromGPX.class);
            MainApplication.getInstance().stopService(serviceIntent);
            if (mLocationIteratorThread != null)
                mLocationIteratorThread.interrupt();
            ExpLogger.logI(TAG, "stopped phone mocking");
        }catch(Exception e){
            ExpLogger.logException(TAG, e);
        }
    }

    public void setMockLocation(double latitude, double longitude) {
            try {
                //        if (locationManager == null){
                //            locationManager = (LocationManager) MainApplication.getInstance().getSystemService(Context.LOCATION_SERVICE);
                //            locationManager.addTestProvider(LocationManager.GPS_PROVIDER, false, false,
                //                    false, false, true, true, true, 0, 5);
                //            locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);
                //        }
                ExpLogger.logI(TAG, "gpsLoc:mock locations: lat, long = " + latitude + ", " + longitude);
                Location mockLocation = new Location(LocationManager.GPS_PROVIDER); // a string
                mockLocation.setLatitude(latitude);  // double
                mockLocation.setLongitude(longitude);
                mockLocation.setAltitude(0D);
                mockLocation.setTime(System.currentTimeMillis());
                mockLocation.setBearing(0F);
                mockLocation.setAccuracy(0);
                mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());

                //        try {
                //            ExpLogger.logI(TAG,"gpsMock:Setting Mock Location");
                //            locationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER, mockLocation);
                //        } catch (SecurityException e) {
                //            e.printStackTrace();
                //        }
                if (!MapsPresenter.getInstance().isNavigationStarted()) {
                    if (mLocationIteratorThread != null)
                        mLocationIteratorThread.interrupt();
                    return;
                }
                CustomLocationListener.getInstance().onLocationChanged(mockLocation);
            }catch(Exception e){
                ExpLogger.logI(TAG, "gpsLoc:Exception "+ e.getMessage());
            }
    }

    public int getPhoneMockIntervalPos() {
        return phoneMockIntervalPos;
    }

    public int getPhoneMockIntervalValue() {
        return (int) (Float.parseFloat(mMockintervals[phoneMockIntervalPos]) * 1000);
    }

    public void setPhoneMockIntervalPos(int phoneMockInterval) {
        this.phoneMockIntervalPos = phoneMockInterval;
    }

    public int getPhoneMockRepeatPos() {
        return phoneMockRepeatPos;
    }
    public int getPhoneMockRepeatValue() {
        return (int) (Float.parseFloat(mMockRepeatCount[phoneMockRepeatPos]));
    }

    int phoneMockRepeatPos;
    public void setPhoneMockRepeatPos(int phoneMockRepeatPos) {
        this.phoneMockRepeatPos = phoneMockRepeatPos;
    }

    public boolean shouldMockPhoneLocations() {
        return allowMockPhoneLocations;
    }

    public void setMockPhoneLocations(boolean allowMockPhoneLocations) {
        this.allowMockPhoneLocations = allowMockPhoneLocations;
    }
    Thread mLocationIteratorThread;
    private class DataUpdateReceiver extends BroadcastReceiver {

        ArrayList<Coordinate> coordinates = new ArrayList<>();
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean hasMoreData = false;
            if (intent.getAction().equals(SERVICE_UPDATE)) {
//                ArrayList<Coordinate>  temp = intent.getParcelableArrayListExtra(Coords);
//                hasMoreData = intent.getExtras().getBoolean(SERVICE_UPDATE_HAS_MORE_DATA, false);
//                coordinates.addAll(temp);
//                if(hasMoreData)
//                    return;
                coordinates = MockGPSServiceFromGPX.getCoordinateList();
            }
            if(coordinates == null){
                ExpLogger.logI(TAG,"gpsLoc:mock DataUpdateReceiver coordinates/locs null, returning");
                return;
            }
            final ArrayList<Coordinate> finalCoordinates = coordinates;
            mLocationIteratorThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        int repeatCount = getPhoneMockRepeatValue();
                        double latitude, longitude;
                        Coordinate coordinate;
                        while(repeatCount>0) {
                            ExpLogger.logI(TAG,"gpsLoc:iterations left = "+ repeatCount);
                            MainApplication.getInstance().showToast("iterations left = "+ repeatCount);
                            for (int i = 0; i < finalCoordinates.size(); i++) {
                                if (Thread.interrupted())
                                    break;
                                coordinate = finalCoordinates.get(i);
                                latitude = coordinate.mLat;
                                longitude = coordinate.mLon;
                                ExpLogger.logI(TAG, "gpsLoc:mock setting mock location as:" + latitude + "," + longitude);
                                setMockLocation(latitude, longitude);
                                Thread.currentThread().sleep(getPhoneMockIntervalValue());
                            }
                            repeatCount --;
                        }
                        coordinates.clear();
                        ExpLogger.logI(TAG,"gpsLoc:mock EXITING mock locations______");
                    } catch (Exception e) {
                        ExpLogger.logE(TAG, e.getMessage());
                    }

                }
            });
            mLocationIteratorThread.start();
        }
    }


}
