package com.exploride.droid.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class FontHelper {

    public static String HELVETICA_BOLD_TEXTVIEW = "helvetica_neue_bold.ttf";

    public static String HELVETICA_CONDENSED_BOLD_TEXTVIEW = "helvetica_neue_condensed_bold.otf";

    public static String MULI_LIGHT_TEXTVIEW = "muli_light.ttf";

    public static String MULI_REGULAR_TEXTVIEW = "muli_regular.ttf";

    public static String DROID = "droid.otf";

    public static String EXO_EXTRA_BOLD = "Exo-ExtraBold.otf";

    public static String EXO_DEMI_BOLD = "Exo-DemiBold.otf";

    public static String QUICKSAND_BOLD = "quicksand-bold.ttf";

    public static String ROBOTO_SLAB_BOLD = "roboto-slab-bold.ttf";

    public static String ROBOTO_MEDIUM = "Roboto-Medium.ttf";

    public static String ROBOTO_BOLD = "Roboto-Bold.ttf";

    public static String ROBOTO_REGULAR = "Roboto-Regular.ttf";

    public static void setFonts(Context context, TextView textView, String fontName) {
        Typeface type = Typeface.createFromAsset(context.getAssets(), fontName);
        textView.setTypeface(type);
    }
}
