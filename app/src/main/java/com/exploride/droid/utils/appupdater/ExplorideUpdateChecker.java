package com.exploride.droid.utils.appupdater;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.presenters.MainPresenter;


/**
 * Created by peeyushupadhyay on 21/12/16.
 */

public class ExplorideUpdateChecker {

    private static final String TAG = "ExplorideUpdateChecker";
    public static final String mUpdateUrl = "https://www.dropbox.com/s/5k0lpantjaksz4p/config.txt?raw=1";

    static ExplorideUpdateChecker mExplorideUpdateChecker;

    public static ExplorideUpdateChecker getInstance(){
        if(mExplorideUpdateChecker==null){
            mExplorideUpdateChecker = new ExplorideUpdateChecker();
        }
        return mExplorideUpdateChecker;
    }
    /**
     * method to handle the response from HUD and take appropriate action
     * @param requestStatusCode
     */
    public void handleStatusCodes(int requestStatusCode){

        BTComm btComm = new BTComm();
        switch(requestStatusCode){

            case ActionConstants.ActionCode.RequestUpdatesCheck:

                btComm.setActionCode(ActionConstants.ActionCode.RequestUpdatesCheck);
                btComm.setData(mUpdateUrl);
                ConnectionHandler.getInstance().sendInfo(btComm);

                break;
            case ActionConstants.ActionCode.UpdatesAvailable:
                //show to the user appropriate prompt
                if(mIsUserInitiated)
                    showUpdatesAvailability(MainApplication.getInstance().getString(R.string.updates_aval_title),
                            MainApplication.getInstance().getString(R.string.updates_available),ActionConstants.ActionCode.UpdatesDownloadNow, true);
                break;

            case ActionConstants.ActionCode.UpdatesNotAvailable:
                //show to the user appropriate prompt if initiated by user

                if(mIsUserInitiated)
                    showUpdatesAvailability(MainApplication.getInstance().getString(R.string.updates_not_aval_title),
                            MainApplication.getInstance().getString(R.string.updates_not_available),-1, false);
                break;

            case ActionConstants.ActionCode.UpdatesDownloadFailed:
                //show to the user appropriate prompt, if initiated by user

                if(mIsUserInitiated)
                    showUpdatesAvailability(MainApplication.getInstance().getString(R.string.download_failed_title),
                            MainApplication.getInstance().getString(R.string.updates_download_failed),ActionConstants.ActionCode.RequestUpdatesCheck, true);
                break;

            case ActionConstants.ActionCode.UpdatesDownloadNow:

                btComm.setActionCode(ActionConstants.ActionCode.UpdatesDownloadNow);
                ConnectionHandler.getInstance().sendInfo(btComm);

                break;

            case ActionConstants.ActionCode.UpdatesDownloadFinished:

                if(mIsUserInitiated)
                    showUpdatesAvailability(MainApplication.getInstance().getString(R.string.updates_download_finished_title),
                            MainApplication.getInstance().getString(R.string.updates_download_finished),-1, false);

                break;

        }
    }
    boolean mIsUserInitiated;
    public void checkForUpdatesAvailable(boolean isUserInitiated){

        mIsUserInitiated = isUserInitiated;
        handleStatusCodes(ActionConstants.ActionCode.RequestUpdatesCheck);
    }


    public void showUpdatesAvailability(final String title, final String msg, final int statusCodeForCallback, final boolean showCancel){

        Runnable r = new Runnable() {
            @Override
            public void run() {

                MainPresenter.getInstance().showDialog(title, msg, new MainPresenter.DialogCallback(){

                    @Override
                    public void onUserOK() {
                        handleStatusCodes(statusCodeForCallback);
                    }

                    @Override
                    public void onUserCancel() {

                    }
                }, showCancel);
            }
        };
        MainApplication.getInstance().getCurrentActivity().runOnUiThread(r);
    }



}
