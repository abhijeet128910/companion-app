package com.exploride.droid.utils;

import java.text.DecimalFormat;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import com.exploride.droid.R;

/**
 * Author Mayank
 */

public class CommonUtils {

    /**
     * converts the number passed into Gb, Mb, or Kb
     * @param size: int size in kbs
     * @return formatted String which is either in Gb, Mb or Kb
     */
    public static String getFormattedMapsSize(long size) {

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        double sizeInMb = size / 1024.0;
        double sizeInGb = size / 1048576.0;
        String mapsSize = null;
        if (sizeInGb > 1) {
            mapsSize = decimalFormat.format(sizeInGb).concat(" GB");
        } else if (sizeInMb > 1) {
            mapsSize = decimalFormat.format(sizeInMb).concat(" MB");
        } else {
            mapsSize = decimalFormat.format(size).concat(" KB");
        }

        return mapsSize;
    }

    /**
     * shows custom prompt to the user
     * @param activity:        {@link Activity} reference
     * @param title:           title for the alert
     * @param message:         message for the alert
     * @param dismissListener: custom {@link android.content.DialogInterface.OnDismissListener} implementation provided by each caller of this method
     */
    public static void showAlert(Activity activity, String title, String message, DialogInterface.OnDismissListener dismissListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        if(dismissListener!=null)
            builder.setOnDismissListener(dismissListener);
        builder.show();
    }
    public interface CustomDialogResult{
        //1 for positive button, -1 for negative
        void onResult(int result);
    }
    public static void showAlert(Activity activity, String title, String message, final CustomDialogResult dialogResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if(dialogResult!=null)
                    dialogResult.onResult(1);
            }
        });
        builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if(dialogResult!=null)
                    dialogResult.onResult(-1);
            }
        });
        builder.show();
    }
}
