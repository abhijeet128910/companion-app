package com.exploride.droid;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import com.cloudant.exploridemobile.framework.AppVersion;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.DevOptions;
import com.cloudant.exploridemobile.framework.DownloadMaps;
import com.cloudant.exploridemobile.framework.Navigation;
import com.cloudant.exploridemobile.framework.RequestDataOffline;
import com.cloudant.exploridemobile.framework.ScreenScaling;
import com.cloudant.exploridemobile.framework.TimeZone;
import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;
import com.google.android.gms.common.api.GoogleApiClient;

public interface MainContract {

    /**
     * methods for Presenter. Available to View
     */

    public interface MainOps extends BasePresenter<MainView> {

        void setCurrentLocation(Location currentLocation);

        void setBluetoothState(int bluetoothState);

        void setHudBrightness(int value);

        int getHudBrightness();

        void setHudAutomaticBrightnessOn(boolean automaticBrightnessOn);

        boolean getHudAutomaticBrightnessOn();

        void setDownloadMapsInformation(DownloadMaps downloadMaps);

        void showLoading(String text);

        void hideLoading();

        void sendSyncPrefsToHUD();

        void notifyRouteCalculationError();

        void setHudAppVersion(AppVersion hudAppVersion);

        void setDevOptions(BTComm btComm);

        void provideOfflineWeatherInfo();

        void provideOfflineTimeInfo();

        void handleOfflineDataRequest(RequestDataOffline requestDataOffline);

        GoogleApiClient getGoogleApiClient();

        void getDeviceTimezone(BTComm btComm);

        void syncHudNavigationState(Navigation.Status status);

        void checkNavigationStateOnHUD();

        boolean isShowRouteOnly();
    }

    /**
     * methods for View. Available to Presenter
     */

    public interface MainView extends BaseView {

        void loadFragment(String fragName, Bundle args);
        void showToast2(String message);
        void showBluetoothState(boolean state);
        void updateNavigationViews();
        void updateFragments();
        void showLoading(String text);
        void hideLoading();
        void showErrorAlert(String message);
        Fragment getActiveFragment();
    }

    /**
     * methods for Model. Available to Presenter
     */
    public interface MainStore {

        void setHudBrightness(int value);

        int getHudBrightness();

        void setHudAutomaticBrightnessOn(boolean hudAutomaticBrightnessOn);

        boolean getHudAutomaticBrightnessOn();

    }
    /**
     * methods for Presenter. Available to Model
     */
    public interface MainPresenterOps {

    }
}
