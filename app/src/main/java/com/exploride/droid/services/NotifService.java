package com.exploride.droid.services;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.CallLog;
import android.provider.Telephony;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.ActivityCompat;

import com.cloudant.exploridemobile.framework.NotificationData;
import com.exploride.droid.MainApplication;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.NotificationHelper;
import com.exploride.droid.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static android.app.Notification.CATEGORY_EMAIL;
import static android.app.Notification.CATEGORY_MESSAGE;

public class NotifService extends NotificationListenerService {

    private String TAG = this.getClass().getSimpleName();
    private NotifServiceReceiver notifServiceReceiver;
    public static String NOTIF_SVC = "notifsvc";
    public static String EXTRA_COMMAND = "command";
    public static String EXTRA_LIST = "list";
    public static String EXTRA_CLEAR_ALL = "clear_all";
    public static String EXTRA_REMOVE = "remove";
    public Context ctx;

    Handler mHandler;

    public static final List<String> mMessagePkgs = new ArrayList<String>() {{
        add("com.google.android.apps.messaging");//message def
        add("com.android.mms"); // message def added for xiaomi phones
        add("com.android.messaging"); // lenovo
        add("com.sonyericsson.conversations"); // Sony
    }};

    public static final String EMAIL_PACKAGE = "com.google.android.gm";
    public static final String CALL_PACKAGE = "com.android.server.telecom";

    private static final String MISSED_CALL_SEARCH_STRING = " missed calls";
    private static final String MESSAGES_SEARCH_STRING = " new messages";

    @Override
    public void onCreate() {
        super.onCreate();
        notifServiceReceiver = new NotifServiceReceiver();
        final IntentFilter filter = new IntentFilter();
        filter.addAction(NOTIF_SVC);
        ctx = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                Looper looper = Looper.myLooper();
                mHandler = new Handler();
                ctx.registerReceiver(notifServiceReceiver,filter, null, mHandler);

                Intent intentNotif = new  Intent(NotificationHelper.Action_Toast);
                sendOrderedBroadcast(intentNotif, null, NotificationHelper.getInstance(), mHandler,
                        Activity.RESULT_OK, null, null);
                Looper.loop();
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(notifServiceReceiver);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
//        try {
//            MainApplication.getInstance().assertBGThread();
//        }catch (Exception e){
//            String a = "";
//        }
        ExpLogger.logI(TAG,"**********  onNotificationPosted");
        ExpLogger.logI(TAG,"ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText + "\t" + sbn.getPackageName());

        if (MainApplication.getInstance().isCommunicationChannelOpen())
            handleNotification(2, sbn);
        else
            handleNotification(1, sbn);
    }
    private void handleNotification(int notificationType, StatusBarNotification sbn)
    {
        if (!isPackageSupported(sbn))
            return;
        NotificationData notificationData = constructNotificationDataFromSbn(sbn);

        // If name and number both are null then do not add in the notification list
        if(notificationData == null)
            return;

        if (!isPackageSupported(sbn))
            return;

        ArrayList<NotificationData> notificationDatas = new ArrayList<>();
        if (mMessagePkgs.contains(sbn.getPackageName())) {
            notificationDatas.addAll(getMessageNotifications(sbn, true));
        } else if (CALL_PACKAGE.contains(sbn.getPackageName()))
            notificationDatas.addAll(getCallNotifications(sbn));
        else if (EMAIL_PACKAGE.contains(sbn.getPackageName()))
            notificationDatas.addAll(readEmailNotifications(sbn));

        Intent intentNotif = new  Intent(NotificationHelper.Action_Notifications);
        intentNotif.putParcelableArrayListExtra(NotificationHelper.Extra_NotificationData, notificationDatas);
        intentNotif.putExtra(NotificationHelper.Extra_Notificationtype, notificationType);
        sendBroadcast(intentNotif);
//        sendOrderedBroadcast(intentNotif, null, NotificationHelper.getInstance(), mHandler,
//                Activity.RESULT_OK, null, null);
    }
    private NotificationData constructNotificationDataFromSbn(StatusBarNotification sbn){

        Notification notification = sbn.getNotification();
        Bundle extras = notification.extras;
        NotificationData notificationData = new NotificationData(sbn.getId(), sbn.getKey(),
                "" + notification.tickerText, notification.category, sbn.getPackageName(), extras.getString("android.title"),
                extras.getString("android.subText"), "" + extras.get("android.text"),
                extras.get("android.bigText")==null?"":extras.get("android.bigText").toString(),
                extras.getString("android.template"), extras.getStringArray("android.textLines"), notification.when);

        return notificationData;
    }

    /**
     *
     * @param sender Can be name or number
     * @param notificationData
     */
    private void setContactDetails(String sender, NotificationData notificationData) {
        try {
            if (sender != null) {
                // If sender is Contact number i.e. contact is not saved so set number only
                if (Utils.isPhoneNumber(sender)) {
                    notificationData.setName(Utils.getContactName(ctx, sender));
                    notificationData.setContactNumber(sender);
                }
                // If sender is a name then get Number of that contact and set to contact number.
                else {
                    String number = Utils.getContactNumber(ctx, sender);
                    notificationData.setName(sender);
                    notificationData.setContactNumber(number);
                }
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * Extracting name and email address for email from NotificationData. "title" is sender of the email.
     * Check sender is email address of just the name.
     * @param notificationData
     */
    private void setDataForEmail(NotificationData notificationData){
        try {
            String sender = notificationData.getTitle();
            if (sender != null) {

                if (Utils.isValidEmail(sender)) {
                    notificationData.setContactEmail(sender);
                }
                notificationData.setName(sender);
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * Returns true if empty
     *
     * @param data
     * @return
     */
    private boolean isEmpty(String data){
        if(data == null || data.trim().length() == 0)
            return true;
        return false;

    }

    public void onListenerConnected() {

        fetchAllNotifications();
    }

    public void onListenerDisconnected() {

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
//        try {
//            MainApplication.getInstance().assertBGThread();
//        }catch (Exception e){
//            String a = "";
//        }
        ExpLogger.logI(TAG,"********** onNOtificationRemoved");
        ExpLogger.logI(TAG,"ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText +"\t" + sbn.getPackageName());

        handleNotification(3, sbn);
    }


    class NotifServiceReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                MainApplication.getInstance().assertBGThread();
                if(intent.getStringExtra(EXTRA_COMMAND).equals(EXTRA_CLEAR_ALL)){
                    NotifService.this.cancelAllNotifications();
                }
                else if(intent.getStringExtra(EXTRA_COMMAND).equals(EXTRA_REMOVE)){
                    String keyId = intent.getStringExtra("keyId");
                    if(keyId!=null){
                        NotifService.this.cancelNotification(keyId);
                    }
                }
                else if(intent.getStringExtra(EXTRA_COMMAND).equals(EXTRA_LIST)){

                    fetchAllNotifications();
                }
            }catch (Exception e){
                ExpLogger.logException(TAG, e);
            }

        }
    }
    void fetchAllNotifications(){
        try {
            ArrayList<NotificationData> notifications = new ArrayList<>();
            StatusBarNotification[] sbs = getActiveNotifications();

            if(sbs == null || sbs.length == 0)
                return;

            for (StatusBarNotification sbn : sbs) {

                if (!isPackageSupported(sbn))
                    continue;

                if (mMessagePkgs.contains(sbn.getPackageName()))
                    notifications.addAll(getMessageNotifications(sbn, false));
                else if (CALL_PACKAGE.contains(sbn.getPackageName()))
                    notifications.addAll(getCallNotifications(sbn));
                else if (EMAIL_PACKAGE.contains(sbn.getPackageName()))
                    notifications.addAll(readEmailNotifications(sbn));
            }

            Intent intentNotif = new Intent(NotificationHelper.Action_Notifications);
            intentNotif.putParcelableArrayListExtra(NotificationHelper.Extra_NotificationData, notifications);
            intentNotif.putExtra(NotificationHelper.Extra_Notificationtype, 1);
//            sendBroadcast(intentNotif);
            sendOrderedBroadcast(intentNotif, null, NotificationHelper.getInstance(), mHandler,
                    Activity.RESULT_OK, null, null);
        }catch (Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    private boolean isPackageSupported(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (mMessagePkgs.contains(packageName) || EMAIL_PACKAGE.equalsIgnoreCase(packageName) ||
                CALL_PACKAGE.equalsIgnoreCase(packageName))
            return true;

        return false;
    }

    /**
     * Get single or grouped message notifications.
     *
     * @param statusBarNotification
     */
    private List<NotificationData> getCallNotifications(StatusBarNotification statusBarNotification){
        List<NotificationData> notificationDatas = new ArrayList<>();
        try {
            int count = getGroupNotifCount("" + statusBarNotification.getNotification().extras
                    .get(Notification.EXTRA_TEXT), MISSED_CALL_SEARCH_STRING);

            if (count > 0) {
                notificationDatas.addAll(getUnreadMissedCalls(count));
            } else
                notificationDatas.addAll(getUnreadMissedCalls(1));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notificationDatas;
    }

    public List<NotificationData> getUnreadMissedCalls(int count) {
        List<NotificationData> notificationDatas = new ArrayList<>();
        Cursor cursor = null;
        try {
            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.READ_CALL_LOG) !=
                    PackageManager.PERMISSION_GRANTED) {
                return null;
            }

            cursor = ctx.getContentResolver().query(Uri.parse("content://call_log/calls"),
                    null, "type = 3 AND is_read = 0", null, "date DESC");
            cursor.moveToFirst();

            if (cursor.getCount() == 0)
                return notificationDatas;

            do {
                NotificationData notificationData = new NotificationData();

                String id = cursor.getString(cursor.getColumnIndex(CallLog.Calls._ID));
                String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                String date = cursor.getString(cursor.getColumnIndex(CallLog.Calls.DATE));

                notificationData.setId(Integer.parseInt(id));
                setContactDetails(number, notificationData);
                notificationData.setPkg(CALL_PACKAGE);
                notificationData.setTime(Long.parseLong(date));
                notificationDatas.add(notificationData);

                if (notificationDatas.size() == count)
                    break;
            }while (cursor.moveToNext());
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
        finally {
            cursor.close();
        }
        return notificationDatas;
    }

    /**
     * Get single or grouped message notifications.
     *
     * @param statusBarNotification
     */
    private List<NotificationData> getMessageNotifications(StatusBarNotification statusBarNotification,
                                                           boolean readSingle) {
        List<NotificationData> notificationDatas = new ArrayList<>();
        try {
            if (isGroupedNotification(statusBarNotification)) {
                CharSequence[] charSequences = statusBarNotification.getNotification().extras.getCharSequenceArray(
                        Notification.EXTRA_TEXT_LINES);
                int count = readSingle ? 1 : charSequences.length;

                // When there are multiple messages from one sender then char sequences will have single
                // entry of that, so verify with extractedCountValue
                int extractedCountValue = getGroupNotifCount("" + statusBarNotification.getNotification().extras
                        .get(Notification.EXTRA_TITLE), MESSAGES_SEARCH_STRING);
                if(extractedCountValue > 0)
                    count = extractedCountValue;

                notificationDatas.addAll(getUnreadMessages(count));
            } else{
                notificationDatas.addAll(getUnreadMessages(1));
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
        return notificationDatas;
    }

    /**
     * Get latest received message too by COLLATE LOCALIZED ASC
     * @param count
     * @return
     */
    public List<NotificationData> getUnreadMessages(int count) {
        List<NotificationData> notificationDatas = new ArrayList<>();
        Uri mSmsinboxQueryUri = Uri.parse("content://sms/inbox");
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(mSmsinboxQueryUri, null, null, null, "date DESC");
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    String type = cursor.getString(cursor.getColumnIndex(Telephony.Sms.TYPE));
                    if (type.equals("1")) // 2 for Sent Sms
                    {
                        String id = cursor.getString(cursor.getColumnIndex(Telephony.Sms._ID));
                        String sender = cursor.getString(cursor.getColumnIndex(Telephony.Sms.ADDRESS));
                        String date = cursor.getString(cursor.getColumnIndex(Telephony.Sms.DATE));
                        String sentDate = cursor.getString(cursor.getColumnIndex(Telephony.Sms.DATE_SENT));

                        String message = cursor.getString(cursor.getColumnIndex(Telephony.Sms.BODY));

                        NotificationData notificationData = new NotificationData();
                        notificationData.setId(Integer.parseInt(id));
                        setContactDetails(sender, notificationData);
                        notificationData.setText(message);
                        notificationData.setTime(Long.parseLong(date));
                        notificationData.setPkg(mMessagePkgs.get(0));
                        notificationDatas.add(notificationData);

                        if (notificationDatas.size() == count)
                            break;
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
        finally {
            cursor.close();
        }
        return notificationDatas;
    }

    /**
     * Get single or grouped email notifications
     *
     * @param statusBarNotification
     */
    private List<NotificationData> readEmailNotifications(StatusBarNotification statusBarNotification) {
        List<NotificationData> notificationDatas = new ArrayList<>();

        NotificationData notificationData = constructNotificationDataFromSbn(statusBarNotification);

        if (isGroupedNotification(statusBarNotification))
            return notificationDatas;

        notificationData.setText("" + statusBarNotification.getNotification().extras.get(Notification.EXTRA_TEXT));
        setDataForEmail(notificationData);
        notificationDatas.add(notificationData);

        return notificationDatas;
    }

    /**
     * Check if title contains string " new messages" or " missed calls"
     * This is called if string array of notification is null
     *
     * @param notificationText
     * @param searchString
     * @return
     */
    private int getGroupNotifCount(String notificationText, String searchString) {
        if (notificationText.contains(searchString)) {
            try {
                int count = Integer.parseInt(notificationText.substring(0, notificationText.indexOf(searchString)));
                return count;
            } catch (NumberFormatException e) {
                ExpLogger.logException(TAG, e);
            }
        }
        return 0;
    }

    private boolean isGroupedNotification(StatusBarNotification statusBarNotification) {

        CharSequence[] charSequences = statusBarNotification.getNotification().extras.getCharSequenceArray(
                Notification.EXTRA_TEXT_LINES);

        if (charSequences == null || charSequences.length == 0)
            return false;

        return true;
    }
}