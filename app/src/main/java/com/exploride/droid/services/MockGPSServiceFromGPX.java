package com.exploride.droid.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.Toast;

import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.Coordinate;
import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.MainApplication;
import com.exploride.droid.location.LocationTracer;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.LocationMockerPhone;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.here.android.mpa.common.GeoCoordinate;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import static com.exploride.droid.logger.ExpLogger.*;

public class MockGPSServiceFromGPX extends Service {

    private double latitude;
    private double longitude;
    private LocationManager mLocationManager;
    static String TAG = "MockGPSServiceFromGPX";
    public static String SERVICE_UPDATE = "service_update";
    public static String SERVICE_UPDATE_HAS_MORE_DATA = "service_update_has_more_data";
    public static String Coords = "coords";
    static ArrayList<Coordinate> mCoordinates = new ArrayList<>();
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, final int flags, int startId) {
        System.out.println("gpsLoc:Mock onStartCommand");
        super.onStartCommand(intent, flags, startId);
        boolean val = false;
        if(intent!=null)
            val = intent.getBooleanExtra("StartNav", false);
        if(val) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        synchronized (waitObj) {


                            if(LocationMockerPhone.getInstance().getMockMode()==0){
                                logI(TAG,"gpsLoc:Mock getting route coordinates");
                                mCoordinates.clear();// shud be empty though
                                List<GeoCoordinate> GeoCoordList =
                                        MapsPresenter.getInstance().getMapRoute().getRoute().getRouteGeometry();
                                if(GeoCoordList!=null)
                                    for (GeoCoordinate coord:GeoCoordList) {

                                        mCoordinates.add(new Coordinate(coord.getLatitude(), coord.getLongitude()));
                                    }
                                logI(TAG,"gpsLoc:Mock added coordinates size = " +mCoordinates.size());
                            }else if(LocationMockerPhone.getInstance().getMockMode()==1){
                                mCoordinates.clear();
                                new GPXParser().init();
                                logI(TAG,"gpsLoc:Mock parsing done");
                            }else if(LocationMockerPhone.getInstance().getMockMode()==2){
                                String locTrace = LocationTracer.getInstance().readLocationTraces();
                                mCoordinates.clear();
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                Gson gson = gsonBuilder.create();
                                Type collectionType = (new TypeToken<ArrayList<Coordinate>>(){}.getType());
                                mCoordinates = gson.fromJson(locTrace, collectionType);
//                                mCoordinates.sort(new Comparator<Coordinate>() {
//                                    @Override
//                                    public int compare(Coordinate o1, Coordinate o2) {
//                                        if(o1.mLoc.getTime()==o2.mLoc.getTime())
//                                            return 0;
//                                        return o1.mLoc.getTime()<o2.mLoc.getTime()?-1:1;
//                                    }
//                                });
                                Collections.sort(mCoordinates);
                                if(mCoordinates.size()>0 && mCoordinates.get(0).mLoc==null){

                                    mCoordinates.clear();
                                    ArrayList<Location> locations = new ArrayList<>();
                                    collectionType = (new TypeToken<ArrayList<Location>>(){}.getType());
                                    locations = gson.fromJson(locTrace, collectionType);

                                    Collections.sort(locations, new Comparator<Location>() {
                                        @Override
                                        public int compare(Location o1, Location o2) {
                                            if(o1.getTime()==o2.getTime())
                                                return 0;
                                            return o1.getTime()<o2.getTime()?-1:1;
                                        }
                                    });
                                    for(Location loc: locations){
                                        mCoordinates.add(new Coordinate(loc));
                                    }
                                }
                                logI(TAG,"gpsLoc:getting location traces");
                            }
                            Intent intent = new Intent(SERVICE_UPDATE);
//                            intent.putParcelableArrayListExtra(Coords, mCoordinates);
                            sendBroadcast(intent);

                        }
                    } catch (Exception e) {
                        logE(TAG, e.getMessage());
                    }

                }
            }).start();
        }
        return START_NOT_STICKY;
    }
    static Object waitObj = new Object();

    public static ArrayList<Coordinate> getCoordinateList() {
        return mCoordinates;
    }
    @Override
    public void onCreate() {
        logI(TAG,"gpsLoc:Mock onCreate");
        super.onCreate();

        logI(TAG,"gpsLoc:Mock starting gpxService ");
//
//        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        mLocationManager.addTestProvider(LocationManager.GPS_PROVIDER, false, false,
//                false, false, true, true, true, 0, 5);
//        mLocationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);


//        if(!LocationMockerPhone.getInstance().mUseGPX){
//            //do nothing here
//
//        }else
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                synchronized (waitObj) {
//                    logI(TAG,"gpsLoc:Mock starting gpxService ");
//
//                    new GPXParser().init();
//                    logI(TAG,"gpsLoc:Mock parsing done");
//                }
//            }
//        }).start();

    }

    private void showToast(final String message) {

        Handler h = new Handler(getMainLooper());

        h.post(new Runnable() {
            @Override
            public void run() {
                if(MainApplication.getInstance().isDebugModeOn())
                    Toast.makeText(MockGPSServiceFromGPX.this, "latitude=" + latitude + " ; longitude=" + longitude, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        logI(TAG,"onDestroy called");

    }

    class GPXParser{
        public void init(){

            try {
                mCoordinates.clear();
                File inputFile = new File("/sdcard/gpx/mapstogpx.gpx");
                if(!inputFile.exists())
                    logI(TAG,"gpsLoc:Mock *********file does not exists, returning**********");
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser saxParser = factory.newSAXParser();
                UserHandler userhandler = new UserHandler();
                saxParser.parse(inputFile, userhandler);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    class UserHandler extends DefaultHandler {

        boolean trkpt = false;
        String Coord = "trkpt";
        String Latitude = "lat", Longitude = "lon";
        @Override
        public void startElement(String uri,
                                 String localName, String qName, Attributes attributes)
                throws SAXException {
            if (qName.equalsIgnoreCase(Coord)) {
                try {
                    String latitude = attributes.getValue(Latitude);
                    String longitude = attributes.getValue(Longitude);
                    Double lat = Double.parseDouble(latitude);
                    Double lon = Double.parseDouble(longitude);
                    logI(TAG,"gpsLoc:Mock Lat, Lon =" + lat + ", " + lon);
                    mCoordinates.add(new Coordinate(lat, lon));
                    logI(TAG,"------------");
                }catch(Exception e){
                    logI(TAG,e.getMessage());
                }
            }
        }
    }
}
