package com.exploride.droid.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.exploride.droid.MainApplication;
import com.exploride.droid.logger.ExpLogger;

/**
 * Created by peeyushupadhyay on 31/10/17.
 */

public class OnClearFromRecentService extends Service {

    private static String TAG = "ClearFromRecentService";
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ExpLogger.logD(TAG, TAG+ " Service Started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ExpLogger.logD(TAG, TAG+ " Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        ExpLogger.logD(TAG, TAG+ " END");
        broadcastStop();
        stopSelf();
    }

    String appStopAction;
    private void broadcastStop() {
        appStopAction = MainApplication.getInstance().getPackageName()+"-app-stop";
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(MainApplication.getInstance());
        Intent intent = new Intent(appStopAction, null);
        localBroadcastManager.sendBroadcast(intent);
    }
}