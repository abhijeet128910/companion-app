/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.exploride.droid.services;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.UUID;

import static android.bluetooth.BluetoothDevice.BOND_NONE;

/**
 * class to set up and manage Bluetooth connections with other devices.
 */
public class BluetoothClientService {
    // Debugging
    private static final String TAG = "BluetoothClientService";

    // Name for the SDP record when creating server socket
    private static final String NAME_SECURE = "BluetoothSecure";
    private static final String NAME_INSECURE = "BluetoothInsecure";
//    public static final String BTEXPNAME = "Nexuspy";
    public static final String BTEXPNAME = "Exploride";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private volatile ConnectThread mConnectThread;
    private volatile ConnectedThread mConnectedThread;
    private volatile int mState;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now BTConnected to a remote device

    /**
     * Constructor. Prepares a new Bluetooth session.
     *
     * @param handler A Handler to send messages back to the UI Activity
     */
    public BluetoothClientService(Handler handler, BluetoothAdapter bluetoothAdapter) {
        mAdapter = bluetoothAdapter;
        MainPresenter.getInstance().setPhoneInfo(mAdapter.getName());
        mState = STATE_NONE;
        mHandler = handler;
    }

    //stores a count of retries pending.
    Hashtable<String, Integer> mDeviceConnectedStateMap = new Hashtable<>();
    /**
     * Set the current state of the Bluetooth connection
     *
     * @param state An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        ExpLogger.logD(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }
    private void tryReConnect(){

        try{
            if(mState == STATE_LISTEN &&
                    mDeviceConnectedStateMap.size()>0 &&
                    mDeviceConnectedStateMap.get(mBTDevice.getAddress())>0){
                //lets retry
                mDeviceConnectedStateMap.put(mBTDevice.getAddress(), mDeviceConnectedStateMap.get(mBTDevice.getAddress())-1);
                ExpLogger.logI(TAG, "BT trying to reconnect");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2000);//sleep to allow re-init on HUD
                            connect(mBTDevice, true);
                        } catch (InterruptedException e) {
                            ExpLogger.logException(TAG, e);
                            e.printStackTrace();
                        }
                    }
                }).start();
                return;
            }
        }catch(Exception e){
            ExpLogger.logException(TAG, e);
        }
    }
    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }
    boolean mShouldAutoTryReconnect = true;
    /**
     * Start the service.
     */
    public synchronized void start() {
        ExpLogger.logD(TAG, "BluetoothClientService start with Thread: "+Thread.currentThread().getId());

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread.quit();
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_LISTEN);
        if(!ConnectionHandler.getInstance().hasUserDisconnectedConnection()
                && !ConnectionHandler.getInstance().isTCPEnabled()
                && !BluetoothHandler.getInstance().isStressTestRunning)
            tryReConnect();
    }

    BluetoothDevice mBTDevice = null;
    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    public synchronized void connect(BluetoothDevice device, boolean secure) {
        if(device==null)
            return;
        ExpLogger.logD(TAG, ".connect to: " + device.getName()+"--"+device.getAddress());
        ExpLogger.logD(TAG, ".connect with Thread: "+Thread.currentThread().getId());

//        if (mState == STATE_CONNECTING) {
//            ExpLogger.logI(TAG, "BT connection in process, returning");
//            return;
//        }
        // Cancel any thread attempting to make a connection
//        if (mState == STATE_CONNECTING) {
//            if (mConnectThread != null) {
//                mConnectThread.cancel();
//                mConnectThread.quit();
//            }
//        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            if(device.getAddress() == mBTDevice.getAddress()) {
                ExpLogger.logI(TAG, "BT already connected to the device: "+mBTDevice.getName());
                return;
            }
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        setState(STATE_CONNECTING);

        // Start the thread to connect with the given device
        if(mConnectThread==null) {
            mBTDevice = device;
            mConnectThread = new ConnectThread(secure);
            mConnectThread.start();
        }else{
            if(mBTDevice!=null && !mBTDevice.getAddress().equals(device.getAddress()))
                //close the existing socket, so new can be init'zed
                mConnectThread.cancel();

            mBTDevice = device;
            synchronized (mConnectThread.mConnectLock) {
                mConnectThread.mConnectLock.notify();
            }
        }
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     */
    public synchronized void connected(BluetoothSocket socket, final String socketType) {
        ExpLogger.logD(TAG, "BTConnected, Socket Type:" + socketType);

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket, socketType);
        mConnectedThread.start();

    }

    /**
     * Stop all threads
     */
    public synchronized void stopBTClient() {
        ExpLogger.logD(TAG, "stopTCPClient");

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread.quit();
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        mShouldAutoTryReconnect = false;
        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Send a failure message back to the Activity
        ExpLogger.logE(TAG, "connectionFailed()");
        Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Bluetooth.TOAST, MainApplication.getInstance().getString(R.string.connection_failed));
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
//        BluetoothClientService.this.start();
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        // Send a failure message back to the Activity
        ExpLogger.logE(TAG, "connectionLost()");
        Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Bluetooth.TOAST, MainApplication.getInstance().getString(R.string.connection_lost));
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
        BluetoothClientService.this.start();
    }
    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;
        private String mSocketType;
        private Object mConnectLock = new Object();
        private boolean isSecure;

        public ConnectThread(boolean secure) {
            isSecure = true;
            mSocketType = secure ? "Secure" : "Insecure";
        }
        private void initSocket(boolean secure){
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                secure = true;
                if (secure) {
                    tmp = mBTDevice.createRfcommSocketToServiceRecord(
                            MY_UUID_SECURE);
                } else {
                    tmp = mBTDevice.createInsecureRfcommSocketToServiceRecord(
                            MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                ExpLogger.logE(TAG, "Socket Type: " + mSocketType + "create() failed " + e.getLocalizedMessage());
            }
            mmSocket = tmp;
        }

        public void run() {
            ExpLogger.logI(TAG, "___Connect Thread-Id:" + Thread.currentThread().getId());
            boolean connected = false;
            ExpLogger.logI(TAG, "BEGIN mConnectThread SocketType:" + mSocketType);
            setName("ConnectThread" + mSocketType);

            while(!connected && !isInterrupted()) {
                try {
                    // Always cancel discovery because it will slow down a connection
                    mAdapter.cancelDiscovery();

                    setState(STATE_CONNECTING);
                    // Make a connection to the BluetoothSocket
                    try {

                        // This is a blocking call and will only return on a
                        // successful connection or an exception
                        if (mmSocket == null)
                            initSocket(isSecure);
                        mmSocket.connect();
                        connected = true;
                    } catch (IOException e) {
                        ExpLogger.logE("BT", "mmSocket.connect() -- FallbackBluetoothSocket " + e.getLocalizedMessage());
                        connected = false;
                        try {
                            int maxPort = 3;
                            int port = 3; //BluetoothSocket.MAX_RFCOMM_CHANNEL?
                            while (++port <= maxPort) {
                                try {
                                    mmSocket = (BluetoothSocket) mBTDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).
                                            invoke(mBTDevice, port);
                                    mmSocket.connect();
                                    if (mmSocket.isConnected()) {
                                        connected = true;
                                        break;
                                    }
                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException e1) {
                                        e1.printStackTrace();
                                    }
                                } catch (Exception e1) {
                                    if (port == maxPort)
                                        throw e1;
                                }
                            }
                        } catch (Exception e1) {
                            ExpLogger.logE("BT", "Could not initialize FallbackBluetoothSocket " + e1.getLocalizedMessage());
                            //
                            //                    // Close the socket
                            //                    try {
                            //                        mmSocket.close();
                            //                    } catch (IOException e2) {
                            //                        Log.e(TAG, "unable to close() " + mSocketType +
                            //                                " socket during connection failure", e2);
                            //                    }
                            //                    connectionFailed();
                            //                    return;
                        }


                    }catch (Exception e) {
                        Log.w("BT", "mmSocket.connect() exception", e);
                        connected = false;
                    }finally {
                        if (!connected) {
                            ExpLogger.logD("BT", "Could not initialize BluetoothSocket...");

                            setState(STATE_LISTEN);
                            connectionFailed();
                            if(!isInterrupted())//do not wait if interrupted bec. daemon will not be waking it up
                                synchronized (mConnectLock) {
                                    mConnectLock.wait();
                                }

                        }
                    }
                }catch (InterruptedException ie) {
                    ExpLogger.logE(TAG, "BT- mmSocket.connect() -Interrupted- " + ie.getLocalizedMessage());
                }
            }

            if(mBTDevice.getBondState()==BOND_NONE){
                connected = false;
                ExpLogger.logI(TAG, "BOND_NONE, disconnecting...");
            }

            // Reset the ConnectThread because we're done
            quit();
            if (connected) {
                // Start the BTConnected thread
                connected(mmSocket, mSocketType);
            }else{
                // Close the socket
                cancel();
            }
        }


        public void quit() {
            try {

                synchronized (BluetoothClientService.this) {
                    if(mConnectThread!=null)
                        mConnectThread.interrupt();
                    synchronized (mConnectLock) {
                        mConnectLock.notify();
                    }
                    mConnectThread = null;
                }
            } catch (Exception e) {
                ExpLogger.logE(TAG, "BT quit() of connect socket failed " + e.getLocalizedMessage());
            }
        }

        public void cancel() {
            try {
                if(mmSocket!=null) {
                    mmSocket.close();
                    mmSocket = null;
                }
            } catch (IOException e) {
                ExpLogger.logE(TAG, "BT close() of connect " + mSocketType + " socket failed \n" + e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private BluetoothSocket mmSocket;
        private InputStream mmInStream;
        private OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket, String socketType) {
            super("BT ConnectedThread");
            ExpLogger.logD(TAG, "create BT ConnectedThread skt type: " + socketType);
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                ExpLogger.logD(TAG, "temp sockets not created " + e.getLocalizedMessage());
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        final byte delimiter1 = 36;
        final byte delimiter2 = 35;
        final byte delimiter3 = 94;
        byte[] readBuffer;
        int readBufferPosition;
        public void run() {

            ExpLogger.logD(TAG, "***Connected Thread-Id : " + Thread.currentThread().getId());
            // Send the name of the BTConnected device back to the UI Activity
            Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_DEVICE_NAME);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.Bluetooth.DEVICE_NAME, mBTDevice.getName());
            bundle.putString(Constants.Bluetooth.DEVICE_ADDR, mBTDevice.getAddress());
            msg.setData(bundle);
            mHandler.sendMessage(msg);

            setState(STATE_CONNECTED);
            updateRetry();

            ExpLogger.logI(TAG, "BEGIN mConnectedThread");
            boolean isComplete = true;
            // Keep listening to the InputStream while BTConnected
            while (mState == STATE_CONNECTED) {
                try {
                    if(isComplete) {
                        readBufferPosition = 0;
                        readBuffer = new byte[1024*64];
                    }
                    byte[] packetBytes = new byte[1024];
                    int bytesAvailable = mmInStream.read(packetBytes);

                    if(bytesAvailable > 0)
                    {
                        try {
                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter3 && i >= 2 && packetBytes[i - 1] == delimiter2 && packetBytes[i - 2] == delimiter1) {
                                    isComplete = true;
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, java.nio.charset.StandardCharsets.UTF_8);
                                    ExpLogger.logI(TAG, "comm: bt: read the text: " + encodedBytes.length + " bytes");
                                    readBufferPosition = 0;
                                    // Send the obtained bytes to the UI Activity
//                                    mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_READ, encodedBytes.length, -1, encodedBytes)
//                                            .sendToTarget();
                                    Message message = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_READ, encodedBytes.length, -1, encodedBytes);
                                    message.setTarget(mHandler);
                                    message.sendToTarget();
                                } else {
                                    isComplete = false;
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }catch (Exception e){
                            ExpLogger.logException(TAG, e);
                        }
                    }

                } catch (Exception e) {
                    ExpLogger.logE(TAG, "disconnected " + e.getLocalizedMessage());
//                    cancel();
                    connectionLost();
                    // Start the service over to restart listening mode
//                    BluetoothClientService.this.start();
                    break;
                }
            }
            cancel();
        }
        void updateRetry(){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //lets delay the "put" so if it gets disconnected within these many secs, we do not
                        //attempt to connect again causing an indefinite loop.
                        Thread.sleep(10);
                        mDeviceConnectedStateMap.clear();
                        mDeviceConnectedStateMap.put(mBTDevice.getAddress(), 1);
                    } catch (InterruptedException e) {
                        ExpLogger.logException(TAG, e);
                    }
                }
            }).start();
        }

        /**
         * Write to the BTConnected OutStream.
         *
         * @param buffer The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                ExpLogger.logD(TAG, "comm: bt: write with Thread-Id:" + Thread.currentThread().getId()+";"+Thread.currentThread().getName());
                mmOutStream.write(buffer);
                mmOutStream.flush();
                ExpLogger.logI(TAG, "comm: bt: wrote the text: "+buffer.length+" bytes");
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
                ExpLogger.logD(TAG, "onBTCommandReceived: message sent : size :" + buffer.length);
            } catch (IOException e) {
                ExpLogger.logE(TAG, "Exception during write " + e.getLocalizedMessage());
                cancel();
                connectionLost();
                // Start the service over to restart listening mode
//                BluetoothClientService.this.start();
            }
            catch (Exception e) {
                ExpLogger.logE(TAG, "Exception during write " + e.getLocalizedMessage());
                cancel();
                connectionLost();
                // Start the service over to restart listening mode
//                BluetoothClientService.this.start();
            }
        }

        public void cancel() {
            try {
                if(mmInStream!=null){
                    mmInStream.close();
                    mmInStream = null;
                }
                if(mmOutStream!=null){
                    mmOutStream.close();
                    mmOutStream = null;
                }
                if(mmSocket!=null) {
                    mmSocket.close();
                    mmSocket = null;
                }
            } catch (IOException e) {
                ExpLogger.logE(TAG, "close() of connect socket failed " + e.getLocalizedMessage());
            }
        }
    }
}
