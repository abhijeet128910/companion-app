/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.exploride.droid.services;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.UUID;

/**
 * This class does all the work for setting up and managing Tcp
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class TcpMobServer {
    // Debugging
    private static final String TAG = "TcpMobServer";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    // Member fields
    private final Handler mHandler;
    private static AcceptThread mSecureAcceptThread;
    private static AcceptThread mInsecureAcceptThread;
    private static ConnectedThread mConnectedThread;
    private int mState;
    ManagerThread mManagerThread;
    int mPortNo = 12345;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device

    /**
     * Constructor. Prepares a new TCPIP session.
     *
     * @param context The UI Activity Context
     * @param handler A Handler to send messages back to the UI Activity
     */
    public TcpMobServer(Context context, Handler handler) {
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of the connection
     *
     * @param state An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        ExpLogger.logD(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }


    /**
     * Start the service. Specifically start AcceptThread to begin a
     * session in listening (server) mode.
     */
    public synchronized void startBTServer() {
        ExpLogger.logD(TAG, "startBTServer");
        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_LISTEN);

        // Start the thread to listen on a TCPServerSocket
        if (mSecureAcceptThread == null) {
            mSecureAcceptThread = new AcceptThread(true);
            mSecureAcceptThread.start();
        }
//        if (mInsecureAcceptThread == null) {
//            mInsecureAcceptThread = new AcceptThread(false);
//            mInsecureAcceptThread.start();
//        }
        if (mManagerThread == null) {
            mManagerThread = new ManagerThread();
            mManagerThread.start();
        }

    }
    private class ManagerThread extends Thread {

        public ManagerThread() {
            ExpLogger.logD(TAG, "create ManagerThread: ");
        }

        public void run() {
            ExpLogger.logD(TAG, "BEGIN ManagerThread");
            while(true){

                try {
                    synchronized (TcpMobServer.this) {
                        if ((mSecureAcceptThread == null || !mSecureAcceptThread.isAlive()) &&
                                (mInsecureAcceptThread == null || !mInsecureAcceptThread.isAlive()) &&
                                (mConnectedThread == null || !mConnectedThread.isAlive())) {
                            ExpLogger.logD(TAG, "ManagerThread re-startBTServer listening");
                            stopBTServer();
                            startBTServer();
                        }
                    }
                    Thread.sleep(25000);
                } catch (InterruptedException e) {
                    ExpLogger.logException(TAG, e);
                }
            }

        }
    }
    /**
     * Start the ConnectedThread to begin managing a TCP connection
     *
     * @param socket The Socket on which the connection was made
     */
    public synchronized void connected(Socket socket) {
        ExpLogger.logD(TAG, "connected, Socket");

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Cancel the accept thread because we only want to connect to one device
        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }
        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        setState(STATE_CONNECTED);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Bluetooth.DEVICE_NAME, "TCP IP");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    /**
     * Stop all threads
     */
    public synchronized void stopBTServer() {
        ExpLogger.logD(TAG, "stopBTServer");

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }

        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }
        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Bluetooth.TOAST, "Unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Bluetooth.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
        mManagerThread.interrupt();
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final ServerSocket mmServerSocket;

        public AcceptThread(boolean secure) {
            ServerSocket tmp = null;

            // Create a new listening server socket
            try {
                tmp = new ServerSocket(mPortNo);
            } catch (IOException e) {
                ExpLogger.logE(TAG, "ServerSocket() failed " + e.getLocalizedMessage());
            }
            mmServerSocket = tmp;
        }
        public void run() {
            ExpLogger.logD(TAG, "BEGIN mAcceptThread" + this);
            setName("AcceptThreadPort-" + mPortNo);

            Socket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    ExpLogger.logE(TAG, "tcp accept() failed " + e.getLocalizedMessage());
                    closeServerSocket();
                    mManagerThread.interrupt();
                    return;
                } catch (Exception e) {
                    ExpLogger.logE(TAG, "tcp accept() failed " + e.getLocalizedMessage());
                    closeServerSocket();
                    mManagerThread.interrupt();
                    return;
                }
//                closeServerSocket();
                // If a connection was accepted
                if (socket != null) {
                    synchronized (TcpMobServer.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket);
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    ExpLogger.logE(TAG, "Could not close unwanted socket " + e.getLocalizedMessage());
                                }
                                break;
                        }
                    }
                }

                if(socket==null)
                {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(mState == STATE_CONNECTED)
                        break;
                }
            }
            ExpLogger.logD(TAG, "END mAcceptThread");

        }

        private void closeServerSocket(){
            try{
                mmServerSocket.close();
            }catch (Exception e){
                ExpLogger.logE(TAG, "AcceptThread closeServerSocket() failed " + e.getLocalizedMessage());
            }
        }

        public void cancel() {
            ExpLogger.logD(TAG, "AcceptThread cancel " + this);
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                ExpLogger.logE(TAG, "AcceptThread cancel() failed " + e.getLocalizedMessage());
            } catch (Exception e) {
                ExpLogger.logE(TAG, "AcceptThread cancel() failed " + e.getLocalizedMessage());
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final Socket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(Socket socket) {
            ExpLogger.logD(TAG, "create ConnectedThread");
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the Socket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                connectionLost();
                ExpLogger.logE(TAG, "temp sockets not created " + e.getLocalizedMessage());
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }
        final byte delimiter1 = 36;
        final byte delimiter2 = 35;
        final byte delimiter3 = 94;
        byte[] readBuffer;
        int readBufferPosition;
        public void run() {
            ExpLogger.logD(TAG, "BEGIN mConnectedThread");
            boolean isComplete = true;
                // Keep listening to the InputStream while connected
                while (mState == STATE_CONNECTED) {
                    try {
                        if(isComplete) {
                            readBufferPosition = 0;
                            readBuffer = new byte[1024*4];
                        }
                        byte[] packetBytes = new byte[1024];
                        int bytesAvailable = mmInStream.read(packetBytes);

                        if(bytesAvailable==-1) {
                            throw new Exception("Connection to Device Lost. Bytes to read: -1");
                        }
                        if(bytesAvailable > 0)
                        {
                            try {
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter3 && i >= 2 && packetBytes[i - 1] == delimiter2 && packetBytes[i - 2] == delimiter1) {
                                        isComplete = true;
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                        final String data = new String(encodedBytes, java.nio.charset.StandardCharsets.UTF_8);
                                        ExpLogger.logI(TAG, "bt: read the text: " + encodedBytes.length + " bytes");
                                        readBufferPosition = 0;
                                        // Send the obtained bytes to the UI Activity
                                        mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_READ, encodedBytes.length, -1, encodedBytes)
                                                .sendToTarget();
                                    } else {
                                        isComplete = false;
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }catch (Exception e){
                                ExpLogger.logException(TAG, e);
                            }
                        }

                    }catch (Exception e) {
                        ExpLogger.logE(TAG, "disconnected " + e.getLocalizedMessage());
                        cancel();
                        connectionLost();
                        // Start the service over to restart listening mode
//                    BluetoothServer.this.startBTServer();
                        break;
                    }
                }
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
                mmOutStream.flush();
                ExpLogger.logI(TAG, "bt: wrote the text: "+buffer.length+" bytes");
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
            } catch (Exception e) {

                cancel();
                connectionLost();
                ExpLogger.logE(TAG, "Exception during write " + e.getLocalizedMessage());
            }
        }

        public void cancel() {
            try {
                if(mmInStream!=null){
                    mmInStream.close();
                }
                if(mmOutStream!=null){
                    mmOutStream.close();
                }
                mmSocket.close();
            } catch (IOException e) {
                ExpLogger.logE(TAG, "close() of connect socket failed " + e.getLocalizedMessage());
            }
        }
    }
    static Object mLockObject = new Object();
    public void sendMessage(String text){
        if(text==null || text.length()==0){
            return;
        }

        synchronized (mLockObject) {
            text += "$#^";
            byte[] send = text.getBytes(java.nio.charset.StandardCharsets.UTF_8);
            ExpLogger.logI(TAG, "bt: text to write:\n" + text + "\n");
            write(send);
        }
    }

}
