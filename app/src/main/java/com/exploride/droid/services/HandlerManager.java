package com.exploride.droid.services;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.exploride.droid.MainApplication;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.Utils;

import static android.os.Looper.myLooper;
import static com.exploride.droid.services.BluetoothClientService.STATE_CONNECTED;
import static com.exploride.droid.settings.SettingsUtils.readPreferenceValueAsString;
import static com.exploride.droid.utils.Constants.CHANNEL.BT;
import static com.exploride.droid.utils.Constants.CHANNEL.TCP;
import static com.exploride.droid.utils.Constants.SharedPrefKeys.KEY_BT_DEV_ADDRESS;
import static com.exploride.droid.utils.Constants.SharedPrefKeys.KEY_BT_DEV_NAME;

/**
 * Created by peeyushupadhyay on 29/12/16.
 */

public class HandlerManager {
    public static final String TAG = MainApplication.class.getSimpleName();
    static HandlerManager mHandlerManager = null;


    public static HandlerManager getInstance(){
        if(mHandlerManager == null){
            mHandlerManager = new HandlerManager();
        }
        return mHandlerManager;
    }
    private Handler mClientTCPHandler, mTCPMobServerHandler;
    public Handler getClientTCPHandler() {

        synchronized (this) {
            while (mLooper == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    String a = "";
                }
            }
        }
        return mClientTCPHandler;
    }
    Looper mLooper;
    public void initTCPHandlers() {
        try{
            if(mLooper!=null)
                return;
            Looper.prepare();
            synchronized (this) {
                mLooper = Looper.myLooper();
            if (mClientTCPHandler == null)
                mClientTCPHandler = new Handler(mLooper) {
                    @Override
                    public void handleMessage(Message msg) {
                        final BluetoothHandler.BTState btStateCallback = MainApplication.getInstance();
                        switch (msg.what) {
                            case Constants.Bluetooth.MESSAGE_STATE_CHANGE:
                                ConnectionHandler.getInstance().handleTCPConnectionChange(msg.arg1);
                                switch (msg.arg1) {
                                    case STATE_CONNECTED:
                                        btStateCallback.BTConnected(TCP);
                                        BTComm btCommResp = new BTComm();
                                        btCommResp.setActionCode(ActionConstants.ActionCode.CheckInternet);
                                        ConnectionHandler.getInstance().sendInfo(btCommResp, BT);
                                        Utils.toggleLocPhone(MainApplication.getInstance().shouldSendLocationFromPhone);
                                        MainApplication.getInstance().showToast("TCP connected");
                                        //                            }
                                        break;
                                    case BluetoothClientService.STATE_CONNECTING:
                                        btStateCallback.BTConnecting(TCP);
                                        break;
                                    case BluetoothClientService.STATE_LISTEN:
                                    case BluetoothClientService.STATE_NONE:
                                        btStateCallback.BTNotConnected(TCP);
                                        //                            MainPresenter.getInstance().setBluetoothState(BluetoothClientService.STATE_NONE);
                                        break;
                                }
                                break;
                            case Constants.Bluetooth.MESSAGE_WRITE:
                                byte[] writeBuf = (byte[]) msg.obj;
                                // construct a string from the buffer
                                String writeMessage = new String(writeBuf);
                                ExpLogger.logI(TAG, "comm: tcp: handler to write");
                                btStateCallback.BTMsgSent(writeMessage, TCP);
                                break;
                            case Constants.Bluetooth.MESSAGE_READ:
                                try {


                                    byte[] readBuf = (byte[]) msg.obj;
                                    // construct a string from the valid bytes in the buffer
                                    String readMessage = new String(readBuf, 0, readBuf.length, java.nio.charset.StandardCharsets.UTF_8);
                                    ExpLogger.logI(TAG, "comm: tcp: handler to read:\n" + readMessage + "\n");
                                    final String[] msgs = readMessage.split("\\$\\#");
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            for (String msgS : msgs) {
                                                if (msgS.length() > 0) {
                                                    ExpLogger.logI(TAG, "comm: tcp: command:\n" + msgS + "\n");
                                                    btStateCallback.BTMsgReceived(msgS, TCP);
                                                }
                                            }
                                        }
                                    }).start();


                                } catch (Exception e2) {
                                    ExpLogger.logException(TAG, new Exception(e2.getMessage()));
                                }
                                break;
                            case Constants.Bluetooth.MESSAGE_DEVICE_NAME:
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String devAddress = SettingsUtils.readPreferenceValueAsString(MainApplication.getInstance().getApplicationContext(),
                                                KEY_BT_DEV_ADDRESS);
                                        String devName = SettingsUtils.readPreferenceValueAsString(MainApplication.getInstance().getApplicationContext(),
                                                KEY_BT_DEV_NAME);
                                        btStateCallback.BTConnectedToDevice(devName, TCP);
                                    }
                                }).start();
                                break;
                            case Constants.Bluetooth.MESSAGE_TOAST:
                                btStateCallback.toastToShow(msg.getData().getString(Constants.Bluetooth.TOAST), TCP);
                                break;
                        }
                    }
                };
            if(mTCPMobServerHandler == null)
                mTCPMobServerHandler = new Handler() {
                //        @Override
//        public void handleMessage(Message msg) {
//            int w = msg.what;
//            Object o = msg.obj;
//            if(Constants.Bluetooth.MESSAGE_READ==w) {
//                byte[] readBuf = (byte[]) msg.obj;
//                // construct a string from the valid bytes in the buffer
//                String readMessage = new String(readBuf, 0, readBuf.length, java.nio.charset.StandardCharsets.UTF_8);
//                MainApplication.getInstance().showToast(readMessage);
//            }
//        }
                @Override
                public void handleMessage(Message msg) {
                    final BluetoothHandler.BTState btStateCallback = MainApplication.getInstance();
                    switch (msg.what) {
                        case Constants.Bluetooth.MESSAGE_STATE_CHANGE:
                            switch (msg.arg1) {
                                case STATE_CONNECTED:
                                    break;
                                case BluetoothClientService.STATE_CONNECTING:
//                            btStateCallback.BTConnecting();
                                    break;
                                case BluetoothClientService.STATE_LISTEN:
                                case BluetoothClientService.STATE_NONE:
//                            btStateCallback.BTNotConnected();
                                    break;
                            }
//                    MainPresenter.getInstance().getBluetoothState();
                            break;
                        case Constants.Bluetooth.MESSAGE_WRITE:
                            byte[] writeBuf = (byte[]) msg.obj;
                            // construct a string from the buffer
                            String writeMessage = new String(writeBuf);
                            ExpLogger.logI(TAG, "comm: tcp: handler to write");
                            btStateCallback.BTMsgSent(writeMessage, TCP);
                            break;
                        case Constants.Bluetooth.MESSAGE_READ:
                            try {


                                byte[] readBuf = (byte[]) msg.obj;
                                // construct a string from the valid bytes in the buffer
                                String readMessage = new String(readBuf, 0, readBuf.length, java.nio.charset.StandardCharsets.UTF_8);
                                ExpLogger.logI(TAG, "comm: tcp: handler to read:\n" + readMessage + "\n");
                                final String[] msgs = readMessage.split("\\$\\#");
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        for (String msgS : msgs) {
                                            if (msgS.length() > 0) {
                                                ExpLogger.logI(TAG, "comm: tcp: command:\n" + msgS + "\n");
                                                btStateCallback.BTMsgReceived(msgS, TCP);
                                            }
                                        }
                                    }
                                }).start();


                            }catch(Exception e2){
                                ExpLogger.logException(TAG, new Exception(e2.getMessage()));
                            }
                            break;
                        case Constants.Bluetooth.MESSAGE_DEVICE_NAME:
                            // save the BTConnected device's name
//                    mConnectedDeviceName = msg.getData().getString(Constants.Bluetooth.DEVICE_NAME);
//                    final String devAddress =  msg.getData().getString(Constants.Bluetooth.DEVICE_ADDR);
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            SettingsUtils.writePreferenceValue(MainApplication.getInstance().getApplicationContext(),
//                                    KEY_BT_DEV_ADDRESS, devAddress);
//                            SettingsUtils.writePreferenceValue(MainApplication.getInstance().getApplicationContext(),
//                                    KEY_BT_DEV_NAME, mConnectedDeviceName);
//                            btStateCallback.BTConnectedToDevice(mConnectedDeviceName);
//                        }
//                    }).start();
//                    break;
                        case Constants.Bluetooth.MESSAGE_TOAST:
                            btStateCallback.toastToShow(msg.getData().getString(Constants.Bluetooth.TOAST), TCP);
                            break;
                    }
                }
            };
                notifyAll();
            }
            Looper.loop();
    }catch(Exception e) {
        ExpLogger.logE(TAG, e.getMessage());
    }
    }


    public Handler getTCPMobServerHandler(){
        return mTCPMobServerHandler;
    }



}
