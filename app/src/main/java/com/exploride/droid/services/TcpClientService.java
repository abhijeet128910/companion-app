/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.exploride.droid.services;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.Enumeration;
import java.util.UUID;


/**
 * class to set up and manage tcp connections with other devices.
 */
public class TcpClientService {
    // Debugging
    private static final String TAG = "TcpClientService";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    // Member fields
    private final Handler mHandler;
    private volatile ConnectThread mConnectThread;
    private volatile ConnectedThread mConnectedThread;
    private volatile Heartbeat mHeartbeat;
    Object mConnectionDaemonLock = new Object();
    volatile boolean hbMessageSent = false;
    volatile boolean hbMessageRcved = false;
    private ConnectionDaemon mConnectionDaemon;
    private String hbText = "hb$#";

    static volatile private int mState;
    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now BTConnected to a remote device

    static volatile String mHostAddress = null;

    int mPortNo = 12345;
    /**
     * Constructor. Prepares a new Bluetooth session.
     *
     * @param handler A Handler to send messages back to the UI Activity
     */
    public TcpClientService(String hostAddress, Handler handler) {
        mState = STATE_NONE;
        mHandler = handler;
        mHostAddress = hostAddress;
    }
    public void setHostAddress(String hostAddress){
        mHostAddress = hostAddress;

    }

    public static String getHostAddress() {
        return mHostAddress;
    }

    String mLocalAddress = "";
    public void setLocalAddress(String localAddress){
        mLocalAddress = localAddress;
    }

    public String getDualAddress() {
        return mLocalAddress+"-"+mHostAddress;
    }

    /**
     * Set the current state of the Bluetooth connection
     *
     * @param state An integer defining the current connection state
     */
    private void setState(int state) {
        ExpLogger.logD(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public int getState() {
        return mState;
    }

    /**
     * Start the service.
     */
    public synchronized void start() {
        ExpLogger.logD(TAG, "start");

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread.quit();
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mConnectionDaemon == null) {
            mConnectionDaemon = new ConnectionDaemon();
            mConnectionDaemon.start();
        }
        setState(STATE_LISTEN);

    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     */
    public synchronized void connect() {
        ExpLogger.logD(TAG, "connect()");
        if (mState == STATE_CONNECTING || mState == STATE_CONNECTED) {
            return;
        }
        // Cancel any thread attempting to make a connection
//        if (mState == STATE_CONNECTING) {
//            if (mConnectThread != null) {
//                mConnectThread.cancel();
//                mConnectThread = null;
//            }
//        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        setState(STATE_CONNECTING);
        // Start the thread to connect with the given device
        if(mConnectThread==null) {
            mConnectThread = new ConnectThread();
            mConnectThread.start();
        }else{
            synchronized (mConnectThread.mConnectLock) {
                mConnectThread.mConnectLock.notify();
            }
        }
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The Socket on which the connection was made
     */
    public synchronized void connected(Socket socket) {
        ExpLogger.logD(TAG, "connected()");
        ExpLogger.logI(TAG, "TCP- Connected to: "+ mHostAddress);
        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread.quit();
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

    }

    /**
     * Stop all threads
     */
    public void stopTCPClient() {
        ExpLogger.logD(TAG, "stopTCPClient");

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread.quit();
        }

        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mConnectionDaemon != null) {
            mConnectionDaemon.interrupt();
        }
        setHostAddress(null);

        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    private class ConnectionDaemon extends Thread {

        public ConnectionDaemon() {
            super("ConnectionDaemon");
            ExpLogger.logD(TAG, "create ConnectionDaemon: ");
        }

        public void run() {
            ExpLogger.logD(TAG, "BEGIN ConnectionDaemon");
            while(ConnectionHandler.getInstance().isTCPEnabled()){

                try {
                    Thread.sleep(5000);
                    if(!ConnectionHandler.getInstance().hasUserDisconnectedConnection()) {
                        if ((mState != STATE_CONNECTED) &&
                                (mConnectedThread == null || !mConnectedThread.isAlive())) {
                            ExpLogger.logD(TAG, "ConnectionDaemon connecting");
                            connect();
                            synchronized (mConnectionDaemonLock) {
                                mConnectionDaemonLock.wait();
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    ExpLogger.logE(TAG, e.getMessage());
                }
            }
            mConnectionDaemon = null;
        }
    }
    private class Heartbeat extends Thread {
        volatile boolean hbInterrupted = false;
        public Heartbeat() {
            setName("Heartbeat");
            ExpLogger.logD(TAG, "create Heartbeat");
        }

        public void run() {
            ExpLogger.logD(TAG, "BEGIN Heartbeat");
            hbInterrupted = false;
            try {
                while(1==1){
                    if(hbInterrupted)
                        throw new InterruptedException("Heartbeat Interrupted through hbInterrupted");
                    if(STATE_CONNECTED != mState) {
                        ExpLogger.logD(TAG, "Heartbeat STATE_CONNECTED != mState, interrupting");
                        throw new InterruptedException("Heartbeat Interrupted through STATE_CONNECTED != mState");
                    }
                    if(!hbMessageRcved) {
                        ExpLogger.logI(TAG, "comm: tcp: no hbMessageRcved, sending hb");
                        sendMessage("hb");
                        //lets wait 3 secs more for hb to arrive
                        Thread.sleep(3000);//take care of boundary condition for HB to be sent from both sides if they started in synched state?
                        if(!hbMessageRcved) {
                            ExpLogger.logD(TAG, "Heartbeat message not received, interrupting");
                            throw new InterruptedException("Heartbeat message not received, interrupting");
                        }
                    }
                    hbMessageRcved = false;
                    if(!hbMessageSent)
                        sendMessage("hb");
                    hbMessageSent = false;
                    Thread.sleep(7000);

                }
            } catch (InterruptedException e) {
                ExpLogger.logE(TAG, "InterruptedException::"+e.getMessage());
                hbInterrupted = true;
                mHeartbeat = null;
                if(mConnectedThread!=null) {
                    mConnectedThread.interrupt();
                    mConnectedThread.cancel();
                    mConnectedThread = null;
                }
                if(mConnectionDaemonLock!=null)
                    synchronized(mConnectionDaemonLock) {
                        if(mConnectionDaemonLock!=null)
                            mConnectionDaemonLock.notify();
                    }
            }
        }
    }
    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Send a failure message back to the Activity
        ExpLogger.logE(TAG, "TCP connectionFailed()");
        Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_TOAST);
//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.Bluetooth.TOAST, MainApplication.getInstance().getString(R.string.tcp_connection_failed));
//        msg.setData(bundle);
//        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
//        TcpClientService.this.start();
        if(mConnectionDaemonLock!=null)
            synchronized(mConnectionDaemonLock) {
                if(mConnectionDaemonLock!=null)
                    mConnectionDaemonLock.notify();
            }
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     * it is essential that you call cancel with this.
     */
    private void connectionLost() {
        // Send a failure message back to the Activity
        ExpLogger.logE(TAG, "TCP connectionLost()");
        Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.Bluetooth.TOAST, MainApplication.getInstance().getString(R.string.tcp_connection_lost));
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        // Start the service over to restart listening mode
        TcpClientService.this.start();

        //hb will do it
//        if(mConnectionDaemonLock!=null)
//            synchronized(mConnectionDaemonLock) {
//                if(mConnectionDaemonLock!=null)
//                    mConnectionDaemonLock.notify();
//            }
    }
    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private Socket mmSocket;
        private Object mConnectLock = new Object();

        public ConnectThread() {super("ConnectThread");
        }

        private void bindLocal(Socket tmp) {
            try{
                Enumeration<NetworkInterface> nifs = NetworkInterface.getNetworkInterfaces();
                while(nifs.hasMoreElements()) {
                    NetworkInterface nif = nifs.nextElement();
                    Enumeration nifAddresses = nif.getInetAddresses();
                    while (nifAddresses.hasMoreElements()) {
                        InetAddress inetAddress = (InetAddress) nifAddresses.nextElement();
                        if (!inetAddress.toString().contains("192.168."))
                            continue;
                        InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, 0);
                        if(!tmp.isBound())
                            tmp.bind(inetSocketAddress);
                        return;
                    }
                }
            }catch (Exception e){
                ExpLogger.logI(TAG, e.getMessage());
            }
        }

        public void run() {

            ExpLogger.logI(TAG, "TCP- BEGIN mConnectThread: "+ Thread.currentThread().getId());
            setName("ConnectThread");
            boolean connected = false;
            while(!connected && !isInterrupted()) {
                try {
                    mmSocket = null;
                    // Get a BluetoothSocket for a connection with the
                    // given BluetoothDevice
                    setState(STATE_CONNECTING);
                    try {
                        ExpLogger.logI(TAG, "TCP- ConnectThread connecting to: " + mHostAddress);
                        if (mHostAddress != null && !mHostAddress.equals("0.0.0.0")) {
                            //                    tmp = new Socket(mHostAddress, mPortNo);
                            mmSocket = new Socket();
                            bindLocal(mmSocket);
                            //                tmp.setSoTimeout(2000);

                            mmSocket.connect(new InetSocketAddress(mHostAddress, mPortNo), 2000);
                        }
                    } catch (IOException e) {
                        ExpLogger.logE(TAG, "TCP- ConnectThread failed " + e.getLocalizedMessage());
                    } catch (IllegalArgumentException e) {
                        ExpLogger.logE(TAG, "TCP- ConnectThread failed " + e.getLocalizedMessage());
                    } catch (Exception e) {
                        ExpLogger.logE(TAG, "TCP- ConnectThread failed " + e.getLocalizedMessage());
                    }

                    try {
                        // This is a blocking call and will only return on a
                        // successful connection or an exception
                        if (mmSocket != null && mmSocket.isConnected()) {
                            connected = true;
                        }
                    } catch (Exception e) {
                        ExpLogger.logE(TAG, "TCP- mmSocket.connect() -- FallbackBluetoothSocket " + e.getLocalizedMessage());
                        connected = false;
                    } finally {
                        if (!connected) {
                            ExpLogger.logE(TAG, "Could not initialize BluetoothSocket...");

                            // Close the socket
                            cancel();
                            connectionFailed();
                            setState(STATE_LISTEN);
                            if(!isInterrupted())//do not wait if interrupted bec. daemon will not be waking it up
                                synchronized (mConnectLock) {
                                    mConnectLock.wait();
                                }
                        }
                    }
                }catch (InterruptedException ie) {
                    ExpLogger.logE(TAG, "TCP- mmSocket.connect() -Interrupted- "+ ie.getLocalizedMessage());
                }
            }
            // Reset the ConnectThread because we're done
            quit();

            // Start the BTConnected thread
            if (connected)
                connected(mmSocket);
        }

        public void quit() {
            try {

                synchronized (TcpClientService.this) {
                    if(mConnectThread!=null)
                        mConnectThread.interrupt();
                    synchronized (mConnectLock) {
                        mConnectLock.notify();
                    }
                    mConnectThread = null;
                }
            } catch (Exception e) {
                ExpLogger.logE(TAG, "TCP quit() of connect socket failed " + e.getLocalizedMessage());
            }
        }

        public void cancel() {
            try {
                if(mmSocket!=null)
                    mmSocket.close();
            } catch (IOException e) {
                ExpLogger.logE(TAG, "TCP close() of connect socket failed "+ e.getLocalizedMessage());
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private Socket mmSocket;
        private InputStream mmInStream;
        private OutputStream mmOutStream;

        public ConnectedThread(Socket socket) {
            super("TCP ConnectedThread");
            ExpLogger.logD(TAG, "create TCP ConnectedThread");
            mmSocket = socket;
            setLocalAddress(mmSocket.getLocalAddress().toString());
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();

            } catch (IOException e) {
                ExpLogger.logE(TAG, "temp sockets not created " + e.getLocalizedMessage());
            } catch (Exception e) {
                ExpLogger.logE(TAG, "temp sockets not created " + e.getLocalizedMessage());
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
            mHeartbeat = new Heartbeat();
        }

        final byte delimiter1 = 36;
        final byte delimiter2 = 35;
        final byte delimiter3 = 94;
        byte[] readBuffer;
        int readBufferPosition;
        public void run() {

            ExpLogger.logI(TAG, "BEGIN mConnectedThread");
            ExpLogger.logI(TAG, "create ConnectedThread: "+ Thread.currentThread().getId());
            // Send the name of the BTConnected device back to the UI Activity
            Message msg = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_DEVICE_NAME);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.Bluetooth.DEVICE_NAME, "tcp C");
            bundle.putString(Constants.Bluetooth.DEVICE_ADDR, "tcp C Add");
            msg.setData(bundle);
            mHandler.sendMessage(msg);


            setState(STATE_CONNECTED);
            mHeartbeat.start();
            boolean isComplete = true;
            // Keep listening to the InputStream while BTConnected
            while (mState == STATE_CONNECTED) {
                try {
                    if(Thread.interrupted()) {
                        ExpLogger.logI(TAG, "interrupted flag set, exiting tcp connection");
                        throw new Exception("interrupted flag set, exiting tcp connection");
                    }
                    if(isComplete) {
                        readBufferPosition = 0;
                        readBuffer = new byte[1024*64];
                    }
                    byte[] packetBytes = new byte[1024];
                    int bytesAvailable = mmInStream.read(packetBytes);

                    if(bytesAvailable > 0)
                    {
                        try {
                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == delimiter3 && i >= 2 && packetBytes[i - 1] == delimiter2 && packetBytes[i - 2] == delimiter1) {
                                    isComplete = true;
                                    hbMessageRcved = true;
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, java.nio.charset.StandardCharsets.UTF_8);
                                    ExpLogger.logI(TAG, "comm: tcp: read the text: " + encodedBytes.length + " bytes");
//                                    ExpLogger.logI(TAG, "comm: tcp: read the text: " + data);
                                    readBufferPosition = 0;
                                    if(data!=null && data.equals(hbText)) {
                                        continue;
                                    }
                                    // Send the obtained bytes to the UI Activity
//                              `      mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_READ, encodedBytes.length, -1, encodedBytes)
//                                            .sendToTarget();
                                    Message message = mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_READ, encodedBytes.length, -1, encodedBytes);
                                    message.setTarget(mHandler);
                                    message.sendToTarget();
                                } else {
                                    isComplete = false;
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }catch (Exception e){
                            ExpLogger.logException(TAG, e);
                        }
                    }else
                        ExpLogger.logE(TAG, "************************bytesAvailable is zero************************");
                    ExpLogger.logD(TAG, "read ConnectedThread: "+ Thread.currentThread().getId());
                } catch (Exception e) {
                    ExpLogger.logE(TAG, "disconnected "+ e.getLocalizedMessage());
//                    cancel();
                    connectionLost();
                    // Start the service over to restart listening mode
//                    TcpClientService.this.start();
                    break;
                }
            }
            cancel();
//            if(mHeartbeat!=null && mHeartbeat.isAlive()){
//                mHeartbeat.hbInterrupted = true;
//                mHeartbeat.interrupt();
//                mHeartbeat = null;
//            }
//            if(mConnectionDaemonLock!=null)
//                 synchronized(mConnectionDaemonLock) {
//                    if(mConnectionDaemonLock!=null)
//                        mConnectionDaemonLock.notify();
//                }
        }

        /**
         * Write to the BTConnected OutStream.
         *
         * @param buffer The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                ExpLogger.logD(TAG, "comm: tcp: write with Thread-Id:" + Thread.currentThread().getId()+";"+Thread.currentThread().getName());
                mmOutStream.write(buffer);
                mmOutStream.flush();
                ExpLogger.logI(TAG, "comm: tcp: wrote the text: "+buffer.length+" bytes");
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(Constants.Bluetooth.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
                ExpLogger.logD(TAG, "onBTCommandReceived: message sent : size :" + buffer.length);
            } catch (IOException e) {
                ExpLogger.logE(TAG, "Exception during write " + e.getLocalizedMessage());
                cancel();
                connectionLost();
                // Start the service over to restart listening mode
//                TcpClientService.this.start();
            }
            catch (Exception e) {
                ExpLogger.logE(TAG, "Exception during write " + e.getLocalizedMessage());
                cancel();
                connectionLost();
                // Start the service over to restart listening mode
//                TcpClientService.this.start();
            }
        }

        public void cancel() {
            try {
                if(mmInStream!=null){
                    mmInStream.close();
                    mmInStream = null;
                }
                if(mmOutStream!=null){
                    mmOutStream.close();
                    mmOutStream = null;
                }
                if(mmSocket!=null) {
                    mmSocket.close();
                    mmSocket = null;
                }
                if(mHeartbeat!=null && mHeartbeat.isAlive()){
                    mHeartbeat.hbInterrupted = true;
                    mHeartbeat.interrupt();
                    mHeartbeat = null;
                }
            } catch (IOException e) {
                ExpLogger.logE(TAG, "close() of connect socket failed " + e.getLocalizedMessage());
            }
        }
    }


    static Object mLockObject = new Object();
    public void sendMessage(String message) {
        synchronized (mLockObject) {
            try {
                hbMessageSent = true;
                // Check that there's actually something to send
                if (message.length() > 0) {
                    // Get the message bytes and tell the BluetoothClientService to write
                    message += "$#^";

                    byte[] send = message.getBytes(java.nio.charset.StandardCharsets.UTF_8);
                    ExpLogger.logI(TAG, "comm: tcp: text to write:\n" + message + "\n");
                    write(send);

                }
            }catch (Exception e){
                ExpLogger.logE(TAG, e.getMessage());
            }
        }
    }
}
