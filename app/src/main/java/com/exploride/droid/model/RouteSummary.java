package com.exploride.droid.model;

import com.here.android.mpa.routing.Route;

import java.util.ArrayList;

/**
 * Model class to hold route summary for the source and destination selected by the user
 *
 * Author Mayank
 */

public class RouteSummary {

    private String eta;

    private String viaPoint;

    private String distance;

    private Route route;

    private int duration;

    private ArrayList<Maneuvers> maneuversArrayList = new ArrayList<>();

    public ArrayList<Maneuvers> getManeuversArrayList() {
        return maneuversArrayList;
    }

    public void setManeuversArrayList(ArrayList<Maneuvers> maneuversArrayList) {
        this.maneuversArrayList = maneuversArrayList;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getViaPoint() {
        return viaPoint;
    }

    public void setViaPoint(String viaPoint) {
        this.viaPoint = viaPoint;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public class Maneuvers {

        private int turnImage;

        private String turnName;

        private String distance;

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public int getTurnImage() {
            return turnImage;
        }

        public void setTurnImage(int turnImage) {
            this.turnImage = turnImage;
        }

        public String getTurnName() {
            return turnName;
        }

        public void setTurnName(String turnName) {
            this.turnName = turnName;
        }
    }
}
