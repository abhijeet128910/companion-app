package com.exploride.droid.model;

import android.location.Location;

import com.exploride.droid.SearchLocationContract;

import java.util.ArrayList;

/**
 * Singleton Model class to hold Destination coordinates
 *
 * Author Mayank
 */


public class SearchLocationStore implements SearchLocationContract.SearchLocationStore {

    private ArrayList<Location> locationList = new ArrayList<>();

    //variable to hold current class instance
    private static SearchLocationStore searchLocationStore;

    /**
     * gets MapsStore instance
     * @return MapsStore new instance if null else existing instance is returned
     */
    public static SearchLocationStore getInstance() {
        if (searchLocationStore == null) {
            searchLocationStore = new SearchLocationStore();
        }
        return searchLocationStore;
    }

    /**
     * adds way point to the list
     * @param location way point location
     */
    @Override
    public void addWayPoints(Location location) {
        locationList.add(location);
    }

    /**
     * gets way points added by the user
     * @return list of way points
     */
    @Override
    public ArrayList<Location> getWayPoints() {
        return locationList;
    }

    @Override
    public void removeAllWayPoints() {
        locationList.clear();
    }
}
