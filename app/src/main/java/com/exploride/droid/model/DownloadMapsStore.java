package com.exploride.droid.model;

import android.util.Log;

import com.exploride.droid.DownloadMapsContract;
import com.exploride.droid.logger.ExpLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * Author Mayank
 */

public class DownloadMapsStore implements DownloadMapsContract.DownloadMapsStore {

    // list to hold map package id
    private List<Integer> mPackageIdList;

    // variable to hold map package id for the map package that user wants to install or uninstall
    private int mapPackageId = -1;

    //variable to hold map install or uninstall progress
    private int progress;

    //variable to hold current class instance
    private static DownloadMapsStore mDownloadMapsStore;

    /**
     * gets DownloadMapsStore instance
     *
     * @return DownloadMapsStore new instance if null else existing instance is returned
     */
    public static DownloadMapsStore getInstance() {
        if (mDownloadMapsStore == null) {
            mDownloadMapsStore = new DownloadMapsStore();
        }
        return mDownloadMapsStore;
    }

    /**
     * sets map package id list
     *
     * @param mPackageIdList
     */
    public void setPackageIdList(List<Integer> mPackageIdList) {
        for(Integer id : mPackageIdList) {
            if(!this.mPackageIdList.contains(id))
                this.mPackageIdList.add(id);
        }

        ExpLogger.logD("DownloadMapsStore", "Download Maps List : " +this.mPackageIdList);
    }

    /**
     * gets map package id list
     *
     * @return List of Map package id
     */
    public List<Integer> getPackageIdList() {
        return mPackageIdList;
    }

    /**
     * gets map package id to install the map in the HUD
     * @return int map package id
     */
    @Override
    public int getMapPackageId() {
        return mapPackageId;
    }

    /**
     * sets map package id for the map
     * @param mapPackageId
     */
    @Override
    public void setMapPackageId(int mapPackageId) {
        this.mapPackageId = mapPackageId;
    }

    /**
     * gets map install or uninstall progress
     * @return int progress
     */
    @Override
    public int getProgress() {
        return progress;
    }

    /**
     * sets map install or uninstall progress
     * @param progress : progress of map on HUD
     */
    @Override
    public void setProgress(int progress) {
        this.progress = progress;
    }

    /**
     * initializes list of map package id's
     */
    @Override
    public void initPackageIdList() {
        mPackageIdList = new ArrayList<>();
    }
}
