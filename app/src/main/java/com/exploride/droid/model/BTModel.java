package com.exploride.droid.model;

/**
 * Created by Abhijeet on 12/17/2016.
 */

public class BTModel {
    private String btName, btAddress;

    public BTModel(String btName, String btAddress) {
        this.btName = btName;
        this.btAddress = btAddress;
    }

    public String getBtName() {
        return btName;
    }

    public String getBtAddress() {
        return btAddress;
    }
}