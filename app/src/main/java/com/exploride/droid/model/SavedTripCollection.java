package com.exploride.droid.model;

import com.cloudant.exploridemobile.framework.Navigation;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijeet on 11/16/2016.
 */

public class SavedTripCollection {

    @SerializedName("TLst")
    List<Navigation> trips;

    public List<Navigation> getTrips() {
        return trips;
    }

    public void addSavedTrip(Navigation savedTripModel) {
        if (trips == null) {
            trips = new ArrayList<>();
        }
        trips.add(savedTripModel);
    }
}