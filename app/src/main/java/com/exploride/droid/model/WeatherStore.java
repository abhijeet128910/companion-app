package com.exploride.droid.model;

/**
 * Created by Mayank on 10/22/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Model to hold data from JsonResponse fetched from OpenWeatherMap
 */
public class WeatherStore {

    @SerializedName("coord")
    @Expose
    public Coord coord;
    @SerializedName("weather")
    @Expose
    public ArrayList<Weather> weather = new ArrayList<Weather>();
    @SerializedName("base")
    @Expose
    public String base;
    @SerializedName("main")
    @Expose
    public Main main;
    @SerializedName("wind")
    @Expose
    public Wind wind;
    @SerializedName("clouds")
    @Expose
    public Clouds clouds;
    @SerializedName("dt")
    @Expose
    public Integer dt;
    @SerializedName("sys")
    @Expose
    public Sys sys;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("cod")
    @Expose
    public Integer cod;

    public class Coord {

        @SerializedName("lon")
        @Expose
        public double lon;
        @SerializedName("lat")
        @Expose
        public double lat;

    }

    public class Weather {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("main")
        @Expose
        public String main;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("icon")
        @Expose
        public String icon;

    }

    public class Main {

        @SerializedName("temp")
        @Expose
        public double temp;
        @SerializedName("pressure")
        @Expose
        public double pressure;
        @SerializedName("humidity")
        @Expose
        public Integer humidity;
        @SerializedName("temp_min")
        @Expose
        public double tempMin;
        @SerializedName("temp_max")
        @Expose
        public double tempMax;
        @SerializedName("sea_level")
        @Expose
        public double seaLevel;
        @SerializedName("grnd_level")
        @Expose
        public double grndLevel;

    }

    public class Wind {

        @SerializedName("speed")
        @Expose
        public double speed;
        @SerializedName("deg")
        @Expose
        public double deg;

    }

    public class Clouds {

        @SerializedName("all")
        @Expose
        public Integer all;

    }

    public class Sys {

        @SerializedName("message")
        @Expose
        public double message;
        @SerializedName("country")
        @Expose
        public String country;
        @SerializedName("sunrise")
        @Expose
        public Integer sunrise;
        @SerializedName("sunset")
        @Expose
        public Integer sunset;

    }
}
