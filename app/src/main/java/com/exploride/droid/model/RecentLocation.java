package com.exploride.droid.model;

import android.location.Location;

/**
 * Created by Abhijeet on 12/17/2016.
 */

public class RecentLocation {
    private String name;
    private Location location;

    public RecentLocation(String name, Location location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }
}