package com.exploride.droid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Model class to hold Auto Suggest places fetched from the server
 *
 * Author Mayank
 */

public class PlaceAutoComplete {

    @SerializedName("results")
    @Expose
    public ArrayList<Result> results = new ArrayList<Result>();

    public class Result {

        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("highlightedTitle")
        @Expose
        public String highlightedTitle;
        @SerializedName("vicinity")
        @Expose
        public String vicinity;
        @SerializedName("highlightedVicinity")
        @Expose
        public String highlightedVicinity;
        @SerializedName("position")
        @Expose
        public ArrayList<Double> position = new ArrayList<Double>();
        @SerializedName("category")
        @Expose
        public String category;
        @SerializedName("bbox")
        @Expose
        public ArrayList<Double> bbox = new ArrayList<Double>();
        @SerializedName("href")
        @Expose
        public String href;
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("google_place_id")
        @Expose
        public String placeId;
        @SerializedName("isFavorite")
        @Expose
        public boolean isFavorite;

    }
}
