package com.exploride.droid.model;

import com.exploride.droid.MapsContract;
import com.here.android.mpa.mapping.MapRoute;

import java.util.ArrayList;

/**
 * Singleton Model class to hold map related fields(source, destination, map route etc.)
 *
 * Author Mayank
 */

public class MapsStore implements MapsContract.MapsStore {

    //variable to hold source latitude
    private double mSourceLatitude;

    //variable to hold source longitude
    private double mSourceLongitude;

    //variable to hold destination latitude
    private double mDestinationLatitude;

    //variable to hold destination longitude
    private double mDestinationLongitude;

    //variable to hold source title
    private String source;

    //variable to hold destination title
    private String destination;

    //variable to hold route summary
    private ArrayList<RouteSummary> routeSummaries;

    //variable to hold map route to be rendered
    private MapRoute mapRoute;

    //variable to hold current class instance
    private static MapsStore mMapsStore;

    private int selectedRouteIndex = -1;

    /**
     * gets MapsStore instance
     * @return MapsStore new instance if null else existing instance is returned
     */
    public static MapsStore getInstance() {
        if (mMapsStore == null) {
            mMapsStore = new MapsStore();
        }
        return mMapsStore;
    }

    /**
     * gets source Latitude
     * @return double source Latitude
     */
    @Override
    public double getSourceLatitude() {
        return mSourceLatitude;
    }

    /**
     * sets source latitude
     * @param latitude: source latitude
     */
    @Override
    public void setSourceLatitude(double latitude) {
        this.mSourceLatitude = latitude;
    }

    /**
     * gets source longitude
     * @return double source Longitude
     */
    @Override
    public double getSourceLongitude() {
        return mSourceLongitude;
    }

    /**
     * sets source longitude
     * @param longitude: source longitude
     */
    @Override
    public void setSourceLongitude(double longitude) {
        this.mSourceLongitude = longitude;
    }

    /**
     * gets Destination Latitude
     * @return double Destination Latitude
     */
    @Override
    public double getDestinationLatitude() {
        return mDestinationLatitude;
    }

    /**
     * sets destination latitude
     * @param latitude: destination latitude
     */
    @Override
    public void setDestinationLatitude(double latitude) {
        this.mDestinationLatitude = latitude;
    }

    /**
     * gets destination longitude
     * @return double Destination Longitude
     */
    @Override
    public double getDestinationLongitude() {
        return mDestinationLongitude;
    }

    /**
     * sets destination longitude
     * @param longitude: destination longitude
     */
    @Override
    public void setDestinationLongitude(double longitude) {
        this.mDestinationLongitude = longitude;
    }


    /**
     * sets source location place name
     * @param title: place name
     */
    @Override
    public void setSourceLocation(String title) {
        this.source = title;
    }

    /**
     * sets destination location place name
     * @param title: place name
     */
    @Override
    public void setDestinationLocation(String title) {
        this.destination = title;
    }

    /**
     * gets source location place name
     * @return String place name
     */
    @Override
    public String getSourceLocation() {
        return this.source;
    }

    /**
     * gets destination location place name
     * @return String place name
     */
    @Override
    public String getDestinationLocation() {
        return this.destination;
    }

    /**
     * sets map route to be rendered on the map
     * @param mapRoute: route selected by the user
     */
    @Override
    public void setMapRoute(MapRoute mapRoute) {
        this.mapRoute = mapRoute;
    }

    /**
     * gets map route
     * @return MapRoute to be rendered on the map
     */
    @Override
    public MapRoute getMapRoute() {
        return mapRoute;
    }

    /**
     * sets route summary like eta, via point, distance
     * @param routeSummaries: summary for the route
     */
    @Override
    public void setRouteSummary(ArrayList<RouteSummary> routeSummaries) {
        this.routeSummaries = routeSummaries;
    }

    /**
     * gets route summary
     * @return RouteSummary summary for the rote
     */
    @Override
    public ArrayList<RouteSummary> getRouteSummary() {
        return routeSummaries;
    }

    /**
     * sets selected route index to be sent to the HUD
     * @param selectedRouteIndex: index of route selected from the listview
     */
    @Override
    public void setSelectedRouteIndex(int selectedRouteIndex) {
        this.selectedRouteIndex = selectedRouteIndex;
    }

    /**
     * gets selected route index to be sent to the HUD
     * @return int index of selected route from the listview
     */
    @Override
    public int getSelectedRouteIndex() {
        return selectedRouteIndex;
    }
}
