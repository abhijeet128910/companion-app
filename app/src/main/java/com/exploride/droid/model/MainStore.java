package com.exploride.droid.model;

import com.exploride.droid.MainApplication;
import com.exploride.droid.MainContract;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;

import static android.R.attr.mode;

/**
 * Model class
 */

public class MainStore implements MainContract.MainStore {

    private int hudBrightness;

    private boolean isHudAutomaticBrightnessOn;
    private static final int BRIGHTNESS_MODE_AUTO= 1;
    private static final int BRIGHTNESS_MODE_MANUAL = 2;
    //variable to hold current class instance
    private static MainStore mMainStore;

    /**
     * gets MainStore instance
     * @return MainStore new instance if null else existing instance is returned
     */
    public static MainStore getInstance() {
        if (mMainStore == null) {
            mMainStore = new MainStore();
            mMainStore.init();
        }
        return mMainStore;
    }

    private void init() {

        int prevBrightness = SettingsUtils.readPreferenceValueAsInt(MainApplication.getInstance(),
                Constants.SharedPrefKeys.KEY_BRIGHTNESS_VALUE);
        int brightnessMode = SettingsUtils.readPreferenceValueAsInt(MainApplication.getInstance(),
                Constants.SharedPrefKeys.KEY_BRIGHTNESS_MODE);
        hudBrightness = (prevBrightness);
        isHudAutomaticBrightnessOn = (brightnessMode == BRIGHTNESS_MODE_MANUAL? false:true);
    }

    /**
     * sets HUD  brightness value
     * @param value: brightness(range 0-255)
     */
    @Override
    public void setHudBrightness(int value) {
        hudBrightness = value;
        SettingsUtils.writePreferenceValue(MainApplication.getInstance(),
                Constants.SharedPrefKeys.KEY_BRIGHTNESS_VALUE, hudBrightness);

    }

    /**
     * gets HUD brightness value
     * @return int brightness value (range 0-255)
     */
    @Override
    public int getHudBrightness() {
        return hudBrightness;
    }

    /**
     * sets whether automatic brightness mode is on for HUD
     * @param hudAutomaticBrightnessOn: boolean stating whether automatic mode is on
     */
    @Override
    public void setHudAutomaticBrightnessOn(boolean hudAutomaticBrightnessOn) {
        isHudAutomaticBrightnessOn = hudAutomaticBrightnessOn;
        int mode = isHudAutomaticBrightnessOn?BRIGHTNESS_MODE_AUTO:BRIGHTNESS_MODE_MANUAL;
        SettingsUtils.writePreferenceValue(MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_BRIGHTNESS_MODE, mode);

    }

    /**
     * gets whether automatic brightness mode is on for HUD
     * @return boolean true if automatic brightness mode is on for HUD else false
     */
    @Override
    public boolean getHudAutomaticBrightnessOn() {
        return isHudAutomaticBrightnessOn;
    }
}
