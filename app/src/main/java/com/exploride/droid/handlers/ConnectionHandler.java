package com.exploride.droid.handlers;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.SyncPreferences;
import com.cloudant.exploridemobile.framework.Utils;
import com.exploride.droid.MainApplication;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.services.BluetoothClientService;
import com.exploride.droid.services.HandlerManager;
import com.exploride.droid.services.TcpClientService;
import com.exploride.droid.services.TcpMobServer;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.views.activities.DeviceListActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.here.android.mpa.common.PositioningManager;

import java.util.HashMap;

import static android.R.attr.action;
import static android.bluetooth.BluetoothDevice.BOND_NONE;
import static android.bluetooth.BluetoothDevice.EXTRA_BOND_STATE;
import static android.bluetooth.BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE;
import static com.exploride.droid.utils.Constants.CHANNEL.BT;
import static com.exploride.droid.utils.Constants.CHANNEL.TCP;
import static com.exploride.droid.utils.Constants.SharedPrefKeys.KEY_BT_DEV_NAME;
import static com.exploride.droid.utils.Constants.SharedPrefKeys.KEY_DEVOPTIONS_TCP_HOST;

public class ConnectionHandler {

    private static ConnectionHandler mConnectionHandler;
    private static Object sLock = new Object();
    private static String TAG = "ConnectionHandler";
    public PositioningManager.LocationMethod mLocationMethod= PositioningManager.LocationMethod.GPS_NETWORK;
    static HUDMessenger hudMessengerCl;
    public ConnectionHandler(){
    }
    public static ConnectionHandler getInstance(){
        if(mConnectionHandler ==null) {
            synchronized (sLock) {
                if (mConnectionHandler == null) {
                    mConnectionHandler = new ConnectionHandler();
                    hudMessengerCl = new HUDMessenger();
                }
            }
        }
        return mConnectionHandler;
    }

    public HUDMessenger getHudMessenger() {
        return hudMessengerCl;
    }

    HashMap<String, String> mBTAddressMap = new HashMap<>();

    public void putAddressEntryForBT(String ipAddress, String btAddress){

        mBTAddressMap.put(btAddress, ipAddress);
    }

    public String getIpForBTAddress(String btAddress){

        return mBTAddressMap.get(btAddress);
    }

    BluetoothHandler mBluetoothHandler;
    public void initConnection(Intent intent) {

        String address = intent.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        mBluetoothHandler = BluetoothHandler.getInstance();
        Constants.CHANNEL channel = ConnectionHandler.getInstance().getUserCommunicationChannelType();
        String currentDeviceAddress = mBluetoothHandler.getCurrentDevice()==null?null:mBluetoothHandler.getCurrentDevice().getAddress();
        if(!address.equals(currentDeviceAddress)) {
            mBluetoothHandler.setCurrentDevice(address);
        }
        if(channel == BT){
            if(isBTConnectedToDevice(address))
                return;
            mBluetoothHandler.connectDevice(intent, true);

        }else if(channel == TCP){
            //do discovery using bt and dc bt thereafter.
            String ip = getIpForBTAddress(address);
            if(ip!=null)
                startTCPClient(ip);
            else{
                if(mTCPClientService!=null)
                        mTCPClientService.setHostAddress(null);
                connectBT(intent);
            }

        }
    }


    public boolean isBTConnected(){

        if (BluetoothHandler.getInstance() != null && BluetoothHandler.getInstance().getBluetoothClientService() != null &&
                BluetoothHandler.getInstance().getBluetoothClientService().getState()== BluetoothClientService.STATE_CONNECTED){
                return true;
        }
        return false;
    }

    private boolean isBTConnectedToDevice(String devAddress){

        if (BluetoothHandler.getInstance() != null && BluetoothHandler.getInstance().getBluetoothClientService() != null &&
                BluetoothHandler.getInstance().getBluetoothClientService().getState()== BluetoothClientService.STATE_CONNECTED){
            String currentDevAddress = BluetoothHandler.getInstance().getLastConnectedDeviceAddress();
            if(devAddress.equals(currentDevAddress))
                return true;

        }
        return false;
    }

    boolean mHasUserDisconnectedConnection;
    public boolean hasUserDisconnectedConnection(){
        return mHasUserDisconnectedConnection;
    }
    public void setHasUserDisconnectedConnection(boolean hasUserDisconnectedConnection){
        mHasUserDisconnectedConnection = hasUserDisconnectedConnection;
    }

    public void disconnect(){

        disconnectBT();
        stopTCPClient();
    }

    private void connectBT(Intent intent) {
        BluetoothHandler.getInstance().connectDevice(intent, true);
    }

    public void disconnectBT() {
        BluetoothHandler.getInstance().stopBTService();
    }

    public void stopTCPClient() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (mTCPClientService != null)
                    mTCPClientService.stopTCPClient();
            }
        }).start();
    }

    public static volatile TcpClientService mTCPClientService;
    //"192.168.43.1"
    public void startTCPClient(String tcpHost){

        try {
            if(tcpHost==null||tcpHost.equals("0.0.0.0"))
                return;
            if (mTCPClientService != null && mTCPClientService.getHostAddress() != null && mTCPClientService.getHostAddress().equals(tcpHost)){
//                    && (mTCPClientService.getState() == TcpClientService.STATE_CONNECTING ||
//                    mTCPClientService.getState() == TcpClientService.STATE_CONNECTED)) {
                ExpLogger.logE(TAG, "TCP already connected/connecting");
                return;
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        HandlerManager.getInstance().initTCPHandlers();
                    } catch (Exception e) {
                        ExpLogger.logE(TAG, e.getMessage());
                    }
                }
            }).start();
            Handler tcpHandler = HandlerManager.getInstance().getClientTCPHandler();
            if(mTCPClientService!=null) {
                mTCPClientService.stopTCPClient();
                mTCPClientService.setHostAddress(tcpHost);
            }
            else {
                mTCPClientService = new TcpClientService(tcpHost, tcpHandler);
                mTCPClientService.start();
            }
        } catch (Exception e) {
//                mTCPClientService = null;
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    public void connectToIp(String ipAddress){
        if(mTCPClientService!=null) {
            mTCPClientService.setHostAddress(ipAddress);
        }else{
            startTCPClient(ipAddress);
        }
    }

    public TcpMobServer mTCPMobServerConnection = null;
    void startTCPMobServer(){

        if(mTCPMobServerConnection==null)
            mTCPMobServerConnection = new TcpMobServer((Context)MainApplication.getInstance().getCurrentActivity(), HandlerManager.getInstance().getTCPMobServerHandler());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    ExpLogger.logI(TAG, "mTCPMobServerConnection.start()");
                    mTCPMobServerConnection.startBTServer();

                }catch (Exception e){
                    ExpLogger.logE(TAG, e.getMessage());
                }
            }
        }).start();
    }

    private Constants.CHANNEL mUserCommChannel = Constants.CHANNEL.TCP;
    public void setUserCommunicationChannelType(Constants.CHANNEL channel) {

        mUserCommChannel = channel;
    }

    public Constants.CHANNEL getUserCommunicationChannelType() {

        return mUserCommChannel;
    }

    public boolean useTCP(){
        return isTCPEnabled()&&mTCPClientService!=null&&mTCPClientService.getState()==TcpClientService.STATE_CONNECTED;
    }

    public boolean isTCPEnabled(){
        return Constants.CHANNEL.TCP==getUserCommunicationChannelType();
    }

//    public void setTCPEnabled(boolean val){
//        isTCP = val;
//        sendTCPStateReq(val? ActionConstants.ActionCode.TCP_EN:ActionConstants.ActionCode.TCP_DS);
//        if(!val)
//            stopTCPClient();
//    }

    public void sendTCPStateReq(int val){
        BTComm btComm = new BTComm();
        btComm.setActionCode(val);
        ConnectionHandler.getInstance().sendInfo(btComm, BT);
    }

    public String getLastConnectedDeviceName(){
        String devName = SettingsUtils.readPreferenceValueAsString(MainApplication.getInstance().getApplicationContext(), KEY_BT_DEV_NAME);
        if(devName == null)
            devName = "";
        return devName;
    }

    public void handleTCPConnectionChange(int status) {
        switch (status){
            case TcpClientService.STATE_CONNECTED:
                SettingsUtils.writePreferenceValue(MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_TCP_HOST, mTCPClientService.getHostAddress());
                disconnectBT();
                break;


            case TcpClientService.STATE_LISTEN:
            case TcpClientService.STATE_NONE:
                //tcp prob failed
                if(!isBTConnected()&&!ConnectionHandler.getInstance().hasUserDisconnectedConnection())
                {
                    try {
                        if(BluetoothHandler.getInstance().getCurrentDevice()==null)
                            return;
                        String currentDeviceAddress = BluetoothHandler.getInstance().getCurrentDevice().getAddress();
                        if(isBTConnectedToDevice(currentDeviceAddress))
                            return;
                        Intent intent = new Intent();
                        intent.putExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS, currentDeviceAddress);
                        BluetoothHandler.getInstance().connectDevice(intent, true);
                    }catch (Exception e){
                        ExpLogger.logException(TAG, e);
                    }
                }
                break;
        }
    }

    /**
     * * send the information as a json through BT.
     * @param pref pref to send
     */
    public void sendInfo(SyncPreferences pref){
        BTComm btComm = new BTComm();
        btComm.setSyncPreferences(pref);
        sendInfo(btComm);
    }
    public void sendInfo(final BTComm btComm){

        sendInfo(btComm, ConnectionHandler.getInstance().useTCP()? Constants.CHANNEL.TCP:Constants.CHANNEL.BT);
    }
    public void sendInfo(final BTComm btComm, final Constants.CHANNEL channel){

        if(!MainApplication.getInstance().isCommunicationChannelOpen()) {
            ExpLogger.logD(TAG, "send info: CommunicationChannel is closed");
            return;
        }

        try {
            Runnable r = new Runnable() {
                @Override
                public void run() {

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    String command = gson.toJson(btComm);
                    boolean msgSent = false;
                    if (channel == Constants.CHANNEL.BT && BluetoothHandler.getInstance() != null)
                        BluetoothHandler.getInstance().sendMessage(command);
                    else {
                        msgSent = sendTCPMessage(command);
                        if (!msgSent && BluetoothHandler.getInstance() != null)
                            BluetoothHandler.getInstance().sendMessage(command);
                    }
                }
            };
            Utils.TaskPriority priority = Utils.getResolvedTaskPriority(btComm.getPriority());
            getHudMessenger().sendMessage(r, priority);


        }catch(Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }
    public void handlePairingRequest(BluetoothDevice device, Intent intent){
        if(intent == null)
            return;
        String action = intent.getAction();
        if (!BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action)) {
            return;
        }
        boolean val = device.setPairingConfirmation(true);
        int bondState = intent.getIntExtra(EXTRA_BOND_STATE, BOND_NONE);
        int prevBondState = intent.getIntExtra(EXTRA_PREVIOUS_BOND_STATE, BOND_NONE);
        ExpLogger.logI(TAG, "mBondReceiver: bondState: " + bondState);
    }
    public boolean sendTCPMessage(String command){
        if(ConnectionHandler.getInstance().useTCP()) {
            ConnectionHandler.getInstance().mTCPClientService.sendMessage(command);
            ExpLogger.logI(TAG, "TCP Message sent\n\n" + command);
            return true;
        }
        else {
            MainApplication.getInstance().showToast("No TCP connection");
            ExpLogger.logI(TAG, "TCP not connected.");
            return false;
        }
    }
}
