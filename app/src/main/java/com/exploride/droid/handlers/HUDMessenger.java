package com.exploride.droid.handlers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.cloudant.exploridemobile.framework.Utils;
import com.exploride.droid.MainApplication;
import com.exploride.droid.logger.ExpLogger;

import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import com.cloudant.exploridemobile.framework.Utils.TaskPriority;

/**
 * Created by peeyushupadhyay on 30/10/17.
 */

public class HUDMessenger {

    private static String TAG = "HUDMessenger";
    int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private volatile PriorityBlockingQueue mPriorityQueue;
    private volatile ExecutorService mExecutor;
    CallbackImpl mCallbackImpl;
    private static Object initLock = new Object();

    public void init(){

        synchronized (initLock) {
            if(mExecutor!=null)
                return;
            ExpLogger.logD(TAG, "HUDMessenger.init");
            ExpLogger.logI(TAG, "NUMBER_OF_CORES:" + NUMBER_OF_CORES);
            mPriorityQueue = new PriorityBlockingQueue<TaskRunnable>();
            mCallbackImpl = new CallbackImpl();
            mExecutor = new ThreadPoolExecutor(
                    3, NUMBER_OF_CORES,
                    60L, TimeUnit.SECONDS,
                    mPriorityQueue, new CustomThreadFactory()
            );
            registerAppStop();
        }
    }
    class AppExitBroadcast extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            ExpLogger.logD(TAG, "HUDMessenger.AppExitBroadcast");
        }
    }
    void registerAppStop(){

        String appStopAction = MainApplication.getInstance().getPackageName()+"-app-stop";
        IntentFilter intentFilter = new IntentFilter(appStopAction);
        LocalBroadcastManager.getInstance(MainApplication.getInstance().getApplicationContext()).registerReceiver(new AppExitBroadcast(), intentFilter);
    }

    private class CallbackImpl implements Callback{

        @Override
        public void complete() {
        }
    }
    private interface Callback{
        void complete();
    }

    private class TaskRunnable implements Comparable, Runnable{

        Runnable mRunnable;
        TaskPriority mPriority;
        Callback mCallback;
        final long seqNum;
        TaskRunnable(Runnable runnable, TaskPriority priority, Callback callback){
            mRunnable = runnable;
            mPriority = priority;
            mCallback = callback;
            seqNum = seq.getAndIncrement();
            if(mCallback==null)
                throw new NullPointerException("Callback cannot be null in HUDMessenger.TaskRunnable");
        }

        public void run() {
            try {
                mRunnable.run();
            }catch(Exception e){
                ExpLogger.logE(TAG, e.getMessage());
            }
            mCallback.complete();
        }

        @Override
        public int compareTo(@NonNull Object o) {
            if(this.mPriority.getVal()>((TaskRunnable)o).mPriority.getVal())
                return -1;
            if(this.mPriority.getVal()<((TaskRunnable)o).mPriority.getVal())
                return 1;
            if((o) == this)
                return 0;
            return (seqNum < ((TaskRunnable)o).seqNum ? -1 : 1);
        }
    }
    static final AtomicLong seq = new AtomicLong(0);

    private static class ComparatorTask<T extends TaskRunnable> implements Comparator<T> {

        @Override
        public int compare(T o1, T o2) {
            return o1.compareTo(o2);
        }
    }
    class CustomThreadFactory implements ThreadFactory {
        public Thread newThread(Runnable r) {
            ExpLogger.logD(TAG, "internal MyThreadFactory newThread");
            Thread t = new Thread(r);
            t.setName("CTF-"+t.getId());
            t.setPriority(Thread.NORM_PRIORITY);
            return t;
        }
    }

    //blocking. should not be called on the main thread.
    private void close(){
        try {
            //TODO: find the right thread for the queue
//            if(MainPresenter.getInstance().isBGThread())
//                Thread.currentThread().interrupt();
            ExpLogger.logD(TAG, "HUDMessenger.close");
            if(mExecutor!=null && !mExecutor.isShutdown()) {
                mExecutor.shutdown();
                mExecutor.awaitTermination(5, TimeUnit.SECONDS);
                if (!mExecutor.isShutdown())
                    mExecutor.shutdownNow();
            }
        } catch (Exception e) {
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    public void sendMessage(Runnable runnable){
        sendMessage(runnable, TaskPriority.Medium);
    }
    public void sendMessage(Runnable runnable, TaskPriority priority){
            try {
                if (mExecutor == null) {
                    init();
                }
                TaskRunnable taskRunnable = new TaskRunnable(runnable, priority, mCallbackImpl);
                mExecutor.execute(taskRunnable);
            } catch (Exception e) {
                ExpLogger.logE(TAG, e.getMessage());
            }
    }

}
