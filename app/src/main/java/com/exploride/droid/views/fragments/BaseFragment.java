package com.exploride.droid.views.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.exploride.droid.views.activities.MainActivity;

import java.lang.reflect.Field;

/**
 * Base fragment class for defining base functions
 */
public abstract class BaseFragment extends Fragment {

    OnTitleChangedListener titleChangedListener;

    private static String TAG = "BaseFragment";
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        titleChangedListener = (MainActivity)getActivity();
        if (view == null)
            view = inflater.inflate(getFragmentLayout(), container, false);

        return view;

    }

    /**
     * Every fragment has to inflate a layout in the onCreateView method. We have added this method to
     * avoid duplicate all the inflate code in every fragment. You only have to return the layout to
     * inflate in this method when extends BaseFragment.
     */
    protected abstract int getFragmentLayout();

    public abstract void onBackPressed();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        //the child FragmentManager ends up with a broken internal state when it is detached from the activity.
        // A short-term workaround that fixed it for me is to add the following to onDetach() of every Fragment
        // which you call getChildFragmentManager() on
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public interface OnTitleChangedListener{
        void onSetTitleText(String title);
    }

}
