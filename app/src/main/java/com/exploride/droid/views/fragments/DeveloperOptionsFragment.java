package com.exploride.droid.views.fragments;

import android.os.Bundle;
import android.os.PatternMatcher;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.DevOptions;
import com.cloudant.exploridemobile.framework.FlipScreen;
import com.cloudant.exploridemobile.framework.ScreenScaling;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.services.TcpClientService;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.LocationMockerPhone;
import com.exploride.droid.utils.Utils;
import com.exploride.droid.utils.appupdater.ExplorideUpdateChecker;

import java.text.DecimalFormat;
import java.util.Arrays;

import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.SC_NAME;
import static com.exploride.droid.handlers.ConnectionHandler.mTCPClientService;
import static com.exploride.droid.utils.Constants.CHANNEL.TCP;

public class DeveloperOptionsFragment extends BaseFragment {

    private static final String TAG = DeveloperOptionsFragment.class.getSimpleName();
    private DeveloperOptionsFragmentListener fragmentListener;

    public static String[] offsetRange = {"-150", "-140", "-130", "-120", "-110", "-100", "-90", "-80", "-70", "-60", "-50", "-40", "-30", "-20", "-10", "0"
            , "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "110", "120", "130", "140", "150"};

    public static String[] mockModeRange = {"use default", "use gpx", "use prev route"};

    public static String[] screenScaleRange = {"0.5", "0.55", "0.6", "0.65", "0.7", "0.75", "0.8", "0.85", "0.9", "0.95","1"};

    Button mBtn_testTCP;
    private boolean zoomSpnrTouched = false, tiltSpnrTouched = false, mockIntSpnrTouched = false,
            mockIntPhSpnrTouched = false, scaleScrSpnrTouched = false, xOffsetSpnrTouched = false,
            yOffsetSpnrTouched = false, isVideoPlaying = false;

    private ImageView playPause;
    private double zoom;
    DecimalFormat df = new DecimalFormat("#.###");
    private TextView tvZoom, tvScName;
    private EditText etSCName;
    private LinearLayout scNameContainer;
    private boolean edittingSCName = false;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragmentListener = (DeveloperOptionsFragmentListener)getActivity();
    }
    BluetoothHandler.BTState mBTStateReceiver;

    @Override
    public void onResume() {
        super.onResume();
        MainApplication.getInstance().registerBTStateListener(mBTStateReceiver);
    }

    @Override
    public void onPause(){
        super.onPause();
        try {
            MainApplication.getInstance().unRegisterBTStateListener(mBTStateReceiver);
        }catch (IllegalArgumentException e){}

    }
    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Switch switchNavigationView = (Switch) view.findViewById(R.id.swtch_navigation_view);
        switchNavigationView.setChecked(SettingsUtils.readPreferenceValueAsBoolean(getActivity(), Constants.KEY_BASE + Constants.KEY_NAVIGATION_VIEW));
        switchNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Save boolean for navigation view in phone as key/value pair.
                SettingsUtils.writePreferenceValue(getActivity(), Constants.KEY_BASE + Constants.KEY_NAVIGATION_VIEW,
                        switchNavigationView.isChecked());
            }
        });

        final Switch switchShowRouteOnly = (Switch) view.findViewById(R.id.swtch_show_route_only);
        switchShowRouteOnly.setChecked(SettingsUtils.readPreferenceValueWithDefault(getActivity(), Constants.KEY_BASE + Constants.SharedPrefKeys.KEY_SHOW_ROUTE_ONLY, true));
        switchShowRouteOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainPresenter.getInstance().handleShowRouteOnly(switchShowRouteOnly.isChecked());
                //Save boolean for navigation view in phone as key/value pair.
                SettingsUtils.writePreferenceValue(getActivity(), Constants.KEY_BASE + Constants.SharedPrefKeys.KEY_SHOW_ROUTE_ONLY,
                        switchShowRouteOnly.isChecked());
            }
        });

        Button updates_btn = (Button) view.findViewById(R.id.updates_btn);
        updates_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExplorideUpdateChecker.getInstance().checkForUpdatesAvailable(true);
            }
        });
        Button emailButton = (Button) view.findViewById(R.id.email_btn);
        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BTComm btComm = new BTComm();
                btComm.setActionCode(ActionConstants.ActionCode.EmailLogs);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });

        Button test_bt_conns = (Button) view.findViewById(R.id.test_bt_conns);
        test_bt_conns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothHandler.getInstance().runStressTest();
            }
        });

        Button testBt = (Button) view.findViewById(R.id.test_bt);
        testBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = "this is a test message";
                if (!ConnectionHandler.getInstance().isBTConnected()) {
                    MainApplication.getInstance().showToast(getString(R.string.not_connected));
                    return;
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        BluetoothHandler.getInstance().sendMessage(message);
                    }
                }).start();
            }
        });
        Button testWifi = (Button) view.findViewById(R.id.test_wifi);
        testWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MainApplication.getInstance().isCommunicationChannelOpen()) {
                    BTComm btCommResp = new BTComm();
                    btCommResp.setActionCode(ActionConstants.ActionCode.CheckInternet);
                    ConnectionHandler.getInstance().sendInfo(btCommResp);
                }
                else {
                    if (getActivity() != null)
                        Toast.makeText(getActivity(), getActivity().getString(R.string.not_connected), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button updateTimeInHUD = (Button) view.findViewById(R.id.update_time_hud);
        updateTimeInHUD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BTComm btComm = new BTComm();
                MainPresenter.getInstance().getDeviceTimezone(btComm);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });
        Button connect_obd = (Button) view.findViewById(R.id.connect_obd);
        connect_obd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BTComm btComm = new BTComm();
                btComm.setActionCode(ActionConstants.ActionCode.CONNECT_OBD);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });
        final Switch obdConnChn = (Switch) view.findViewById(R.id.obd_conn_chn);
        boolean obdIsBt = false; //serial by def.
        int val = SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_OBD_CHN);
        if(val>1)//if val ==-1 or 1, use serial else bt.
            obdIsBt = true;
        obdConnChn.setChecked(obdIsBt);
        obdConnChn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = obdConnChn.isChecked();
                //Save boolean for navigation view in phone as key/value pair.
                SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_OBD_CHN,
                        isChecked?2:1);
                BTComm btComm = new BTComm();
                btComm.setData(""+(isChecked?2:1));
                btComm.setActionCode(ActionConstants.ActionCode.OBD_CONN_CHN);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });


        final Switch sw_toggleTCP = (Switch) view.findViewById(R.id.toggle_tcp);
        sw_toggleTCP.setChecked(ConnectionHandler.getInstance().isTCPEnabled());
//        sw_toggleTCP.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                SettingsUtils.writePreferenceValue(getActivity(),
//                        Constants.SharedPrefKeys.KEY_DEVOPTIONS_STATE_TCP, sw_toggleTCP.isChecked());
//                mBtn_testTCP.setText(sw_toggleTCP.isChecked()?"Enabling TCP":"Disabling TCP");
//                ConnectionHandler.getInstance().setTCPEnabled(sw_toggleTCP.isChecked());
//
//            }
//        });
        sw_toggleTCP.setVisibility(View.GONE);
        mBtn_testTCP = (Button) view.findViewById(R.id.test_tcp);
        String txt = "TCP Not Connected";
        if(mTCPClientService==null)
            txt = "TCP Not Connected";
        else if(mTCPClientService.getState()== TcpClientService.STATE_CONNECTED)
            txt = "TCP'ed to "+mTCPClientService.getDualAddress()+" ("+BluetoothHandler.getInstance().getLastConnectedDeviceName()+")";
        else if (mTCPClientService.getState()== TcpClientService.STATE_CONNECTING)
            txt = "Connecting to "+mTCPClientService.getDualAddress()+" ("+BluetoothHandler.getInstance().getLastConnectedDeviceName()+")";
        mBtn_testTCP.setText(txt);
        mBtn_testTCP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String msg2 = "hello, how is your day?";
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ConnectionHandler.getInstance().sendTCPMessage(msg2);
                    }
                }).start();
            }
        });

//        emailButton.setVisibility(View.GONE);

        final Switch sendLocationFromPhone = (Switch) view.findViewById(R.id.send_location_from_phone);
        sendLocationFromPhone.setChecked(MainApplication.getInstance().shouldSendLocationFromPhone);
        sendLocationFromPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.toggleLocPhone(sendLocationFromPhone.isChecked());
            }
        });

        final Switch sendDestinationOnly = (Switch) view.findViewById(R.id.swtch_send_destPoint);
        sendDestinationOnly.setChecked(SettingsUtils.readPreferenceValueAsBoolean(getActivity(),
                Constants.SharedPrefKeys.KEY_DEVOPTIONS_DEST_PT));
        sendDestinationOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsUtils.writePreferenceValue(getActivity(),Constants.SharedPrefKeys.KEY_DEVOPTIONS_DEST_PT,
                        sendDestinationOnly.isChecked());
            }
        });

        final Switch switchTopNavigationView = (Switch) view.findViewById(R.id.swtch_top_navigation_view);
        switchTopNavigationView.setChecked(SettingsUtils.readPreferenceValueAsBoolean(getActivity(), Constants.KEY_BASE + Constants.KEY_TOP_NAVIGATION_VIEW_HUD));
        switchTopNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = switchTopNavigationView.isChecked();
                //Save boolean for navigation view in phone as key/value pair.
                SettingsUtils.writePreferenceValue(getActivity(), Constants.KEY_BASE + Constants.KEY_TOP_NAVIGATION_VIEW_HUD, isChecked);
                fragmentListener.onSendTopMapViewMode(isChecked);
            }
        });

        final Switch switchEnableDebugMode = (Switch) view.findViewById(R.id.enable_debug_mode);
        switchEnableDebugMode.setChecked(MainApplication.getInstance().isDebugModeOn());
        switchEnableDebugMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = switchEnableDebugMode.isChecked();
                //Save boolean for navigation view in phone as key/value pair.
                SettingsUtils.writePreferenceValue(getActivity(), Constants.KEY_BASE + Constants.KEY_ENABLE_DEBUG_MODE, isChecked);

                MainApplication.getInstance().setDebugModeOn(isChecked);

                fragmentListener.sendDebugMode(isChecked);
            }
        });

        final Switch switchFlipScreen = (Switch) view.findViewById(R.id.flip_screen);
        switchFlipScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = switchFlipScreen.isChecked();
                //Save boolean for navigation view in phone as key/value pair.
                SettingsUtils.writePreferenceValue(getActivity(), Constants.KEY_BASE +
                        Constants.SharedPrefKeys.KEY_DEVOPTIONS_FLIP_SCREEN, isChecked);

                BTComm btComm = new BTComm();
                FlipScreen flipScreen = new FlipScreen();
                flipScreen.setFlipScreenXValue(isChecked);
                btComm.setFlipScreen(flipScreen);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });

        switchFlipScreen.setChecked(SettingsUtils.readPreferenceValueAsBoolean(getActivity(), Constants.KEY_BASE +
                Constants.SharedPrefKeys.KEY_DEVOPTIONS_FLIP_SCREEN));

        final Spinner zoomSpinner = (Spinner) view.findViewById(R.id.zoom_value);
        Spinner tiltSpinner = (Spinner) view.findViewById(R.id.tilt_value);
        populateZoomSpinner(zoomSpinner);
        populateTiltSpinner(tiltSpinner);

        final Switch switchEnableMock = (Switch) view.findViewById(R.id.swtch_enable_mock);
        switchEnableMock.setChecked(SettingsUtils.readPreferenceValueAsBoolean(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_ALLOW_MOCK));
        switchEnableMock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = switchEnableMock.isChecked();
                //Save boolean for navigation view in phone as key/value pair.
                SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_ALLOW_MOCK, isChecked);

                DevOptions devOptions = new DevOptions();

                DevOptions.AllowMockLocation allowMockLocation = new DevOptions().new AllowMockLocation();
                allowMockLocation.setEnable(isChecked);
                devOptions.setAllowMockLocations(allowMockLocation);

                BTComm btComm = new BTComm();
                btComm.setDevOptions(devOptions);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });

        Spinner intervalSpinner = (Spinner) view.findViewById(R.id.interval_value);
        populateIntervalSpinner(intervalSpinner);


        final Switch switchEnablePhoneMocking = (Switch) view.findViewById(R.id.switch_enable_phone_mock);
        switchEnablePhoneMocking.setChecked(LocationMockerPhone.getInstance().shouldMockPhoneLocations());
        switchEnablePhoneMocking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = switchEnablePhoneMocking.isChecked();
                SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_ALLOW_PHONE_MOCK, isChecked);

                LocationMockerPhone.getInstance().setMockPhoneLocations(isChecked);
                LocationMockerPhone.getInstance().handlePhoneMockingStatusChange(isChecked);
            }
        });

        Spinner spinner_mock_mode = (Spinner) view.findViewById(R.id.spinner_mock_mode);
        populateMockModeSpinner(spinner_mock_mode);

        Spinner phoneMockIntervalSpinner = (Spinner) view.findViewById(R.id.phone_mock_interval_value);
        populatePhoneMockIntervalSpinner(phoneMockIntervalSpinner);

        Spinner phoneMockRepeatSpinner = (Spinner) view.findViewById(R.id.spinner_mock_repeat);
        populatePhoneMockRepeatSpinner(phoneMockRepeatSpinner);

        Spinner scaleScreenSpinner = (Spinner) view.findViewById(R.id.screen_scale_spinner);
        populateScaleScreenSpinner(scaleScreenSpinner);

        Spinner xOffsetSpinner = (Spinner) view.findViewById(R.id.x_offset_spinner);
        populateXOffsetSpinner(xOffsetSpinner);

        Spinner yOffsetSpinner = (Spinner) view.findViewById(R.id.y_offset_spinner);
        populateYOffsetSpinner(yOffsetSpinner);

        ImageView playNext = (ImageView) view.findViewById(R.id.video_play_next);
        playNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainApplication.getInstance().isCommunicationChannelOpen()) {
                    updatePlayPauseStatus(true);
                    BTComm btComm = new BTComm();
                    btComm.setActionCode(ActionConstants.ActionCode.VIDEO_NEXT);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
            }
        });

        playPause = (ImageView) view.findViewById(R.id.video_play_pause);
        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (MainApplication.getInstance().isCommunicationChannelOpen()) {

                    int actionCode;

                    // Playing (Pause) is shown. So show play icon to play again
                    if(isVideoPlaying)
                        actionCode = ActionConstants.ActionCode.VIDEO_PAUSE;
                    else
                        actionCode = ActionConstants.ActionCode.VIDEO_PLAY;

                    updatePlayPauseStatus(!isVideoPlaying);
                    BTComm btComm = new BTComm();
                    btComm.setActionCode(actionCode);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
            }
        });

        ImageView playPrev = (ImageView) view.findViewById(R.id.video_play_prev);
        playPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainApplication.getInstance().isCommunicationChannelOpen()) {
                    updatePlayPauseStatus(true);

                    BTComm btComm = new BTComm();
                    btComm.setActionCode(ActionConstants.ActionCode.VIDEO_PREV);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
            }
        });

        final Switch swtchToggleVideo = (Switch) view.findViewById(R.id.swtch_toggle_video);
        swtchToggleVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainApplication.getInstance().isCommunicationChannelOpen()) {

                    updatePlayPauseStatus(swtchToggleVideo.isChecked());

                    BTComm btComm = new BTComm();
                    btComm.setActionCode(swtchToggleVideo.isChecked() == true ? ActionConstants.ActionCode.VIDEO_START :
                            ActionConstants.ActionCode.VIDEO_EXIT);
                    ConnectionHandler.getInstance().sendInfo(btComm);

                    if (getActivity() != null)
                        SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PLAY_VIDEOS_ON_START,
                                swtchToggleVideo.isChecked());
                }
            }
        });
        if (getActivity() != null)
            swtchToggleVideo.setChecked(SettingsUtils.readPreferenceValueAsBoolean(getActivity(),
                    Constants.SharedPrefKeys.KEY_DEVOPTIONS_PLAY_VIDEOS_ON_START));

        final Switch swtchVerifyTextSizes = (Switch) view.findViewById(R.id.swtch_verify_textsizes);
        swtchVerifyTextSizes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainApplication.getInstance().isCommunicationChannelOpen()) {
                    BTComm btComm = new BTComm();
                    btComm.setActionCode(swtchVerifyTextSizes.isChecked() ? ActionConstants.ActionCode.VERIFY_TEXT_SIZES_SHOW :
                            ActionConstants.ActionCode.VERIFY_TEXT_SIZES_HIDE);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
            }
        });

        scNameContainer = (LinearLayout) view.findViewById(R.id.sc_name_container);
        final Switch swtchSCType = (Switch) view.findViewById(R.id.swtch_sc_type);
        swtchSCType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                scNameContainer.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        swtchSCType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainApplication.getInstance().isCommunicationChannelOpen()) {
                    BTComm btComm = new BTComm();
                    DevOptions devOptions = new DevOptions();
                    DevOptions.SCType scType = devOptions.new SCType();
                    scType.setBluClick(!swtchSCType.isChecked());
                    devOptions.setScType(scType);
                    btComm.setDevOptions(devOptions);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_SC_TYPE,
                            !swtchSCType.isChecked());
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
            }
        });
        swtchSCType.setChecked(!SettingsUtils.readPreferenceValueWithDefault(getActivity(),
                Constants.SharedPrefKeys.KEY_SC_TYPE, true));

        final Switch swtchTextUtilisation = (Switch) view.findViewById(R.id.swtch_text_utilisation);
        swtchTextUtilisation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainApplication.getInstance().isCommunicationChannelOpen()) {
                    BTComm btComm = new BTComm();
                    btComm.setActionCode(swtchTextUtilisation.isChecked() ? ActionConstants.ActionCode.TEXT_UTILISATION_SHOW:
                            ActionConstants.ActionCode.TEXT_UTILISATION_HIDE);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
            }
        });


        final Switch swtchPauseNavigation = (Switch) view.findViewById(R.id.swtch_navigation);
        swtchPauseNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainApplication.getInstance().isCommunicationChannelOpen()) {
                    BTComm btComm = new BTComm();
                    btComm.setActionCode(swtchPauseNavigation.isChecked() ? ActionConstants.ActionCode.NAVIGATION_PAUSE:
                            ActionConstants.ActionCode.NAVIGATION_RESUME);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
            }
        });

        // Test images
        ImageView next, prev;
        final Switch swtchToggleImage = (Switch) view.findViewById(R.id.swtch_toggle_image);
        swtchToggleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BTComm btComm = new BTComm();
                btComm.setActionCode(swtchToggleImage.isChecked() == true ? ActionConstants.ActionCode.IMAGE_SHOW :
                        ActionConstants.ActionCode.IMAGE_HIDE);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });

        next = (ImageView) view.findViewById(R.id.image_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BTComm btComm = new BTComm();
                btComm.setActionCode(ActionConstants.ActionCode.IMAGE_NEXT);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });
        prev = (ImageView) view.findViewById(R.id.image_prev);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BTComm btComm = new BTComm();
                btComm.setActionCode(ActionConstants.ActionCode.IMAGE_PREV);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });

        final EditText etZoomInterval = (EditText) view.findViewById(R.id.et_zoom_interval) ;
        tvZoom = (TextView) view.findViewById(R.id.tv_zoom);

        Button btnZoomOut = (Button) view.findViewById(R.id.btn_zoom_out);
        btnZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etZoomInterval.getText().toString()) || (Double.parseDouble(etZoomInterval.getText().toString()) > 20) || (Double.parseDouble(etZoomInterval.getText().toString()) < 0)
                        || (zoom - Double.parseDouble(etZoomInterval.getText().toString())) < 0)
                    return;

                if (zoom == 0)
                    zoom = Math.abs(Double.parseDouble(zoomSpinner.getSelectedItem().toString()) - Double.parseDouble(etZoomInterval.getText().toString()));
                else
                    zoom = Math.abs(zoom - Double.parseDouble(etZoomInterval.getText().toString()));

                DevOptions devOptions = new DevOptions();
                zoom = Double.valueOf(df.format(zoom));
                devOptions.setZoomLevel(zoom);

                tvZoom.setText("" + zoom);

                ExpLogger.logD(TAG, "Zoom Level: " + zoom);

                BTComm btComm = new BTComm();
                btComm.setDevOptions(devOptions);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });



        Button btnZoomIn = (Button) view.findViewById(R.id.btn_zoom_in);
        btnZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etZoomInterval.getText().toString()) || (Double.parseDouble(etZoomInterval.getText().toString()) > 20) || (Double.parseDouble(etZoomInterval.getText().toString()) < 0)
                        || (zoom + Double.parseDouble(etZoomInterval.getText().toString())) > 20)
                    return;

                if (zoom == 0)
                    zoom = Double.parseDouble(zoomSpinner.getSelectedItem().toString()) + Double.parseDouble(etZoomInterval.getText().toString());
                else
                    zoom = zoom + Double.parseDouble(etZoomInterval.getText().toString());

                DevOptions devOptions = new DevOptions();
                zoom = Double.valueOf(df.format(zoom));
                devOptions.setZoomLevel(zoom);

                tvZoom.setText("" + zoom);

                ExpLogger.logD(TAG, "Zoom Level: " + zoom);

                BTComm btComm = new BTComm();
                btComm.setDevOptions(devOptions);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });

        etSCName = (EditText) view.findViewById(R.id.et_sc_name);
        etSCName.setText(MainPresenter.getInstance().getScName());
        etSCName.setSelection(etSCName.getText().length());
        tvScName = (TextView) view.findViewById(R.id.tv_sc_name);
        tvScName.setText(MainPresenter.getInstance().getScName());
        final Button saveName = (Button) view.findViewById(R.id.save_name);
        saveName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edittingSCName == false){
                    etSCName.setVisibility(View.VISIBLE);
                    tvScName.setVisibility(View.GONE);
                    saveName.setText("Save");
                }
                else{
                    etSCName.setVisibility(View.GONE);
                    tvScName.setVisibility(View.VISIBLE);
                    saveName.setText("Edit");

                    StringBuilder scNameStr = new StringBuilder(etSCName.getText().toString());
                    tvScName.setText(scNameStr);
                    if(scNameStr.toString().trim().isEmpty())
                        return;

                    if (!scNameStr.toString().contains(Constants.SC_NAME))
                        scNameStr.insert(0, Constants.SC_NAME);
                    BTComm btComm = new BTComm();
                    MainPresenter.getInstance().setScName(scNameStr.toString());
                    btComm.setActionCode(SC_NAME);
                    btComm.setData(scNameStr.toString());
                    ConnectionHandler.getInstance().sendInfo(btComm);
                }
                edittingSCName = !edittingSCName;
            }
        });

        final EditText etIpAddress = (EditText) view.findViewById(R.id.et_ip_address);
        view.findViewById(R.id.save_ip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ipAddress = etIpAddress.getText().toString();
                if (Patterns.IP_ADDRESS.matcher(ipAddress).matches())
                    ConnectionHandler.getInstance().connectToIp(ipAddress);
                else
                    Toast.makeText(MainApplication.getInstance(), MainPresenter.getInstance()
                            .getStringResource(R.string.invalid_ip), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Called when play/pause toggled. When next/prev is clicked. When video switch is turned on/off.
     * @param play
     */
    private void updatePlayPauseStatus(boolean play){

        isVideoPlaying = play;
        // Playing (Pause) is shown. So show play icon to play again
        if(isVideoPlaying) {
            playPause.setImageResource(R.drawable.ic_pause_video);
        }
        else{
            playPause.setImageResource(R.drawable.ic_play_video);
        }
    }

    private void populateZoomSpinner(Spinner spinner) {
        final String[] zoomLevels = new String[20];
        for (int i = 0; i < 20; i++) {
            zoomLevels[i] = ""+(i + 1);
        }
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                zoomLevels);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                zoomSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(zoomSpnrTouched) {
                    DevOptions devOptions = new DevOptions();
                    devOptions.setZoomLevel(Double.parseDouble(zoomLevels[position]));

                    zoom = devOptions.getZoomLevel();
                    tvZoom.setText("" + zoom);

                    BTComm btComm = new BTComm();
                    btComm.setDevOptions(devOptions);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_ZOOM_LEVEL,
                            Integer.parseInt(zoomLevels[position]));
                }
                zoomSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_ZOOM_LEVEL) != -1)
            spinner.setSelection(SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_ZOOM_LEVEL) - 1);
        else {
            // Default zoom level is 18 (at index 17)
            int selectionIndex = Arrays.asList(zoomLevels).indexOf("" + (int)Constants.DEFAULT_ZOOM);
            spinner.setSelection(selectionIndex);
        }
    }
    private void populateIntervalSpinner(Spinner spinner) {
        final String[] intervals = {"0.5", "1", "1.5", "2", "2.5", "3", "3.5", "4", "4.5", "5"};
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                intervals);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mockIntSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(mockIntSpnrTouched) {
                    DevOptions devOptions = new DevOptions();
                    devOptions.setMockInterval(Float.parseFloat(intervals[position]));
                    BTComm btComm = new BTComm();
                    btComm.setDevOptions(devOptions);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_MOCK_INTERVAL,
                            position);
                }
                mockIntSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_MOCK_INTERVAL) != -1)
            spinner.setSelection(SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_MOCK_INTERVAL));
    }
    public static final String[] mMockintervals = {"0.005", "0.010", "0.020", "0.030", "0.040", "0.050", "0.1", "0.3", "0.5", "1", "1.5", "2", "2.5", "3", "3.5", "4", "4.5", "5", "8", "10", "12", "15", "18", "20", "25", "30", "35", "40", "50", "60", "100"};

    private void populatePhoneMockIntervalSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                mMockintervals);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mockIntPhSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mockIntPhSpnrTouched) {
                    LocationMockerPhone.getInstance().setPhoneMockIntervalPos(position);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_MOCK_INTERVAL_POS, position);
                }
                mockIntPhSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner.setSelection(LocationMockerPhone.getInstance().getPhoneMockIntervalPos());
    }
    boolean mMockRepeatSpnrTouched = false;
    public static final String[] mMockRepeatCount= {"1", "2", "5", "10", "20", "40", "70", "100", "200", "500", "1000"};

    private void populatePhoneMockRepeatSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                mMockRepeatCount);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mMockRepeatSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mMockRepeatSpnrTouched) {
                    LocationMockerPhone.getInstance().setPhoneMockRepeatPos(position);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_MOCK_REPEAT_POS, position);
                }
                mMockRepeatSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner.setSelection(LocationMockerPhone.getInstance().getPhoneMockRepeatPos());
    }

    private void populateScaleScreenSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                screenScaleRange);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scaleScrSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(scaleScrSpnrTouched) {
                    ScreenScaling screenScaling = new ScreenScaling();
                    screenScaling.setScaleRate(screenScaleRange[position]);
                    BTComm btComm = new BTComm();
                    btComm.setScreenScaling(screenScaling);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_SCREEN_SCALE,
                            position);
                }
                scaleScrSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_SCREEN_SCALE) != -1)
            spinner.setSelection(SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_SCREEN_SCALE));
        else {
            // Default scale is 0.75 at index 5
            int selectionIndex = Arrays.asList(screenScaleRange).indexOf("" + (int)Constants.DEFAULT_SCALE);
            spinner.setSelection(selectionIndex);
        }
    }

    private void populateXOffsetSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                offsetRange);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                xOffsetSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(xOffsetSpnrTouched) {
                    ScreenScaling screenScaling = new ScreenScaling();
                    screenScaling.setOffsetX(offsetRange[position]);
                    BTComm btComm = new BTComm();
                    btComm.setScreenScaling(screenScaling);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_X_OFFSET,
                            position);
                }
                xOffsetSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_X_OFFSET) != -1)
            spinner.setSelection(SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_X_OFFSET));
        else {
            // default x-offset is 0 (at index 15)
            int selectionIndex = Arrays.asList(offsetRange).indexOf("" + (int)Constants.DEFAULT_X_OFFSET);
            spinner.setSelection(selectionIndex);
        }
    }

    private void populateYOffsetSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                offsetRange);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                yOffsetSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(yOffsetSpnrTouched) {
                    ScreenScaling screenScaling = new ScreenScaling();
                    screenScaling.setOffsetY(offsetRange[position]);
                    BTComm btComm = new BTComm();
                    btComm.setScreenScaling(screenScaling);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_Y_OFFSET,
                            position);
                }
                yOffsetSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_Y_OFFSET) != -1)
            spinner.setSelection(SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_Y_OFFSET));
        else {
            // default y-offset is -70 (at index 8)
            int selectionIndex = Arrays.asList(offsetRange).indexOf("" + (int)Constants.DEFAULT_Y_OFFSET);
            spinner.setSelection(selectionIndex);
        }
    }

    boolean mockModeTouched =false;
    private void populateMockModeSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                mockModeRange);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mockModeTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(mockModeTouched) {
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_MOCK_MODE,
                            position);
                    LocationMockerPhone.getInstance().setMockMode(position);
                }
                mockModeTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner.setSelection(LocationMockerPhone.getInstance().getMockMode());
    }


    private void populateTiltSpinner(Spinner spinner) {
        final String[] tiltLevels = new String[8];
        for (int i = 0; i < 8; i++) {
            tiltLevels[i] = "" + ((i + 1) * 10);
        }
        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<String>(getActivity(),android.R.layout.simple_dropdown_item_1line,
                tiltLevels);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tiltSpnrTouched = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(tiltSpnrTouched) {
                    DevOptions devOptions = new DevOptions();
                    devOptions.setTiltLevel(Float.parseFloat(tiltLevels[position]));

                    BTComm btComm = new BTComm();
                    btComm.setDevOptions(devOptions);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                    SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_TILT_LEVEL,
                            Integer.parseInt(tiltLevels[position]));
                }
                tiltSpnrTouched = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_TILT_LEVEL) != -1)
            spinner.setSelection((SettingsUtils.readPreferenceValueAsInt(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_TILT_LEVEL)/10) - 1);
        else {
            // Default tilt is 70
            int selectionIndex = Arrays.asList(tiltLevels).indexOf("" + (int)Constants.DEFAULT_TILT);
            spinner.setSelection(selectionIndex);
        }
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_developer_options;
    }

    @Override
    public void onBackPressed() {
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    public interface DeveloperOptionsFragmentListener {
        void onSendTopMapViewMode(boolean isTopMapViewMode);

        void sendDebugMode(boolean isDebugModeEnabled);
    }

    @Override
    public void onStart() {
        super.onStart();

        mBTStateReceiver = new BluetoothHandler.BTState(){

            @Override
            public void BTConnected(Constants.CHANNEL channel) {
                if(channel== TCP)
                    DeveloperOptionsFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mBtn_testTCP.setText(ConnectionHandler.getInstance().useTCP()?"TCP'ed to "+mTCPClientService.getDualAddress()+" ("+BluetoothHandler.getInstance().getLastConnectedDeviceName()+")":"TCP Not Connected");
                        }
                    });
            }

            @Override
            public void BTConnectedToDevice(final String deviceName, Constants.CHANNEL channel) {
            }

            @Override
            public void BTConnecting(Constants.CHANNEL channel) {
                if(channel== TCP)
                    DeveloperOptionsFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mBtn_testTCP.setText("Connecting to "+mTCPClientService.getDualAddress());
                        }
                    });
            }

            @Override
            public void BTNotConnected(Constants.CHANNEL channel) {

                if (channel == TCP) {
                    DeveloperOptionsFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mBtn_testTCP.setText(ConnectionHandler.getInstance().useTCP()?"TCP'ed to "+mTCPClientService.getDualAddress()+" ("+BluetoothHandler.getInstance().getLastConnectedDeviceName()+")":"TCP Not Connected");
                        }
                    });
                }
            }

            @Override
            public void toastToShow(String msg, Constants.CHANNEL channel) {

            }

            @Override
            public void BTMsgReceived(String msg, Constants.CHANNEL channel){
            }

            @Override
            public void BTMsgSent(String msg, Constants.CHANNEL channel){
            }
        };
    }

}
