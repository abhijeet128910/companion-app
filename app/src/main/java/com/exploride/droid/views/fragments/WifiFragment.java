
package com.exploride.droid.views.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.WifiConnect;
import com.cloudant.exploridemobile.framework.WifiScanResult;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.WifiConnectionContract;
import com.exploride.droid.adapter.BTDevicesAdapter;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.BTModel;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.WifiConnectionPresenter;
import com.exploride.droid.services.BluetoothClientService;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.DividerItemDecoration;
import com.exploride.droid.views.activities.WifiListActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class WifiFragment extends BaseFragment implements WifiConnectionContract.WifiConnectionView{

    private static final String TAG = "WifiFragment";
    private static final int REQUEST_CONNECT_WIFI = 3;
    // Layout Views
    private Button mBtn_TestConnection;
    private Switch mSwitch_Connect;
    private Button mBtn_ManualConnect;
    private TextView noDevices;
    BluetoothHandler mBluetoothHandler;
    public static String EXTRA_DEVICE_SSID = "device_address";
    WifiManager mWifiManager;
//    WifiReceiver mWifiReceiver;
    private ImageView signalImage;

    private RecyclerView btDeviceList;
    private List<BTModel> mWifiList;
    private BTDevicesAdapter wifiDevicesAdapter;
    private int mSelectedIndex = -1;
    private ProgressBar progressBar;
    private WifiConnectionContract.WifiConnectionOps presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        mBluetoothHandler = getInstance();
        presenter = WifiConnectionPresenter.getInstance();
        presenter.init(this);

        mWifiList = new ArrayList<>();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStartView();
        if(getActivity() != null) {
            mWifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);

//            mWifiReceiver = new WifiReceiver();
//            getActivity().registerReceiver(mWifiReceiver, new IntentFilter(
//                    WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            startWifiScan();
        }
    }

    public void startWifiScan()
    {
        ExpLogger.logI(TAG, "startWifiScan()");
        mWifiList.clear();
        showScanningProgress();
//        if(mWifiManager.isWifiEnabled()==false)
//        {
//            mWifiManager.setWifiEnabled(true);
//        }
        sendRequest(ActionConstants.ActionCode.WifiScanRequest);

    }

    public void stopWifiScan()
    {
        ExpLogger.logI(TAG, "stopWifiScan()");
        sendRequest(ActionConstants.ActionCode.WifiStopScan);
    }
    void sendRequest(int actionCode){

        ExpLogger.logI(TAG, "sendScanRequest()");
        BTComm btComm = new BTComm();
        btComm.setActionCode(actionCode);
        ConnectionHandler.getInstance().sendInfo(btComm);

    }
    @Override
    public void onConnectionCompleted(final String ssid) {
        try {
            wifiDevicesAdapter.setBtName(ssid);
            if(getActivity()!=null){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainPresenter.getInstance().hideLoading();
                        wifiDevicesAdapter.notifyDataSetChanged();
                    }
                });
            }

        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    @Override
    public void showToast(String message) {

    }
    public void onScanResultsReceived(final List<WifiScanResult> wifiList){

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                processScanResults(wifiList);
            }
        });

    }

//    class WifiReceiver extends BroadcastReceiver
//    {
//        public void onReceive(Context c, Intent intent) {
//            List<ScanResult> wifiList = mWifiManager.getScanResults();
//            processScanResults(wifiList);
//        }
//    }
    void processScanResults(List<WifiScanResult> wifiList){
        hideScanningProgress();
        ArrayList<String> connections = new ArrayList<String>();
        ArrayList<Float> Signal_Strenth = new ArrayList<Float>();
        try {
            mWifiList.clear();
            for (int i = 0; i < wifiList.size(); i++) {
                WifiScanResult wifiScanResult = wifiList.get(i);
                if(!wifiScanResult.SSID.isEmpty()) {
                    BTModel wifiDevice = new BTModel(wifiScanResult.SSID, "");
                    if(wifiScanResult.isConnected())
                        MainPresenter.getInstance().setWifiName(wifiScanResult.SSID);
                    if (("\"" + wifiList.get(i).SSID + "\"").equalsIgnoreCase(MainPresenter.getInstance().getWifiName())
                            || wifiList.get(i).SSID.equalsIgnoreCase(MainPresenter.getInstance().getWifiName())) {
                        wifiDevicesAdapter.setBtName(wifiList.get(i).SSID);
                    }

                    WifiFragment.this.mWifiList.add(wifiDevice);
                }
            }
            if (WifiFragment.this.mWifiList.size() == 0)
                showNoDevicesText();
            else {
                wifiDevicesAdapter.notifyDataSetChanged();
                hideNoDevicesText();
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG,e);
        }
    }

    private void showNoDevicesText(){
        noDevices.setVisibility(View.VISIBLE);
    }

    private void hideNoDevicesText(){
        noDevices.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
//        if (getActivity() != null) {
//            getActivity().unregisterReceiver(mWifiReceiver);
//        }
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.bluetooth_connect;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        titleChangedListener.onSetTitleText(getResources().getString(R.string.wifi_settings));
        mBtn_TestConnection = (Button) view.findViewById(R.id.testconnection);
        mSwitch_Connect = (Switch) view.findViewById(R.id.connect);
        mSwitch_Connect.setText(MainPresenter.getInstance().getWifiStatusText());
        mSwitch_Connect.setChecked(MainPresenter.getInstance().getWifiName()!=null);
        mBtn_ManualConnect = (Button) view.findViewById(R.id.connect_manual);
        mSwitch_Connect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!mSwitch_Connect.isPressed())
                    return;
                if(mSwitch_Connect.isChecked())
                {
//                    tryBTConnect();
                }else{
//                    if (mBluetoothHandler.getBluetoothClientService() != null) {
//                        mBluetoothHandler.getBluetoothClientService().stopTCPClient();
//                    }
                }
            }
        });
        // Initialize the send button with a listener that for click events
        mBtn_TestConnection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BTComm btCommResp = new BTComm();
                btCommResp.setActionCode(ActionConstants.ActionCode.CheckInternet);
                ConnectionHandler.getInstance().sendInfo(btCommResp);
            }
        });

        Switch mBTOnly = (Switch) view.findViewById(R.id.bt_only);
        mBTOnly.setVisibility(View.GONE);
        mBtn_ManualConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualConnect();
            }
        });
        btDeviceList = (RecyclerView) view.findViewById(R.id.lv_bt_devices);
        btDeviceList.setHasFixedSize(true);
        btDeviceList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        btDeviceList.setItemAnimator(new DefaultItemAnimator());
        progressBar = (ProgressBar) view.findViewById(R.id.scanning_progress);

        initWifiAdapter();
        noDevices = (TextView) view.findViewById(R.id.no_devices_found);
        signalImage = (ImageView) view.findViewById(R.id.signal);
        signalImage.setImageResource(R.drawable.wifi);

        // Get a set of currently paired devices
        onWifiConnected();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_WIFI:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    String ssid = data.getStringExtra(EXTRA_DEVICE_SSID);
                    BTComm btComm = new BTComm();
                    WifiConnect wifiConnect = new WifiConnect();
                    wifiConnect.setSSID(ssid);
                    btComm.setWifiConnect(wifiConnect);
                    ConnectionHandler.getInstance().sendInfo(btComm);
                    MainPresenter.getInstance().hideLoading();
//                    mBluetoothHandler.sendMessage(ssid);
                }
                break;
//            case REQUEST_CONNECT_DEVICE_INSECURE:
//                // When DeviceListActivity returns with a device to connect
//                if (resultCode == Activity.RESULT_OK) {
//                    mBluetoothHandler.connectDevice(data, false);
//                }
//                break;
        }
    }
    public void onWifiConnected(){
        mSwitch_Connect.setChecked(MainPresenter.getInstance().getWifiName()!=null);
        mSwitch_Connect.setText(MainPresenter.getInstance().getWifiStatusText());
    }

    void manualConnect(){
        Intent serverIntent = new Intent(getActivity(), WifiListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_WIFI);
    }

    @Override
    public void onBackPressed() {
        // handle back button
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    private void initWifiAdapter() {
        if (wifiDevicesAdapter == null) {
            wifiDevicesAdapter = new BTDevicesAdapter(getActivity(), mWifiList,
                    new BTDevicesAdapter.OnBTDeviceItemClickListener() {
                        @Override
                        public void setOnItemClick(final BTModel btDevice, final int position) {

                            try {
                                if (mSelectedIndex != position || !btDevice.getBtName().equals(MainPresenter.getInstance().getWifiName())) {
                                    if (MainApplication.getInstance().isCommunicationChannelOpen()) {
                                        mSelectedIndex = position;
                                        MainPresenter.getInstance().showLoading(getActivity().getString(R.string.bt_connecting));
                                        connectToSelectedDevice(btDevice);
                                    }
                                    else{
                                        if(getActivity()!=null)
                                            Toast.makeText(getActivity(), getActivity().getString(R.string.not_connected), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } catch (Exception e) {
                                ExpLogger.logException(TAG, e);
                            }
                        }
                    });
            btDeviceList.setAdapter(wifiDevicesAdapter);
        }
        else
            wifiDevicesAdapter.notifyDataSetChanged();
    }

    private void connectToSelectedDevice(BTModel mSelectedDevice) {
        try {
            BTComm btComm = new BTComm();
            WifiConnect wifiConnect = new WifiConnect();
            wifiConnect.setSSID(mSelectedDevice.getBtName());
            btComm.setWifiConnect(wifiConnect);
            ConnectionHandler.getInstance().sendInfo(btComm);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    private void showScanningProgress(){
        if(progressBar != null){
            progressBar.setVisibility(View.VISIBLE);
        }
    }
    private void hideScanningProgress(){
        if(progressBar != null){
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onStopView();
        stopWifiScan();
    }
}
