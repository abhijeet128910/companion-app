package com.exploride.droid.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.cloudant.exploridemobile.framework.DestinationReached;
import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.R;
import com.exploride.droid.ReachedLocationContract;
import com.exploride.droid.adapter.ChooserArrayAdapter;
import com.exploride.droid.network.VolleySingleton;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.ReachedLocationPresenter;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.FontHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReachedLocationFragment extends BaseFragment implements ReachedLocationContract.ReachedLocationView{

    // Commonly used sharing apps that are shown on screen. Others in dialog list.
    private String[] shareLinks = {"com.facebook.katana","com.facebook.lite", "com.twitter.android",
            "com.google.android.apps.plus", "com.snapchat.android", "com.whatsapp","com.google.android.gm"};
    private LinearLayout shareProviders;
    private ReachedLocationContract.ReachedLocationOps presenter;
    static ReachedLocationFragment reachedLocationFragment;
    private TextView destinationNameText, distanceTimeText, avgSpeedText;
    private ImageView destinationImage;
    private List<String> packages;
    private FrameLayout rootView;
    private TextView locationExists, tripExists;
    private AlertDialog saveLocationDlg, saveTripDlg;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleChangedListener.onSetTitleText("");
        rootView = (FrameLayout) view.findViewById(R.id.root_view);
        presenter = ReachedLocationPresenter.getInstance();
        presenter.init(this);
        TextView reachedHeader = (TextView) rootView.findViewById(R.id.reached_header);
        FontHelper.setFonts(getActivity(), reachedHeader, FontHelper.ROBOTO_BOLD);
        TextView shareWithHeader = (TextView) rootView.findViewById(R.id.share_with_header);
        FontHelper.setFonts(getActivity(), shareWithHeader, FontHelper.ROBOTO_BOLD);
        shareProviders = (LinearLayout) view.findViewById(R.id.share_providers);

        destinationNameText = (TextView) view.findViewById(R.id.destination_name);
        FontHelper.setFonts(getActivity(), destinationNameText, FontHelper.ROBOTO_MEDIUM);
        distanceTimeText = (TextView) view.findViewById(R.id.distance_time);
        FontHelper.setFonts(getActivity(), distanceTimeText, FontHelper.ROBOTO_REGULAR);
        avgSpeedText = (TextView) view.findViewById(R.id.avg_speed);
        FontHelper.setFonts(getActivity(), avgSpeedText, FontHelper.ROBOTO_REGULAR);
        destinationImage = (ImageView) view.findViewById(R.id.destination_image);

        TextView saveLocation, saveTrip;
        saveLocation = (TextView) view.findViewById(R.id.save_location);
        FontHelper.setFonts(getActivity(), saveLocation, FontHelper.ROBOTO_REGULAR);
        saveLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveLocationDialog();
            }
        });
        saveTrip = (TextView) view.findViewById(R.id.save_trip);
        FontHelper.setFonts(getActivity(), saveTrip, FontHelper.ROBOTO_REGULAR);
        saveTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSaveTripDialog();
            }
        });

        if (presenter.getNavigationDetails() != null) {
            showNavigationDetails(presenter.getNavigationDetails());
            showSharingOptions(getShareMessage());
        }
    }

    @Override
    public void showNavigationDetails(Navigation navigation){
        if(navigation.getDestinationReached() != null) {
            DestinationReached destinationReached = navigation.getDestinationReached();
            if (navigation.getStatus().getDestination() != null && !navigation.getStatus().getDestination().isEmpty()) {
                destinationNameText.setText(navigation.getStatus().getDestination());
                destinationNameText.setVisibility(View.VISIBLE);
            }
            if (destinationReached.getDestinationDistance() != null && !destinationReached.getDestinationDistance().isEmpty()) {
                String reachingTime = destinationReached.getDestinationDistance();
                if (destinationReached.getReachingTime() != null && !destinationReached.getReachingTime().isEmpty()) {
                    reachingTime += " in " + destinationReached.getReachingTime();
                }
                distanceTimeText.setText(reachingTime);
                distanceTimeText.setVisibility(View.VISIBLE);
            }
            if (getActivity() != null && destinationReached.getAverageSpeed() != null && !destinationReached.getAverageSpeed().isEmpty()) {
                avgSpeedText.setText(getActivity().getString(R.string.avg_speed) + destinationReached.getAverageSpeed());
                avgSpeedText.setVisibility(View.VISIBLE);
            }

            if (destinationReached.getDestinationImageUrl() != null && destinationReached.getDestinationImageUrl().length() > 0) {

                ImageRequest request = new ImageRequest(destinationReached.getDestinationImageUrl(),
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap bitmap) {
                                destinationImage.setImageBitmap(bitmap);
                                destinationImage.setVisibility(View.VISIBLE);
                            }
                        }, 0, 0, ImageView.ScaleType.CENTER_INSIDE, null,
                        new Response.ErrorListener() {
                            public void onErrorResponse(VolleyError error) {
                                destinationImage.setImageResource(R.mipmap.ic_launcher);
                                destinationImage.setVisibility(View.GONE);
                            }
                        });
                VolleySingleton.getInstance(getActivity()).addToRequestQueue(request);
            }
        }
    }

    private void showSaveTripDialog(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.save_trip_layout, null);
        saveTripDlg = new AlertDialog.Builder(getActivity()).create();
        saveTripDlg.setView(view);
        TextView saveTripHeader = (TextView) view.findViewById(R.id.save_trip_header);
        FontHelper.setFonts(getActivity(), saveTripHeader, FontHelper.ROBOTO_BOLD);

        final EditText tripName = (EditText) view.findViewById(R.id.trip_name);
        FontHelper.setFonts(getActivity(), tripName, FontHelper.ROBOTO_MEDIUM);

        tripExists = (TextView) view.findViewById(R.id.already_exists);
        FontHelper.setFonts(getActivity(), tripExists, FontHelper.ROBOTO_MEDIUM);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        FontHelper.setFonts(getActivity(), cancel, FontHelper.ROBOTO_MEDIUM);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTripDlg.dismiss();
            }
        });

        TextView save = (TextView) view.findViewById(R.id.save);
        FontHelper.setFonts(getActivity(), save, FontHelper.ROBOTO_MEDIUM);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tripLabel = "" + tripName.getText();
                presenter.saveTrip(tripLabel);
            }
        });
        saveTripDlg.show();
    }

    /**
     * Dialog to enter name for location to be saved
     */
    private void showSaveLocationDialog() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.save_location, null);
        saveLocationDlg = new AlertDialog.Builder(getActivity()).create();
        saveLocationDlg.setView(view);
        TextView saveLocationHeader = (TextView) view.findViewById(R.id.save_location_header);
        FontHelper.setFonts(getActivity(), saveLocationHeader, FontHelper.ROBOTO_BOLD);

        final EditText locationName = (EditText) view.findViewById(R.id.location_name);
        FontHelper.setFonts(getActivity(), locationName, FontHelper.ROBOTO_MEDIUM);

        locationExists = (TextView) view.findViewById(R.id.already_exists);
        FontHelper.setFonts(getActivity(), locationExists, FontHelper.ROBOTO_MEDIUM);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        FontHelper.setFonts(getActivity(), cancel, FontHelper.ROBOTO_MEDIUM);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveLocationDlg.dismiss();
            }
        });

        TextView save = (TextView) view.findViewById(R.id.save);
        FontHelper.setFonts(getActivity(), save, FontHelper.ROBOTO_MEDIUM);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String locationLabel = "" + locationName.getText();
                presenter.saveLocation(locationLabel);
            }
        });
        saveLocationDlg.show();
    }

    /**
     * Called from presenter when location is saved.
     *
     * @param locationLabel
     */
    private void showSaveSuccess(String locationLabel) {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.success_layout, null);
        TextView labelText = (TextView) view.findViewById(R.id.success_text);
        FontHelper.setFonts(getActivity(), labelText, FontHelper.ROBOTO_MEDIUM);
        labelText.setText(locationLabel + getString(R.string.saved_successfully));

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 20, 40);
        layoutParams.gravity = Gravity.BOTTOM;
        view.setLayoutParams(layoutParams);
        rootView.addView(view);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rootView.removeView(view);
            }
        }, 3000);
    }

    /**
     * Is called from presenter when a location is not saved
     * @param locationLabel
     */
    private void showSaveError(String locationLabel) {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.success_layout, null);
        TextView labelText = (TextView) view.findViewById(R.id.success_text);
        FontHelper.setFonts(getActivity(), labelText, FontHelper.ROBOTO_MEDIUM);
        labelText.setText(locationLabel + getString(R.string.save_error));

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 20, 40);
        layoutParams.gravity = Gravity.BOTTOM;
        view.setLayoutParams(layoutParams);
        rootView.addView(view);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                rootView.removeView(view);
            }
        }, 3000);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ReachedLocationFragment.
     */
    public static ReachedLocationFragment getInstance() {

        if (reachedLocationFragment == null)
            reachedLocationFragment = new ReachedLocationFragment();

        return reachedLocationFragment;
    }

    /**
     * Prepares text to be shared through social platforms.
     * @return
     */
    private String getShareMessage(){
        String shareMsg = "";
        Navigation navigation = presenter.getNavigationDetails();
        if (navigation.getStatus() != null) {
            for (Navigation.GeoCoordinate geoCoordinate : navigation.getGeoCoordinateList()){
               if(shareMsg.length()>0)
                   shareMsg += "\n";
                shareMsg += geoCoordinate.getLatitude() + geoCoordinate.getLongitude();
            }
        }
        return shareMsg;
    }

    /**
     * Get list of packages that share text intent
     * @param message
     */
    private void showSharingOptions(final String message) {
        shareProviders.removeAllViews();
        PackageManager pm = getActivity().getPackageManager();
        Intent main = new Intent(Intent.ACTION_SEND, null);
        main.setType("text/plain");

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(main, 0);

        Collections.sort(resolveInfos,
                new ResolveInfo.DisplayNameComparator(pm));

        int providerCount = 0;
        packages = new ArrayList<>();

        for (ResolveInfo resolveInfo : resolveInfos) {
            String provider = resolveInfo.activityInfo.packageName;

            boolean packageFound = false;
            // Show 4 sharing option in linear layout and others in list
            if (providerCount < 4) {
                for (String shareLink : shareLinks) {
                    if (provider.contains(shareLink)) {
                        shareProviders.addView(addSharingOption(provider, message));
                        packageFound = true;
                        providerCount++;
                        break;
                    }
                }
            }
            if (!packageFound) {
                if (!packages.contains(provider))
                    packages.add(provider);
            }
        }

        /**
         * 4 commonly used sharing options are displayed. If not available add.
         */
        if (providerCount < 4) {
            for (int i = 0; i < providerCount - 4; i++) {
                shareProviders.addView(addSharingOption(packages.get(0), message));
                packages.remove(0);
            }
        }
        if (packages.size() > 4) {
            shareProviders.addView(addSharingOption(null, message));
        }
    }

    /**
     * Display share options in list.
     * @param message
     */
    private void showMoreSharingOptions(final String message) {
        if (packages.size() > 1) {
            ArrayAdapter<String> adapter = new ChooserArrayAdapter(getActivity(),
                    android.R.layout.select_dialog_item, android.R.id.text1, packages);

            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.share_list_title)
                    .setAdapter(adapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            invokeApplication(packages.get(item), message);
                        }
                    })
                    .show();
        }
    }

    /**
     * Start the selected sharing intent with sharing text.
     * @param packageName
     * @param message
     */
    void invokeApplication(String packageName, String message) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Exploride trip information");
        intent.setPackage(packageName);
        startActivity(intent);
    }

    /**
     * Adds sharing option in horizontal linear layout.
     * @param packageName: If null add show more button.
     * @param message
     * @return
     */
    private View addSharingOption(final String packageName, final String message) {
        ImageView imageView = new ImageView(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) getResources()
                .getDimension(R.dimen.share_pro_height), (int) getResources()
                .getDimension(R.dimen.share_pro_height));
        layoutParams.setMarginEnd((int) getResources()
                .getDimension(R.dimen.share_pro_margin));
        imageView.setLayoutParams(layoutParams);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (packageName != null) {
                    invokeApplication(packageName, message);
                } else {
                    showMoreSharingOptions(message);
                }
            }
        });

        try {
            if (packageName != null) {
                Drawable icon = getActivity().getPackageManager().getApplicationIcon(packageName);
                imageView.setImageDrawable(icon);
            } else {
                imageView.setImageResource(R.drawable.share_more);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageView;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_reached_location;
    }

    @Override
    public void onBackPressed() {
        MapsPresenter.getInstance().resetNavigation();
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    /**
     * Received when requested to save a location.
     *
     * @param response
     */
    @Override
    public void onSavedLocationResponse(final String locationLabel, final Constants.SAVE_DATA_RESPONSE response) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (response.equals(Constants.SAVE_DATA_RESPONSE.Success)) {
                    saveLocationDlg.dismiss();
                    showSaveSuccess(locationLabel);
                } else if (response.equals(Constants.SAVE_DATA_RESPONSE.Exists)) {
                    if (locationExists != null) {
                        locationExists.setVisibility(View.VISIBLE);
                    }
                } else if (response.equals(Constants.SAVE_DATA_RESPONSE.Error)) {
                    saveTripDlg.dismiss();
                    showSaveError(locationLabel);
                }
            }
        });
    }

    /**
     * Received when requested to save a trip.
     * @param response
     */
    @Override
    public void onSavedTripResponse(String tripLabel, Constants.SAVE_DATA_RESPONSE response) {
        if (response.equals(Constants.SAVE_DATA_RESPONSE.Success)) {
            saveTripDlg.dismiss();
            showSaveSuccess(tripLabel);
        } else if (response.equals(Constants.SAVE_DATA_RESPONSE.Exists)) {
            if (tripExists != null) {
                tripExists.setVisibility(View.VISIBLE);
            }
        } else if (response.equals(Constants.SAVE_DATA_RESPONSE.Error)) {
            saveTripDlg.dismiss();
            showSaveError(tripLabel);
        }
    }

    @Override
    public void showToast(String message) {

    }

    @Override
    public void onStart() {
        presenter.onStartView();
        super.onStart();
    }

    @Override
    public void onStop() {
        presenter.onStopView();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }
}