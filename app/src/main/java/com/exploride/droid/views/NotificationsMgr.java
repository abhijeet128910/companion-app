package com.exploride.droid.views;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.cloudant.exploridemobile.framework.NotificationReq;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.logger.ExpLogger;

import java.util.Random;

/**
 * Created by peeyushupadhyay on 21/12/16.
 */

public class NotificationsMgr {

    private static final String TAG = "NotificationsMgr";
    static int mRequestId=-1;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    static NotificationsMgr mNotificationsMgr;
    int mMaxProgress = -1;
    static final Object _lock = new Object();

    public static NotificationsMgr getInstance(){

        if(mNotificationsMgr==null){
            mNotificationsMgr = new NotificationsMgr();
        }
        return mNotificationsMgr;
    }

    public void initRequest(NotificationReq notificationReq){

        synchronized (_lock) {
            //cancel previous if any
            if (mRequestId != -1)
                mNotificationManager.cancel(mRequestId);
            mRequestId = notificationReq.getReqId();
            mNotificationManager = (NotificationManager) MainApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
            mBuilder = new NotificationCompat.Builder(MainApplication.getInstance());
            mBuilder.setContentTitle("Your Exploride is Updating")
                    .setContentText("Download in progress")
                    .setSmallIcon(R.mipmap.ic_launcher);
            mMaxProgress = notificationReq.getProgress1();
            publishProgress(mMaxProgress, notificationReq.getProgress2());
            mNotificationManager.notify(mRequestId, mBuilder.build());
        }
    }
    public void handleNotificationRequest(NotificationReq notificationReq){

        synchronized (_lock) {
            ExpLogger.logI(TAG, "notificationReq.getReqId() == mRequestId"+ notificationReq.getReqId()+", " +mRequestId);
            if (notificationReq.getReqId() == mRequestId) {
                updateProgress(notificationReq);
//we will for now cancel previous and show the new notification
//        }else if(mRequestId == -1){
//            initRequest(notificationReq);
//        }
            } else
                initRequest(notificationReq);
        }
    }

    private void publishProgress(final int max, final int progress){

        synchronized (_lock) {
            MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        synchronized (_lock) {
                            mBuilder.setProgress(mMaxProgress, progress, false);
                            mNotificationManager.notify(mRequestId, mBuilder.build());
                            if (progress == 100) {
                                ExpLogger.logI(TAG, "publishProgress cancelling request id:" + mRequestId);
                                mNotificationManager.cancel(mRequestId);
                                mRequestId = -1;
                            }
                        }
                    } catch (Exception e) {
                        String a = "";
                    }
                }
            });
        }
    }

    void updateProgress(NotificationReq notificationReq){

        if(notificationReq.getReqId()==mRequestId)
            publishProgress(notificationReq.getProgress1(), notificationReq.getProgress2());
    }


}
