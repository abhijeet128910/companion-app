package com.exploride.droid.views.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.exploride.droid.DownloadMapsContract;
import com.exploride.droid.R;
import com.exploride.droid.adapter.DownloadMapsAdapter;
import com.exploride.droid.adapter.DownloadedMapsAdapter;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.DownloadMapsStore;
import com.exploride.droid.presenters.DownloadMapsPresenter;
import com.exploride.droid.utils.DividerItemDecoration;
import com.exploride.droid.views.activities.MainActivity;
import com.here.android.mpa.odml.MapPackage;

import java.util.List;

import static com.exploride.droid.R.drawable.ic_clear;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DownloadMapsFragment.OnDownloadMapFragmentInteractionListener} interface
 * to handle interaction events.
 * <p>
 * Author Mayank
 */
public class DownloadMapsFragment extends BaseFragment implements DownloadMapsContract.DownloadMapsView, DownloadMapsAdapter.OnMapInstallClickListener, DownloadedMapsAdapter.OnMapUninstallClickListener {

    private static String TAG = "DownloadMapsFragment";

    static DownloadMapsFragment downloadMapsFragment;

    private DownloadMapsContract.DownloadMapsOps presenter;

    private EditText etSearch;
    private RecyclerView mapsList;

    private DownloadMapsAdapter downloadMapsAdapter;
    private DownloadedMapsAdapter downloadedMapsAdapter;

    private ProgressDialog progressDialog;

    private MapPackage mapPackage;

    private Activity mActivity;

    private List<MapPackage> downloadedMapsList;

    private OnDownloadMapFragmentInteractionListener onDownloadMapFragmentInteractionListener;
    private AlertDialog.Builder errorAlert;

    public DownloadMapsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param context {@link MainActivity} context to initialize Fragment
     * @return A new instance of fragment AutoZoomFragment.
     */
    public static android.app.Fragment newInstance(MainActivity context) {
        return android.app.Fragment.instantiate(context, DownloadMapsFragment.class.getName(), null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = DownloadMapsPresenter.getInstance();

        //call to initialize View used by Presenter to interact with the fragment
        presenter.init(this);

        //call to initialize list containing installed map package ids
        presenter.initInstalledPackageIdList();

        RelativeLayout layoutSearchMaps = (RelativeLayout) view.findViewById(R.id.rl_download_maps);
        ImageView ivSearch = (ImageView) layoutSearchMaps.findViewById(R.id.iv_address);
        etSearch = (EditText) layoutSearchMaps.findViewById(R.id.et_address);
        mapsList = (RecyclerView) view.findViewById(R.id.lv_maps);

        ivSearch.setImageResource(R.drawable.search_icon);
        etSearch.setHint(getResources().getString(R.string.search_maps));
        etSearch.setVisibility(View.VISIBLE);

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try{
                    if (event.getRawX() >= (etSearch.getWidth() - (etSearch.getCompoundDrawables()[2].
                            getBounds().width() / 2))) {
                        if(!TextUtils.isEmpty(etSearch.getText().toString()))
                            etSearch.setText("");

                        return true;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    ExpLogger.logException(TAG, e);
                }

                return false;
            }
        });

        mapsList.setHasFixedSize(true);
        mapsList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mapsList.setItemAnimator(new DefaultItemAnimator());

        //Add a text change listener to implement autocomplete functionality
        etSearch.addTextChangedListener(textWatcher);

        // initializes progress dialog
        initProgressBar();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.dowanload_maps_actions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_downloaded_maps) {

            progressDialog.show();

            //gets map package from HUD to show the downloaded maps
            onDownloadMapFragmentInteractionListener.onGetMapPackageId(true, 0, false);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * initializes progress dialog to be shown when fetching Map Package Id from HUD
     */
    private void initProgressBar() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();

        onDownloadMapFragmentInteractionListener = (OnDownloadMapFragmentInteractionListener) getActivity();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        onDownloadMapFragmentInteractionListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroyView();

        if (progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();

        progressDialog = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStartView();
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onStopView();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_download_maps;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {

        super.onResume();

        progressDialog.show();

        //call to HUD to get Here MapPackage to show the Maps List
        onDownloadMapFragmentInteractionListener.onGetMapPackageId(false, 0, false);

    }

    /**
     * drills down to Country, State, City, Maps list and populates it into listview
     *
     * @param mapPackage: Here Map Package used to populate maps list
     */
    @Override
    public void onShowMapsChildren(MapPackage mapPackage) {
        if (mapPackage != null && mapPackage.getChildren() != null && mapPackage.getChildren().size() > 0) {

            this.mapPackage = mapPackage;

            //call to HUD to get Here MapPackage to show the Maps List
            onDownloadMapFragmentInteractionListener.onGetMapPackageId(false, mapPackage.getId(), false);

            progressDialog.show();

        }
    }

    /**
     * shows filtered Maps List as per user entered query
     *
     * @param mapPackageList: list of {@link MapPackage} class instance
     */
    @Override
    public void onSearchCompleted(List<MapPackage> mapPackageList) {
        if (mapsList.getAdapter() != null) {

            if (mapsList.getAdapter() instanceof DownloadMapsAdapter) {
                downloadMapsAdapter.animateTo(mapPackageList);
            } else {
                downloadedMapsAdapter.animateTo(mapPackageList);
            }
        }

        mapsList.scrollToPosition(0);
    }

    /**
     * text change listener for Maps List
     */
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if(s.length() > 0)
                etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, ic_clear, 0);
            else
                etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            if (mapsList.getAdapter() instanceof DownloadMapsAdapter) {
                if (mapPackage != null && mapPackage.getChildren() != null && mapPackage.getChildren().size() > 0) {
                    presenter.onQueryTextChange(s.toString(), mapPackage.getChildren());
                }
            } else {
                if (downloadedMapsList != null && downloadedMapsList.size() > 0) {
                    presenter.onQueryTextChange(s.toString(), downloadedMapsList);
                }
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * Shows Complete Maps fetched from HUD into a listview
     *
     * @param mapPackage: Here Map Package used to populate maps list
     */
    @Override
    public void showMapsList(MapPackage mapPackage) {

        //checks whether user is views complete maps list or downloaded maps list
        if (mapsList.getAdapter() == null || mapsList.getAdapter() instanceof DownloadMapsAdapter) {
            if (mapPackage != null && mapPackage.getChildren() != null && mapPackage.getChildren().size() > 0) {
                this.mapPackage = mapPackage;
                populateMapList(mapPackage);
            } else if (this.mapPackage != null && this.mapPackage.getChildren() != null && this.mapPackage.getChildren().size() > 0) {
                populateMapList(this.mapPackage);
            }
        }

    }

    /**
     * populates map list into list view
     *
     * @param mapPackage: Here Map Package used to populate maps list
     */
    private void populateMapList(final MapPackage mapPackage) {
        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!TextUtils.isEmpty(etSearch.getText().toString()))
                        etSearch.setText("");

                    if (downloadMapsAdapter == null) {
                        downloadMapsAdapter = new DownloadMapsAdapter(mapPackage.getChildren(), DownloadMapsFragment.this, presenter.getDownloadedMapPackageIdList());
                        mapsList.setAdapter(downloadMapsAdapter);
                    } else {
                        downloadMapsAdapter.swap(mapPackage.getChildren(), presenter.getDownloadedMapPackageIdList());
                        mapsList.getLayoutManager().scrollToPosition(0);
                    }

                    if (progressDialog!=null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * shows Installed Maps fetched from HUD into a listview
     *
     * @param downloadedMapsList: List of {@link MapPackage} class instance
     */
    @Override
    public void showInstalledMapsList(final List<MapPackage> downloadedMapsList) {

        try {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DownloadMapsFragment.this.downloadedMapsList = downloadedMapsList;
                    downloadedMapsAdapter = new DownloadedMapsAdapter(downloadedMapsList, DownloadMapsFragment.this);
                    mapsList.setAdapter(downloadedMapsAdapter);

                    if (progressDialog!=null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * call to set initial Map Install progress as 0 which eventually be updated by the progress received from HUD once Map Install starts.
     * Sets Map Package Id to {@link com.exploride.droid.model.MapsStore} class to be used for hiding or showing views while populating maps list.
     * Finally makes a call to HUD for Installing Maps, passing the Map Package Id as a parameter.
     *
     * @param mapPackageId: Here Maps Package Id to be installed
     */
    @Override
    public void onInstallMaps(int mapPackageId) {

        if (mapPackageId == -1) {
            showToast("Please wait un till current download completes");
            return;
        }

        // call to show start progress as 0% and will eventually be updated by the map download progress
        // retrieved from HUD
        presenter.onMapInstallProgress(0);

        //sets the map package id to be installed
        presenter.setMapPackageId(mapPackageId);

        onDownloadMapFragmentInteractionListener.onInstallMaps(mapPackageId);
    }

    /**
     * call to set initial Map Uninstall progress as 0 which eventually be updated by the progress received from HUD once Map Uninstall starts.
     * Sets Map Package Id to {@link com.exploride.droid.model.MapsStore} class to be used for hiding or showing views while populating maps list.
     * Finally makes a call to HUD for uninstalling Maps, passing the Map Package Id as a parameter.
     *
     * @param mapPackageId: Here Maps Package Id to be uninstalled
     */
    @Override
    public void onUninstallMaps(int mapPackageId) {
        if (mapPackageId == -1) {
            showToast("Please wait un till current operation completes");
            return;
        }

        //sets the map package id to be uninstalled
        presenter.setMapPackageId(mapPackageId);

        progressDialog.show();

        onDownloadMapFragmentInteractionListener.onUninstallMaps(mapPackageId);

    }

    /**
     * gets Map Package Id using which maps will be installed in HUD
     *
     * @return int Map package id
     */
    @Override
    public int getMapPackageId() {
        return presenter.getMapPackageId();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnDownloadMapFragmentInteractionListener {

        void onInstallMaps(int mapPackageId);

        void onUninstallMaps(int mapPackageId);

        void onGetMapPackageId(boolean getInstalledMapPackageId, int mapPackageId, boolean enableMapEngineOnline);

        void onCancelDownload();

        void enableMapEngineOnline();
    }

    @Override
    public void onBackPressed() {

        if(!TextUtils.isEmpty(etSearch.getText().toString()))
            etSearch.setText("");

        if (mapsList.getAdapter() != null) {

            if (mapsList.getAdapter() instanceof DownloadMapsAdapter) {

                if (mapPackage.getParent() != null) {
                    mapPackage = mapPackage.getParent();
                    downloadMapsAdapter.swap(mapPackage.getChildren(), presenter.getDownloadedMapPackageIdList());
                } else {
                    // handle back button
                    presenter.switchToMapsScreen();
                }

            } else {
                if (mapPackage != null && mapPackage.getChildren() != null && mapPackage.getChildren().size() > 0) {
                    downloadMapsAdapter.swap(mapPackage.getChildren(), presenter.getDownloadedMapPackageIdList());
                    mapsList.setAdapter(downloadMapsAdapter);
                }

            }

        }else {
            // handle back button
            presenter.switchToMapsScreen();
        }

    }

    /**
     * call to get map install progress to be shown to the user
     *
     * @return int map install progress
     */
    @Override
    public int getMapInstallProgress() {
        return presenter.getMapInstallProgress();
    }

    /**
     * shows Map Package Error to the user
     */
    @Override
    public void showMapPackageError() {
        try {
            ExpLogger.logD(TAG, "DownloadMapsInformation : showMapPackageError");
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog!=null && progressDialog.isShowing())
                        progressDialog.dismiss();

                   //call to show error occurred while getting map packages on HUD
                   showErrorAlert(getResources().getString(R.string.map_package_error), false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * shows error for Map Engine offline on HUD
     */
    @Override
    public void showMapEngineOfflineError() {
        try {
            ExpLogger.logD(TAG, "DownloadMapsInformation : showMapEngineOfflineError");
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();

                    //call to show error occurred while getting map packages on HUD
                    showErrorAlert(getResources().getString(R.string.map_engine_offline_error), true);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * shows map install progress sent from HUD to mobile
     */
    @Override
    public void showMapInstallProgress() {
        try {
            if (mapsList.getAdapter() != null && mapsList.getAdapter() instanceof DownloadMapsAdapter && downloadMapsAdapter != null) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        downloadMapsAdapter.notifyDataSetChanged();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * notifies user of the success while installing maps on HUD
     *
     * @param mapPackage: {@link MapPackage} downloaded on HUD
     */
    @Override
    public void showMapInstallSuccess(final MapPackage mapPackage) {
        try {
            ExpLogger.logD(TAG, "DownloadMapsInformation : showMapInstallSuccess");

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mapsList.getAdapter() != null) {
                        if (mapsList.getAdapter() instanceof DownloadMapsAdapter && downloadMapsAdapter != null) {
                            downloadMapsAdapter.notifyDataSetChanged();
                        } else if (mapsList.getAdapter() instanceof DownloadedMapsAdapter && downloadedMapsAdapter != null) {
                            downloadedMapsAdapter.addDownloadedMapPackage(mapPackage);
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * notifies user of the error occurred while installing maps on HUD
     */
    @Override
    public void showMapInstallError() {
        try {
            ExpLogger.logD(TAG, "DownloadMapsInformation : showMapInstallError");

            if (mapsList.getAdapter() != null && mapsList.getAdapter() instanceof DownloadMapsAdapter && downloadMapsAdapter != null) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(progressDialog!=null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        downloadMapsAdapter.notifyDataSetChanged();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * notifies user of the success while uninstalling maps on HUD
     */
    @Override
    public void showMapUninstallSuccess() {
        try {
            ExpLogger.logD(TAG, "DownloadMapsInformation : showMapUninstallError");

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog!=null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    if (mapsList.getAdapter() != null && mapsList.getAdapter() instanceof DownloadedMapsAdapter && downloadedMapsAdapter != null) {
                        downloadedMapsAdapter.onMapUninstallSuccess();
                        downloadedMapsAdapter.notifyDataSetChanged();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * notifies user of the error occurred while uninstalling maps on HUD
     */
    @Override
    public void showMapUninstallError() {
        try {
            ExpLogger.logD(TAG, "DownloadMapsInformation : showMapUninstallError");

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog!=null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * call to HUD to stop the ongoing download
     */
    @Override
    public void onCancelDownload() {
        //call to reset Map Package ID
        presenter.setMapPackageId(-1);

        onDownloadMapFragmentInteractionListener.onCancelDownload();
    }

    /**
     * notifies user of the error occurred while Map Engine Initialization on HUD
     */
    @Override
    public void showMapEngineInitializationError() {
        try {
            ExpLogger.logD(TAG, "DownloadMapsInformation : showMapEngineInitializationError");

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog!=null && progressDialog.isShowing())
                        progressDialog.dismiss();

                    //call to show error occurred while getting map packages on HUD
                    showErrorAlert(getResources().getString(R.string.map_package_error), false);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * shows alert prompt to the user notifying of the error. User can retry getting map options
     * for download by interacting with the prompt or simply close it
     * @param message: String title error to be shown to the user
     */
    private void showErrorAlert(String message, final boolean enableMapEngineOnline){
        if (errorAlert == null)
            errorAlert = new AlertDialog.Builder(mActivity);

        errorAlert.setMessage(message);
        errorAlert.setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                progressDialog.show();

                if (enableMapEngineOnline){
                    onDownloadMapFragmentInteractionListener.enableMapEngineOnline();
                }
                //call to HUD to get Here MapPackage to show the Maps List
                onDownloadMapFragmentInteractionListener.onGetMapPackageId(false, 0, enableMapEngineOnline);

            }
        });
        errorAlert.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        errorAlert.show();
    }
}