package com.exploride.droid.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;

import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.R;
import com.exploride.droid.SavedTripsContract;
import com.exploride.droid.adapter.SavedTripsAdapter;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.SavedTripsPresenter;
import com.exploride.droid.utils.Constants;

import java.util.List;

public class SavedTripsFragment extends BaseFragment implements SavedTripsContract.SavedTripsView,
        SavedTripsAdapter.OnTripItemClickListener {

    private RecyclerView savedTripsList;
    private SavedTripsContract.SavedTripsOps presenter;
    static SavedTripsFragment savedTripsFragment;

    public static SavedTripsFragment getInstance() {
        if (savedTripsFragment == null) {
            savedTripsFragment = new SavedTripsFragment();
        }
        return savedTripsFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleChangedListener.onSetTitleText(getResources().getString(R.string.saved_trips));
        presenter = SavedTripsPresenter.getInstance();
        presenter.init(this);
        savedTripsList = (RecyclerView) view.findViewById(R.id.saved_trips_list);
        savedTripsList.setHasFixedSize(true);
        savedTripsList.setItemAnimator(new DefaultItemAnimator());
        presenter.fetchSavedTrips();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_saved_trips;
    }

    @Override
    public void onBackPressed() {
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    @Override
    public void onResume() {
        super.onResume();

        //override back key listener
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button
                    MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.onStartView();
    }

    @Override
    public void onStop() {
        presenter.onStopView();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }

    @Override
    public void showSavedTrips(List<Navigation> trips) {
        if (trips != null && trips.size() > 0) {
            SavedTripsAdapter savedTripsAdapter = new SavedTripsAdapter(getActivity(), trips, this);
            savedTripsList.setAdapter(savedTripsAdapter);
        }
    }

    @Override
    public void showToast(String message) {
    }

    @Override
    public void setOnItemClick(Navigation trip) {
        presenter.setSavedTrip(trip);
    }
}
