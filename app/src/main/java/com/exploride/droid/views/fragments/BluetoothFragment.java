
package com.exploride.droid.views.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.exploride.droid.adapter.BTDevicesAdapter;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.MainApplication;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.BTModel;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.services.BluetoothClientService;
import com.exploride.droid.bluetooth.DiscoveryHandler;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.CommonUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.DividerItemDecoration;
import com.exploride.droid.views.activities.DeviceListActivity;
import com.exploride.droid.R;
import com.exploride.droid.views.activities.MainActivity;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import static com.exploride.droid.bluetooth.BluetoothHandler.*;
import static com.exploride.droid.services.BluetoothClientService.STATE_CONNECTED;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class BluetoothFragment extends BaseFragment {

    private static final String TAG = "BluetoothFragment";
    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private int mSelectedIndex = -1;

    // Layout Views
    private Button mBtn_TestConnection;
    private Switch mSwitch_Connect;
    private Button mBtn_ManualConnect;
    private RecyclerView btDeviceList;
    private List<BTModel> btDevices = new ArrayList<>();
    private LinkedHashMap<String, BTModel> btDevicesMap = new LinkedHashMap<>();
    private BTDevicesAdapter btDevicesAdapter;
    private TextView noDevices;
    private BluetoothAdapter mBtAdapter;
    private ProgressBar progressBar;
    Switch mBTOnly;

    BluetoothHandler mBluetoothHandler;
    BTState mBTStateReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mBluetoothHandler = getInstance();
//        if (mBluetoothHandler.getLastConnectedDeviceName() != null && mBluetoothHandler.getLastConnectedDeviceAddress() != null) {
//            mSelectedDevice = new BTModel(mBluetoothHandler.getLastConnectedDeviceName(), mBluetoothHandler.getLastConnectedDeviceAddress());
//            if (mBluetoothHandler.getBluetoothClientService().getState() == STATE_CONNECTED)
//                mSelectedDevice.setCurrentDevice(true);
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBtAdapter == null)
            mBtAdapter = mBluetoothHandler.getBluetoothAdapter();

        mBTStateReceiver = new BTState(){

            @Override
            public void BTConnected(Constants.CHANNEL channel) {
//                if(channel == ConnectionHandler.getInstance().getUserCommunicationChannelType())
                    BluetoothFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MainPresenter.getInstance().hideLoading();
                            mSwitch_Connect.setChecked(true);
                        }
                    });
            }

            @Override
            public void BTConnectedToDevice(final String deviceName, Constants.CHANNEL channel) {
//                if(channel == ConnectionHandler.getInstance().getUserCommunicationChannelType())
                    BluetoothFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            MainPresenter.getInstance().hideLoading();
                            mSwitch_Connect.setText("Connected to "+ mBluetoothHandler.getLastConnectedDeviceName());
                            btDevicesAdapter.setBtName(deviceName);
                            btDevicesAdapter.notifyDataSetChanged();
                        }
                    });
            }

            @Override
            public void BTConnecting(Constants.CHANNEL channel) {
//                if(channel == ConnectionHandler.getInstance().getUserCommunicationChannelType())
                    BluetoothFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSwitch_Connect.setChecked(false);
                        }
                    });
            }

            @Override
            public void BTNotConnected(Constants.CHANNEL channel) {

                if(!MainApplication.getInstance().isCommunicationChannelOpen()){
                    BluetoothFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MainPresenter.getInstance().hideLoading();
                                mSwitch_Connect.setChecked(false);
                                mSwitch_Connect.setText("Connect to " + mBluetoothHandler.getLastConnectedDeviceName());
                                if (getActivity() != null)
                                    Toast.makeText(getActivity(), "Connection failed", Toast.LENGTH_SHORT).show();
                                btDevicesAdapter.setBtName(null);
                                btDevicesAdapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                ExpLogger.logException(TAG, e);
                            }
                        }
                    });
                }
            }

            @Override
            public void toastToShow(String msg, Constants.CHANNEL channel) {

            }

            @Override
            public void BTMsgReceived(String msg, Constants.CHANNEL channel){
            }

            @Override
            public void BTMsgSent(String msg, Constants.CHANNEL channel){
            }
        };
        doDiscovery();
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private void doDiscovery() {
        ExpLogger.logD(TAG, "doDiscovery()");

        // Indicate scanning in the title
//        MainPresenter.getInstance().showLoading(getString(R.string.scanning));
        showScanningProgress();

        // If we're already discovering, stopTCPClient it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        DiscoveryHandler discoveryHandler = new DiscoveryHandler(new DiscoveryHandler.DiscoveryHandlerCallback() {
            @Override
            public void BTDiscoveryDeviceFound() {
                String a="";
            }

            @Override
            public void BTDiscoveryStarted() {
                if (getActivity() != null)
                    MainPresenter.getInstance().showLoading(getActivity().getString(R.string.bt_connecting));
            }

            @Override
            public void BTDiscoveryFinished() {
                if(mBluetoothHandler.getBluetoothClientService().getState() != STATE_CONNECTED)
                    mSwitch_Connect.setChecked(false);
                MainPresenter.getInstance().hideLoading();
            }

            @Override
            public void BTDiscoveryPairingRequestReceived() {

            }
        });

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }
        if(mReceiver!=null)
            getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBluetoothHandler.getBluetoothClientService() != null)
            mSwitch_Connect.setChecked(mBluetoothHandler.getBluetoothClientService().getState() == BluetoothClientService.STATE_CONNECTED);
        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        getActivity().registerReceiver(mReceiver, filter);
        MainApplication.getInstance().registerBTStateListener(mBTStateReceiver);

    }

    @Override
    public void onPause(){
        super.onPause();
        try {
            MainApplication.getInstance().unRegisterBTStateListener(mBTStateReceiver);
        }catch (IllegalArgumentException e){}

    }

    /*@Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bluetooth_connect, container, false);
    }*/

    @Override
    protected int getFragmentLayout() {
        return R.layout.bluetooth_connect;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        titleChangedListener.onSetTitleText(getResources().getString(R.string.bluetooth_settings));
        mBtn_TestConnection = (Button) view.findViewById(R.id.testconnection);
        mSwitch_Connect = (Switch) view.findViewById(R.id.connect);

        if (BluetoothHandler.getInstance() != null && BluetoothHandler.getInstance().getBluetoothClientService() != null) {
            if (BluetoothClientService.STATE_CONNECTED == BluetoothHandler.getInstance().getBluetoothClientService().getState())
                mSwitch_Connect.setText(getString(R.string.connected_to) + mBluetoothHandler.getLastConnectedDeviceName());
            else
                mSwitch_Connect.setText(getString(R.string.connect_to) + mBluetoothHandler.getLastConnectedDeviceName());
        }
        mBtn_ManualConnect = (Button) view.findViewById(R.id.connect_manual);
        mSwitch_Connect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!mSwitch_Connect.isPressed())
                    return;
                if(mSwitch_Connect.isChecked())
                {
//                    tryBTConnect();
                }else{
                    if (mBluetoothHandler.getBluetoothClientService() != null) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                mBluetoothHandler.getBluetoothClientService().stopBTClient();
                            }
                        });
                    }
                }
            }
        });
        // Initialize the send button with a listener that for click events
        mBtn_TestConnection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Send a message using content of the edit text widget
                View view = getView();
                if (null != view) {

                    String message = "this is a test message";
                    mBluetoothHandler.sendMessage(message);
                }
            }
        });
        mBtn_ManualConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualConnect();
            }
        });

        mBTOnly = (Switch) view.findViewById(R.id.bt_only);
        int val = ConnectionHandler.getInstance().getUserCommunicationChannelType().getValue();
        mBTOnly.setChecked(val==Constants.CHANNEL.BT.getValue()?true:false);
        mBTOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!mBTOnly.isPressed())
                    return;
                int val;
                if(mBTOnly.isChecked())
                {
                    val = Constants.CHANNEL.BT.getValue();
                    ConnectionHandler.getInstance().setUserCommunicationChannelType(Constants.CHANNEL.BT);
                    ConnectionHandler.getInstance().stopTCPClient();
                }else{
                    val = Constants.CHANNEL.TCP.getValue();
                    ConnectionHandler.getInstance().setUserCommunicationChannelType(Constants.CHANNEL.TCP);
                    ConnectionHandler.getInstance().disconnectBT();
                }
                if(mSelectedDevice!=null){
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_DEVICE_ADDRESS, mSelectedDevice.getBtAddress());
                    ConnectionHandler.getInstance().initConnection(intent);
                }
                SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_USER_COMM_CHANNEL, val);

            }
        });

        noDevices = (TextView) view.findViewById(R.id.no_devices_found);
        btDeviceList = (RecyclerView) view.findViewById(R.id.lv_bt_devices);
        btDeviceList.setHasFixedSize(true);
        btDeviceList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        btDeviceList.setItemAnimator(new DefaultItemAnimator());

        progressBar = (ProgressBar) view.findViewById(R.id.scanning_progress);

        // Get a set of currently paired devices
        if (mBtAdapter == null)
            mBtAdapter = mBluetoothHandler.getBluetoothAdapter();

        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
        if (pairedDevices != null) {
            for (BluetoothDevice device : pairedDevices) {
                BTModel btModel = new BTModel(device.getName(), device.getAddress());
                btDevicesMap.put(device.getName(), btModel);
            }
        }
        btDevices.clear();
        btDevices.addAll(btDevicesMap.values());
        initBTAdapter();

    }

    private void showNoDevices(){
        noDevices.setVisibility(View.VISIBLE);
    }

    /**
     * Set up the UI and background operations for Bluetooth.
     */
    private void sendTestMessage() {
        ExpLogger.logD(TAG, "sendTestMessage()");



    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    mBluetoothHandler.connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    mBluetoothHandler.connectDevice(data, false);
                }
                break;
        }
    }


    void manualConnect(){

        Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
    }

    @Override
    public void onBackPressed() {
        // handle back button
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                BTModel btModel = new BTModel(device.getName(),device.getAddress());

                if (btModel.getBtName() == null || btModel.getBtAddress() == null || btModel.getBtName().trim().isEmpty()
                        || btModel.getBtAddress().trim().isEmpty())
                    return;

                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    btDevicesMap.put(btModel.getBtName(), btModel);
                    btDevices.clear();
                    btDevices.addAll(btDevicesMap.values());
                    btDevicesAdapter.notifyDataSetChanged();
                }
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                hideScanningProgress();
                if (btDevices.size() == 0) {
                    showNoDevices();
                }
            }
        }
    };


    BTModel mSelectedDevice;
    private void initBTAdapter() {
        if (btDevicesAdapter == null) {
            btDevicesAdapter = new BTDevicesAdapter(getActivity(), btDevices,
                    new BTDevicesAdapter.OnBTDeviceItemClickListener() {
                        @Override
                        public void setOnItemClick(final BTModel btDevice, final int position) {

                            // Checking if the last attempt for connection was successful
                            try {
                                if(MainApplication.getInstance().isCommunicationChannelOpen()) {
                                    CommonUtils.showAlert(MainApplication.getInstance().getCurrentActivity(), "Disconnect?",
                                            "Do you want to disconnect the existing connection?",
                                            new CommonUtils.CustomDialogResult() {

                                                @Override
                                                public void onResult(int result) {
                                                    if (result == -1) {
                                                        //user cancelled, so return.
                                                        return;
                                                    } else if (result == 1) {
                                                        //user clicked OK.
                                                        ConnectionHandler.getInstance().setHasUserDisconnectedConnection(true);
                                                        //disconnect any existing connection, either to same or different device

                                                        if (MainApplication.getInstance().isCommunicationChannelOpen()) {
                                                            ConnectionHandler.getInstance().disconnect();
                                                            return;
                                                        }
                                                    }

                                                }
                                            });
                                    return;
                                }

                                //connect to the new device or reconnect
                                mSelectedDevice = btDevice;
                                mSelectedIndex = position;
                                MainPresenter.getInstance().showLoading(getActivity().getString(R.string.bt_connecting));
                                ConnectionHandler.getInstance().setHasUserDisconnectedConnection(false);
                                initConnection(btDevice);
                            } catch (Exception e) {
                                ExpLogger.logException(TAG, e);
                            }
                        }
                    });

//            if (mBluetoothHandler != null && mBluetoothHandler.getBluetoothClientService() != null) {
                if(MainApplication.getInstance().isCommunicationChannelOpen())
                    btDevicesAdapter.setBtName(ConnectionHandler.getInstance().getLastConnectedDeviceName());
                btDeviceList.setAdapter(btDevicesAdapter);
//            }
        }
        else
            btDevicesAdapter.notifyDataSetChanged();
    }

    /**
     * initiate the connection tcp or bt
     * @param btDevice
     */
    void initConnection(final BTModel btDevice){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_DEVICE_ADDRESS, btDevice.getBtAddress());
                    ConnectionHandler.getInstance().initConnection(intent);
//                    mBluetoothHandler.connectDevice(intent, true);
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        }).start();

    }

    private void connectToSelectedDevice(BTModel btDevice) {
        try {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_DEVICE_ADDRESS, btDevice.getBtAddress());
            mBluetoothHandler.connectDevice(intent, true);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    private void showScanningProgress(){
        if(progressBar != null){
            progressBar.setVisibility(View.VISIBLE);
        }
    }
    private void hideScanningProgress(){
        if(progressBar != null){
            progressBar.setVisibility(View.GONE);
        }
    }
}