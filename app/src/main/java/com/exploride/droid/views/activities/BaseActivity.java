package com.exploride.droid.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.exploride.droid.MainApplication;
import com.exploride.droid.bluetooth.BluetoothHandler;

import static com.exploride.droid.bluetooth.BluetoothHandler.REQUEST_ENABLE_BT;

/**
 * Base activity class for all the activities
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication.getInstance().setCurrentActivity(this);
    }

    @Override

    protected void onDestroy() {
        super.onDestroy();
        BluetoothHandler.getInstance().stopBTService();
    }

    protected void onResume(){
        super.onResume();
        MainApplication.getInstance().setCurrentActivity(this);
        BluetoothHandler.getInstance().onActivityResume();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(REQUEST_ENABLE_BT==requestCode)
            BluetoothHandler.getInstance().onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onBackPressed(){
//        if(this.isTaskRoot())
//            MainApplication.getInstance().cleanUpOnExit();
//        super.onBackPressed();
    }
}
