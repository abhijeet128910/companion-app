package com.exploride.droid.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.settings.SettingsUtils;

import static com.exploride.droid.utils.Constants.SharedPrefKeys.KEY_SPLASH_SCREEN;

/**
 * Class to show splash screen at the app start
 *
 * Author Mayank
 */

public class SplashActivity extends AppCompatActivity {

    private final String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainApplication.getInstance().setCurrentActivity(this);
        moveToNextScreen();

    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    /**
     * starts the Main screen which is either the login screen or Home screen
     */
    private void moveToNextScreen(){
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
