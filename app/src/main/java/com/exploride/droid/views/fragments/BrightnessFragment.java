package com.exploride.droid.views.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.SeekBar;

import com.exploride.droid.R;
import com.exploride.droid.views.activities.MainActivity;

/**
 * Author Mayank
 */

public class BrightnessFragment extends DialogFragment {

    private SeekBar seekBar;
    private CheckBox cbAutomatic;
    private int brightness;
    private boolean isAutomaticBrightnessOn;
    private static final String KEY_BRIGHTNESS_VALUE = "value";
    private static final String KEY_AUTOMATIC_BRIGHTNESS_MODE = "automatic_mode";
    private onBrightnessChangeListener mListener;
    private boolean shown = false;

    public interface onBrightnessChangeListener {
        void setOnAutomaticBrightnessSelectedListener(boolean isAutomaticBrightnessModeSelected);

        void setOnBrightnessChangeListener(int brightness);

        void setOnAutomaticBrightnessUncheckedListener();
    }


    public BrightnessFragment() {
        // Empty constructor required for DialogFragment
    }

    public static BrightnessFragment newInstance(int brightnessValue, boolean isHudBrightnessModeOn) {
        BrightnessFragment frag = new BrightnessFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_BRIGHTNESS_VALUE, brightnessValue);
        args.putBoolean(KEY_AUTOMATIC_BRIGHTNESS_MODE, isHudBrightnessModeOn);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mListener = (MainActivity) getActivity();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        if (getArguments() != null) {
            brightness = getArguments().getInt(KEY_BRIGHTNESS_VALUE);
            isAutomaticBrightnessOn = getArguments().getBoolean(KEY_AUTOMATIC_BRIGHTNESS_MODE);
        }

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.fragment_brightness);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        seekBar = (SeekBar) dialog.findViewById(R.id.seek_bar);
        cbAutomatic = (CheckBox) dialog.findViewById(R.id.cb_automatic);

        if (isAutomaticBrightnessOn) {

            cbAutomatic.setChecked(true);
            seekBar.setEnabled(false);

        } else {
            seekBar.setProgress(brightness);
        }

        cbAutomatic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbAutomatic.isChecked()) {
                    seekBar.setEnabled(false);
                    seekBar.setProgress(0);

                    mListener.setOnAutomaticBrightnessSelectedListener(cbAutomatic.isChecked());

//                    dialog.dismiss();
                } else {
                    seekBar.setEnabled(true);
                    mListener.setOnAutomaticBrightnessUncheckedListener();
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mListener.setOnBrightnessChangeListener(seekBar.getProgress());
//                dialog.dismiss();
            }
        });

        return dialog;
    }

    public boolean isFragmentDialogActive() {
        return shown;
    }

    public void onGetCurrentScreenBrightness(int progress) {
        if (seekBar != null)
            seekBar.setProgress(progress);
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        if (shown) return;

        super.show(manager, tag);
        shown = true;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        shown = false;
        super.onDismiss(dialog);
    }
}
