/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.exploride.droid.views.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.logger.ExpLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * This Activity appears as a dialog. It lists any paired devices and
 * devices detected in the area after discovery. When a device is chosen
 * by the user, the MAC address of the device is sent back to the parent
 * Activity in the result Intent.
 */
public class WifiListActivity extends Activity {

    /**
     * Tag for Log
     */
    private static final String TAG = "WifiListActivity";

    /**
     * Return Intent extra
     */
    public static String EXTRA_DEVICE_SSID = "device_address";

    /**
     * Member fields
     */
    private BluetoothAdapter mBtAdapter;

    /**
     * Newly discovered devices
     */
//    private ArrayAdapter<String> mNewDevicesArrayAdapter;

    WifiManager mWifiManager;
    ArrayAdapter<String> mPairedDevicesArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_device_list);

        // Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);

//        // Initialize the button to perform device discovery
        Button scanButton = (Button) findViewById(R.id.button_scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startWifiScan();
                v.setVisibility(View.GONE);
            }
        });
        scanButton.setVisibility(View.GONE);
        // Initialize array adapters. One for already paired devices and
        // one for newly discovered devices
        mPairedDevicesArrayAdapter =
                new ArrayAdapter<String>(this, R.layout.device_name);
//        mNewDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);

        // Find and set up the ListView for paired devices
        ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setAdapter(mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);

//        // Find and set up the ListView for newly discovered devices
//        ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
//        newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
//        newDevicesListView.setOnItemClickListener(mDeviceClickListener);
        mWifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        mWifiReceiver = new WifiReceiver();
        registerReceiver(mWifiReceiver, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        startWifiScan();
    }
    WifiReceiver mWifiReceiver;
    public void startWifiScan()
    {
        mPairedDevicesArrayAdapter.clear();
        mPairedDevicesArrayAdapter.add("Scanning...");
        if(mWifiManager.isWifiEnabled()==false)
        {
            mWifiManager.setWifiEnabled(true);
        }
        mWifiManager.startScan();
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run()
//            {
//                mWifiManager.startScan();
//            }
//        }, 100);
        ExpLogger.logD(TAG, "exiting startWifiScan()");
    }
    class WifiReceiver extends BroadcastReceiver
    {
        public void onReceive(Context c, Intent intent)
        {
            ArrayList<String> connections=new ArrayList<String>();
            ArrayList<Float> Signal_Strenth= new ArrayList<Float>();
            mPairedDevicesArrayAdapter.clear();;
            List<ScanResult> wifiList = mWifiManager.getScanResults();
            for(int i = 0; i < wifiList.size(); i++)
            {

                mPairedDevicesArrayAdapter.add(wifiList.get(i).SSID);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.getInstance().setCurrentActivity(this);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.unregisterReceiver(mWifiReceiver);
    }

    /**
     * The on-click listener for all devices in the ListViews
     */
    private AdapterView.OnItemClickListener mDeviceClickListener
            = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            // Get the device MAC address, which is the last 17 chars in the View
            String ssid = ((TextView) v).getText().toString();
            ((TextView) v).setText((ssid+"\n"+"connecting"));
            // Create the result Intent and include the MAC address
            Intent intent = new Intent();
            intent.putExtra(EXTRA_DEVICE_SSID, ssid);

            // Set result and finish this Activity
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };

}
