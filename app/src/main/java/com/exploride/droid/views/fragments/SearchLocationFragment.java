package com.exploride.droid.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudant.exploridemobile.ExpDocument;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.SearchLocationContract;
import com.exploride.droid.adapter.PlacesAdapter;
import com.exploride.droid.adapter.RecentLocationsAdapter;
import com.exploride.droid.adapter.RoutesAdapter;
import com.exploride.droid.adapter.SavedLocationsAdapter;
import com.exploride.droid.adapter.WayPointsAdapter;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.model.RecentLocation;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.presenters.SearchLocationPresenter;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.DividerItemDecoration;
import com.exploride.droid.utils.FontHelper;
import com.exploride.droid.utils.Utils;
import com.exploride.droid.views.custom_views.RecyclerViewEmptySupport;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.RoutingError;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.exploride.droid.R.drawable.ic_clear;

/**
 * Author Mayank
 */

public class SearchLocationFragment extends BaseFragment implements SearchLocationContract.SearchLocationView, PlacesAdapter.OnItemClickListener, RoutesAdapter.OnItemClickListener, WayPointsAdapter.OnItemClickListener, SavedLocationsAdapter.OnLocationItemClickListener, RecentLocationsAdapter.OnLocationItemClickListener {

    private static String TAG = "SearchLocationFragment";

    private RelativeLayout layoutSource;
    private RelativeLayout layoutDestination;
    private ImageView ivSource;
    private EditText etSource;
    private ImageView ivDestination;
    private EditText etDestination;
    private RecyclerViewEmptySupport placesList;
    private RecyclerViewEmptySupport routesList;
    private RecyclerView savedLocationsList;
    private RecyclerView recentLocationsList;
    private EditText etPlaceSelected;

    private LinearLayout layoutSearchLocation;
    private LinearLayout layoutAddWayPointsParent;
    private LinearLayout layoutAddWayPoints;
    private RecyclerViewEmptySupport wayPointsList;
    private LinearLayout layoutWayPointsHintIcons;
    private TextView tvWayPointsHint;
    private EditText etWayPoints;

    static SearchLocationFragment searchLocationFragment;
    private OnTitleChangedListener mListener;
    private PlacesAdapter placesAdapter;
    private RoutesAdapter routesAdapter;
    private WayPointsAdapter wayPointsAdapter;
    private SearchLocationContract.SearchLocationOps presenter;
    private InputMethodManager inputMethodManager;
    private Location sourceLocation;
    private Location destinationLocation;
    private boolean isLocationSelected;
    private boolean isWayPointAdded;
    private View savedLocationContainer;
    private List<ExpDocument> mLocations;
    private List<RecentLocation> mRecentLocations;
    private ProgressBar destinationProgressBar;
    private ProgressBar wayPointsProgressBar;
    private AlertDialog.Builder errorDialog;
    private ProgressDialog progressDialog;
    private Timer timer;
    private int placeCharacterLength;
    private TextView tvNoPlacesFound;
    private TextView tvNoRoutesFound;
    private TextView tvNoWayPointsFound;
    //holds various routes for a destination
    private ArrayList<RouteSummary> routeSummaries = new ArrayList<>();
    private CoreRouter coreRouter;
    private boolean mShowSavedLocations = true;
    public enum Error {PLACE_ERROR, ROUTE_ERROR}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapsDestinationFragment.
     */
    public static SearchLocationFragment getInstance() {

        if (searchLocationFragment == null)
            searchLocationFragment = new SearchLocationFragment();

        return searchLocationFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // initializes progress dialog
        initProgressDialog();

        titleChangedListener.onSetTitleText(getResources().getString(R.string.search_location));
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        presenter = SearchLocationPresenter.getInstance();

        presenter.getReverseGeoCodedLocation();

        //call to initialize View used by Presenter to interact with the fragment
        presenter.init(this);

        layoutSource = (RelativeLayout) view.findViewById(R.id.rl_source);
        ivSource = (ImageView) layoutSource.findViewById(R.id.iv_source);
        etSource = (EditText) layoutSource.findViewById(R.id.et_source);
        layoutDestination = (RelativeLayout) view.findViewById(R.id.rl_destination);
        ivDestination = (ImageView) layoutDestination.findViewById(R.id.iv_address);
        etDestination = (EditText) layoutDestination.findViewById(R.id.et_address);
        destinationProgressBar = (ProgressBar) (layoutDestination.findViewById(R.id.progress_view));
        placesList = (RecyclerViewEmptySupport) view.findViewById(R.id.lv_place_search);
        routesList = (RecyclerViewEmptySupport) view.findViewById(R.id.lv_route);
        savedLocationsList = (RecyclerView) view.findViewById(R.id.lv_saved_locations);
        recentLocationsList = (RecyclerView) view.findViewById(R.id.lv_recent_locations);
        layoutSearchLocation = (LinearLayout) view.findViewById(R.id.ll_search_location);
        layoutAddWayPointsParent = (LinearLayout) view.findViewById(R.id.ll_add_way_points_parent);
        layoutAddWayPoints = (LinearLayout) view.findViewById(R.id.ll_add_way_points);
        wayPointsList = (RecyclerViewEmptySupport) view.findViewById(R.id.lv_search_way_points);
        layoutWayPointsHintIcons = (LinearLayout) layoutAddWayPoints.findViewById(R.id.ll_way_points_hint_icons);
        tvWayPointsHint = (TextView) layoutAddWayPoints.findViewById(R.id.tv_way_points_hint);
        etWayPoints = (EditText) layoutAddWayPoints.findViewById(R.id.et_address);
        wayPointsProgressBar = (ProgressBar) (layoutAddWayPoints.findViewById(R.id.progress_view));
        savedLocationContainer = view.findViewById(R.id.saved_locations_container);
        TextView savedLocationsHeader= (TextView) savedLocationContainer.findViewById(R.id.saved_locations_header);
        tvNoPlacesFound = (TextView) view.findViewById(R.id.tv_places_list_empty);
        tvNoRoutesFound = (TextView) view.findViewById(R.id.tv_routes_list_empty);
        tvNoWayPointsFound = (TextView) view.findViewById(R.id.tv_way_points_list_empty);

        ivSource.setImageResource(R.drawable.my_location);
        if(presenter.getSourceLocationName() != null)
            etSource.setText(presenter.getSourceLocationName());

        etSource.setHint(getResources().getString(R.string.current_location));
        etSource.setVisibility(View.VISIBLE);
        etSource.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    isLocationSelected = false;

                    //Add a text change listener to implement autocomplete functionality
                    etSource.addTextChangedListener(textWatcher);


                } else
                    etSource.removeTextChangedListener(textWatcher);
            }
        });

        ivDestination.setImageResource(R.drawable.searched_location);

        etDestination.setCompoundDrawablesWithIntrinsicBounds(0, 0, ic_clear, 0);
        etDestination.setHint(getResources().getString(R.string.desired_location));
        etDestination.setVisibility(View.VISIBLE);

        etDestination.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try{
                    if (etDestination.getCompoundDrawables()[2] != null) {
                        int leftEdgeOfRightDrawable = etDestination.getRight()
                                - etDestination.getCompoundDrawables()[2].getBounds().width();
                        if (event.getRawX() >= leftEdgeOfRightDrawable) {
                            etDestination.setText("");

                            if (placesList.getVisibility() != View.GONE)
                                placesList.setVisibility(View.GONE);

                            routesList.setVisibility(View.GONE);
                            presenter.resetDestination();
                            presenter.getRecentLocations();
                            mShowSavedLocations = true;
                            showSavedLocationList();

                            //resets the place character length
                            placeCharacterLength = 0;
                            return true;
                        }
                    }
                }catch (Exception e){
                    ExpLogger.logException(TAG, e);
                }

                return false;
            }
        });

        etDestination.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    isLocationSelected = false;

                    //Add a text change listener to implement autocomplete functionality
                    etDestination.addTextChangedListener(textWatcher);

                } else
                    etDestination.removeTextChangedListener(textWatcher);
            }
        });
        if(presenter.getDestinationLocationName() != null)
            etDestination.setText(presenter.getDestinationLocationName());
        else {
            //destination field gets focus when screen starts
            etDestination.requestFocus();
            inputMethodManager.showSoftInput(etDestination, InputMethodManager.SHOW_IMPLICIT);
            //Add a text change listener to implement autocomplete functionality
            etDestination.addTextChangedListener(textWatcher);
        }

        destinationProgressBar.setIndeterminate(true);
        wayPointsProgressBar.setIndeterminate(true);

        placesList.setHasFixedSize(true);
        placesList.setItemAnimator(new DefaultItemAnimator());
        placesList.setEmptyView(tvNoPlacesFound);

        routesList.setHasFixedSize(true);
        routesList.setItemAnimator(new DefaultItemAnimator());
        routesList.setEmptyView(tvNoRoutesFound);

        FontHelper.setFonts(getActivity(),savedLocationsHeader, FontHelper.ROBOTO_BOLD);

        savedLocationsList.setHasFixedSize(true);
        savedLocationsList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        savedLocationsList.setItemAnimator(new DefaultItemAnimator());

        recentLocationsList.setHasFixedSize(true);
        recentLocationsList.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recentLocationsList.setItemAnimator(new DefaultItemAnimator());

        wayPointsList.setHasFixedSize(true);
        wayPointsList.setItemAnimator(new DefaultItemAnimator());
        wayPointsList.setEmptyView(tvNoWayPointsFound);

        etWayPoints.setVisibility(View.VISIBLE);
        etWayPoints.setHint(getResources().getString(R.string.search));
        etWayPoints.setCompoundDrawablesWithIntrinsicBounds(0, 0, ic_clear, 0);
        etWayPoints.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (etWayPoints.getCompoundDrawables()[2] != null) {
                        int leftEdgeOfRightDrawable = etWayPoints.getRight()
                                - etWayPoints.getCompoundDrawables()[2].getBounds().width();
                        if (event.getRawX() >= leftEdgeOfRightDrawable) {
                            etWayPoints.setText("");
                            wayPointsList.setVisibility(View.GONE);

                            //resets the place character length
                            placeCharacterLength = 0;
                            return true;
                        }
                    }
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }

                return false;
            }
        });

        //Add a text change listener to implement autocomplete functionality
        etWayPoints.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //Add a text change listener to implement autocomplete functionality
                    etWayPoints.addTextChangedListener(textWatcher1);

                } else
                    etWayPoints.removeTextChangedListener(textWatcher1);
            }
        });

        if (getArguments() != null) {
            if (getArguments().getBoolean("alternate_routes") || getArguments().getBoolean(Constants.IntentExtraKeys.DEST_SELECTED_ON_MAP)) {
                getDirections();
            }

            if(getArguments().getBoolean("way_points")){
                placesList.setVisibility(View.GONE);
                routesList.setVisibility(View.VISIBLE);

                layoutSearchLocation.setVisibility(View.GONE);
                layoutAddWayPointsParent.setVisibility(View.VISIBLE);
            }else {
                layoutSearchLocation.setVisibility(View.VISIBLE);
                layoutAddWayPointsParent.setVisibility(View.GONE);
            }
        }else {
            presenter.getSavedLocations();
            presenter.getRecentLocations();
        }

        // Initialize CoreRouter
        coreRouter = new CoreRouter();
    }

    @Override
    public void addPlaceAsFavorite(final PlaceAutoComplete.Result place)
    {
        if(place.position == null || place.position.size() == 0)
            return;

        presenter.addPlaceAsFavorite(place.placeId != null ? place.placeId : place.id, place.title, "", "" + place.position.get(0),
                "" + place.position.get(1));
    }

    @Override
    public void removePlaceAsFavorite(final PlaceAutoComplete.Result place)
    {
        presenter.removePlaceAsFavorite(place.placeId != null ? place.placeId : place.id);
    }

    /**
     * call to find coordinates for the place user selected
     * @param place:{@link com.exploride.droid.model.PlaceAutoComplete.Result} instance
     */
    @Override
    public void findCoordinatesByPlaceId(final PlaceAutoComplete.Result place) {
        //call to find coordinates
        presenter.findPlaceCoordinates(place.placeId, new SearchLocationContract.LocationResolverListener() {
            @Override
            public void onLocationResolved(final ArrayList<Double> coordinates) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        place.position = coordinates;
                        if(layoutAddWayPointsParent.getVisibility() == View.VISIBLE)
                            setOnWayPointSelected(place);
                        else
                            setOnPlaceSelected(place);
                    }
                });
            }
        });

//        Thread thread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    while (presenter.getPlaceCoordinates() == null)
//                        Thread.sleep(1000);
//                }catch (Exception e){
//                    ExpLogger.logException(TAG, e);
//                }finally {
//                    if(getActivity() != null){
//                        //sets the place coordinates to PlaceAutoComplete.Result instance
//                        place.position = presenter.getPlaceCoordinates();
//
//                        //call to reset place coordinates
//                        presenter.setPlaceCoordinates(null);
//
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if(layoutAddWayPointsParent.getVisibility() == View.VISIBLE)
//                                    setOnWayPointSelected(place);
//                                else
//                                    setOnPlaceSelected(place);
//                            }
//                        });
//                    }
//                }
//            }
//        });
//        thread.start();
    }

    /**
     * callback for auto suggest place selected by the user
     *
     * @param place: place selected by the user which could either be source or destination
     */
    @Override
    public void setOnPlaceSelected(PlaceAutoComplete.Result place) {

        try {
            placeCharacterLength = place.title.length();

            Location location = new Location("");
            if(place.position == null || place.position.size() == 0)
                return;

            location.setLatitude(place.position.get(0));
            location.setLongitude(place.position.get(1));
            RecentLocation recentLocation = new RecentLocation(place.title, location);
            presenter.saveRecentLocation(recentLocation);

            placesList.setVisibility(View.GONE);

            isLocationSelected = true;
            etPlaceSelected.removeTextChangedListener(textWatcher);
            etPlaceSelected.setText(place.title);

            if (etPlaceSelected.getText().hashCode() == etSource.getText().hashCode()) {
                presenter.setSourceLocation(place.title, place.position);

                etSource.clearFocus();

            } else if (etPlaceSelected.getText().hashCode() == etDestination.getText().hashCode()) {
                presenter.setDestinationLocation(place.title, place.position);

                etDestination.clearFocus();
            }

            if (!TextUtils.isEmpty(etSource.getText().toString()) && !TextUtils.isEmpty(etDestination.getText().toString())) {

                inputMethodManager.hideSoftInputFromWindow(etPlaceSelected.getWindowToken(), 0);
                getDirections();

            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * callback for route selected among various different routes from source to destination
     *
     * @param routeSummary:                 holds route summary like eta, via point, distance
     * @param routeSummarySelectedPosition: listview item's position selected by the user
     */
    @Override
    public void setOnRouteClick(RouteSummary routeSummary, int routeSummarySelectedPosition) {
        // create a map route object and place it on the map
        MapRoute mapRoute = new MapRoute(routeSummaries.get(routeSummarySelectedPosition).getRoute());

        presenter.setMapRoute(mapRoute);

        presenter.setRouteSummary(routeSummaries);

        presenter.setSelectedRouteIndex(routeSummarySelectedPosition);

        presenter.switchToMapsScreen();

    }

    /**
     * Callback for error returned by Google or Here Api autocomplete request
     * @param error: error occurred while making a network request
     */
    @Override
    public void showErrorMessage(String error) {

        toggleProgressBar(destinationProgressBar, false);
        toggleProgressBar(wayPointsProgressBar, false);

        ExpLogger.logE(TAG, error);
        showError(Error.PLACE_ERROR, error);
    }

    private void toggleProgressBar(ProgressBar progressBar, boolean show){
        if (progressBar != null) {
            if(show) {
                if(progressBar.getVisibility()==View.VISIBLE)
                    return;
                etWayPoints.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                etDestination.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                progressBar.setVisibility(View.VISIBLE);
            }
            else {
                if(progressBar.getVisibility()==View.GONE)
                    return;
                etWayPoints.setCompoundDrawablesWithIntrinsicBounds(0, 0, ic_clear, 0);
                etDestination.setCompoundDrawablesWithIntrinsicBounds(0, 0, ic_clear, 0);
                progressBar.setVisibility(View.GONE);
            }
        }

    }
    /**
     * shows auto suggest location fetched from the server in listview
     *
     * @param placeAutoComplete: {@link PlaceAutoComplete} instance
     */
    @Override
    public void showGeocodeLocations(PlaceAutoComplete placeAutoComplete) {

        ExpLogger.logD(TAG, "AutoComplete Request : " + placeAutoComplete.results.size());

        toggleProgressBar(destinationProgressBar, false);
        toggleProgressBar(wayPointsProgressBar, false);

        if (placeAutoComplete.results != null && placeAutoComplete.results.size() == 0) {
            Utils.hideSoftKeyboard(getActivity());
            if (layoutSearchLocation.getVisibility() == View.VISIBLE)
                tvNoPlacesFound.setVisibility(View.VISIBLE);
            else if (layoutAddWayPointsParent.getVisibility() == View.VISIBLE)
                tvNoWayPointsFound.setVisibility(View.VISIBLE);
            return;
        }

        //list to hold valid places with geo coordinate information
        ArrayList<PlaceAutoComplete.Result> resultList = new ArrayList<>();

        for(PlaceAutoComplete.Result result : placeAutoComplete.results){
            if((result.position != null && result.position.size() > 0 ) ||(result.placeId != null))
                resultList.add(result);
        }

        if(etPlaceSelected == etSource || etPlaceSelected == etDestination) {
            routesList.setVisibility(View.GONE);

            // sets adapter to listview to show auto suggest locations
            if (placesAdapter == null) {
                placesAdapter = new PlacesAdapter(resultList, this, new OnAppendSuggestionListener() {
                    @Override
                    public void appendSuggestionText(String suggestionText) {
                        try {
                            // If the edit text has the same text then avoid setting it again
                            if (!etDestination.getText().toString().equals(suggestionText)) {
                                etDestination.setText(suggestionText);
                            }
                            etDestination.setSelection(etDestination.getText().length());
                        } catch (Exception e) {
                            ExpLogger.logException(TAG, e);
                        }
                    }
                });
                placesList.setAdapter(placesAdapter);
            } else {
                placesAdapter.swap(resultList);
            }

            placesList.setVisibility(View.VISIBLE);

        } else if(etPlaceSelected == etWayPoints){

            // sets adapter to listview to show auto suggest locations
            if (wayPointsAdapter == null) {
                wayPointsAdapter = new WayPointsAdapter(resultList, this, new OnAppendSuggestionListener() {
                    @Override
                    public void appendSuggestionText(String suggestionText) {
                        try {
                            // If the edit text has the same text then avoid setting it again
                            if (!etWayPoints.getText().toString().equals(suggestionText)) {
                                etWayPoints.setText(suggestionText);
                            }
                            etWayPoints.setSelection(etWayPoints.getText().length());
                        } catch (Exception e) {
                            ExpLogger.logException(TAG, e);
                        }
                    }
                });
                wayPointsList.setAdapter(wayPointsAdapter);
            } else {
                wayPointsAdapter.swap(resultList);
            }

            wayPointsList.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showToast(String message) {
        if(MainApplication.getInstance().isDebugModeOn())
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_search_location;
    }

    @Override
    public void setOnItemClick(com.cloudant.exploridemobile.Location location) {

        placeCharacterLength = location.getDescription().length();

        isLocationSelected = true;
        etPlaceSelected = etDestination;
        etPlaceSelected.removeTextChangedListener(textWatcher);
        etDestination.setText(location.getDescription());

        etDestination.clearFocus();
        Location destLocation = new Location("");
        destLocation.setLatitude(location.getGeometry().getLatitude());
        destLocation.setLongitude(location.getGeometry().getLongitude());

        presenter.setSavedLocations(location.getDescription(), destLocation);

        if (!TextUtils.isEmpty(etSource.getText().toString()) && !TextUtils.isEmpty(etDestination.getText().toString())) {
            inputMethodManager.hideSoftInputFromWindow(etPlaceSelected.getWindowToken(), 0);
            getDirections();

        }
        hideSavedLocationsList();
        hideRecentLocationsList();
    }

    /**
     * text change listener for source and destination fields
     */
    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // user is typing: reset already started timer (if existing)
            if (timer != null) {
                timer.cancel();
                ExpLogger.logD(TAG, "AutoComplete Cancelled.");
            }

        }

        @Override
        public void afterTextChanged(final Editable s) {
            // user typed: start the timer
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //check to avoid text watcher being called multiple times
                            if(placeCharacterLength == s.length())
                                return;

                            if (mListener != null)
                                mListener.onSetTitleText("Search Location");

                            if (tvNoPlacesFound.getVisibility() == View.VISIBLE)
                                tvNoPlacesFound.setVisibility(View.GONE);

                            if (tvNoRoutesFound.getVisibility() == View.VISIBLE)
                                tvNoRoutesFound.setVisibility(View.GONE);

                            // optimised way is to start searching for action after user has typed minimum 3 chars
                            if (s.length() > 3 && !isLocationSelected) {

                                placeCharacterLength = s.length();

                                routesList.setVisibility(View.GONE);
                                hideSavedLocationsList();
                                hideRecentLocationsList();

                                if (etSource.getText().hashCode() == s.hashCode()) {
                                    etPlaceSelected = etSource;
                                } else if (etDestination.getText().hashCode() == s.hashCode()) {
                                    etPlaceSelected = etDestination;
                                } else if (etWayPoints.getText().hashCode() == s.hashCode()) {
                                    etPlaceSelected = etWayPoints;
                                }

                                ExpLogger.logD(TAG, "AutoComplete Request : " + s.toString());

                                if(placesAdapter != null)
                                    //call to clear previous search results shown to the user
                                    placesAdapter.clearPreviousSearchResults();

                                //call to get auto complete place suggestion for the user's query
                                getPlaceSuggestions(s.toString());

                            } else {
                                if (placesList.getVisibility() != View.GONE)
                                    placesList.setVisibility(View.GONE);

                                if (wayPointsList.getVisibility() != View.GONE)
                                    wayPointsList.setVisibility(View.GONE);

                                if (routesList.getVisibility() != View.GONE)
                                    routesList.setVisibility(View.GONE);
                            }

                            if (etPlaceSelected == etDestination && s.length() == 0) {
                                mShowSavedLocations = true;
                                showSavedLocationList();
                                showRecentLocationList();

                                //resets the place character length
                                placeCharacterLength = 0;
                            }
                        }
                    });

                }
            }, 600); // 600ms delay before the timer executes the "run" method from TimerTask
        }
    };

    /**
     * gets auto complete place suggestion for the user entered query
     * @param s: query to be searched for
     */
    private void getPlaceSuggestions(String s){
        toggleProgressBar(destinationProgressBar, true);
        toggleProgressBar(wayPointsProgressBar, true);

        // fetches auto suggest locations based on user's entered text
        presenter.fetchAutoSuggestLocations(s);
    }

    /**
     * text change listener for way point fields
     */
    private TextWatcher textWatcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            // user is typing: reset already started timer (if existing)
            if (timer != null) {
                timer.cancel();
                ExpLogger.logD(TAG, "AutoComplete Cancelled.");
            }

        }

        @Override
        public void afterTextChanged(final Editable s) {
            // user typed: start the timer
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            //check to avoid text watcher being called multiple times
                            if(placeCharacterLength == s.length())
                                return;

                            if (tvNoWayPointsFound.getVisibility() == View.VISIBLE)
                                tvNoWayPointsFound.setVisibility(View.GONE);

                            // optimised way is to start searching for action after user has typed minimum 3 chars
                            if (s.length() > 3) {

                                placeCharacterLength = s.length();

                                layoutWayPointsHintIcons.setVisibility(View.GONE);
                                tvWayPointsHint.setVisibility(View.GONE);

                                if (etSource.getText().hashCode() == s.hashCode()) {
                                    etPlaceSelected = etSource;
                                } else if (etDestination.getText().hashCode() == s.hashCode()) {
                                    etPlaceSelected = etDestination;
                                } else if (etWayPoints.getText().hashCode() == s.hashCode()) {
                                    etPlaceSelected = etWayPoints;
                                }

                                ExpLogger.logD(TAG, "AutoComplete Request : " + s.toString());

                                if(wayPointsAdapter != null)
                                    //call to clear previous search results shown to the user
                                    wayPointsAdapter.clearPreviousSearchResults();

                                //call to get auto complete place suggestion for the user's query
                                getPlaceSuggestions(s.toString());

                            } else {
                                wayPointsList.setVisibility(View.GONE);
                                layoutWayPointsHintIcons.setVisibility(View.VISIBLE);
                                tvWayPointsHint.setVisibility(View.VISIBLE);

                            }
                        }
                    });
                }
            }, 600); // 600ms delay before the timer executes the "run" method from TimerTask
        }
    };

    @Override
    public void onStart() {
        presenter.onStartView();
        super.onStart();
    }

    @Override
    public void onStop() {
        presenter.onStopView();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroyView();
        //resets the place character length
        placeCharacterLength = 0;

        //checks if there is a ongoing route calculation then cancel
        if(coreRouter != null &&  coreRouter.isBusy()) {
            //call to stop ongoing route calculation if any
            coreRouter.cancel();
            ExpLogger.logI(TAG, "ExpHUD: Core router is busy");
        }

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * gets route to be drawn on map from source to destination
     */
    public void getDirections() {

        progressDialog.show();

        // Select routing options via RoutingMode
        RoutePlan routePlan = new RoutePlan();
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.FASTEST);
        routeOptions.setRouteCount(10);
        routePlan.setRouteOptions(routeOptions);

        //holds source location for the route to be drawn
        sourceLocation = presenter.getSourceLocation();

        // Select Waypoints for your routes
        routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(sourceLocation.getLatitude(), sourceLocation.getLongitude())));

        ArrayList<Location> wayPointsList = presenter.getWayPoints();

        if(wayPointsList != null && wayPointsList.size() >0){

            for (Location location : wayPointsList){
                // adds way points for your routes
                routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(location.getLatitude(), location.getLongitude())));
            }
        }

        //holds destination location for the route to be drawn
        destinationLocation = presenter.getDestinationLocation();

        // END: Searched Location
        routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(destinationLocation.getLatitude(), destinationLocation.getLongitude())));

        //Sets whether route calculation should take traffic into account
        coreRouter.getDynamicPenalty().setTrafficPenaltyMode(Route.TrafficPenaltyMode.OPTIMAL);

        // Retrieve Routing information via CoreRouter.Listener
        coreRouter.calculateRoute(routePlan, routerListener);

    }

    private CoreRouter.Listener routerListener =
            new CoreRouter.Listener() {
                public void onCalculateRouteFinished(List<RouteResult> routeResults,
                                                     RoutingError errorCode) {
                    if (errorCode == RoutingError.NONE) {

                        routeSummaries.clear();

                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }

                        if (routeResults == null || routeResults.size() == 0) {
                            Utils.hideSoftKeyboard(getActivity());
                            tvNoRoutesFound.setVisibility(View.VISIBLE);
                            return;
                        }

                        for (RouteResult routeResult : routeResults) {

                            if (routeResult.getRoute() != null) {

                                MapRoute mapRoute = new MapRoute(routeResult.getRoute());

                                RouteSummary routeSummary = new RouteSummary();

                                presenter.getETA(routeSummary, mapRoute);

                                presenter.getViaPoint(routeSummary, mapRoute);

                                presenter.getDistance(routeSummary, mapRoute);

                                //checks if current route has eta, via point and distance, then only it's considered as valid route
                                if (routeSummary.getEta() != null && !TextUtils.isEmpty(routeSummary.getEta()) &&
                                        routeSummary.getViaPoint() != null && !TextUtils.isEmpty(routeSummary.getViaPoint()) &&
                                        routeSummary.getDistance() != null && !TextUtils.isEmpty(routeSummary.getDistance())) {

                                    routeSummary.setRoute(routeResult.getRoute());

                                    ArrayList<RouteSummary.Maneuvers> maneuversArrayList = new ArrayList<>();
                                    for (Maneuver maneuver : routeResult.getRoute().getManeuvers()) {

                                        RouteSummary.Maneuvers maneuvers = new RouteSummary().new Maneuvers();
                                        presenter.getManeuversInfo(maneuver, maneuvers);

                                        maneuversArrayList.add(maneuvers);
                                    }

                                    routeSummary.setManeuversArrayList(maneuversArrayList);

                                    routeSummaries.add(routeSummary);
                                }
                            }

                        }

                        //call to sort Array List based of ETA
                       presenter.sortRoutesList(routeSummaries);

                        if (routeSummaries != null && routeSummaries.size() > 0) {

                            // shows various routes to destination from source in a listview
                            if (routesAdapter == null) {
                                routesAdapter = new RoutesAdapter(routeSummaries, SearchLocationFragment.this);
                                routesList.setAdapter(routesAdapter);
                            } else {
                                routesAdapter.swap(routeSummaries);
                            }

                            routesList.setVisibility(View.VISIBLE);

                            if (isWayPointAdded) {

                                //by default selecting first route after adding way point. User can still choose any route from the UI
                                setOnRouteClick(routeSummaries.get(0), 0);

                                isWayPointAdded = false;

                                resetViews();

                                wayPointsList.setVisibility(View.GONE);

                            }
                        } else {
                            Utils.hideSoftKeyboard(getActivity());
                            tvNoRoutesFound.setVisibility(View.VISIBLE);
                        }

                    }else {//notify user of the error occurred while route calculation
                        ExpLogger.logE(TAG, errorCode.name());

                        progressDialog.dismiss();

                        if (errorCode == RoutingError.ROUTING_CANCELLED)
                            return;

                        //check if the fragment is currently added to its activity.
                        if (isAdded())
                            showError(Error.ROUTE_ERROR, getResources().getString(R.string.no_routes_found));
                    }

                }

                public void onProgress(int percentage) {
                    ExpLogger.logD("pylogs:", String.format("... %d percent done ...", percentage));
                }
            };

    @Override
    public void setOnWayPointSelected(PlaceAutoComplete.Result place) {

        inputMethodManager.hideSoftInputFromWindow(etWayPoints.getWindowToken(), 0);

        etWayPoints.removeTextChangedListener(textWatcher1);
        etWayPoints.setText(place.title);

        presenter.setWayPoint(place.position);

        routesList.setVisibility(View.GONE);

        isWayPointAdded = true;

        getDirections();

    }

    /**
     * shows reverse geo coded current location
     *
     * @param location: current location that is reverse geo coded
     */
    @Override
    public void showSourceLocation(String location) {

        if (!TextUtils.isEmpty(location)) {
            etSource.setText(location);
            presenter.setSourceLocation();
        }

    }

    @Override
    public void showSavedLocations(List<ExpDocument> locations) {
        mLocations = locations;
        if (mLocations != null) {
            final SavedLocationsAdapter savedLocationsAdapter = new SavedLocationsAdapter(getActivity(), mLocations, this);

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        savedLocationsList.setAdapter(savedLocationsAdapter);
                        showSavedLocationList();
                    }
                });
            }
        }
    }

    @Override
    public void showRecentLocations(List<RecentLocation> locations) {
        mRecentLocations = locations;
        if (mRecentLocations != null && mRecentLocations.size() > 0) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    RecentLocationsAdapter recentLocationsAdapter = new RecentLocationsAdapter(mRecentLocations,
                            SearchLocationFragment.this);
                    recentLocationsList.setAdapter(recentLocationsAdapter);
                    showRecentLocationList();
                }
            });
        }
    }

    private void showSavedLocationList(){
        if (mLocations != null && mLocations.size() > 0 && mShowSavedLocations)
            savedLocationContainer.setVisibility(View.VISIBLE);
        else
            savedLocationContainer.setVisibility(View.GONE);
    }

    private void hideSavedLocationsList(){
        mShowSavedLocations = false;
        savedLocationContainer.setVisibility(View.GONE);
    }

    private void showRecentLocationList(){
        if (mRecentLocations != null && mRecentLocations.size() > 0) {
            recentLocationsList.setVisibility(View.VISIBLE);
        }
    }

    private void hideRecentLocationsList(){
        recentLocationsList.setVisibility(View.GONE);
    }

    private void resetViews(){

        etWayPoints.setText("");

        layoutSearchLocation.setVisibility(View.VISIBLE);

        layoutAddWayPointsParent.setVisibility(View.GONE);

        layoutWayPointsHintIcons.setVisibility(View.VISIBLE);
        tvWayPointsHint.setVisibility(View.VISIBLE);

    }

    @Override
    public void onBackPressed() {
        // handle back button
        presenter.switchToMapsScreen();
    }

    /** shows the error prompt to the user that occurred while making search location request
     * @param error_type: @{@link Error} value (PLACES_ERROR or ROUTE_ERROR)
     * @param message: description on error
     */
    private void showError(final Error error_type, String message) {
        if (errorDialog == null)
            errorDialog = new AlertDialog.Builder(getActivity());

        errorDialog.setTitle(getResources().getString(R.string.alert));
        errorDialog.setMessage(message);
        errorDialog.setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                switch (error_type){
                    case PLACE_ERROR:
                        //resets the place character length
                        placeCharacterLength = 0;

                        //retry to get auto complete place suggestion for the user's query
                        getPlaceSuggestions(etDestination.getText().toString());
                        break;

                    case ROUTE_ERROR:
                        //retry to perform route calculation to the previous selected destination
                        getDirections();
                        break;
                }
            }
        });
        errorDialog.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        errorDialog.show();
    }

    @Override
    public void setOnItemClick(RecentLocation location) {

        placeCharacterLength = location.getName().length();

        Location destLocation = new Location("");
        destLocation.setLatitude(location.getLocation().getLatitude());
        destLocation.setLongitude(location.getLocation().getLongitude());

        presenter.setSavedLocations(location.getName(), destLocation);

        hideSavedLocationsList();
        hideRecentLocationsList();

        isLocationSelected = true;
        etPlaceSelected = etDestination;
        etPlaceSelected.removeTextChangedListener(textWatcher);
        etPlaceSelected.setText(location.getName());

        if (etPlaceSelected.getText().hashCode() == etDestination.getText().hashCode()) {
            ArrayList<Double> position = new ArrayList<>();
            position.add(location.getLocation().getLatitude());
            position.add(location.getLocation().getLongitude());
            presenter.setDestinationLocation(location.getName(), position);

            etDestination.clearFocus();
        }

        if (!TextUtils.isEmpty(etSource.getText().toString()) && !TextUtils.isEmpty(etDestination.getText().toString())) {

            inputMethodManager.hideSoftInputFromWindow(etPlaceSelected.getWindowToken(), 0);
            getDirections();

        }
    }

    /**
     * initializes progress dialog to be shown when calculating routes
     */
    private void initProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //checks if core router is calculating any route
                if(coreRouter != null &&  coreRouter.isBusy()) {
                    //call to stop ongoing route calculation if any
                    coreRouter.cancel();
                    ExpLogger.logI(TAG, "Cancelling the ongoing route calculation");
                }
            }
        });
    }

    public interface OnAppendSuggestionListener{
        void appendSuggestionText(String suggestionText);
    }

}
