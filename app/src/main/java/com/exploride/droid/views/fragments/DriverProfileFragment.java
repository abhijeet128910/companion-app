package com.exploride.droid.views.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cloudant.exploridemobile.ConnectedDriverAPI.API;
import com.cloudant.exploridemobile.ConnectedDriverAPI.DriverStatistics;
import com.cloudant.exploridemobile.ConnectedDriverAPI.ScoringBehavior;
import com.cloudant.exploridemobile.Helper;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.profile.ProfileDataAdapter;
import com.exploride.droid.utils.Constants;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;

/**
 * Created by pranav.r on 12/6/17.
 */

public class DriverProfileFragment extends BaseFragment {
        public DriverStatistics stat;

    public ArrayList<ScoringBehavior> behaviors = new ArrayList<ScoringBehavior>();

    public DriverStatistics.TimeRange timesOfDay;
    public DriverStatistics.SpeedPattern trafficConditions;
    public DriverStatistics.RoadType roadTypes;

    private View view;
    @Override
    protected int getFragmentLayout() {
        return R.layout.layout_trips;
    }

    @Override
    public void onBackPressed() {
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_reservations, container, false);

        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        ((AppCompatActivity) activity).getSupportActionBar().setTitle("Fetching your profile...");

        API.runInAsyncUIThread(new Runnable() {
            @Override
            public void run() {
                getDriverStats();
            }
        }, activity);

        return view;
    }

    private void getDriverStats() {
        final String url = API.driverStats;

        try {
            final API.doRequest task = new API.doRequest(new API.doRequest.TaskListener() {
                     @Override
                    public void postExecute(JSONArray result)//throws JSONException
                    {
                        try {
                    result.remove(result.length() - 1);

                    final ListView listView = (ListView) view.findViewById(R.id.listView);

                    final ArrayList<DriverStatistics> stats = new ArrayList<DriverStatistics>();

                    for (int i = 0; i < result.length(); i++) {
                        final DriverStatistics tempDriverStatistics = new DriverStatistics(result.getJSONObject(i));
                        stats.add(tempDriverStatistics);
                    }

                    final AppCompatActivity activity = (AppCompatActivity) getActivity();
                    if (activity == null) {
                        return;
                    }
                    final ActionBar supportActionBar = ((AppCompatActivity) activity).getSupportActionBar();
                    if (stats.size() > 0) {
                        stat = stats.get(0);
                        behaviors = stat.scoring.getScoringBehaviors();

                        Collections.sort(behaviors, new Comparator<ScoringBehavior>() {
                            @Override
                            public int compare(ScoringBehavior b1, ScoringBehavior b2) {
                                return b2.count - b1.count;
                            }
                        });

                        timesOfDay = stat.timeRange;
                        timesOfDay.toDictionary();

                        roadTypes = stat.roadType;
                        roadTypes.toDictionary();

                        trafficConditions = stat.speedPattern;
                        trafficConditions.toDictionary();

                        supportActionBar.setTitle("Your score is " + Math.round(stat.scoring.score) + "% for " + Math.round(stat.totalDistance / 16.09344) / 100 + " miles.");

                        final ProfileDataAdapter adapter = new ProfileDataAdapter(activity.getApplicationContext(), stats, behaviors, timesOfDay, trafficConditions, roadTypes);
                        listView.setAdapter(adapter);
                    } else {
                        supportActionBar.setTitle("You have no analyzed trips.");
                    }


                    Log.i("Profile Data", result.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            });

            task.execute(url, "GET").get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
