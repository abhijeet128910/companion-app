package com.exploride.droid.views.fragments;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.exploride.droid.AppVersionContract;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.AppVersionPresenter;
import com.exploride.droid.utils.appupdater.ExplorideUpdateChecker;

import static android.R.id.message;
import static com.exploride.droid.utils.appupdater.ExplorideUpdateChecker.mUpdateUrl;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnAppVersionFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AppVersionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AppVersionFragment extends BaseFragment implements AppVersionContract.VersionView {

    private static final String TAG = "DownloadMapsFragment";
    private AppVersionContract.VersionOps presenter;
    private OnAppVersionFragmentInteractionListener mListener;
    private Activity mActivity;
    private TextView tvMobVersionName;
    private TextView tvHudVersionName;
    private TextView tvUpdateUrl;
    static AppVersionFragment appAppVersionFragment;

    public AppVersionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AppVersionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AppVersionFragment newInstance() {
        if (appAppVersionFragment == null)
            appAppVersionFragment = new AppVersionFragment();
        return appAppVersionFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = AppVersionPresenter.getInstance();

        //call to initialize View used by Presenter to interact with the fragment
        presenter.init(this);

        tvMobVersionName = (TextView) view.findViewById(R.id.mob_version_name);
        tvHudVersionName = (TextView) view.findViewById(R.id.hud_version_name);
        tvUpdateUrl = (TextView) view.findViewById(R.id.update_url);
        tvUpdateUrl.setText(mUpdateUrl);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!MainApplication.getInstance().isDebugModeOn())
            tvUpdateUrl.setVisibility(View.GONE);
        //call to show mobile app version
        showMobileAppVersion();

        //call to request HUD app version
        getHudAppVersion();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();

        mListener = (OnAppVersionFragmentInteractionListener) getActivity();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        tvMobVersionName.setText("");
        tvHudVersionName.setText("");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAppVersionFragmentInteractionListener {
        // TODO: Update argument type and name
        void getHudAppVersion();
    }

    /**
     * shows mobile app version to the user
     */
    private void showMobileAppVersion() {
        try {
            PackageManager manager = mActivity.getPackageManager();
            PackageInfo info = manager.getPackageInfo(
                    mActivity.getPackageName(), 0);
            tvMobVersionName.setText(info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * gets HUD app version
     */
    private void getHudAppVersion() {
        if(!MainApplication.getInstance().isCommunicationChannelOpen()){
            showHudAppVersionInfo(getString(R.string.not_connected));
        }
        mListener.getHudAppVersion();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_app_version;
    }

    @Override
    public void onBackPressed() {
        // handle back button
        presenter.switchToMapsScreen();
    }

    /**
     * shows toast to the user
     *
     * @param message: message shown to the user
     */
    @Override
    public void showToast(String message) {
        if (MainApplication.getInstance().isDebugModeOn())
            Toast.makeText(this.getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * shows HUD App version info
     * @param versionName: HUD app version name
     */
    @Override
    public void showHudAppVersionInfo(String versionName) {
        tvHudVersionName.setText(versionName);
    }
}
