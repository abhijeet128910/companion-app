package com.exploride.droid.views.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudant.exploridemobile.framework.DevOptions;
import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.MainApplication;
import com.exploride.droid.MapsContract;
import com.exploride.droid.R;
import com.exploride.droid.adapter.RoutesPagerAdapter;
import com.exploride.droid.location.CustomLocationProviderDefault;
import com.exploride.droid.location.LocationTracer;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.presenters.SearchLocationPresenter;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.CommonUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.FontHelper;
import com.exploride.droid.utils.LocationMockerPhone;
import com.exploride.droid.views.custom_views.SlidingUpPanelLayout;
import com.here.android.mpa.cluster.ClusterLayer;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.MapTrafficLayer;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import static com.here.android.mpa.guidance.NavigationManager.UnitSystem.IMPERIAL_US;

/**
 * Class to search and find destinations and eventually send
 * it to the HUD.
 * <p>
 * Author Mayank
 */

public class MapsDestinationFragment extends BaseFragment implements MapsContract.MapsView, RoutesPagerAdapter.OnRouteInteractionListener {

    private NavigationManager navigationManager;

    private static String TAG = "MapsDestinationFragment";
    private Context context;

    // Initial map scheme, initialized in onCreate() and accessed in goHome()
    MapRoute mapRoute;

    MapsContract.MapsOps presenter;
    private PositioningManager posManager;

    //holds status of positioning manager active or paused.
    private boolean stopped;

    private boolean isMapReady = false;

    // map for this activity's map fragment
    private Map map = null;

    // map fragment of this activity
    private MapFragment mapFragment = null;

    private ClusterLayer clusterLayer;

    private MapMarker destinationMarker;
    private MapMarker wayPointMarker;

    private Location sourceLocation;
    private Location destinationLocation;
    private GeoBoundingBox geoBoundingBox;

    private LinearLayout layoutAddressFields;
    private LinearLayout layoutExitNavigation;
    private LinearLayout layoutStopNavigation;
    private LinearLayout stopNavigationView;
    private RelativeLayout layoutSource;
    private RelativeLayout layoutDestination;
    private ImageView ivSource;
    private TextView tvSource;
    private ImageView ivDestination;
    private TextView tvDestination;
    private ViewPager viewPagerRouteSummary;
    private LinearLayout layoutDestinationPreview;
    private TextView tvDestinationName;
    private ImageView ivCloseRouteSummary;
    private ImageView ivSearchWayPoint;
    private AlertDialog.Builder confirmExitAlert;
    private ProgressBar progressBar;
    private boolean isMapViewVisible;
    private SlidingUpPanelLayout mLayout;
    private FloatingActionButton fabCurrentLocation;

    private OnFragmentInteractionListener mListener;

    //gesture listener for map
    private MapOnGestureListener mapOnGestureListener = new MapOnGestureListener();

    private boolean isDragging;
    private float lastX;
    private float lastY;
    private float deltaX;

    private Location mCurrentLocation = new Location("");

    private ProgressDialog progressDialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initProgressBar();
        progressDialog.show();

        titleChangedListener.onSetTitleText(null);
        presenter = MapsPresenter.getInstance();

        //call to initialize View used by Presenter to interact with the fragment
        presenter.init(this);

        mLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_panel_layout);
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                ExpLogger.logI(TAG, "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                ExpLogger.logI(TAG, "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.map_fragment);

        layoutDestinationPreview = (LinearLayout) view.findViewById(R.id.ll_destination_preview);
        viewPagerRouteSummary = (ViewPager) view.findViewById(R.id.viewpager_route_summary);
        layoutExitNavigation = (LinearLayout) view.findViewById(R.id.ll_exit_navigation);
        layoutStopNavigation = (LinearLayout) layoutExitNavigation.findViewById(R.id.ll_stop_navigation);
        stopNavigationView = (LinearLayout) view.findViewById(R.id.stop_navigation);
        layoutAddressFields = (LinearLayout) view.findViewById(R.id.ll_address_fields);
        layoutSource = (RelativeLayout) view.findViewById(R.id.rl_source);
        layoutDestination = (RelativeLayout) view.findViewById(R.id.rl_destination);

        ivSource = (ImageView) layoutSource.findViewById(R.id.iv_address);
        ivSource.setImageResource(R.drawable.my_location);

        tvSource = (TextView) layoutSource.findViewById(R.id.tv_address);

        tvSource.setHint(getResources().getString(R.string.current_location));
        tvSource.setVisibility(View.VISIBLE);

        progressBar = (ProgressBar) (layoutSource.findViewById(R.id.progress_view));
        progressBar.setIndeterminate(true);
        toggleProgressBar(true);

        ivDestination = (ImageView) layoutDestination.findViewById(R.id.iv_address);
        ivDestination.setImageResource(R.drawable.searched_location);

        tvDestination = (TextView) layoutDestination.findViewById(R.id.tv_address);
        tvDestination.setHint(getResources().getString(R.string.desired_location));
        tvDestination.setVisibility(View.VISIBLE);
        tvDestination.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_clear, 0);
        tvDestination.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getX() >= (tvDestination.getRight() - tvDestination.getCompoundDrawables()[2].
                        getBounds().width())) {
                    clearSelectedLocation();
                    presenter.resetDestinationLocationName();
                    return true;
                } else {
                    if(!TextUtils.isEmpty(tvSource.getText().toString()))
                        mListener.onSearchLocation();
                }
                return false;
            }
        });

        ivCloseRouteSummary = (ImageView) layoutDestinationPreview.findViewById(R.id.iv_close_route_summary);
        tvDestinationName = (TextView) layoutDestinationPreview.findViewById(R.id.tv_destination_name);

        ivSearchWayPoint = (ImageView) view.findViewById(R.id.iv_search_way_point);
        fabCurrentLocation = (FloatingActionButton) view.findViewById(R.id.fab_current_location);
        fabCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call to set current location on map
                setCurrentLocation();
            }
        });

        try {
            initMap();
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }

        layoutStopNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // If bluetooth is connected, requesting HUD to send end trip
                if (MainApplication.getInstance().isCommunicationChannelOpen())
                    sendExitNavigationMsg();
                // Should be exited in both cases
                presenter.resetNavigation();

            }
        });

        stopNavigationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.setNavigationStarted(false);
                layoutStopNavigation.performClick();
                stopNavigation();
            }
        });
        //drag event to start navigation
        /*ivStartNavigation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();

                if (action == MotionEvent.ACTION_DOWN && !isDragging) {
                    isDragging = true;
                    deltaX = event.getX();
                    return true;
                } else if (isDragging) {
                    if (action == MotionEvent.ACTION_MOVE) {

                        v.setX(v.getX() + event.getX() - deltaX);
                        v.setY(v.getY());
                        return true;
                    } else if (action == MotionEvent.ACTION_UP) {
                        isDragging = false;
                        lastX = event.getX();
                        lastY = event.getY();

                        // call to pass the destination to HUD
                        sendRouteInfo(tvDestination.getText().toString());

                        return true;
                    } else if (action == MotionEvent.ACTION_CANCEL) {
                        v.setX(lastX);
                        v.setY(lastY);
                        isDragging = false;
                        return true;
                    }
                }

                return false;
            }
        });*/

        ivCloseRouteSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navigationManager != null && navigationManager.getRunningState().equals(NavigationManager.NavigationState.RUNNING)){
                    stopNavigationView.performClick();
                } else {
                    presenter.resetNavigation();
                }
            }
        });

        layoutAddressFields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mListener.onSearchLocation();
            }
        });

        ivSearchWayPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //start fragment
                mListener.onAddWayPoint();
            }
        });
        if (getArguments() != null)
            readArgs();
    }

    private void readArgs() {
        if (getArguments().containsKey("ReachedDest"))
            presenter.resetNavigation();
    }

    @Override
    public void onStart() {
        presenter.onStartView();
        super.onStart();

        //request position updates
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    while (!isMapReady)
                        Thread.sleep(2000);
                }catch (Exception e){
                    e.printStackTrace();
                    ExpLogger.logException(TAG, e);
                }finally {
                    if(getActivity() != null){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{

                                    //call to set current location on map
                                    setCurrentLocation();

                                    initPositionListener();
//                                    //start current location updates
//                                    if (posManager != null && posManager.start(PositioningManager.LocationMethod.GPS_NETWORK)) {
//                                        // Position updates started successfully.
//                                        Log.d(TAG, "Position updates started successfully");
//
//                                        stopped = false;
//                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                    ExpLogger.logException(TAG, e);
                                }
                            }
                        });
                    }
                }
            }
        });thread.start();

    }

    CustomLocationProviderDefault mCustomLPD;
    void setDataSrcDefault(){
        if (getActivity() != null) {
            ExpLogger.logI(TAG, "gpsMock: enabling CustomLocationProviderDefault");
            mCustomLPD = CustomLocationProviderDefault.getInstance(getActivity());
            posManager.setDataSource(mCustomLPD);
        }
    }

    public void initPositionListener() {

        ExpLogger.logI(TAG, "gpsMock: initPositionListener");
        if (posManager == null) {
            posManager = PositioningManager.getInstance();
        }

        if(posManager.isActive())
            posManager.stop();
        setDataSrcDefault();
        stopped = true;
        if(posManager != null) {

            // indicates that positioning manager is active
            stopped = false;
            // register positioning manager to get current location updates
            posManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));

            //start current location updates
            if (posManager.start(mCustomLPD.mLocationMethod)) {
                // Position updates started successfully.
                ExpLogger.logD(TAG, "Position updates started successfully");
            }
        }
        posManager.setLogType(EnumSet.of(PositioningManager.LogType.DATA_SOURCE));
    }

    @Override
    public void onStop() {
        //stop position updates
        if (posManager != null) {
            posManager.stop();

            // indicates that positioning manager is stopped
            stopped = true;
        }
        presenter.onStopView();
        super.onStop();

    }

    void initMap() {
        if(getActivity() != null && MainApplication.getInstance().isDebugModeOn())
            Toast.makeText(getActivity(), "Initializing map...", Toast.LENGTH_LONG).show();

        new Thread(new Runnable() {
            @Override
            public void run() {

                mapFragment.init(new OnEngineInitListener() {
                    @Override
                    public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {

                        if (error == OnEngineInitListener.Error.NONE) {

                            try {

                                //initializing cluster layer to hold marker
                                clusterLayer = new ClusterLayer();

                                onMapFragmentInitializationCompleted();

                                initPositionListener();
//                                posManager = PositioningManager.getInstance();
//                                if (posManager != null) {
//
//                                    // register positioning manager to get current location updates
//                                    posManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));
//
//                                }

                            } catch (NullPointerException e) {
                                ExpLogger.logException(TAG, e);
                            }

                        } else {

                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                            System.out.println("ERROR: Cannot initialize Map Fragment:"+error);
                        }
                    }
                });
            }
        }).start();
    }

    private void onMapFragmentInitializationCompleted() {

        // retrieve a reference of the map from the map fragment
        if (map == null)
            map = mapFragment.getMap();

        if (map == null)
            return;

        // Sets the zoom level to max
        map.setZoomLevel(map.getMaxZoomLevel());

        // Set the map center to the US region with zoomed out view
        if(MainPresenter.getInstance().getCurrentLocation()!=null){

            map.setCenter(new GeoCoordinate(MainPresenter.getInstance().getCurrentLocation().getLatitude(),
                    MainPresenter.getInstance().getCurrentLocation().getLongitude()),
                    Map.Animation.LINEAR);
        }else
            map.setCenter(new GeoCoordinate(Constants.latitude, Constants.longitude),
                    Map.Animation.LINEAR);

        // Display position indicator
        map.getPositionIndicator().setVisible(true);

        // setting current location icon
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.current_location);
        Image image = new Image();
        image.setBitmap(icon);

        map.getPositionIndicator().setMarker(image);

        map.setMapScheme(Map.Scheme.NORMAL_DAY_TRANSIT);

        map.setTrafficInfoVisible(true);

        MapTrafficLayer mapTrafficLayer = map.getMapTrafficLayer();

        //Show traffic flow layer
        mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.FLOW, false);

        //Show on route traffic layer
        mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.ONROUTE, true);

        //Show traffic incidents layer
        mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.INCIDENT, false);

        isMapReady = true;

        if (TextUtils.isEmpty(tvSource.getText().toString()))//call presenter to reverse geocode user's current location
            presenter.requestCurrentLocToGeocode();
        else if (presenter.getSourceLocationName() != null && !TextUtils.isEmpty(presenter.getSourceLocationName())) {//sets source location selected by the user
            tvSource.setText(presenter.getSourceLocationName());
        }

        //sets destination location selected by the user
        if (presenter.getDestinationLocationName() != null && !TextUtils.isEmpty(presenter.getDestinationLocationName())) {

            tvDestination.setText(presenter.getDestinationLocationName());
        }

        tvDestinationName.setText(presenter.getDestinationLocationName());

        if (presenter.getMapRoute() != null && presenter.getRouteSummary() != null) {

            //renders route on map selected by the user
            renderRouteOnMap(presenter.getMapRoute());

            layoutDestinationPreview.setVisibility(View.VISIBLE);

            if(!presenter.isNavigationStarted()) {
                //call to hide tool bar
                mListener.onHideToolBar();

                //shows summary for the route selected
                showRouteSummary(presenter.getRouteSummary());
            }

            presenter.setRouteDrawn(true);
        }else{
            layoutAddressFields.setVisibility(View.VISIBLE);

            //call to show tool bar
            mListener.onShowToolBar();
            presenter.setRouteDrawn(false);
        }

        if(navigationManager == null)
            navigationManager = NavigationManager.getInstance();

        if(presenter.isNavigationStarted()){

            //call to hide tool bar
            mListener.onHideToolBar();

            layoutAddressFields.setVisibility(View.GONE);

            if (layoutDestinationPreview.getVisibility() != View.VISIBLE)
                layoutDestinationPreview.setVisibility(View.VISIBLE);

            mLayout.setShadowHeight(0);
            viewPagerRouteSummary.setVisibility(View.GONE);
            ivSearchWayPoint.setVisibility(View.GONE);
            fabCurrentLocation.setVisibility(View.GONE);

            boolean isNavigationView = false;
            if (getActivity() != null)
                isNavigationView = SettingsUtils.readPreferenceValueAsBoolean(getActivity(),
                        Constants.KEY_BASE + Constants.KEY_NAVIGATION_VIEW);
            if (isNavigationView) {
                stopNavigationView.setVisibility(View.VISIBLE);
            } else {
                layoutExitNavigation.setVisibility(View.VISIBLE);
            }
            startNavigation();
        } else
            fabCurrentLocation.setVisibility(View.VISIBLE);

        // sets gesture listener on map
        mapFragment.getMapGesture().addOnGestureListener(mapOnGestureListener);

        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void showToast(String message) {
//            Toast.makeText(this.getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * It's not called because this method has been added in API 23. If you run application on a device with API 23 (marshmallow)
     * then onAttach(Context) will be called. On all previous Android Versions onAttach(Activity) will be called.
     *
     * @param context: Context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;
    }

    static MapsDestinationFragment MapsDestinationFragment;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapsDestinationFragment.
     */
    public static MapsDestinationFragment getInstance() {

        if (MapsDestinationFragment == null)
            MapsDestinationFragment = new MapsDestinationFragment();

        return MapsDestinationFragment;
    }

    public MapsDestinationFragment() {

        // Required empty public constructor
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_maps;
    }

    // current location changed listener
    private PositioningManager.OnPositionChangedListener positionListener = new
            PositioningManager.OnPositionChangedListener() {

                public void onPositionUpdated(PositioningManager.LocationMethod method,
                                              GeoPosition position, boolean isMapMatched) {
                    // set the center only when the app is in the foreground
                    // to reduce CPU consumption
                    mCurrentLocation.setLatitude(position.getCoordinate().getLatitude());
                    mCurrentLocation.setLongitude(position.getCoordinate().getLongitude());
//                    MainPresenter.getInstance().setCurrentLocation(mCurrentLocation);
                }

                public void onPositionFixChanged(PositioningManager.LocationMethod method,
                                                 PositioningManager.LocationStatus status) {
                }
            };


    private void confirmExit(){
        if(getActivity() != null) {
            if (confirmExitAlert == null) {
                confirmExitAlert = new AlertDialog.Builder(getActivity());
                confirmExitAlert.setTitle(getResources().getString(R.string.exit_nav_confirm));
                confirmExitAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendExitNavigationMsg();
                        presenter.resetNavigation();
                    }
                });

                confirmExitAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
            }
            confirmExitAlert.show();
        }
    }

    /**
     * Resets navigation. Called when stop navigation or back pressed.
     */
    @Override
    public void resetNavigation(){
        if (map != null) {
            //reset map to default state
            resetMap();
        }
        stopNavigation();
        LocationMockerPhone.getInstance().handlePhoneMockingStatusChange(false);
        // resets view to default state
        viewPagerRouteSummary.setVisibility(View.GONE);
        layoutDestinationPreview.setVisibility(View.GONE);
        fabCurrentLocation.setVisibility(View.VISIBLE);
        tvSource.setText("");
        tvDestination.setText("");
        tvDestinationName.setText("");

        ivSearchWayPoint.setVisibility(View.GONE);
        layoutStopNavigation.setVisibility(View.GONE);
        layoutExitNavigation.setVisibility(View.GONE);
        stopNavigationView.setVisibility(View.GONE);
        layoutAddressFields.setVisibility(View.VISIBLE);

        //call to show tool bar
        mListener.onShowToolBar();
    }

    /**
     * Show confirmation dialog when destination selected
     */
    private void showDestConfirmDialog(final String address) {
        if(getActivity() != null) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.destination_confirm_layout, null);
            final AlertDialog destConfirmDest = new AlertDialog.Builder(getActivity()).create();
            destConfirmDest.setView(view);

            // When dialog is cancelled by tapping outside the area of dialog
            destConfirmDest.setOnCancelListener(
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            clearSelectedLocation();
                        }
                    }
            );

            TextView desiredLocationHeader = (TextView) view.findViewById(R.id.desired_location_header);
            FontHelper.setFonts(getActivity(), desiredLocationHeader, FontHelper.ROBOTO_MEDIUM);

            TextView desiredLocationAddress = (TextView) view.findViewById(R.id.desired_location_address);
            FontHelper.setFonts(getActivity(), desiredLocationAddress, FontHelper.ROBOTO_MEDIUM);
            String addressCheck = address;
            if (addressCheck != null && !addressCheck.isEmpty()) {
                if (addressCheck.contains("\n"))
                    addressCheck = addressCheck.replaceAll("\n", "");

                desiredLocationAddress.setText(addressCheck.trim());
            }

            Button confirm = (Button) view.findViewById(R.id.dest_confirm);
            FontHelper.setFonts(getActivity(), confirm, FontHelper.ROBOTO_MEDIUM);
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    destConfirmDest.dismiss();
                    if (map != null && !TextUtils.isEmpty(address)
                            && !TextUtils.isEmpty(address)) {
                        presenter.getRouteForLocationsSel();
                    }
                }
            });

            ImageView closeDlg = (ImageView) view.findViewById(R.id.cancel_dest);
            closeDlg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    destConfirmDest.dismiss();
                    clearSelectedLocation();
                }
            });
            destConfirmDest.show();
        }
    }

    @Override
    public void onDestroy() {

        //call to reset View used by the Presenter to interact with the fragment
        if (presenter != null) {
            presenter.onDestroyView();
        }

        if (posManager != null) {
            // Cleanup
            posManager.removeListener(
                    positionListener);
        }
        if (navigationManager != null) {

            navigationManager.removePositionListener(navigationPositionListener);
        }

            map = null;
        super.onDestroy();

        if (progressDialog.isShowing())
            progressDialog.dismiss();

        progressDialog = null;

    }

    /**
     * sets source and destination location marker on the map
     */
    private void setMarker() {

        if (map == null)
            return;

        // removing previous markers drawn
        if (clusterLayer != null) {
            map.removeClusterLayer(clusterLayer);
            clusterLayer.removeMarkers(clusterLayer.getMarkers());
        }

        //holds source location for the route to be drawn
        sourceLocation = presenter.getSourceLocation();

        //holds destination location for the route to be drawn
        destinationLocation = presenter.getDestinationLocation();
        if (destinationLocation != null) {
            destinationMarker = new MapMarker();

            // sets destination location icon
            Bitmap icon = BitmapFactory.decodeResource(getResources(),
                    R.drawable.flag);
            Image image = new Image();
            image.setBitmap(icon);

            destinationMarker.setIcon(image);
            destinationMarker.setCoordinate(new GeoCoordinate(destinationLocation.getLatitude(), destinationLocation.getLongitude()));

            clusterLayer.addMarker(destinationMarker);

        }

        ArrayList<Location> wayPointsList = SearchLocationPresenter.getInstance().getWayPoints();

        if(wayPointsList != null && wayPointsList.size() >0){

            for (Location location : wayPointsList){
                // adds way points for your routes
                wayPointMarker = new MapMarker();

                // setting source location icon
                Bitmap icon = BitmapFactory.decodeResource(getResources(),
                        R.drawable.way_point);
                Image image = new Image();
                image.setBitmap(icon);

                wayPointMarker.setIcon(image);
                wayPointMarker.setCoordinate(new GeoCoordinate(location.getLatitude(), location.getLongitude()));

                clusterLayer.addMarker(wayPointMarker);
            }
        }

        map.addClusterLayer(clusterLayer);

    }

    /**
     * check whether map is initialized and rendered
     *
     * @return boolean true if Map is initialized else false
     */
    @Override
    public boolean isMapInitialized() {
        return isMapReady;
    }


    /**
     * renders route on the map
     * @param mapRoute: route selected by the user
     */
    private void renderRouteOnMap(MapRoute mapRoute) {
        // clear previous results
        if (map != null && this.mapRoute != null) {
            map.removeMapObject(this.mapRoute);
            this.mapRoute = null;
        }

        // create a map route object and place it on the map
        this.mapRoute = new MapRoute(mapRoute.getRoute());
        this.mapRoute.setTrafficEnabled(true);
        map.addMapObject(this.mapRoute);

        // Get the bounding box containing the route and zoom in
        geoBoundingBox = mapRoute.getRoute().getBoundingBox();

       /* int length = this.mapRoute.getRoute().getLength();

        if (length > 0) {
            geoBoundingBox.expand(length, length);
        }*/

//        map.zoomTo(geoBoundingBox, Map.Animation.NONE, Map.MOVE_PRESERVE_ORIENTATION);

        setMarker();

    }

    /**
     * shows summary for the route drawn
     * @param routeSummaries: eta, via point, distance for routes
     */
    private void showRouteSummary(ArrayList<RouteSummary> routeSummaries) {

        RoutesPagerAdapter routesPagerAdapter = new RoutesPagerAdapter(this, getActivity(), routeSummaries);
        viewPagerRouteSummary.setAdapter(routesPagerAdapter);
        viewPagerRouteSummary.addOnPageChangeListener(routesPagerAdapter);
        viewPagerRouteSummary.setCurrentItem(presenter.getSelectedRouteIndex());

        //thread to wait for 100 ms before rendering the sliding up panel
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                }catch (Exception e){
                    ExpLogger.logException(TAG, e);
                }finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewPagerRouteSummary.setVisibility(View.VISIBLE);
                            mLayout.setShadowHeight(4);
                            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        }
                    });
                }
            }
        });
        thread.start();

        ivSearchWayPoint.setVisibility(View.VISIBLE);
        layoutAddressFields.setVisibility(View.GONE);

    }

    /**
     * Listener to send destination to HUD
     */
    public interface OnFragmentInteractionListener {

        void onSendDestination(DevOptions.Destination destination);

        void onSendGeoCoordinates(ArrayList<Navigation.GeoCoordinate> routeGeoCoordinateList);

//        void onSendRouteInfo(String destination, ArrayList<Navigation.GeoCoordinate> routeGeoCoordinateList);

        void onSendRouteInfo(String destination, ArrayList<Navigation.GeoCoordinate> routeGeoCoordinateList, double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude);

//        void onSendDelimiter(String delimiter);

        void onSearchLocation();

        void onShowAlternateRoutes();

        void onAddWayPoint();

        void onExitNavigation();

        void onHideToolBar();

        void onShowToolBar();

        void onExitApp();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mListener = (OnFragmentInteractionListener) getActivity();
    }

    /**
     * called on start navigation click. sends the route information to the HUD for starting the navigation
     */
    public void sendRouteInfo() {
        if (mListener != null) {

            // sends selected destination to HUD
//            mListener.onSendDestination(tvDestination.getText().toString());

            // sends seleected destination to HUD
            if (getActivity() != null && SettingsUtils.readPreferenceValueAsBoolean(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_DEST_PT)) {
                DevOptions.Destination destination = new DevOptions().new Destination();
                destination.setDestination(tvDestination.getText().toString());
                destination.setDestLat(presenter.getDestinationLocation().getLatitude());
                destination.setDestLon(presenter.getDestinationLocation().getLongitude());
                mListener.onSendDestination(destination);
            } else {

                ArrayList<Navigation.GeoCoordinate> locationList = new ArrayList<>();

                List<Maneuver> maneuverList = mapRoute.getRoute().getManeuvers();

//            int completeBatch = (int) (maneuverList.size() / 5);
//            int remainingBatch = maneuverList.size() % 5;

                for (int counter = 0; counter < maneuverList.size(); counter++) {

                    GeoCoordinate geoCoordinate = maneuverList.get(counter).getCoordinate();

                    Navigation.GeoCoordinate location = new Navigation().new GeoCoordinate(geoCoordinate.getLatitude(), geoCoordinate.getLongitude());
//                    location.setLatitude(geoCoordinate.getLatitude());
//                    location.setLongitude(geoCoordinate.getLongitude());
                    locationList.add(location);

                /*if(completeBatch > 0  && (counter + 1) % 5 == 0){

                    mListener.onSendGeoCoordinates(locationList);
                    locationList.clear();

                    completeBatch--;

                }else if(remainingBatch == locationList.size() && completeBatch == 0){

                    mListener.onSendGeoCoordinates(locationList);
                    locationList.clear();

                }*/

                }
                //sends list of geo coordinates retrieved from maneuver list for the selected route
//            mListener.onSendGeoCoordinates(locationList);
                int location_size = locationList.size();
//                if (locationList.size() > 10)
//                    locationList = new ArrayList<>(locationList.subList(0, 10));

                GeoCoordinate sourceGeoCoordinate = presenter.getMapRoute().getRoute().getStart();

                mListener.onSendRouteInfo(tvDestination.getText().toString(), locationList, sourceGeoCoordinate.getLatitude(), sourceGeoCoordinate.getLongitude(), presenter.getDestinationLocation().getLatitude(), presenter.getDestinationLocation().getLongitude());

//            mListener.onSendDelimiter("Route");

            }

            boolean isNavigationView = false;
            // gets is navigation view mode on from shared preferences
            if (getActivity() != null)
                isNavigationView = SettingsUtils.readPreferenceValueAsBoolean(getActivity(),
                        Constants.KEY_BASE + Constants.KEY_NAVIGATION_VIEW);
            mLayout.setShadowHeight(0);
            viewPagerRouteSummary.setVisibility(View.GONE);
            fabCurrentLocation.setVisibility(View.GONE);
            ivSearchWayPoint.setVisibility(View.GONE);
            if (isNavigationView) {
                stopNavigationView.setVisibility(View.VISIBLE);
            } else {
                layoutExitNavigation.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * called on stop navigation click, sends the info to the HUD
     * for exiting the navigation
     *
     */
    public void sendExitNavigationMsg() {
        if (mListener != null) {
            mListener.onExitNavigation();
        }
    }

    /**
     * shows reverse geo coded current location
     *
     * @param location: current location that is reverse geo coded
     */
    @Override
    public void showSourceLocation(String location) {

        if (!TextUtils.isEmpty(location) && !location.equalsIgnoreCase(tvSource.getText().toString())) {
            toggleProgressBar(false);
            tvSource.setText(location);
        }

    }

    @Override
    public void showDestinationLocation(String locationText) {
        tvDestination.setText(locationText);
        showDestConfirmDialog(locationText);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    // Map gesture listener
    private class MapOnGestureListener implements MapGesture.OnGestureListener {

        @Override
        public void onPanStart() {
        }

        @Override
        public void onPanEnd() {
        }

        @Override
        public void onMultiFingerManipulationStart() {
        }

        @Override
        public void onMultiFingerManipulationEnd() {
        }

        @Override
        public boolean onMapObjectsSelected(List<ViewObject> objects) {
            return false;
        }

        @Override
        public boolean onTapEvent(PointF p) {
            //hides route summary and start navigation to enable full map
            if(presenter.isNavigationStarted() || presenter.isRouteDrawn()) {
                if (!isMapViewVisible) {
                    if (presenter.isNavigationStarted()) {
                        stopNavigationView.setVisibility(View.GONE);
                    }else {
                        viewPagerRouteSummary.setVisibility(View.GONE);
                    }
                    layoutDestinationPreview.setVisibility(View.GONE);
                    ivSearchWayPoint.setVisibility(View.GONE);
                    isMapViewVisible = true;
                } else {//shows route summary and start navigation
                    if (presenter.isNavigationStarted()) {
                        stopNavigationView.setVisibility(View.VISIBLE);
                        ivSearchWayPoint.setVisibility(View.GONE);
                    }else {
                        viewPagerRouteSummary.setVisibility(View.VISIBLE);
                        ivSearchWayPoint.setVisibility(View.VISIBLE);
                    }
                    layoutDestinationPreview.setVisibility(View.VISIBLE);

                    isMapViewVisible = false;
                }
            } else {
                if (map != null && !TextUtils.isEmpty(tvSource.getText().toString())) {
                    GeoCoordinate geoCoordinate = map.pixelToGeo(p);
                    getDestinationFromMap(geoCoordinate);
                }
            }
            return false;
        }

        @Override
        public boolean onDoubleTapEvent(PointF p) {
            return false;
        }

        @Override
        public void onPinchLocked() {
        }

        @Override
        public boolean onPinchZoomEvent(float scaleFactor, PointF p) {
            return false;
        }

        @Override
        public void onRotateLocked() {
        }

        @Override
        public boolean onRotateEvent(float rotateAngle) {
            return false;
        }

        @Override
        public boolean onTiltEvent(float angle) {
            return false;
        }

        @Override
        public boolean onLongPressEvent(PointF p) {
            return false;
        }

        @Override
        public void onLongPressRelease() {
        }

        @Override
        public boolean onTwoFingerTapEvent(PointF p) {
            return false;
        }
    }

    /**
     * When user interacts with map to get destination location.
     *
     * @param geoCoordinate
     */
    private void getDestinationFromMap(final GeoCoordinate geoCoordinate){
        try {

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Location destLocation = new Location("");
                    destLocation.setLatitude(geoCoordinate.getLatitude());
                    destLocation.setLongitude(geoCoordinate.getLongitude());

                    if(destinationMarker !=null && clusterLayer != null)
                        clusterLayer.removeMarker(destinationMarker);
                    destinationMarker = new MapMarker();

                    if (getActivity() != null) {
                        // sets destination location icon
                        Bitmap icon = BitmapFactory.decodeResource(getActivity().getResources(),
                                R.drawable.flag);
                        Image image = new Image();
                        image.setBitmap(icon);

                        destinationMarker.setIcon(image);
                    }
                    destinationMarker.setCoordinate(new GeoCoordinate(destLocation.getLatitude(), destLocation.getLongitude()));

                    if (clusterLayer != null) {

                        map.removeClusterLayer(clusterLayer);

                        clusterLayer.addMarker(destinationMarker);
                        map.addClusterLayer(clusterLayer);
                    }

                    presenter.reverseGeoCodeLocation(destLocation, Constants.LocationType.DESTINATION);

                }
            });
            t.start();
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * Resets Map to default state
     */
    public void resetMap(){

        //remove route drawn on map
        try {
            if(mapRoute !=null){
                map.removeMapObject(mapRoute);
                mapRoute = null;
            }

            // remove the markers added on map
            if(clusterLayer!=null){
                map.removeClusterLayer(clusterLayer);
            }

            // removes gesture listener from map
//            mapFragment.getMapGesture().removeOnGestureListener(mapOnGestureListener);
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * If selected location is cancelled or cleared.
     * Called when-
     * 1. clear button on confirmation dialog is pressed
     * 2. dialog is cancelled by tapping outside.
     * 3. clear image is tapped in destination text view
     *
     */
    private void clearSelectedLocation() {
        // Removes destination marker
        if (destinationMarker != null && clusterLayer != null)
            clusterLayer.removeMarker(destinationMarker);
        destinationMarker = new MapMarker();

        if (tvDestination != null)
            tvDestination.setText("");
    }

    @Override
    public void onBackPressed() {

        if (mLayout != null &&
                (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            if (mapRoute != null) {
                confirmExit();
            } else {
                //call to exit the app
                mListener.onExitApp();
            }
        }

    }

    private void startNavigation() {
        if (navigationManager != null && !navigationManager.getRunningState().equals(NavigationManager.NavigationState.RUNNING) && mapRoute != null && mapRoute.getRoute() != null) {
            LocationMockerPhone.getInstance().handlePhoneMockingStatusChange(LocationMockerPhone.getInstance().shouldMockPhoneLocations());
            LocationTracer.getInstance().startLoggingLocation();
            navigationManager.setMap(map);
            navigationManager.setDistanceUnit(IMPERIAL_US);
            NavigationManager.Error error = navigationManager.startNavigation(mapRoute.getRoute());

            navigationManager.addPositionListener(
                    new WeakReference<NavigationManager.PositionListener>(navigationPositionListener));
            navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.POSITION_ANIMATION);
            navigationManager.addRerouteListener(new WeakReference<>(rerouteListener));
        }
    }

    private void stopNavigation(){
        if (navigationManager != null)
            navigationManager.stop();
        presenter.setRouteDrawn(false);
        LocationTracer.getInstance().stopLoggingLocation();
    }

    private NavigationManager.RerouteListener rerouteListener = new NavigationManager.RerouteListener() {
        @Override
        public void onRerouteBegin() {
            super.onRerouteBegin();
        }

        @Override
        public void onRerouteEnd(Route route) {
            super.onRerouteEnd(route);
            if (map != null && mapRoute != null) {
//                map.removeMapObject(mapRoute);
//                mapRoute = new MapRoute(route);
//                map.addMapObject(mapRoute);

                mapRoute.setRoute(route);
                navigationManager.setRoute(route);
                presenter.setMapRoute(mapRoute);
            }
        }

        @Override
        public void onRerouteFailed() {
            super.onRerouteFailed();
        }
    };

    private NavigationManager.PositionListener navigationPositionListener
            = new NavigationManager.PositionListener() {
        @Override
        public void onPositionUpdated(GeoPosition loc) {
            loc.getCoordinate();
            loc.getHeading();

//            navigationManager.getDestinationDistance();
//            presenter.getNavigationETA(navigationManager);
        }
    };

    /**
     * shows progress bar in the source field till current location is not set
     * @param show: boolean if true then show progress bar else hide
     */
    private void toggleProgressBar(boolean show){
        if (progressBar != null) {
            if(show) {
                if(progressBar.getVisibility()==View.VISIBLE)
                    return;

                progressBar.setVisibility(View.VISIBLE);
            }
            else {
                if(progressBar.getVisibility()==View.GONE)
                    return;

                progressBar.setVisibility(View.GONE);
            }
        }

    }

    /**
     * updates the view on route calculation error on HUD
     */
    @Override
    public void updateViewsOnRouteCalculationError() {
        //checks if user already stopped navigation in mobile app and by that HUD returned with an error for route calculation
        if(mapRoute == null)
            return;

        if (navigationManager != null && navigationManager.getRunningState().equals(NavigationManager.NavigationState.RUNNING))
            navigationManager.stop();

        if(stopNavigationView.getVisibility() == View.VISIBLE) {
            stopNavigationView.setVisibility(View.GONE);
        }

        showRouteSummary(presenter.getRouteSummary());
        //renders route on map selected by the user
        renderRouteOnMap(presenter.getMapRoute());

        if (layoutExitNavigation.getVisibility() != View.GONE)
            layoutExitNavigation.setVisibility(View.GONE);
    }

    /**
     * shows stop navigation UI to user in order to be in sync with HUD navigation state
     */
    @Override
    public void updateNavigationState() {
        //call to hide tool bar
        mListener.onHideToolBar();

        tvDestinationName.setText(presenter.getDestinationLocationName());

        if (layoutDestinationPreview.getVisibility() != View.VISIBLE)
            layoutDestinationPreview.setVisibility(View.VISIBLE);

        layoutAddressFields.setVisibility(View.GONE);

        mLayout.setShadowHeight(0);
        viewPagerRouteSummary.setVisibility(View.GONE);
        ivSearchWayPoint.setVisibility(View.GONE);
        fabCurrentLocation.setVisibility(View.GONE);

        boolean isNavigationView = false;
        if (getActivity() != null)
            isNavigationView = SettingsUtils.readPreferenceValueAsBoolean(getActivity(),
                    Constants.KEY_BASE + Constants.KEY_NAVIGATION_VIEW);
        if (isNavigationView) {
            stopNavigationView.setVisibility(View.VISIBLE);
        } else {
            layoutExitNavigation.setVisibility(View.VISIBLE);
            layoutStopNavigation.setVisibility(View.VISIBLE);
        }
    }

    /**
     * updates the route rendered on map and it's summary if user swipes the route summary
     * @param selectedRouteIndex: int index to set route summary view pager item
     */
    @Override
    public void switchRoute(int selectedRouteIndex) {
        if (mapRoute != null) {
            presenter.setSelectedRouteIndex(selectedRouteIndex);
            mapRoute.setRoute(presenter.getRouteSummary().get(selectedRouteIndex).getRoute());
            presenter.setMapRoute(mapRoute);
        }
    }

    /**
     * call to start navigation
     */
    @Override
    public void handleStartNavigation() {
        // checks if Mobile app is not connected to HUD then navigation won't be started
        if (!MainApplication.getInstance().isCommunicationChannelOpen()) {
            CommonUtils.showAlert(getActivity(), getResources().getString(R.string.alert), getResources().getString(R.string.nav_not_connected),
                    new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {

                        }
                    });
            return;
        }

        // call to pass the destination to HUD
        presenter.setNavigationStarted(true);

        // When navigation is started stop videos in HUD
        if(SettingsUtils.readPreferenceValueWithDefault(getActivity(),
                Constants.SharedPrefKeys.KEY_DEVOPTIONS_PLAY_VIDEOS_ON_START,true)){
            SettingsUtils.writePreferenceValue(getActivity(), Constants.SharedPrefKeys.KEY_DEVOPTIONS_PLAY_VIDEOS_ON_START, false);
        }

        sendRouteInfo();
        startNavigation();
    }

    /**
     * initializes progress dialog to be shown when fetching Map Package Id from HUD
     */
    private void initProgressBar() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        progressDialog.setMessage(getResources().getString(R.string.loading_map));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * sets current location on map
     */
    private void setCurrentLocation(){
        Location location = MainPresenter.getInstance().getCurrentLocation();
        if (location != null && map != null) {
            //	center map to last known location if route is not drawn else zoom to route drawn area
            if(mapRoute != null){
                map.zoomTo(geoBoundingBox, Map.Animation.LINEAR, Map.MOVE_PRESERVE_ORIENTATION);
            }else{
                GeoCoordinate geoCoordinate = new GeoCoordinate(location.getLatitude(), location.getLongitude());
                map.setCenter(geoCoordinate, Map.Animation.LINEAR);

                if (map.getZoomLevel() != map.getMaxZoomLevel())
                    // Sets the zoom level to max
                    map.setZoomLevel(map.getMaxZoomLevel());
            }
        }
    }
}
