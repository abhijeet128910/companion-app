package com.exploride.droid.views;

public interface BaseView {
    void showToast(String message);
}
