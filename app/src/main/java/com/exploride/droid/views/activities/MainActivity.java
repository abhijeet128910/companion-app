
package com.exploride.droid.views.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.cloudant.exploridemobile.ExpLoginManager;
import com.cloudant.exploridemobile.Helper;
import com.cloudant.exploridemobile.LocationsMgr;
import com.cloudant.exploridemobile.SettingsMgr;
import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.AppVersion;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.DevOptions;
import com.cloudant.exploridemobile.framework.DownloadMaps;
import com.cloudant.exploridemobile.framework.Navigation;
import com.cloudant.exploridemobile.framework.NavigationOption;
import com.cloudant.exploridemobile.framework.RouteCalculationError;
import com.cloudant.exploridemobile.framework.SyncPreferences;
import com.cloudant.exploridemobile.framework.SyncPreferences.Languages;
import com.crashlytics.android.Crashlytics;
import com.exploride.droid.MainApplication;
import com.exploride.droid.MainContract;
import com.exploride.droid.R;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.logger.Log4jHelper;
import com.exploride.droid.network.NetworkStateReceiver;
import com.exploride.droid.network.VolleySingleton;
import com.exploride.droid.presenters.CallReceiver;
import com.exploride.droid.presenters.ContactsHelper;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.presenters.NotificationHelper;
import com.exploride.droid.presenters.ReachedLocationPresenter;
import com.exploride.droid.presenters.SearchLocationPresenter;
import com.exploride.droid.receivers.MusicUpdateReceiver;
import com.exploride.droid.receivers.TimeZoneChangedReceiver;
import com.exploride.droid.services.NotificationCollectorMonitorService;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.FontHelper;
import com.exploride.droid.utils.Utils;
import com.exploride.droid.views.fragments.AppVersionFragment;
import com.exploride.droid.views.fragments.BaseFragment;
import com.exploride.droid.views.fragments.BluetoothFragment;
import com.exploride.droid.views.fragments.BrightnessFragment;
import com.exploride.droid.views.fragments.DeveloperOptionsFragment;
import com.exploride.droid.views.fragments.DownloadMapsFragment;
import com.exploride.droid.views.fragments.DriverProfileFragment;
import com.exploride.droid.views.fragments.MapsDestinationFragment;
import com.exploride.droid.views.fragments.ReachedLocationFragment;
import com.exploride.droid.views.fragments.SavedTripsFragment;
import com.exploride.droid.views.fragments.SearchLocationFragment;
import com.exploride.droid.views.fragments.TripsFragment;
import com.exploride.droid.views.fragments.WifiFragment;
import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.network.HttpRequest;

import static android.content.Intent.ACTION_TIMEZONE_CHANGED;
import static com.exploride.droid.R.string.nav_not_connected;

public class MainActivity extends BaseActivity implements BaseFragment.OnTitleChangedListener, MapsDestinationFragment.OnFragmentInteractionListener, MainContract.MainView, SettingsMgr.SettingsUpdateListener, BrightnessFragment.onBrightnessChangeListener, DeveloperOptionsFragment.DeveloperOptionsFragmentListener, DownloadMapsFragment.OnDownloadMapFragmentInteractionListener, AppVersionFragment.OnAppVersionFragmentInteractionListener {

    public static final String TAG = "MainActivity";
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private ExpLoginManager mLm = null;
//    private UserLoginTask mAuthTask = null;
    private View mProgressView;
    private View mView;
    private LinearLayout drawerIndicator;

    private Boolean mInitialized = false;
    private static final int RC_SIGN_IN = 100;
    public static final int RC_NOTIF_ACCESS = 101;

    private static final int READ_CONTACT_PERMISSION = 102;
    private static String[] PERMISSIONS = {
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private FragmentManager fragmentManager;
    private android.app.FragmentTransaction fragmentTransaction;
    //holds old fragment reference when user moves to another screen
    private BaseFragment oldFragment = null;
    public BaseFragment fragment;
    private FrameLayout container;
    private MainContract.MainOps presenter;
    private Toolbar mToolbar;
    DrawerLayout mDrawer;
    NavigationView mNavigationView;
    ActionBarDrawerToggle mActionBarDrawerToggle;
    // flag to load home fragment when user presses back key
    private boolean mLoadHomeFragOnBackPress = false;

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static String TAG_HOME = "Welcome Mark";
    private static final String TAG_BT_SETTINGS = "Bluetooth Settings";
    private static final String TAG_NOTIFICATIONS = "Notifications";
    private static final String TAG_SETTINGS = "BT_Settings";
    public static String CURRENT_TAG = TAG_HOME;
    private Handler mHandler;
    // Textview in navigation bar to show current bluetooth status
    private TextView navBtStatus, wifi_connected_view;

    // Navigation options in drawer
    Switch swtchMotorways, swtchTollroads, swtchFerries, swtchLandmarks,
            swtchLiveTraffic, swtchSpeedAlert, swtchTracking, swtchGestures, switchVoiceOnline, swtchOfflineNav;
    private SwitchCompat swtchLocaleUnit;

    RadioButton rdButtonMale, rdButtonFemale, rdButtonUs, rdButtonUk;
    private boolean drawerInitialized = false;
    private BrightnessFragment brightnessFragment;
    private SyncPreferences pref;
    private ProgressDialog progressDialog;
    private AlertDialog.Builder bluetoothNotConnectedAlert;
    private android.support.v7.app.AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MainApplication.mFirstLaunch){
            Fabric.with(this, new Crashlytics());
            Crashlytics.setString("devInfo", Utils.getDeviceName());
        }
        setContentView(R.layout.activity_main);
        MainApplication.getInstance().setCurrentActivity(this);
        MainApplication.getInstance().registerBTStateListener(mBTStateReceiver);
        BluetoothHandler.getInstance();
        registerNetworkReceiver();
        registerCallReceiver();
        registerTimeZoneChangedReceiver();
        registerNetworkStateReceiver();
        registerNotificationHelper();
        registerMusicUpdateReceiver();
        mView = findViewById(R.id.drawer_layout);
        drawerIndicator = (LinearLayout) findViewById(R.id.drawer_indicator);
        mProgressView = findViewById(R.id.login_progress);

        //one time inits that need activity else do in MainApplication.initSingletons
        if(MainApplication.mFirstLaunch){
            MainApplication.mFirstLaunch = false;
            BluetoothHandler.getInstance();
            Helper.initialize(getString(R.string.base_url),this,getString(R.string.app_name), Helper.AppType.MobileApp);
            mLm = Helper.getTheLoginManager();
        }
        initialize(savedInstanceState);
        verifyPermissions();

        if(NotificationHelper.getInstance().checkNotifAccess(this)){
            startService(new Intent(this, NotificationCollectorMonitorService.class));
        }else
            NotificationHelper.getInstance().askNotifAccess(this);

    }
    String userEmailForLogging = null;
    BluetoothHandler.BTState mBTStateReceiver = new BluetoothHandler.BTState(){

        @Override
        public void BTConnected(Constants.CHANNEL channel) {

        }

        @Override
        public void BTConnectedToDevice(String deviceName, Constants.CHANNEL channel) {

        }

        @Override
        public void BTConnecting(Constants.CHANNEL channel) {

        }

        @Override
        public void BTNotConnected(Constants.CHANNEL channel) {

        }

        @Override
        public void toastToShow(String msg, Constants.CHANNEL channel) {

        }

        @Override
        public void BTMsgReceived(String msg, Constants.CHANNEL channel){
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            try {
                BTComm btComm = gson.fromJson(msg, BTComm.class);
                final Navigation navigation = btComm.getNavigation();
                final SyncPreferences syncPreferences = btComm.getSyncPreferences();
                final DownloadMaps downloadMaps = btComm.getDownloadMaps();
                final RouteCalculationError routeCalculationError = btComm.getRouteCalculationError();
                final NavigationOption navigationOption = btComm.getNavigationOption();
                final AppVersion appVersion = btComm.getAppVersion();

                MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (navigation != null) {

                            Navigation.Status status = navigation.getStatus();
                            if (status != null) {
                                if (status.isDestinationReached()) {
                                    // When HUD sends msg when destination is reached
                                    if (navigation.getDestinationReached() != null) {
                                        ReachedLocationPresenter.getInstance().setNavigationCompleted(navigation);
                                    }
                                    MapsPresenter.getInstance().resetNavigation();
                                } else
                                    presenter.syncHudNavigationState(status);
                            }
                        }

                        if(syncPreferences != null){
                            setSyncPreferences(syncPreferences);
                        }

                        if(downloadMaps != null){
                            presenter.setDownloadMapsInformation(downloadMaps);
                        }

                        if(routeCalculationError != null) {
                            presenter.notifyRouteCalculationError();

                            if(MainApplication.getInstance().isDebugModeOn()) {
                                showErrorAlert(routeCalculationError.getError());
                            } else {
                                showErrorAlert(getResources().getString(R.string.route_calculation_generic_error));
                            }
                        }

                        if(appVersion != null){
                            //call to set HUD app version info to presenter
                            presenter.setHudAppVersion(appVersion);
                        }

                        if(navigationOption != null){
                            String navOption = navigationOption.getOption();
                            switch (navOption) {
                                case Constants.NavigationOptions.MotorWays:
                                    showErrorAlert(getResources().getString(R.string.navigation_option_generic_error));
                                    swtchMotorways.setChecked(!swtchMotorways.isChecked());
                                    break;
                                case Constants.NavigationOptions.TollRoads:
                                    showErrorAlert(getResources().getString(R.string.navigation_option_generic_error));
                                    swtchTollroads.setChecked(!swtchTollroads.isChecked());
                                    break;
                                case Constants.NavigationOptions.Ferries:
                                    showErrorAlert(getResources().getString(R.string.navigation_option_generic_error));
                                    swtchFerries.setChecked(!swtchFerries.isChecked());
                                    break;
                            }
                        }
                    }
                });
            } catch (Exception e) {
                ExpLogger.logException(TAG, e);
            }
        }

        @Override
        public void BTMsgSent(String msg, Constants.CHANNEL channel){
        }
    };

    /**
     * notifies user of the route calculation error occurred in HUD
     */
    @Override
    public void showErrorAlert(String message) {
        if (!isFinishing()) {
            if (builder == null)
                builder = new android.support.v7.app.AlertDialog.Builder(this);

            builder.setTitle("Alert!");
            builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            builder.show();
        }
    }


    /**
     * sets sync preferences
     * @param syncPreferences
     */
    private void setSyncPreferences(SyncPreferences syncPreferences){

        SyncPreferences.Brightness brightness = syncPreferences.getBrightness();

        if(brightness != null){

            if(brightnessFragment != null && brightnessFragment.isFragmentDialogActive())
                brightnessFragment.onGetCurrentScreenBrightness(brightness.getValue());

            presenter.setHudBrightness(brightness.getValue());
            presenter.setHudAutomaticBrightnessOn(brightness.isALS());

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        MainApplication.getInstance().setCurrentActivity(this);

        //checks if HUD is connected to Mobile app
        if(MainApplication.getInstance().isCommunicationChannelOpen())
            //call to check navigation state at HUD end
            presenter.checkNavigationStateOnHUD();

    }


    @Override
    protected void onPause() {
        super.onPause();
        //All Windows & Dialogs should be closed before leaving an Activity
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    NetworkStateReceiver networkStateReceiver;

    public void registerNetworkReceiver(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        if (networkStateReceiver == null) {
            networkStateReceiver = new NetworkStateReceiver();
            registerReceiver(networkStateReceiver, intentFilter);
        }
    }

    public void unregisterNetworkReceiver(){
        if(networkStateReceiver!=null) {
            unregisterReceiver(networkStateReceiver);
            networkStateReceiver = null;
        }
    }

    CallReceiver mCallReceiver;
    public void registerCallReceiver(){
        if (mCallReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(android.telephony.TelephonyManager.ACTION_PHONE_STATE_CHANGED);
            intentFilter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);
            mCallReceiver = new CallReceiver();
            registerReceiver(mCallReceiver, intentFilter);
        }
    }

    public void unregisterCallReceiver(){
        if(mCallReceiver!=null) {
            unregisterReceiver(mCallReceiver);
            mCallReceiver = null;
        }
    }

    TimeZoneChangedReceiver mTimeZoneChangedReceiver;
    public void registerTimeZoneChangedReceiver(){
        if (mTimeZoneChangedReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ACTION_TIMEZONE_CHANGED);
            mTimeZoneChangedReceiver = new TimeZoneChangedReceiver();
            registerReceiver(mTimeZoneChangedReceiver, intentFilter);
        }
    }

    public void unregisterTimeZoneChangedReceiver(){
        if(mTimeZoneChangedReceiver!=null) {
            unregisterReceiver(mTimeZoneChangedReceiver);
            mTimeZoneChangedReceiver = null;
        }
    }

    NetworkStateReceiver mNetworkStateReceiver;
    public void registerNetworkStateReceiver(){
        if (mNetworkStateReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            mNetworkStateReceiver = new NetworkStateReceiver();
            registerReceiver(mNetworkStateReceiver, intentFilter);
        }
    }

    public void unregisterNetworkStateReceiver(){
        if(mNetworkStateReceiver!=null) {
            unregisterReceiver(mNetworkStateReceiver);
            mNetworkStateReceiver = null;
        }
    }

    NotificationHelper mNotificationHelper;
    public void registerNotificationHelper(){
        if (mNotificationHelper == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(NotificationHelper.Action_Notifications);
            mNotificationHelper = new NotificationHelper();
            registerReceiver(mNotificationHelper, intentFilter);
        }
    }

    public void unregisterNotificationHelper(){
        if(mNotificationHelper!=null) {
            unregisterReceiver(mNotificationHelper);
            mNotificationHelper = null;
        }
    }

    MusicUpdateReceiver mMusicUpdateReceiver;
    public void registerMusicUpdateReceiver(){
        if (mMusicUpdateReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();

            //Sony
            intentFilter.addAction("com.sonyericsson.music.metachanged");
            intentFilter.addAction("com.sonyericsson.music.updateprogress");
            intentFilter.addAction("com.sonyericsson.music.playbackcontrol.ACTION_PLAYBACK_PLAY");
            intentFilter.addAction("com.sonyericsson.music.TRACK_COMPLETED");
            intentFilter.addAction("com.sonyericsson.music.playbackcomplete");
            intentFilter.addAction("com.sonyericsson.music.playstatechanged");
            intentFilter.addAction("com.sonyericsson.music.playbackcontrol.ACTION_TRACK_STARTED");
            intentFilter.addAction("com.sonyericsson.music.playbackcontrol.ACTION_PAUSED");

            // Default android
            intentFilter.addAction("com.android.music.metachanged");
            intentFilter.addAction("com.android.music.playstatechanged");
            intentFilter.addAction("com.android.music.playbackcomplete");
            intentFilter.addAction("com.android.music.queuechanged");
            intentFilter.addAction("com.android.music.updateprogress");
            intentFilter.addAction("com.google.android.gms.car.media.STATUS");

            // xiaomi
            intentFilter.addAction("soundbar.music.metachanged");
            intentFilter.addAction("com.miui.player.metachanged");

            // Others
            intentFilter.addAction("com.htc.music.metachanged");
            intentFilter.addAction("com.nullsoft.winamp.metachanged");
            intentFilter.addAction("com.real.IMP.metachanged");
            intentFilter.addAction("fm.last.android.metachanged");
            intentFilter.addAction("com.sec.android.app.music.metachanged");
            intentFilter.addAction("com.amazon.mp3.metachanged");
            intentFilter.addAction("com.real.IMP.metachanged");
            intentFilter.addAction("com.rdio.android.metachanged");
            intentFilter.addAction("com.andrew.apollo.metachanged");
            intentFilter.addAction("com.lge.music.metachanged");
            intentFilter.addAction("com.pantech.app.music.metachanged");
            intentFilter.addAction("com.neowiz.android.bugs.metachanged");
            intentFilter.addAction("com.soundcloud.android.metachanged");
            intentFilter.addAction("com.soundcloud.android.playback.playcurrent");
            intentFilter.addAction("com.nullsoft.winamp.metachanged");
            intentFilter.addAction("com.amazon.mp3.metachanged");
            intentFilter.addAction("com.miui.player.metachanged");
            intentFilter.addAction("com.real.IMP.metachanged");
            intentFilter.addAction("com.samsung.sec.android.MusicPlayer.metachanged");
            intentFilter.addAction("com.andrew.apollo.metachanged");
            intentFilter.addAction("com.htc.music.metachanged");
            intentFilter.addAction("com.spotify.music.metadatachanged");
            mMusicUpdateReceiver = new MusicUpdateReceiver();
            registerReceiver(mMusicUpdateReceiver, intentFilter);
        }
    }

    public void unregisterMusicUpdateReceiver(){
        if(mMusicUpdateReceiver!=null) {
            unregisterReceiver(mMusicUpdateReceiver);
            mMusicUpdateReceiver = null;
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            /*mView.setVisibility(show ? View.GONE : View.VISIBLE);
            mView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
*/
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
           // mView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    void showLogin(){
        // Not signed in, launch the Sign In activity
        String[] providers = {AuthUI.GOOGLE_PROVIDER};
        overridePendingTransition(R.anim.slide_in,R.anim.slide_in);
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                .setLogo(AuthUI.NO_LOGO)
                .setProviders(providers)
                .setTheme(R.style.AppTheme)
                //.setTosUrl(GOOGLE_TOS_URL)
                //.setIsSmartLockEnabled(mEnableSmartLock.isChecked())
                .setIsSmartLockEnabled(false)
                .build(),RC_SIGN_IN);
    }
    void initialize(Bundle savedInstanceState){

        try {
            mToolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
            setSupportActionBar(mToolbar);

            mHandler = new Handler();
            fragmentManager = getFragmentManager();
            container = (FrameLayout) findViewById(R.id.container);
            MainApplication.getInstance().setCurrentActivity(this);
            MainApplication.getInstance().initServices(null);
            presenter = MainPresenter.getInstance();
            presenter.init(this);

            mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            mNavigationView = (NavigationView) findViewById(R.id.nav_view);
            setUpNavigationView();

            LayoutInflater layoutInflater = LayoutInflater.from(this);
            FrameLayout lyt_titleBar = (FrameLayout) layoutInflater.inflate(R.layout.title_layout, null);
            ImageButton btn_settings = (ImageButton) lyt_titleBar.findViewById(R.id.settings_button);
            btn_settings.setBackgroundResource(android.R.drawable.ic_menu_manage);
        }catch (Exception e){
            ExpLogger.logException(TAG, e);
        }
        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadFragmentByNavIndex();
        }
        mInitialized = true;

        //call to set notification bar theme
        setNotificationBarTheme();
        new Thread(new Runnable() {
            @Override
            public void run() {
                checkForFirebaseLogin();
            }
        }).start();
    }

    /**
     * sets notification bar theme color
     */
    private void setNotificationBarTheme(){
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.color_notification_bar));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_trips) {
            loadFragment(Constants.Fragments.SAVED_TRIPS, null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpNavigationView() {
        pref = new SyncPreferences();
        View headerView = mNavigationView.getHeaderView(0);

        final RadioGroup ttsVoice, ttsLanguage;
        final TextView ttsPrefsHeader, navigationOptions, downloadMaps, tncHeader, username,
        navLogout, hud, navRestart,trips,driverprofile, brightness, tvVersionInfo;

        navBtStatus = (TextView) headerView.findViewById(R.id.bt_connect);
        FontHelper.setFonts(this, navBtStatus, FontHelper.ROBOTO_BOLD);
        navBtStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CURRENT_TAG = TAG_BT_SETTINGS;
                navItemIndex = 1;
                loadFragmentByNavIndex();
            }
        });

        tvVersionInfo = (TextView) headerView.findViewById(R.id.tv_app_version);
        FontHelper.setFonts(this, tvVersionInfo, FontHelper.ROBOTO_BOLD);

        wifi_connected_view = (TextView) headerView.findViewById(R.id.wifi_connect);
        FontHelper.setFonts(this, wifi_connected_view, FontHelper.ROBOTO_BOLD);
        wifi_connected_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CURRENT_TAG = TAG_BT_SETTINGS;
                navItemIndex = 2;
                loadFragmentByNavIndex();
            }
        });

//        if (MainApplication.BuildV.IS_DEFAULTF) {
            LinearLayout devOptionsContainer= (LinearLayout) headerView.findViewById(R.id.dev_options_container);
            devOptionsContainer.setVisibility(View.VISIBLE);
            devOptionsContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawer.closeDrawers();
                    loadFragment(Constants.Fragments.DeveloperOptionsFragment, null);

                    if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                        mDrawer.closeDrawers();
                        return;
                    }
                }
            });
//        }

        // Show user details- username, login provider, userphoto
        username = (TextView) headerView.findViewById(R.id.username);
        FontHelper.setFonts(this, username, FontHelper.ROBOTO_BOLD);
        if (mFirebaseUser != null) {
            username.setText(mFirebaseUser.getDisplayName());

            // Get which is the login provider
            TextView providerText = (TextView) headerView.findViewById(R.id.provider);
            List<UserInfo> userInfos = (List<UserInfo>) mFirebaseAuth.getCurrentUser().getProviderData();
            if (userInfos != null && userInfos.size() > 0) {
                String provider = mFirebaseAuth.getCurrentUser().getProviderData().get(userInfos.size() - 1).getProviderId().toString();
                if(provider!=null && provider.length()>0) {
                    providerText.setText(provider.substring(0, 1).toUpperCase() + provider.substring(1).toLowerCase());
                    providerText.setVisibility(View.VISIBLE);
                }

            }
            final ImageView navUserPhoto = (ImageView) headerView.findViewById(R.id.nav_user_photo);
            ImageRequest request = new ImageRequest(mFirebaseAuth.getCurrentUser().getPhotoUrl().toString(),
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            navUserPhoto.setImageBitmap(bitmap);
                            navUserPhoto.setVisibility(View.VISIBLE);
                        }
                    }, 0, 0, ImageView.ScaleType.CENTER_INSIDE, null,
                    new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            navUserPhoto.setImageResource(R.mipmap.ic_launcher);
                            navUserPhoto.setVisibility(View.GONE);
                        }
                    });
            VolleySingleton.getInstance(this).addToRequestQueue(request);
        }

        hud = (TextView) headerView.findViewById(R.id.hud);
        FontHelper.setFonts(this, hud, FontHelper.ROBOTO_BOLD);

        navLogout = (TextView) headerView.findViewById(R.id.nav_logout);
        FontHelper.setFonts(this, navLogout, FontHelper.ROBOTO_REGULAR);
        navLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAuth.signOut();
                showLogin();
            }
        });

        navRestart = (TextView) headerView.findViewById(R.id.nav_restart);
        FontHelper.setFonts(this, navRestart, FontHelper.ROBOTO_REGULAR);
        navRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BTComm btComm = new BTComm();
                btComm.setActionCode(ActionConstants.ActionCode.Restart);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        });

        ttsPrefsHeader = (TextView) headerView.findViewById(R.id.tts_pref_header);
        FontHelper.setFonts(this, ttsPrefsHeader, FontHelper.ROBOTO_BOLD);

        navigationOptions = (TextView) headerView.findViewById(R.id.navigation_options_header);
        FontHelper.setFonts(this, navigationOptions, FontHelper.ROBOTO_BOLD);

        /**
         * Switch on:HUD uses maps and navigation offine. Online otherwise.
         */
        swtchOfflineNav = (Switch) headerView.findViewById(R.id.offline_navigation);
        swtchOfflineNav.setChecked(SettingsUtils.readPreferenceValueAsBoolean(this, Constants.SharedPrefKeys.KEY_ENABLE_OFFLINE_NAVIGATION));
        swtchOfflineNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.OfflineNavigation offlineNavigation = preferences.new OfflineNavigation();
                    offlineNavigation.setOffline(swtchOfflineNav.isChecked());
                    preferences.setOfflineNavigation(offlineNavigation);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableOfflineNavigation(swtchOfflineNav.isChecked());

                    SettingsUtils.writePreferenceValue(MainActivity.this,
                            Constants.SharedPrefKeys.KEY_ENABLE_OFFLINE_NAVIGATION,swtchOfflineNav.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        downloadMaps = (TextView) headerView.findViewById(R.id.download_maps_header);
        FontHelper.setFonts(this, downloadMaps, FontHelper.ROBOTO_BOLD);

        brightness = (TextView) headerView.findViewById(R.id.brightness_header);
        FontHelper.setFonts(this, brightness, FontHelper.ROBOTO_BOLD);


        trips = (TextView) headerView.findViewById(R.id.trips_header);
        FontHelper.setFonts(this, trips, FontHelper.ROBOTO_BOLD);

        driverprofile = (TextView) headerView.findViewById(R.id.driverprofile_header);
        FontHelper.setFonts(this, driverprofile, FontHelper.ROBOTO_BOLD);


        tncHeader = (TextView) headerView.findViewById(R.id.t_n_c_header);
        FontHelper.setFonts(this, tncHeader, FontHelper.ROBOTO_BOLD);

        rdButtonMale = (RadioButton) headerView.findViewById(R.id.rd_button_male);
        FontHelper.setFonts(this, rdButtonMale, FontHelper.ROBOTO_REGULAR);
        rdButtonMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Hear hear = preferences.new Hear();
                    hear.setIsMaleVoice(true);
                    preferences.setHear(hear);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setIsMaleVoice(true);
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        rdButtonFemale = (RadioButton) headerView.findViewById(R.id.rd_button_female);
        FontHelper.setFonts(this, rdButtonFemale, FontHelper.ROBOTO_REGULAR);
        rdButtonFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Hear hear = preferences.new Hear();
                    hear.setIsMaleVoice(false);
                    preferences.setHear(hear);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setIsMaleVoice(false);
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        // Female voice is currently available by here maps
        rdButtonFemale.setChecked(true);

        rdButtonUs = (RadioButton) headerView.findViewById(R.id.rd_button_us);
        FontHelper.setFonts(this, rdButtonUs, FontHelper.ROBOTO_REGULAR);
        rdButtonUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Language language = preferences.new Language();
                    language.setLang(Languages.ENG_US);
                    preferences.setLanguage(language);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setLang(Languages.ENG_US);
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        // English (US) is default selected for Here maps TTS.
        rdButtonUs.setChecked(true);

        rdButtonUk = (RadioButton) headerView.findViewById(R.id.rd_button_uk);
        FontHelper.setFonts(this, rdButtonUk, FontHelper.ROBOTO_REGULAR);
        rdButtonUk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Language language = preferences.new Language();
                    language.setLang(Languages.ENG_UK);
                    preferences.setLanguage(language);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setLang(Languages.ENG_UK);
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });


        switchVoiceOnline = (Switch) headerView.findViewById(R.id.switch_voice_online);
        FontHelper.setFonts(this, switchVoiceOnline, FontHelper.ROBOTO_REGULAR);
        switchVoiceOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.VoicePref voicePref = preferences.new VoicePref();
                    voicePref.setIsVoiceOnline(switchVoiceOnline.isChecked());
                    switchVoiceOnline.setText(switchVoiceOnline.isChecked()?MainPresenter.getInstance()
                            .getStringResource(R.string.voice_online):MainPresenter.getInstance()
                            .getStringResource(R.string.voice_offline));
                    preferences.setVoicePref(voicePref);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setIsVoiceOnline(switchVoiceOnline.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });
        switchVoiceOnline.setChecked(true);
        switchVoiceOnline.setText(switchVoiceOnline.isChecked()?MainPresenter.getInstance()
                .getStringResource(R.string.voice_online):MainPresenter.getInstance()
                .getStringResource(R.string.voice_offline));

        swtchMotorways = (Switch) headerView.findViewById(R.id.swtch_motorways);
        FontHelper.setFonts(this, swtchMotorways, FontHelper.ROBOTO_REGULAR);
        swtchMotorways.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Motorway motorway = preferences.new Motorway();
                    motorway.setEnableMotorway(swtchMotorways.isChecked());
                    preferences.setMotorway(motorway);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableMotorway(swtchMotorways.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        swtchTollroads = (Switch) headerView.findViewById(R.id.swtch_tollroads);
        // Setting to true to get long distant routes
        swtchTollroads.setChecked(true);
        FontHelper.setFonts(this, swtchTollroads, FontHelper.ROBOTO_REGULAR);
        swtchTollroads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Tollroad tollroad = preferences.new Tollroad();
                    tollroad.setEnableTollroad(swtchTollroads.isChecked());
                    preferences.setTollroad(tollroad);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableTollroad(swtchTollroads.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        swtchFerries = (Switch) headerView.findViewById(R.id.swtch_ferries);
        FontHelper.setFonts(this, swtchFerries, FontHelper.ROBOTO_REGULAR);
        swtchFerries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Ferries ferries = preferences.new Ferries();
                    ferries.setEnableFerries(swtchFerries.isChecked());
                    preferences.setFerries(ferries);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableFerries(swtchFerries.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        swtchLandmarks = (Switch) headerView.findViewById(R.id.swtch_landmardks);
        FontHelper.setFonts(this, swtchLandmarks, FontHelper.ROBOTO_REGULAR);
        swtchLandmarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Landmarks landmarks = preferences.new Landmarks();
                    landmarks.setEnableLandmarks(swtchLandmarks.isChecked());
                    preferences.setLandmarks(landmarks);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableLandmarks(swtchLandmarks.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        swtchLiveTraffic = (Switch) headerView.findViewById(R.id.swtch_live_traffic);
        // Setting to true to get live traffic by default
        swtchLiveTraffic.setChecked(true);
        FontHelper.setFonts(this, swtchLiveTraffic, FontHelper.ROBOTO_REGULAR);
        swtchLiveTraffic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.LiveTraffic liveTraffic = preferences.new LiveTraffic();
                    liveTraffic.setEnableLiveTraffic(swtchLiveTraffic.isChecked());
                    preferences.setLiveTraffic(liveTraffic);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableLiveTraffic(swtchLiveTraffic.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        swtchSpeedAlert = (Switch) headerView.findViewById(R.id.swtch_speed_alert);
        // Setting to true to get speed alert by default
        swtchSpeedAlert.setChecked(true);
        FontHelper.setFonts(this, swtchSpeedAlert, FontHelper.ROBOTO_REGULAR);
        swtchSpeedAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.SpeedAlert speedAlert = preferences.new SpeedAlert();
                    speedAlert.setEnableSpeedAlert(swtchSpeedAlert.isChecked());
                    preferences.setSpeedAlert(speedAlert);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableSpeedAlert(swtchSpeedAlert.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        swtchTracking = (Switch) headerView.findViewById(R.id.swtch_tracking);
        FontHelper.setFonts(this, swtchTracking, FontHelper.ROBOTO_REGULAR);
        swtchTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.AllowTracking allowTracking = preferences.new AllowTracking();
                    allowTracking.setEnableTracking(swtchTracking.isChecked());
                    preferences.setAllowTracking(allowTracking);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableTracking(swtchTracking.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        swtchLocaleUnit = (SwitchCompat) headerView.findViewById(R.id.swtch_locale_unit);
        FontHelper.setFonts(this, swtchLocaleUnit, FontHelper.ROBOTO_REGULAR);
        swtchLocaleUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    swtchLocaleUnit.setText(swtchLocaleUnit.isChecked() ? getResources().getString(R.string.unit_kilometers) : getResources().getString(R.string.unit_miles));
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Locale locale = preferences.new Locale();
                    locale.setIsLocaleUnitKms(swtchLocaleUnit.isChecked());
                    preferences.setLocale(locale);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setIsLocaleUnitKms(swtchLocaleUnit.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        //Switch on: gestures are enabled on HUD else disabled.
        swtchGestures = (Switch) headerView.findViewById(R.id.swtch_gestures);
        FontHelper.setFonts(this, swtchGestures, FontHelper.ROBOTO_REGULAR);
        swtchGestures.setChecked(SettingsUtils.readPreferenceValueAsBoolean(this, Constants.KEY_BASE + Constants.KEY_ENABLE_GESTURES));
        swtchGestures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SyncPreferences preferences = new SyncPreferences();
                    SyncPreferences.Gestures gestures = preferences.new Gestures();
                    gestures.setEnabled(swtchGestures.isChecked());
                    preferences.setGestures(gestures);
                    ConnectionHandler.getInstance().sendInfo(preferences);
                    if (SettingsMgr.getSettings() != null)
                        SettingsMgr.getSettings().setEnableGestures(swtchGestures.isChecked());

                    //stores whether gestures are enabled or disabled
                    SettingsUtils.writePreferenceValue(MainActivity.this, Constants.KEY_BASE + Constants.KEY_ENABLE_GESTURES,
                            swtchGestures.isChecked());
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });

        brightness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                brightnessFragment = BrightnessFragment.newInstance(presenter.getHudBrightness(), presenter.getHudAutomaticBrightnessOn());
                brightnessFragment.show(fragmentManager, Constants.Fragments.BrightnessFragment);

                if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                    mDrawer.closeDrawers();
                }

            }
        });

        trips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Closing drawer on item click
                mDrawer.closeDrawers();

                loadFragment(Constants.Fragments.Trips, null);
                //switch to About us screen
            }
        });


        driverprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Closing drawer on item click
                mDrawer.closeDrawers();

                loadFragment(Constants.Fragments.DriverProfile, null);
                //switch to About us screen
            }
        });

        downloadMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Closing drawer on item click
                mDrawer.closeDrawers();

                //call to notify user that bluetooth is not connected
                if(!MainApplication.getInstance().isCommunicationChannelOpen()) {
                    showBluetoothNotConnectedAlert();
                    return;
                }

                loadFragment(Constants.Fragments.DownloadMapsFragment, null);
            }
        });

        tvVersionInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Closing drawer on item click
                mDrawer.closeDrawers();

                //call to notify user that bluetooth is not connected
//                if(!isBluetoothConnected) {
//                    showBluetoothNotConnectedAlert();
//                    return;
//                }

                loadFragment(Constants.Fragments.AppVersion, null);
                //switch to About us screen
            }
        });


        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);

                getHudScreenBrightness();
                updateNavigationViews();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                drawerIndicator.setTranslationX(slideOffset * drawerView.getWidth());
            }
        };

        //Setting the actionbarToggle to drawer layout
        mDrawer.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        //back stack change listener for fragments
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                    mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    //show hamburger
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    mActionBarDrawerToggle.syncState();
                    mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDrawer.openDrawer(GravityCompat.START);
                        }
                    });

                    CURRENT_TAG = TAG_HOME;
                    onSetTitleText(null);
                }
            }
        });

        drawerInitialized = true;
        if (SettingsMgr.getSettings() != null && SettingsMgr.getSettings().getExpSettings() != null &&
                SettingsMgr.getSettings().getExpSettings().getSyncPreferences() != null) {
//            showUpdatedSettings(SettingsMgr.getSettings().getExpSettings().getSyncPreferences());
        }

    }

    /**
     * gets HUD screen brightness
     */
    private void getHudScreenBrightness(){
        //sends a request to HUD to get its current screen brightness
        SyncPreferences.Brightness brightness = new SyncPreferences().new Brightness();
        brightness.setShouldGetBrightness(true);

        pref.setBrightness(brightness);
        ConnectionHandler.getInstance().sendInfo(pref);
    }

    /**
     * sends message to HUD that user unselected the automatic brightness mode
     */
    @Override
    public void setOnAutomaticBrightnessUncheckedListener() {
        //sends a request to HUD to get its current screen brightness and set brightness mode to manual
        SyncPreferences.Brightness brightness = new SyncPreferences().new Brightness();
        brightness.setValue(-1);
        brightness.setALS(false);
        isALSEnabled = false;

        pref.setBrightness(brightness);
        ConnectionHandler.getInstance().sendInfo(pref);
    }

    /**
     * Update navigation options in drawer. This is called when replication is completed in SettingsMgr
     * @param syncPreferences
     */
    public void showUpdatedSettings(final SyncPreferences syncPreferences) {
        Handler handler=new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (syncPreferences != null && drawerInitialized) {
                    SyncPreferences.Hear hear = syncPreferences.getHear();
                    SyncPreferences.VoicePref voicePref = syncPreferences.getVoicePref();
                    SyncPreferences.Language language = syncPreferences.getLanguage();
                    SyncPreferences.Motorway motorway = syncPreferences.getMotorway();
                    SyncPreferences.Tollroad tollroad = syncPreferences.getTollroad();
                    SyncPreferences.Ferries ferries = syncPreferences.getFerries();
                    SyncPreferences.Landmarks landmarks = syncPreferences.getLandmarks();
                    SyncPreferences.SpeedAlert speedAlert = syncPreferences.getSpeedAlert();
                    SyncPreferences.LiveTraffic liveTraffic = syncPreferences.getLiveTraffic();
                    SyncPreferences.AllowTracking allowTracking = syncPreferences.getAllowTracking();
                    SyncPreferences.Locale locale = syncPreferences.getLocale();

                    if (hear != null) {
                        if (hear.mMale) {
                            rdButtonMale.setChecked(true);
                        } else {
                            rdButtonFemale.setChecked(true);
                        }
                    }

                    if (voicePref != null) {
                        switchVoiceOnline.setChecked(voicePref.isOnline);
                    }

                    if (language != null) {
                        if (language.mLang.compareTo(SyncPreferences.Languages.ENG_US) == 0) {
                            rdButtonUs.setChecked(true);
                        }
                        if (language.mLang.compareTo(SyncPreferences.Languages.ENG_UK) == 0) {
                            rdButtonUk.setChecked(true);
                        }
                    }

                    if (motorway != null) {
                        swtchMotorways.setChecked(motorway.isEnableMotorway());
                    }

//                    if (tollroad != null) {
//                        swtchTollroads.setChecked(tollroad.isEnableTollroad());
//                    }
                    // Setting to true to maintain same state as HUD to get long distant routes
                    swtchTollroads.setChecked(true);

                    if (ferries != null) {
                        swtchFerries.setChecked(ferries.isEnableFerries());
                    }

                    if (landmarks != null) {
                        swtchLandmarks.setChecked(landmarks.isEnableLandmarks());
                    }

                    if (speedAlert != null) {
                        swtchSpeedAlert.setChecked(speedAlert.isEnableSpeedAlert());
                    }

                    if (liveTraffic != null) {
                        swtchLiveTraffic.setChecked(liveTraffic.isEnableLiveTraffic());
                    }

                    if (allowTracking != null) {
                        swtchTracking.setChecked(allowTracking.isEnableTracking());
                    }

                    if(locale != null) {
                        swtchLocaleUnit.setChecked(locale.isLocaleUnitKms);
                        swtchLocaleUnit.setText(swtchLocaleUnit.isChecked() ? getResources().getString(R.string.unit_kilometers) : getResources().getString(R.string.unit_miles));
                    }
                }
            }
        });
    }
    @Override
    public void updateNavigationViews(){
        wifi_connected_view.setText(MainPresenter.getInstance().getWifiStatusText());

        String str = "";
        if(MainApplication.getInstance().isCommunicationChannelOpen())
                navBtStatus.setText(getString(R.string.connected_to) + BluetoothHandler.getInstance().getLastConnectedDeviceName());
        else
            navBtStatus.setText(getString(R.string.nav_not_connected));
    }
    @Override
    public void updateFragments(){
        Fragment fragment = fragmentManager.findFragmentByTag(Constants.Fragments.WIFI_Settings);
        if (fragment != null && fragment.isVisible()) {
            ((WifiFragment)fragment).onWifiConnected();
        }
    }

    @Override
    public void showLoading(String text) {
        if (!isFinishing()) {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this);
            }
            progressDialog.setTitle(text);
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        }
    }

    @Override
    public void hideLoading() {
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

    String getFragmentNameFromNavigationIndex() {
        switch (navItemIndex) {
            case 1:
                return Constants.Fragments.BT_Settings;
            case 2:
                return Constants.Fragments.WIFI_Settings;
            default:
                return Constants.Fragments.MapsDestinationFragment;
        }
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadFragmentByNavIndex() {
        // set toolbar title

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            mDrawer.closeDrawers();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                loadFragment(getFragmentNameFromNavigationIndex(), null);
            }
        };
        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }
        //Closing drawer on item click
        mDrawer.closeDrawers();
        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        try {
            if (mDrawer != null && mDrawer.isDrawerOpen(GravityCompat.START)) {
                mDrawer.closeDrawers();
                return;
            }

            // This code loads home fragment when back key is pressed
            // when user is in other fragment than home
            if (mLoadHomeFragOnBackPress) {
                // checking if user is on other navigation menu
                // rather than home
                if (navItemIndex != 0) {
                    navItemIndex = 0;
                    CURRENT_TAG = TAG_HOME;
                    loadFragmentByNavIndex();
                    return;
                } else finish();
            }

            if (fragment != null)
                fragment.onBackPressed();

        }catch (Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
        super.onBackPressed();
    }

    private BaseFragment getFragmentInstance(String name) {

        switch (name) {
            case Constants.Fragments.BT_Settings: {
                return new BluetoothFragment();
            }
            case Constants.Fragments.WIFI_Settings: {
                return new WifiFragment();
            }
            case Constants.Fragments.MapsDestinationFragment: {
                return new MapsDestinationFragment();
            }
            case Constants.Fragments.SearchLocationFragment: {
//                return SearchLocationFragment.getInstance();
                return new SearchLocationFragment();
            }
            case Constants.Fragments.DownloadMapsFragment: {
                return (BaseFragment) DownloadMapsFragment.newInstance(this);
            }
            case Constants.Fragments.ReachedLocationFragment: {
                return ReachedLocationFragment.getInstance();
            }
            case Constants.Fragments.SAVED_TRIPS: {
                return new SavedTripsFragment();
            }
            case Constants.Fragments.DeveloperOptionsFragment: {
                return new DeveloperOptionsFragment();
            }
            case Constants.Fragments.AppVersion: {
                return AppVersionFragment.newInstance();
            }
            case Constants.Fragments.Trips: {
                return new TripsFragment();
            }
            case Constants.Fragments.DriverProfile: {
                return new DriverProfileFragment();
            }

        }
        return null;
    }

    /**
     * method called to show settings/preferences
     */
    public void showSettings() {
        loadFragment(Constants.Fragments.BT_Settings, null);
    }

    /**
     * method called to show settings/preferences
     */
    public void showWifiSettings() {
        loadFragment(Constants.Fragments.WIFI_Settings, null);
    }
    /**
     * Load a fragment by resolving the name.
     *
     * @param fragName name of the fragment to be loaded
     * @param args     arguments to set in the fragment
     */
    public void loadFragment(String fragName, Bundle args) {
        try {
            fragment = getFragmentInstance(fragName);
//            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
//                    android.R.anim.fade_out);

            if(fragment == null)
                return;

            //checks if user chose the same screen option  to navigate from Navigation Drawer
            if(oldFragment != null && oldFragment == fragment)
                return;

            if (args != null)
                fragment.setArguments(args);

            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(container.getId(), fragment, fragName);
//            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            //assigns current fragment's value
            oldFragment = fragment;
        } catch (Exception e) {
            //In case of any exception assigns back the old fragment's value
            fragment = oldFragment;
            ExpLogger.logException(TAG, e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mDrawer!=null)
            mDrawer.removeDrawerListener(mActionBarDrawerToggle);
        presenter.onDestroyView();

        try {
            MainApplication.getInstance().unRegisterBTStateListener(mBTStateReceiver);
            unregisterNetworkReceiver();
            unregisterCallReceiver();
            unregisterTimeZoneChangedReceiver();
            unregisterNetworkStateReceiver();
            unregisterNotificationHelper();
            unregisterMusicUpdateReceiver();
        }catch (IllegalArgumentException e){
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * call to HUD passing the destination selected by the user
     * @param destination: place user wanted to navigate
     */
    @Override
    public void onSendDestination(DevOptions.Destination destination) {
        DevOptions devOptions = new DevOptions();
        devOptions.setDestination(destination);

        BTComm btComm = new BTComm();
        btComm.setDevOptions(devOptions);

        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * call to HUD to switch between top view or navigation view
     */
    @Override
    public void onSendTopMapViewMode(boolean isTopMapViewMode) {

        DevOptions devOptions = new DevOptions();
        DevOptions.TopMapView topMapView = new DevOptions().new TopMapView();
        topMapView.setEnable(isTopMapViewMode);
        devOptions.setTopView(topMapView);

        BTComm btComm = new BTComm();
        btComm.setDevOptions(devOptions);

        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * call to HUD passing all the geo coordinates retrieved from maneuver list for the selected route
     * @param routeGeoCoordinateList: list of geo coordinates
     */
    @Override
    public void onSendGeoCoordinates(ArrayList<Navigation.GeoCoordinate> routeGeoCoordinateList) {
        Navigation navigation = new Navigation();
        navigation.setGeoCoordinateList(routeGeoCoordinateList);
        BTComm btComm = new BTComm();
        btComm.setNavigation(navigation);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * call to HUD passing the destination selected by the user and all the geo coordinates retrieved from maneuver list for the selected route
     * @param destination: place user wanted to navigate
     * @param routeGeoCoordinateList: list of geo coordinates
     */
    @Override
    public void onSendRouteInfo(String destination, ArrayList<Navigation.GeoCoordinate> routeGeoCoordinateList, double sourceLatitude, double sourceLongitude, double destinationLatitude, double destinationLongitude) {
        Navigation navigation = new Navigation();

        Navigation.Status status = navigation.new Status();
        status.setDestination(destination);
        status.setSourceLat(sourceLatitude);
        status.setSourceLng(sourceLongitude);
        status.setDestLat(destinationLatitude);
        status.setDestLng(destinationLongitude);

        navigation.setStatus(status);
        navigation.setGeoCoordinateList(routeGeoCoordinateList);

        BTComm btComm = new BTComm();
        btComm.setNavigation(navigation);

        ConnectionHandler.getInstance().sendInfo(btComm);
        ExpLogger.logI(TAG, "ExpMobile: Start Navigation command sent");
    }

    /*@Override
    public void onSendDelimiter(String delimiter) {
        Navigation navigation = new Navigation();

        Navigation.Status status = navigation.new Status();
        status.setDelimiter(delimiter);

        BTComm btComm = new BTComm();
        btComm.setNavigation(navigation);

        ConnectionHandler.getInstance().sendInfo(btComm);
    }*/

    /**
     * call to HUD indicating user wants to exit navigation
     */
    @Override
    public void onExitNavigation() {
        Navigation navigation = new Navigation();

        Navigation.Status status = navigation.new Status();
        status.setExitNavigation(true);

        navigation.setStatus(status);

        BTComm btComm = new BTComm();
        btComm.setNavigation(navigation);

        ConnectionHandler.getInstance().sendInfo(btComm);
        ExpLogger.logI(TAG, "ExpMobile: Exit Navigation command sent");

    }

    @Override
    public void showToast(String message) {
        if(MainApplication.getInstance().isDebugModeOn())
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mInitialized)
            presenter.onStartView();
        NotificationHelper.getInstance().toggleNotificationListenerService(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mInitialized)
            presenter.onStopView();
    }

    String doHash(String data){
        try {
            DESKeySpec keySpec = new DESKeySpec("#eDiROLPXE".getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(keySpec);
            byte[] cleartext = data.getBytes("UTF8");
            Cipher cipher = Cipher.getInstance("DES"); // cipher is not thread safe
            cipher.init(Cipher.ENCRYPT_MODE, key);
            String encryptedPwd = HttpRequest.Base64.encodeBytes(cipher.doFinal(cleartext));
            return encryptedPwd;
        }
        catch(Exception ex)
        {
            ExpLogger.logException(TAG, ex);
            return "";
        }

    }
    private void endWithMessage(String message){
        showToast2(message);
        finish();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            //GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (resultCode == RESULT_OK) {
                // Google Sign In was successful, authenticate with Bluemix
                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                ExpLogger.logD(TAG,mFirebaseUser.getEmail());

//                mAuthTask = new UserLoginTask(mFirebaseUser.getEmail(), mFirebaseUser.getUid());
//                mAuthTask.execute((Void) null);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        initiateLogin(mFirebaseUser.getEmail(), mFirebaseUser.getUid());
                    }
                }).start();

            }
            // Sign in cancelled
            else if (resultCode == RESULT_CANCELED) {
                endWithMessage(getString(R.string.sign_in_cancelled));
            }
            else {
                // Google Sign In failed
                ExpLogger.logE(TAG, "Google Sign In failed.");

                endWithMessage(getString(R.string.sign_in_failed));

            }

            verifyPermissions();
        }
        // checks whether user granted the location permission or not
        else if (requestCode == Constants.PLAY_SERVICES_RESOLUTION_REQUEST) {

            if (resultCode == 0) {// user revoked location permission
                System.exit(1);
                finish();
                //TODO Replace with endWithMessage
            } else if (resultCode == -1) {// user gave location permission

            }

        }
        else if (requestCode == RC_NOTIF_ACCESS){

            if(NotificationHelper.getInstance().checkNotifAccess(this))
                startService(new Intent(this, NotificationCollectorMonitorService.class));
            if (resultCode == RESULT_CANCELED) {
                //send error to HUD
            }else{

                //TODO test code
//                NotificationHelper.getInstance().getAllNotifications();
            }
        }
    }

    public void showToast2(String message) {
        showToast(message);
    }

    /**
     * Update bluetooth status text in navigation drawer
     * @param state
     */
    @Override
    public void showBluetoothState(boolean state) {
        navBtStatus.setText(state ? getString(R.string.nav_connected) : getString(nav_not_connected));
    }

    /**
     * takes user to @link {@link SearchLocationFragment} class to select source and destination
     */
    @Override
    public void onSearchLocation() {
        loadFragment(Constants.Fragments.SearchLocationFragment, null);
    }

    /**
     * sets title bar text
     * @param title: string text to be set, set username when title is null
     */
    @Override
    public void onSetTitleText(String title) {
        if (title == null)
            getSupportActionBar().setTitle(mFirebaseUser == null ? "Welcome" : mFirebaseUser.getDisplayName());
        else
            getSupportActionBar().setTitle(title);
    }

    /**
     * takes user to @link {@link SearchLocationFragment} class to select among various routes
     */
    @Override
    public void onShowAlternateRoutes() {
        Bundle args = new Bundle();
        args.putBoolean("alternate_routes", true);
        loadFragment(Constants.Fragments.SearchLocationFragment, args);
    }

    @Override
    public void onAddWayPoint() {
        Bundle args = new Bundle();
        args.putBoolean("way_points", true);
        loadFragment(Constants.Fragments.SearchLocationFragment, args);
    }

    public void onSettingsUpdated(final SyncPreferences syncPreferences) {
        presenter.sendSyncPrefsToHUD();
//        this.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                showUpdatedSettings(syncPreferences);
//            }
//        });

    }

    void checkForFirebaseLogin(){
        try {
            mFirebaseAuth = FirebaseAuth.getInstance();
            mFirebaseUser = mFirebaseAuth.getCurrentUser();

            //        mAuthTask = new UserLoginTask("testUser3", doHash("testUser3"));
            //        mAuthTask.execute((Void) null);

            if (mFirebaseUser == null) {
                showLogin();

                return;

            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String username = mFirebaseUser.getDisplayName();
                        TAG_HOME = username;
                        ExpLogger.logI(TAG, "User name is " + username + ", " + mFirebaseUser.getEmail());

                        try {
                            Helper.loadCredential(getApplicationContext());
                            Helper.getTheDocumentManager().initialize(getApplicationContext(), false);
                            SearchLocationPresenter.getInstance().initLocationMgr();
                            Helper.getDriverBehavior().initialize(getApplicationContext());
                            SettingsMgr.getSettings().init(MainActivity.this);
                        } catch (Exception e) {
                            //                        showProgress(true);
                            //                        mAuthTask = new UserLoginTask(mFirebaseUser.getEmail(), mFirebaseUser.getUid());
                            //                        mAuthTask.execute((Void) null);
                            initiateLogin(mFirebaseUser.getEmail(), mFirebaseUser.getUid());
                            return;
                        }
                    }
                }).start();
            }
        }catch(Exception e){
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * is on Bg thread
     */
    void onLoginSuccess(){
        ExpLogger.logI(TAG, "onLoginSuccess");
        try {
            final MainActivity act = this;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    SearchLocationPresenter.getInstance().initLocationMgr();
                    SettingsMgr.getSettings().init(act);
                }
            }).start();
        }catch(Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
        if(mFirebaseUser!=null)
            userEmailForLogging = mFirebaseUser.getEmail();
        Crashlytics.setUserEmail(userEmailForLogging==null?"":userEmailForLogging);
        ExpLogger.logI(TAG, "userId:"+userEmailForLogging);
    }
    void initiateLogin(String mEmail, String mPassword){

            boolean success = false;
            try {

                mLm.doLogin(mEmail,mPassword);
                success = true;
            }catch (Exception e) {
                if (e instanceof FileNotFoundException)
                {
                    try {
                        ExpLogger.logD(TAG,"Try registering as Login failed with FileNotFoundException " + e.getMessage());
                        mLm.doRegister(mEmail,mPassword);
                        success = true;
                    } catch (Exception re) {
                        ExpLogger.logD(TAG,"Register failed with " + re.getMessage());
                        success = false;
                    }
                }else {
                    ExpLogger.logD(TAG, "Login failed " + e.getMessage());
                    success = false;
                }
            }

            if (success || null != mFirebaseUser /*or logged in earlier*/) {
                onLoginSuccess();
            } else {
                //TODO - Handle failure
                //If logged in previously then used cached credential else fail.
                // In case it fails user is shown a prompt to try login again else exit from app
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showLoginFailedPrompt();
                    }
                });
            }

    }
//    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
//
//        private final String mEmail;
//        private final String mPassword;
//        private static final String LOG_TAG = "UserLoginTask";
//
//        UserLoginTask(String email, String password) {
//            mEmail = email;
//            mPassword = password;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
////            showProgress(true);
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... params) {
//
//            try {
//
//                mLm.doLogin(mEmail,mPassword);
//
//            } /*catch (InterruptedException e) {
//                return false;
//            } catch (FileNotFoundException fe) {
//                try {
//                    Log.d(LOG_TAG,"Try registering as Login failed with FileNotFoundException " + fe.getMessage());
//                    mLm.doRegister(mEmail,mPassword);
//
//                } catch (Exception re) {
//                    Log.d(LOG_TAG,"Register failed with " + re.getMessage());
//                    return false;
//                }
//            }*/ catch (Exception e) {
//                if (e instanceof FileNotFoundException)
//                {
//                    try {
//                        Log.d(LOG_TAG,"Try registering as Login failed with FileNotFoundException " + e.getMessage());
//                        mLm.doRegister(mEmail,mPassword);
//
//                    } catch (Exception re) {
//                        Log.d(LOG_TAG,"Register failed with " + re.getMessage());
//                        return false;
//                    }
//                    return true;
//                }
//                Log.d(LOG_TAG,"Login failed " + e.getMessage());
//                return false;
//            }
//            return true;
//        }
//
//        @Override
//        protected void onPostExecute(final Boolean success) {
//            mAuthTask = null;
////            showProgress(false);
//
//            if (success || null != mFirebaseUser /*or logged in earlier*/) {
////                initialize(getIntent().getExtras());
//                onLoginSuccess();
//               } else {
//               //TODO - Handle failure
//                //If logged in previously then used cached credential else fail.
//               // In case it fails user is shown a prompt to try login again else exit from app
//                showLoginFailedPrompt();
//            }
//        }
//
//        @Override
//        protected void onCancelled() {
//            mAuthTask = null;
////            showProgress(false);
//            endWithMessage(getString(R.string.sign_in_cancelled));
//        }
//    }

    /**
     * call to HUD to download Maps corresponding to this Map Package Id
     * @param mapPackageId: Here MapPackage Id
     */
    @Override
    public void onInstallMaps(int mapPackageId) {
        DownloadMaps downloadMaps = new DownloadMaps();

        downloadMaps.setInstall(true);
        List<Integer> mapPackageIdList = new ArrayList<>();
        mapPackageIdList.add(mapPackageId);
        downloadMaps.setMapPackageIdList(mapPackageIdList);

        BTComm btComm = new BTComm();
        btComm.setDownloadMaps(downloadMaps);

        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * call to HUD to uninstall Maps corresponding to this Map Package Id
     * @param mapPackageId: Here MapPackage Id
     */
    @Override
    public void onUninstallMaps(int mapPackageId) {
        DownloadMaps downloadMaps = new DownloadMaps();

        downloadMaps.setUninstall(true);
        List<Integer> mapPackageIdList = new ArrayList<>();
        mapPackageIdList.add(mapPackageId);
        downloadMaps.setMapPackageIdList(mapPackageIdList);

        BTComm btComm = new BTComm();
        btComm.setDownloadMaps(downloadMaps);

        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * call to HUD to get MapPackage for populating Maps into ListView
     */
    @Override
    public void onGetMapPackageId(boolean getInstalledMapPackageId, int mapPackageId, boolean enableMapEngineOnline) {

        DownloadMaps downloadMaps = new DownloadMaps();

        downloadMaps.setInstalledMapPackageId(getInstalledMapPackageId);
        downloadMaps.setMapPackageId(mapPackageId);
        downloadMaps.setMapEngineOnline(enableMapEngineOnline);

        BTComm btComm = new BTComm();
        btComm.setDownloadMaps(downloadMaps);

        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * call to HUD to notify that user has cancelled the ongoing Map download
     */
    @Override
    public void onCancelDownload() {
        BTComm btComm = new BTComm();
        btComm.setActionCode(ActionConstants.ActionCode.CancelMapDownload);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
      *  shows prompt to the user indicating login failed. User can either try logging again else exit from the app
     */
    private void showLoginFailedPrompt(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog
                .setTitle("Login failed")
                .setMessage("Do you want to try again?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

//                        if(mAuthTask == null)
//                            mAuthTask = new UserLoginTask(mFirebaseUser.getEmail(), mFirebaseUser.getUid());
//
//                        mAuthTask.execute((Void) null);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                initiateLogin(mFirebaseUser.getEmail(), mFirebaseUser.getUid());
                            }
                        }).start();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mFirebaseAuth.signOut();
                        showLogin();
//                        finish();
//                        System.exit(1);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    //TODO: persist
    boolean isALSEnabled = true;
    /**
     * sends boolean as true to the HUD indicating that user selected the automatic brightness mode
     * @param isAutomaticBrightnessModeSelected
     */
    @Override
    public void setOnAutomaticBrightnessSelectedListener(boolean isAutomaticBrightnessModeSelected) {
        SyncPreferences syncPreferences = new SyncPreferences();

        //sends a request to HUD to set its current screen brightness to automatic
        SyncPreferences.Brightness brightness = syncPreferences.new Brightness();
        brightness.setShouldGetBrightness(false);

//        brightness.setAutomaticBrightnessOn(isAutomaticBrightnessModeSelected);
        brightness.setALS(isAutomaticBrightnessModeSelected);
        isALSEnabled = isAutomaticBrightnessModeSelected;

        syncPreferences.setBrightness(brightness);
        ConnectionHandler.getInstance().sendInfo(syncPreferences);
    }

    /**
     * sends the brightness to the HUD selected by the user
     * @param brightnessValue: brightness value (range 0-255)
     */
    @Override
    public void setOnBrightnessChangeListener(int brightnessValue) {
        SyncPreferences syncPreferences = new SyncPreferences();

        //sends a request to HUD to set its current screen brightness
        SyncPreferences.Brightness brightness = syncPreferences.new Brightness();
        brightness.setShouldGetBrightness(false);
        brightness.setALS(isALSEnabled);

        brightness.setValue(brightnessValue);

        syncPreferences.setBrightness(brightness);
        ConnectionHandler.getInstance().sendInfo(syncPreferences);
    }

    /**
     * hides the tool bar
     */
    @Override
    public void onHideToolBar() {

        mToolbar.setVisibility(View.GONE);
    }

    /**
     * shows the tool bar
     */
    @Override
    public void onShowToolBar() {

        mToolbar.setVisibility(View.VISIBLE);
    }

    /**
     * exits the app
     */
    @Override
    public void onExitApp() {
        System.exit(1);
    }

    /**
     * notify user if Bluetooth is not connected when user tries to switch to Download Maps screen
     */
    private void showBluetoothNotConnectedAlert(){
        if (bluetoothNotConnectedAlert == null)
            bluetoothNotConnectedAlert = new AlertDialog.Builder(this);

        bluetoothNotConnectedAlert.setTitle(getResources().getString(R.string.not_connected));
        bluetoothNotConnectedAlert.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        bluetoothNotConnectedAlert.show();
    }

    /**
     * call to HUD to set the debug mode status
     * @param isDebugModeEnabled: boolean true if debug mode enabled else false
     */
    @Override
    public void sendDebugMode(boolean isDebugModeEnabled) {
        BTComm btComm = new BTComm();
        DevOptions devOptions = new DevOptions();
        DevOptions.DebugMode debugMode = devOptions.new DebugMode();
        debugMode.setEnable(isDebugModeEnabled);
        devOptions.setDebugMode(debugMode);
        btComm.setDevOptions(devOptions);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * call to HUD to get app version
     */
    @Override
    public void getHudAppVersion() {
        BTComm btComm = new BTComm();
        btComm.setActionCode(ActionConstants.ActionCode.HUD_APP_VERSION);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == READ_CONTACT_PERMISSION) {

            boolean allowed = true;

            for (int i = 0; i < permissions.length; i++) {
                int grantResult = grantResults[i];

                if (grantResult != PackageManager.PERMISSION_GRANTED)
                    allowed = false;
            }

            if (allowed) {
                ContactsHelper.getInstance().getContacts(this);
                Log4jHelper.configureLogger();
            } else
                requestPermissions();
        }
    }

    private void verifyPermissions(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            ContactsHelper.getInstance().getContacts(this);
            Log4jHelper.configureLogger();
        } else
            requestPermissions();
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, READ_CONTACT_PERMISSION);
    }

    /**
     * gets the current active fragment
     * @return Fragment instance
     * @return Fragment instance
     */
    @Override
    public Fragment getActiveFragment() {
        return fragment;
    }

    /**
     * disables the offline navigation
     */
    @Override
    public void enableMapEngineOnline() {
        swtchOfflineNav.performClick();
    }
}
