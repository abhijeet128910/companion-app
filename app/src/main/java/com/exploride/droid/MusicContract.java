package com.exploride.droid;

import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;

public interface MusicContract {

    /**
     * methods for Presenter. Available to View
     */
    interface MusicOps extends BasePresenter<MusicView> {

        void play();

        void pause();

        void stop();

        void next();

        void prev();

        void togglePlayPause();

        void increaseVolume();

        void decreaseVolume();
    }

    /**
     * methods for View. Available to Presenter
     */

    interface MusicView extends BaseView {

    }

    /**
     * methods for Model. Available to Presenter
     */
    interface MusicStore {

    }

    /**
     * methods for Presenter. Available to Model
     */
    interface MusicPresenterOps {

    }
}
