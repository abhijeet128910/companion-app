package com.exploride.droid;

import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.views.BaseView;

/**
 * Created by Abhijeet on 11/18/2016.
 */

public interface ReachedLocationContract {
    /**
     * methods for Presenter. Available to View
     */
    public interface ReachedLocationOps extends BasePresenter<ReachedLocationContract.ReachedLocationView> {

        void saveTrip(String trip);

        void saveLocation(String locationLabel);

        Navigation getNavigationDetails();
    }


    /**
     * methods for View. Available to Presenter
     */

    public interface ReachedLocationView extends BaseView {

        void showNavigationDetails(Navigation navigation);

        void onSavedLocationResponse(String locationLabel, Constants.SAVE_DATA_RESPONSE response);

        void onSavedTripResponse(String tripLabel, Constants.SAVE_DATA_RESPONSE response);
    }

}