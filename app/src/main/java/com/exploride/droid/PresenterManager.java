package com.exploride.droid;

import com.exploride.droid.presenters.BasePresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to handle all the presenters and operations that have to be
 * communicated to all the presenters
 */
public class PresenterManager {
    public static PresenterManager mPresenterManager;
    static Object mPresenterManagerLock = new Object();
    List<BasePresenter> mActivePresenters = new ArrayList<>();
    boolean init = false;
    public static PresenterManager getInstance() {
        synchronized (mPresenterManagerLock) {
            if (null == mPresenterManager) {
                mPresenterManager = new PresenterManager();
            }
        }
        return mPresenterManager;
    }

    public void addActivePresenter(BasePresenter presenter){
        removeActivePresenter(presenter);
        mActivePresenters.add(presenter);
    }

    public void removeActivePresenter(BasePresenter presenter){
        mActivePresenters.remove(presenter);
    }

    public void setActivePresenter(BasePresenter presenter){
        mActivePresenters.clear();
        addActivePresenter(presenter);
    }
}
