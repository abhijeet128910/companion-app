package com.exploride.droid;

import android.location.Location;

import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.model.RecentLocation;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapRoute;

import java.util.ArrayList;

/**
 * Author Mayank
 */
public interface MapsContract {

    /**
     * methods for Presenter. Available to View
     */
    interface MapsOps extends BasePresenter<MapsView> {

        void setSourceLocation(Location location);

        Location getSourceLocation();

        Location getDestinationLocation();

        String getSourceLocationName();

        void resetSourceLocationName();

        String getDestinationLocationName();

        void resetDestinationLocationName();

        void requestCurrentLocToGeocode();

        void reverseGeoCodeLocation(Location location, String locationType);

        void onLocationReverseGeoCoded(String location, GeoCoordinate geoCoordinate);

        void onReverseGeoCodingFailed(GeoCoordinate geoCoordinate);

        ArrayList<RouteSummary> getRouteSummary();

        MapRoute getMapRoute();

        void setRouteSummary(ArrayList<RouteSummary> routeSummaries);

        void setMapRoute(MapRoute mapRoute);

        void resetNavigation();

        void setNavigationStarted(boolean navigationStarted);

        boolean isNavigationStarted();

        void setRouteDrawn(boolean routeDrawn);

        boolean isRouteDrawn();

        void removeWayPoints();

        void getRouteForLocationsSel();

        int getSelectedRouteIndex();

        void setSelectedRouteIndex(int selectedRouteIndex);

        void onRouteCalculationError();

        void syncHudNavigationState(String destination, double destinationLatitude, double destinationLogitude);
    }


    /**
     * methods for View. Available to Presenter
     */

    interface MapsView extends BaseView {

        boolean isMapInitialized();

        void showSourceLocation(String Location);

        void showDestinationLocation(String Location);

        void resetNavigation();

        void updateViewsOnRouteCalculationError();

        void updateNavigationState();
    }

    /**
     * methods for Model. Available to Presenter
     */
    interface MapsStore {

        void setSourceLatitude(double latitude);

        double getSourceLatitude();

        void setSourceLongitude(double longitude);

        double getSourceLongitude();

        void setDestinationLatitude(double latitude);

        double getDestinationLatitude();

        void setDestinationLongitude(double longitude);

        double getDestinationLongitude();

        void setSourceLocation(String title);

        void setDestinationLocation(String title);

        String getSourceLocation();

        String getDestinationLocation();

        void setRouteSummary(ArrayList<RouteSummary> routeSummaries);

        void setMapRoute(MapRoute mapRoute);

        ArrayList<RouteSummary> getRouteSummary();

        MapRoute getMapRoute();

        void setSelectedRouteIndex(int routeSelectedIndex);

        int getSelectedRouteIndex();
    }

    /**
     * methods for Presenter. Available to Model
     */
    interface MapsPresenterOps {

    }
}
