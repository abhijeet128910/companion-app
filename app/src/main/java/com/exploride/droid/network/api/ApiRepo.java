package com.exploride.droid.network.api;

import android.app.Activity;
import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.network.VolleyJSONRequest;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Helper class to prepare url's and then makes a call to volley for making network request

 * Author Mayank
 */
public class ApiRepo {

    private final static String SERVER_URL = Constants.UrlConstants.BASE_URL;

    private final static int SOCKET_TIMEOUT_MS= 15000;


    public enum API_Type {

        GET_AUTO_SUGGEST_LOCATIONS,

    }

    public final static String AUTO_SUGGEST_LOCATIONS_API = "autosuggest?";

    private final static String getAPIUrl(API_Type requestType, String appendUrl) {
        StringBuilder url = new StringBuilder(SERVER_URL);
        switch (requestType) {

            case GET_AUTO_SUGGEST_LOCATIONS: {
                url.append(AUTO_SUGGEST_LOCATIONS_API);
            }
            break;

            default:
                break;
        }
        url.append(appendUrl);
        return url.toString();
    }

    /**
     * fetches auto suggest places from the server
     *
     * @param activity: current activity's context
     * @param responseListener: response Listener for the network request made
     * @param errorListener: error listener for the network request made
     * @param currentLatitude: current latitude
     * @param currentLongitude: current longitude
     * @param searchLocation: user's query to get auto suggest places
     * @param tag: tag for the network request
     */
    public final static void getAutoSuggestionResponse(Activity activity, Response.Listener<String> responseListener, Response.ErrorListener errorListener, double currentLatitude, double currentLongitude, String searchLocation, String tag) {

        String url = null;

        try {
            url = getAPIUrl(API_Type.GET_AUTO_SUGGEST_LOCATIONS, "&app_id=" + activity.getResources().getString(R.string.app_id) + "&app_code=" + activity.getResources().getString(R.string.app_code)
                    +"&at=" + currentLatitude + "," + currentLongitude + "&q=" + URLEncoder.encode(searchLocation, "utf8") + "&pretty");
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
        MainApplication.volleyQueueInstance.cancelRequestInQueue(tag);

        //build Get url of Place Autocomplete and hit the url to fetch result.
        VolleyJSONRequest request = new VolleyJSONRequest(Request.Method.GET, url, null, null, responseListener, errorListener);

        //Give a tag to your request so that you can use this tag to cancel request later.
        request.setTag(tag);

        //sets request timeout
        request.setRetryPolicy(new DefaultRetryPolicy(
                SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MainApplication.volleyQueueInstance.addToRequestQueue(request);

    }


    /**
     * fetches weather data from OpenWeatherMap
     *
     * @param context: current context
     * @param responseListener: response Listener for the network request made
     * @param errorListener: error listener for the network request made
     * @param currentLatitude: the current latitude
     * @param currentLongitude: the current longitude
     * @param tag: tag for the network request
     */
    public static void fetchWeatherInfo(Context context, Response.Listener<String> responseListener, Response.ErrorListener errorListener, double currentLatitude, double currentLongitude, String tag) {

        String url = "http://api.openweathermap.org/data/2.5/weather?"+ "lat=" + currentLatitude + "&lon=" + currentLongitude +"&units=imperial&APPID=" + context.getResources().getString(R.string.open_weather_map_app_id);

        // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
        MainApplication.volleyQueueInstance.cancelRequestInQueue(tag);

        //build Get url of Place Autocomplete and hit the url to fetch result.
        VolleyJSONRequest request = new VolleyJSONRequest(Request.Method.GET, url, null, null, responseListener, errorListener);

        //Give a tag to your request so that you can use this tag to cancel request later.
        request.setTag(tag);

        //sets request timeout
        request.setRetryPolicy(new DefaultRetryPolicy(
                SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MainApplication.volleyQueueInstance.addToRequestQueue(request);
    }


}
