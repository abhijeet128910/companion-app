package com.exploride.droid.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.WifiConnect;
import com.exploride.droid.MainApplication;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.utils.NetworkUtils;

import static com.exploride.droid.logger.ExpLogger.*;

public class NetworkStateReceiver extends BroadcastReceiver {
    private static String TAG = "NetworkStateReceiver";
    public void onReceive(Context context, Intent intent) {
        try {
            ExpLogger.logD("app", "Network connectivity change");
            if(isConnected(intent)){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ConnectionHandler.getInstance().startTCPClient(null);
                    }
                }).start();
            }
        }catch (Exception e){
            logException(TAG, e);
        }
    }


    public boolean isConnected(Intent intent) {
        ExpLogger.logD(TAG, "Entering isConnected");
        final String action = intent.getAction();

        if (action.equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)) {

            if (intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)){
                ExpLogger.logD(TAG, "isConnected: "+ true);
                return true;
            }
        }
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
            int networkType = intent.getIntExtra(
                    android.net.ConnectivityManager.EXTRA_NETWORK_TYPE, -1);

//            if (ConnectivityManager.TYPE_WIFI == networkType)
            {

                NetworkInfo networkInfo = (NetworkInfo) intent
                        .getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null) {
                    if (networkInfo.isConnected()) {
                        ExpLogger.logD(TAG, "isConnected: "+ true);
                        return true;
                    } else {
                    }
                }
            }
        }
        return false;
    }
}
