package com.exploride.droid;

import android.net.wifi.ScanResult;

import com.cloudant.exploridemobile.framework.WifiScanResult;
import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;

import java.util.List;

/**
 * @author Abhijeet
 */

public interface WifiConnectionContract {
    /**
     * methods for Presenter. Available to View
     */
    interface WifiConnectionOps extends BasePresenter<WifiConnectionContract.WifiConnectionView> {

        void connectionCompleted(String ssid);
    }

    /**
     * methods for View. Available to Presenter
     */

    interface WifiConnectionView extends BaseView {

        void onConnectionCompleted(String ssid);

        void onScanResultsReceived(List<WifiScanResult> scanResults);
    }
}
