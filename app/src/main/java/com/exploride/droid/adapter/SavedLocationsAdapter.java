package com.exploride.droid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cloudant.exploridemobile.ExpDocument;
import com.cloudant.exploridemobile.Location;
import com.exploride.droid.R;

import java.util.List;

/**
 * Class to load auto saved locations fetched from the cloudant into a listview
 * <p>
 * Author Abhijeet.
 */

public class SavedLocationsAdapter extends RecyclerView.Adapter<SavedLocationsAdapter.ViewHolder> {

    private List<ExpDocument> mLocations;
    private Context context;

    private OnLocationItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, location;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.label);
            location = (TextView) view.findViewById(R.id.location_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.setOnItemClick((Location) mLocations.get(getAdapterPosition()));
                }
            });
        }
    }


    public SavedLocationsAdapter(Context context ,List<ExpDocument> places, OnLocationItemClickListener itemClickListener) {
        this.context = context;
        this.mLocations = places;
        onItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.saved_location_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ExpDocument expDocument = mLocations.get(position);
        Location location= (Location) expDocument;
        holder.name.setText(location.getName());
        holder.location.setText(location.getDescription());
    }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnLocationItemClickListener {

        /**
         * sets the place user selected from auto suggest places displayed in a listview
         *
         * @param location
         */
        void setOnItemClick(Location location);

    }
}

