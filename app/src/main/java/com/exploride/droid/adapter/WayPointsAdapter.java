package com.exploride.droid.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.views.fragments.SearchLocationFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to load auto suggest places fetched from the server into a listview
 *
 * Author Mayank.
 */

public class WayPointsAdapter extends RecyclerView.Adapter<WayPointsAdapter.ViewHolder> {

    private List<PlaceAutoComplete.Result> mPlaces;

    private WayPointsAdapter.OnItemClickListener onItemClickListener;
    private SearchLocationFragment.OnAppendSuggestionListener onAppendSuggestionListener;

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, location;

        public ImageView appendSuggestion;

        ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.place_name);
            location = (TextView) view.findViewById(R.id.place_detail);
            appendSuggestion = (ImageView) view.findViewById(R.id.append_suggestion);

            appendSuggestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceAutoComplete.Result placeSelected = mPlaces.get(getAdapterPosition());
                    onAppendSuggestionListener.appendSuggestionText(placeSelected.title);
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceAutoComplete.Result placeSelected = mPlaces.get(getAdapterPosition());
                    if (placeSelected.placeId != null)
                        onItemClickListener.findCoordinatesByPlaceId(placeSelected);
                    else
                        onItemClickListener.setOnWayPointSelected(placeSelected);
                }
            });
        }
    }


    public WayPointsAdapter(List<PlaceAutoComplete.Result> places, WayPointsAdapter.OnItemClickListener itemClickListener,
                            SearchLocationFragment.OnAppendSuggestionListener appendSuggestionListener) {
        if(mPlaces == null)
            mPlaces = new ArrayList<>();

        this.mPlaces.addAll(places);
        
        onItemClickListener = itemClickListener;
        this.onAppendSuggestionListener = appendSuggestionListener;
    }

    @Override
    public WayPointsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_place_row, parent, false);

        return new WayPointsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WayPointsAdapter.ViewHolder holder, int position) {
        PlaceAutoComplete.Result result = mPlaces.get(position);
        holder.name.setText(result.title);

        if(result.vicinity != null) {
            // add a version check and use the old method on Android M and below, on Android N and higher you should use the new method.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.location.setText(Html.fromHtml(result.vicinity, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.location.setText(Html.fromHtml(result.vicinity));
            }
        }

    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }

    public void swap(List<PlaceAutoComplete.Result> places){
        mPlaces.clear();
        mPlaces.addAll(places);
        notifyDataSetChanged();
    }

    /**
     * clears previous results shown when the user's types or deletes character in the input field
     */
    public void clearPreviousSearchResults(){
        mPlaces.clear();
        notifyDataSetChanged();
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnItemClickListener {

        /**
         * sets the place user selected from auto suggest places displayed in a listview
         * @param place: {@link com.exploride.droid.model.PlaceAutoComplete.Result} instance
         */
        void setOnWayPointSelected(PlaceAutoComplete.Result place);

        /**
         * call to find place coordinates
         * @param place: {@link com.exploride.droid.model.PlaceAutoComplete.Result} instance
         */
        void findCoordinatesByPlaceId(PlaceAutoComplete.Result place);

    }
}