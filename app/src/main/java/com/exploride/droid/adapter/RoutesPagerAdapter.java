package com.exploride.droid.adapter;

/**
 * View Pager adapter class to be used for switching between different routes
 * Author: Mayank(4/11/2017).
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.utils.DividerItemDecoration;
import com.exploride.droid.views.fragments.MapsDestinationFragment;

import java.util.ArrayList;

public class RoutesPagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

    private final String TAG = "RoutesPagerAdapter";
    private Context mContext;
    private TextView tvEtaVia;
    private TextView tvDistance;
    private LinearLayout layoutStartNavigation;
    private RecyclerView maneuversListView;
    private ArrayList<RouteSummary> routeSummariesList;
    private OnRouteInteractionListener onRouteInteractionListener;

    public RoutesPagerAdapter(MapsDestinationFragment mapsDestinationFragment, Context context, ArrayList<RouteSummary> routeSummaries) {
        mContext = context;
        onRouteInteractionListener = mapsDestinationFragment;
        routeSummariesList = new ArrayList<>(routeSummaries.size());
        for (RouteSummary routeSummary : routeSummaries) {
            routeSummariesList.add(routeSummary);
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RouteSummary routeSummary = routeSummariesList.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.layout_route_selected, container, false);

        layoutStartNavigation = (LinearLayout) itemView.findViewById(R.id.ll_start_navigation);
        tvEtaVia = (TextView) itemView.findViewById(R.id.tv_eta_via);
        tvDistance = (TextView) itemView.findViewById(R.id.tv_distance);
        tvEtaVia.setText(routeSummary.getEta() + " " + routeSummary.getViaPoint());
        tvDistance.setText(routeSummary.getDistance());

        layoutStartNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRouteInteractionListener.handleStartNavigation();
            }
        });

        maneuversListView = (RecyclerView) itemView.findViewById(R.id.lv_route_maneuvers);
        maneuversListView.setHasFixedSize(true);
        maneuversListView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(maneuversListView.getContext(),
                DividerItemDecoration.VERTICAL_LIST);
        maneuversListView.addItemDecoration(dividerItemDecoration);
        RoutesManeuversAdapter routesManeuversAdapter = new RoutesManeuversAdapter(routeSummary.getManeuversArrayList());
        maneuversListView.setAdapter(routesManeuversAdapter);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return routeSummariesList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void onPageSelected(int position) {
        ExpLogger.logD(TAG, "onPageSelected : position " + position);
        onRouteInteractionListener.switchRoute(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        ExpLogger.logD(TAG, "onPageScrollStateChanged : state " + state);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    public interface OnRouteInteractionListener {
        void switchRoute(int selectedRouteIndex);

        void handleStartNavigation();
    }

}