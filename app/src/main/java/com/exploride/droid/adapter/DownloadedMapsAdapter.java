package com.exploride.droid.adapter;


import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.utils.CommonUtils;
import com.here.android.mpa.odml.MapPackage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to load downloaded maps list for download
 * Author Mayank
 */

public class DownloadedMapsAdapter extends RecyclerView.Adapter<DownloadedMapsAdapter.ViewHolder> {

    private List<MapPackage> downloadedMapPackagesList = new ArrayList<>();

    private OnMapUninstallClickListener onMapUninstallClickListener;

    private int index;

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMapsItem;
        private ImageView ivDrillDownMapsList;
        private ImageView ivDownloadMaps;

        ViewHolder(View view) {
            super(view);

            tvMapsItem = (TextView) view.findViewById(R.id.place_name);
            ivDrillDownMapsList = (ImageView) view.findViewById(R.id.iv_drill_down_maps_list);
            ivDownloadMaps = (ImageView) view.findViewById(R.id.iv_download_maps);

        }
    }

    public DownloadedMapsAdapter(List<MapPackage> downloadedMapPackagesList, OnMapUninstallClickListener onMapUninstallClickListener) {
        for (MapPackage mapPackage : downloadedMapPackagesList) {

            this.downloadedMapPackagesList.add(mapPackage);
        }

        this.onMapUninstallClickListener = onMapUninstallClickListener;
    }

    @Override
    public DownloadedMapsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_download_maps_row, parent, false);

        return new DownloadedMapsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DownloadedMapsAdapter.ViewHolder holder, int position) {
        MapPackage mapPackage = downloadedMapPackagesList.get(position);

        holder.tvMapsItem.setText(mapPackage.getTitle() + " " + CommonUtils.getFormattedMapsSize(mapPackage.getSize()));
        holder.tvMapsItem.setTextColor(Color.GRAY);
        holder.tvMapsItem.setTextSize(14);

        holder.ivDrillDownMapsList.setImageResource(R.drawable.delete);
        holder.ivDrillDownMapsList.setVisibility(View.VISIBLE);
        holder.ivDrillDownMapsList.setTag(position);
        holder.ivDrillDownMapsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DownloadedMapsAdapter.this.index = (Integer) holder.ivDrillDownMapsList.getTag();

                onMapUninstallClickListener.onUninstallMaps(downloadedMapPackagesList.get(DownloadedMapsAdapter.this.index).getId());
            }
        });

        holder.ivDownloadMaps.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return downloadedMapPackagesList.size();
    }

    /**
     * call to filter the map package list shown to the user based on the query
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    public void animateTo(List<MapPackage> mapPackageList) {
        applyAndAnimateRemovals(mapPackageList);
        applyAndAnimateAdditions(mapPackageList);
        applyAndAnimateMovedItems(mapPackageList);
        notifyDataSetChanged();
    }

    /**
     * call to remove the non matching results from the list when user performs a search
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    private void applyAndAnimateRemovals(List<MapPackage> mapPackageList) {
        for (int i = downloadedMapPackagesList.size() - 1; i >= 0; i--) {
            final MapPackage mapPackage = downloadedMapPackagesList.get(i);
            if (!mapPackageList.contains(mapPackage)) {
                removeItem(i);
            }
        }
    }

    /**
     * call to add the matching results into the list when user performs a search
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    private void applyAndAnimateAdditions(List<MapPackage> mapPackageList) {
        for (int i = 0, count = mapPackageList.size(); i < count; i++) {
            final MapPackage mapPackage = mapPackageList.get(i);
            if (!downloadedMapPackagesList.contains(mapPackage)) {
                addItem(i, mapPackage);
            }
        }
    }

    /**
     * call to rearrange the matching results into the list when user performs a search
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    private void applyAndAnimateMovedItems(List<MapPackage> mapPackageList) {
        for (int toPosition = mapPackageList.size() - 1; toPosition >= 0; toPosition--) {
            final MapPackage mapPackage = mapPackageList.get(toPosition);
            final int fromPosition = downloadedMapPackagesList.indexOf(mapPackage);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    /**
     * removes non matching result from the list of map packages shown to the user
     * @param position: index of map package item to be removed
     * @return mapPackage that's removed from the list
     */
    private MapPackage removeItem(int position) {
        final MapPackage mapPackage = downloadedMapPackagesList.remove(position);
        notifyItemRemoved(position);
        return mapPackage;
    }

    /**
     * adds matching result to the list of map packages shown to the user
     * @param position: index of map package item to be added
     * @param mapPackage that's added to the list
     */
    private void addItem(int position, MapPackage mapPackage) {
        downloadedMapPackagesList.add(position, mapPackage);
        notifyItemInserted(position);
    }

    /**
     * rearranges the map packages list results based on the closely matched items on the top position and so on
     * @param fromPosition: current index of map package item
     * @param toPosition: new index of map package item after it's moved
     */
    private void moveItem(int fromPosition, int toPosition) {
        final MapPackage mapPackage = downloadedMapPackagesList.remove(fromPosition);
        downloadedMapPackagesList.add(toPosition, mapPackage);
        notifyItemMoved(fromPosition, toPosition);
    }

    /**
     * adds the currently installed Map Package into the existing list
     * @param mapPackage: {@link MapPackage} that just got installed on HUD
     */
    public void addDownloadedMapPackage(MapPackage mapPackage) {
        this.downloadedMapPackagesList.add(mapPackage);

        notifyDataSetChanged();
    }

    /**
     * resets the list of downloaded maps shown to the user
     */
    public void onMapUninstallSuccess() {
        this.downloadedMapPackagesList.remove(index);
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnMapUninstallClickListener {

        void onUninstallMaps(int mapPackageId);

    }

}
