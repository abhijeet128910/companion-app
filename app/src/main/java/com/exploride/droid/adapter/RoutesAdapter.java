package com.exploride.droid.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.model.RouteSummary;

import java.util.ArrayList;

/**
 * Class to load multiple routes from source to destination into a listview
 *
 * Author Mayank.
 */

public class RoutesAdapter extends RecyclerView.Adapter<RoutesAdapter.ViewHolder> {

    private ArrayList<RouteSummary> mRouteSummaries;

    private RoutesAdapter.OnItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvEta;
        public TextView tvViaPoint;
        public TextView tvDistance;

        public ViewHolder(View view) {
            super(view);
            tvEta = (TextView) view.findViewById(R.id.tv_eta);
            tvViaPoint = (TextView) view.findViewById(R.id.tv_via_point);
            tvDistance = (TextView) view.findViewById(R.id.tv_distance);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.setOnRouteClick(mRouteSummaries.get(getAdapterPosition()), getAdapterPosition());
                }
            });
        }
    }


    public RoutesAdapter(ArrayList<RouteSummary> routeSummaries, RoutesAdapter.OnItemClickListener itemClickListener) {
        if(mRouteSummaries == null)
            mRouteSummaries = new ArrayList<>();

        this.mRouteSummaries.addAll(routeSummaries);
        onItemClickListener = itemClickListener;
    }

    @Override
    public RoutesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_route_row, parent, false);

        return new RoutesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RoutesAdapter.ViewHolder holder, int position) {
        RouteSummary result = mRouteSummaries.get(position);
        holder.tvEta.setText(result.getEta());

        String viaPoint = result.getViaPoint();
        //show via point only if has value else hide it
        if (viaPoint != null && !TextUtils.isEmpty(viaPoint))
            holder.tvViaPoint.setText(result.getViaPoint());
        else
            holder.tvViaPoint.setVisibility(View.GONE);

        holder.tvDistance.setText(result.getDistance());

    }

    @Override
    public int getItemCount() {
        return mRouteSummaries.size();
    }

    public void swap(ArrayList<RouteSummary> routeSummaries){
        mRouteSummaries.clear();
        mRouteSummaries.addAll(routeSummaries);
        notifyDataSetChanged();
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnItemClickListener {

        /**
         * sets the place user selected from auto suggest places displayed in a listview
         * @param routeSummary: holds information about route like eta, via point, distance
         * @param routeSummarySelectedPosition: listview item's position selected by the user
         */
        void setOnRouteClick(RouteSummary routeSummary, int routeSummarySelectedPosition);

    }
}

