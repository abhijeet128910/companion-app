package com.exploride.droid.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.model.DownloadMapsStore;
import com.exploride.droid.utils.CommonUtils;
import com.here.android.mpa.odml.MapPackage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to load maps list for download
 * Author Mayank.
 */

public class DownloadMapsAdapter extends RecyclerView.Adapter<DownloadMapsAdapter.ViewHolder> {

    // list to hold downloaded map package id
    private List<Integer> mDownloadedPackageIdList;

    private List<MapPackage> mapPackagesList = new ArrayList<>();

    private OnMapInstallClickListener onMapInstallClickListener;

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMapsItem;
        private ImageView ivDrillDownMapsList;
        private ImageView ivCancelDownload;
        private ImageView ivDownloadMaps;
        private RelativeLayout layoutProgressBar;
        private ProgressBar progressBar;
        private TextView tvProgress;

        ViewHolder(View view) {
            super(view);

            tvMapsItem = (TextView) view.findViewById(R.id.place_name);
            ivDrillDownMapsList = (ImageView) view.findViewById(R.id.iv_drill_down_maps_list);
            ivDownloadMaps = (ImageView) view.findViewById(R.id.iv_download_maps);
            ivCancelDownload = (ImageView) view.findViewById(R.id.iv_cancel_download_maps);
            layoutProgressBar = (RelativeLayout) view.findViewById(R.id.rl_progress_bar);
            progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
            tvProgress = (TextView) view.findViewById(R.id.tv_progress);

        }
    }

    public DownloadMapsAdapter(List<MapPackage> mapPackagesList, OnMapInstallClickListener itemClickListener, List<Integer> downloadedPackageIdList) {
        for (MapPackage mapPackage : mapPackagesList) {
            this.mapPackagesList.add(mapPackage);
        }

        //sets the downloaded map package DI list
        mDownloadedPackageIdList = downloadedPackageIdList;

        onMapInstallClickListener = itemClickListener;
    }

    @Override
    public DownloadMapsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_download_maps_row, parent, false);

        return new DownloadMapsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DownloadMapsAdapter.ViewHolder holder, int position) {
        MapPackage mapsItem = mapPackagesList.get(position);

        String mapTitle = mapsItem.getTitle() + " - " + CommonUtils.getFormattedMapsSize(mapsItem.getSize());

        //checks if Maps are downloaded or not
        if (mDownloadedPackageIdList.contains(mapsItem.getId())) {
            holder.tvMapsItem.setTextColor(Color.GRAY);
            mapTitle = mapTitle + "\n Downloaded";
            holder.ivDownloadMaps.setVisibility(View.INVISIBLE);
        } else {
            holder.tvMapsItem.setTextColor(Color.BLACK);
            holder.ivDownloadMaps.setVisibility(View.VISIBLE);
        }

        holder.tvMapsItem.setText(mapTitle);
        holder.tvMapsItem.setTextSize(14);

        //checks if current Map Package can be further drilled down to view child map packages or not.
        if (mapsItem.getChildren() != null && mapsItem.getChildren().size() > 0)
            holder.ivDrillDownMapsList.setVisibility(View.VISIBLE);
        else
            holder.ivDrillDownMapsList.setVisibility(View.INVISIBLE);

        holder.ivDrillDownMapsList.setTag(position);
        holder.ivDrillDownMapsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) holder.ivDrillDownMapsList.getTag();
                onMapInstallClickListener.onShowMapsChildren(mapPackagesList.get(position));
            }
        });

        holder.ivDownloadMaps.setTag(position);
        holder.ivDownloadMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) holder.ivDownloadMaps.getTag();
                onMapInstallClickListener.onInstallMaps(onMapInstallClickListener.getMapPackageId() == -1 ? mapPackagesList.get(position).getId() : -1);
            }
        });

        holder.ivCancelDownload.setTag(position);
        holder.ivCancelDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMapInstallClickListener.onCancelDownload();
                notifyDataSetChanged();
            }
        });

        //checks if current Map Package is being downloaded or not
        if (onMapInstallClickListener.getMapPackageId() != mapsItem.getId()) {
            holder.layoutProgressBar.setVisibility(View.INVISIBLE);
            holder.ivCancelDownload.setVisibility(View.INVISIBLE);
        } else {
            holder.layoutProgressBar.setVisibility(View.VISIBLE);
            holder.ivCancelDownload.setVisibility(View.VISIBLE);
            holder.ivDrillDownMapsList.setVisibility(View.INVISIBLE);
            holder.ivDownloadMaps.setVisibility(View.INVISIBLE);
            holder.progressBar.setProgress(onMapInstallClickListener.getMapInstallProgress());
            String installProgress = onMapInstallClickListener.getMapInstallProgress() + "%";
            holder.tvProgress.setText(installProgress);
        }

    }

    @Override
    public int getItemCount() {
        return mapPackagesList.size();
    }

    /**
     * swaps the Map Package List with the new set of data
     * @param mapPackagesList: list of map packages to be shown in the listview
     */
    public void swap(List<MapPackage> mapPackagesList, List<Integer> downloadedPackageIdList) {
        this.mapPackagesList = new ArrayList<>();
        this.mapPackagesList.addAll(mapPackagesList);

        //sets the downloaded map package DI list
        mDownloadedPackageIdList = downloadedPackageIdList;

        notifyDataSetChanged();
    }

    /**
     * call to filter the map package list shown to the user based on the query
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    public void animateTo(List<MapPackage> mapPackageList) {
        applyAndAnimateRemovals(mapPackageList);
        applyAndAnimateAdditions(mapPackageList);
        applyAndAnimateMovedItems(mapPackageList);
        notifyDataSetChanged();
    }

    /**
     * call to remove the non matching results from the list when user performs a search
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    private void applyAndAnimateRemovals(List<MapPackage> mapPackageList) {
        for (int i = mapPackagesList.size() - 1; i >= 0; i--) {
            final MapPackage mapPackage = mapPackagesList.get(i);
            if (!mapPackageList.contains(mapPackage)) {
                removeItem(i);
            }
        }
    }

    /**
     * call to add the matching results into the list when user performs a search
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    private void applyAndAnimateAdditions(List<MapPackage> mapPackageList) {
        for (int i = 0, count = mapPackageList.size(); i < count; i++) {
            final MapPackage mapPackage = mapPackageList.get(i);
            if (!mapPackagesList.contains(mapPackage)) {
                addItem(i, mapPackage);
            }
        }
    }

    /**
     * call to rearrange the matching results into the list when user performs a search
     * @param mapPackageList: list of matching map packages to be shown in the listview
     */
    private void applyAndAnimateMovedItems(List<MapPackage> mapPackageList) {
        for (int toPosition = mapPackageList.size() - 1; toPosition >= 0; toPosition--) {
            final MapPackage mapPackage = mapPackageList.get(toPosition);
            final int fromPosition = mapPackagesList.indexOf(mapPackage);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    /**
     * removes non matching result from the list of map packages shown to the user
     * @param position: index of map package item to be removed
     * @return mapPackage that's removed from the list
     */
    private MapPackage removeItem(int position) {
        final MapPackage mapPackage = mapPackagesList.remove(position);
        notifyItemRemoved(position);
        return mapPackage;
    }

    /**
     * adds matching result to the list of map packages shown to the user
     * @param position: index of map package item to be added
     * @param mapPackage that's added to the list
     */
    private void addItem(int position, MapPackage mapPackage) {
        mapPackagesList.add(position, mapPackage);
        notifyItemInserted(position);
    }

    /**
     * rearranges the map packages list results based on the closely matched items on the top position and so on
     * @param fromPosition: current index of map package item
     * @param toPosition: new index of map package item after it's moved
     */
    private void moveItem(int fromPosition, int toPosition) {
        final MapPackage mapPackage = mapPackagesList.remove(fromPosition);
        mapPackagesList.add(toPosition, mapPackage);
        notifyItemMoved(fromPosition, toPosition);
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnMapInstallClickListener {

        void onShowMapsChildren(MapPackage mapPackage);

        void onInstallMaps(int mapPackageId);

        int getMapPackageId();

        int getMapInstallProgress();

        void onCancelDownload();
    }


}
