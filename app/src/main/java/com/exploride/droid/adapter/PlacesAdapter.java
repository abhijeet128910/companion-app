package com.exploride.droid.adapter;

import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.SearchLocationContract;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.presenters.SearchLocationPresenter;
import com.exploride.droid.views.fragments.SearchLocationFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to load auto suggest places fetched from the server into a listview
 *
 * Author Mayank.
 */

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.ViewHolder> {

    private final String TAG = "PlacesAdapter";
    private List<PlaceAutoComplete.Result> mPlaces;

    private OnItemClickListener onItemClickListener;
    private SearchLocationFragment.OnAppendSuggestionListener onAppendSuggestionListener;

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name, location;
        public ImageView appendSuggestion;
        public ImageView toggleFavorite;
        private ContentLoadingProgressBar progressBar;
        ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.place_name);
            location = (TextView) view.findViewById(R.id.place_detail);
            toggleFavorite=(ImageView)view.findViewById(R.id.fav);
            progressBar = (ContentLoadingProgressBar) view.findViewById(R.id.loading_progress);
            toggleFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPlaces == null || mPlaces.size() == 0)
                        return;

                    try {
                        final PlaceAutoComplete.Result placeSelected = mPlaces.get(getAdapterPosition());
                        if (placeSelected == null)
                            return;

                        if (placeSelected.isFavorite) {
                            toggleFavorite.setImageResource(R.drawable.ic_fav_uncheck);
                            placeSelected.isFavorite = false;

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    onItemClickListener.removePlaceAsFavorite(placeSelected);
                                }
                            }).start();

                        } else {
                            progressBar.setVisibility(View.VISIBLE);
                            toggleFavorite.setVisibility(View.GONE);
                            if(placeSelected.position == null || placeSelected.position.size() == 0) {
                                SearchLocationPresenter.getInstance().findPlaceCoordinates(placeSelected.placeId,
                                        new SearchLocationContract.LocationResolverListener() {
                                            @Override
                                            public void onLocationResolved(ArrayList<Double> coordinates) {
                                                progressBar.setVisibility(View.GONE);
                                                toggleFavorite.setVisibility(View.VISIBLE);

                                                if (coordinates == null || coordinates.size() == 0) {
                                                    toggleFavorite.setImageResource(R.drawable.ic_fav_uncheck);
                                                    return;
                                                }
                                                placeSelected.position = coordinates;
                                                toggleFavorite.setImageResource(R.drawable.ic_fav);
                                                placeSelected.isFavorite = true;
                                                onItemClickListener.addPlaceAsFavorite(placeSelected);
                                            }
                                        });
                            }
                            else{
                                progressBar.setVisibility(View.GONE);
                                toggleFavorite.setVisibility(View.VISIBLE);

                                toggleFavorite.setImageResource(R.drawable.ic_fav);
                                placeSelected.isFavorite = true;
                                onItemClickListener.addPlaceAsFavorite(placeSelected);
                            }
                        }
                    } catch (Exception e) {
                        ExpLogger.logException(TAG, e);
                    }
                }
            });
            appendSuggestion = (ImageView) view.findViewById(R.id.append_suggestion);

            appendSuggestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceAutoComplete.Result placeSelected = mPlaces.get(getAdapterPosition());
                    onAppendSuggestionListener.appendSuggestionText(placeSelected.title);
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlaceAutoComplete.Result placeSelected = mPlaces.get(getAdapterPosition());
                    if (placeSelected.placeId != null)
                        onItemClickListener.findCoordinatesByPlaceId(placeSelected);
                    else
                        onItemClickListener.setOnPlaceSelected(placeSelected);
                }
            });
        }
    }


    public PlacesAdapter(List<PlaceAutoComplete.Result> places, OnItemClickListener itemClickListener,
                         SearchLocationFragment.OnAppendSuggestionListener appendSuggestionListener) {
        if(mPlaces == null)
            mPlaces = new ArrayList<>();

        this.mPlaces.addAll(places);

        onItemClickListener = itemClickListener;
        this.onAppendSuggestionListener = appendSuggestionListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_place_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PlaceAutoComplete.Result result = mPlaces.get(position);
        holder.name.setText(result.title);
        if(result.isFavorite)
        {
            holder.toggleFavorite.setImageResource(R.drawable.ic_fav);

        }
        else
        {
            holder.toggleFavorite.setImageResource(R.drawable.ic_fav_uncheck);

        }

//        holder.toggleFavorite.setImageResource(result.isFavorite ? R.drawable.ic_fav
//                : R.drawable.ic_fav_uncheck);

        if(result.vicinity != null) {
            // add a version check and use the old method on Android M and below, on Android N and higher you should use the new method.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                holder.location.setText(Html.fromHtml(result.vicinity, Html.FROM_HTML_MODE_LEGACY));
            } else {
                holder.location.setText(Html.fromHtml(result.vicinity));
            }
        }

    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }

    public void swap(List<PlaceAutoComplete.Result> places){
        mPlaces.clear();
        mPlaces.addAll(places);
        notifyDataSetChanged();
    }

    /**
     * clears previous results shown when the user's types or deletes character in the input field
     */
    public void clearPreviousSearchResults(){
        mPlaces.clear();
        notifyDataSetChanged();
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnItemClickListener {

        /**
         * sets the place user selected from auto suggest places displayed in a listview
         * @param place: {@link com.exploride.droid.model.PlaceAutoComplete.Result} instance
         */
        void setOnPlaceSelected(PlaceAutoComplete.Result place);

        /**
         * call to find place coordinates
         * @param place: {@link com.exploride.droid.model.PlaceAutoComplete.Result} instance
         */
        void findCoordinatesByPlaceId(PlaceAutoComplete.Result place);

        void addPlaceAsFavorite(PlaceAutoComplete.Result place);
        void removePlaceAsFavorite(PlaceAutoComplete.Result place);

    }
}

