package com.exploride.droid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.R;
import com.exploride.droid.utils.FontHelper;

import java.util.List;

/**
 * Class to load saved trips from the cloudant into a listview
 * <p>
 * Author Abhijeet.
 */

public class SavedTripsAdapter extends RecyclerView.Adapter<SavedTripsAdapter.ViewHolder> {

    private List<Navigation> trips;
    private Context context;

    private OnTripItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tripName, origin, destination;

        public ViewHolder(View view) {
            super(view);
            tripName = (TextView) view.findViewById(R.id.trip_name);
            origin = (TextView) view.findViewById(R.id.origin);
            destination = (TextView) view.findViewById(R.id.destination);
            FontHelper.setFonts(context, tripName, FontHelper.ROBOTO_BOLD);
            FontHelper.setFonts(context, tripName, FontHelper.ROBOTO_REGULAR);
            FontHelper.setFonts(context, tripName, FontHelper.ROBOTO_REGULAR);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.setOnItemClick(trips.get(getAdapterPosition()));
                }
            });
        }
    }


    public SavedTripsAdapter(Context context, List<Navigation> trips, OnTripItemClickListener itemClickListener) {
        this.trips = trips;
        this.context = context;
        onItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.saved_trip_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Navigation trip = trips.get(position);
        if (trip != null) {
            try {
                holder.tripName.setText(trip.getStatus().getDescription());
                String source = trip.getStatus().getSource();
                if (source != null && source.contains("\n "))
                    source = source.replaceAll("\n ", " ");
                holder.origin.setText(source);
                holder.destination.setText(trip.getStatus().getDestination());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnTripItemClickListener {

        /**
         * sets the source and destination user selected from saved trips displayed in a listview
         *
         * @param trip
         */
        void setOnItemClick(Navigation trip);

    }
}

