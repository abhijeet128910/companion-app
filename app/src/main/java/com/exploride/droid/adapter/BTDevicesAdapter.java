package com.exploride.droid.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.BTModel;
import com.exploride.droid.utils.FontHelper;

import java.util.List;

/**
 * Class to load bluetooth devices and list of available WiFi
 * <p>
 * Author Abhijeet.
 */

public class BTDevicesAdapter extends RecyclerView.Adapter<BTDevicesAdapter.ViewHolder> {

    private List<BTModel> mBtDevices;
    private Context mContext;
    private static final String TAG = "BTDevicesAdapter";
    private String btName;

    private OnBTDeviceItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;

        public ViewHolder(final View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.bt_name);
        }
    }

    public void setBtName(String btName) {
        this.btName = btName;
    }

    public BTDevicesAdapter(Context context, List<BTModel> btDevices, OnBTDeviceItemClickListener itemClickListener) {
        mContext = context;
        mBtDevices = btDevices;
        onItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bt_device_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        try {

            BTModel btModel = mBtDevices.get(position);

            if(btModel != null && btModel.getBtName() != null && !btModel.getBtName().isEmpty()){

                holder.itemView.setTag(position);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = (Integer) holder.itemView.getTag();
                        BTModel childBtModel = mBtDevices.get(position);
                        onItemClickListener.setOnItemClick(childBtModel, position);
                    }
                });

                holder.name.setText(mBtDevices.get(position).getBtName());
                if (btName != null && (btName.equalsIgnoreCase(btModel.getBtName()) || btName.equalsIgnoreCase("\"" + btModel.getBtName() + "\""))) {
                    selectItem(holder.itemView);
                }else {
                    deselectItem(holder.itemView);
                }
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    @Override
    public int getItemCount() {
        return mBtDevices.size();
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnBTDeviceItemClickListener {

        /**
         * sets the bt device displayed in a listview
         *
         * @param btDevice
         */
        void setOnItemClick(BTModel btDevice, int position);

    }

    public void deselectItem(View view){
        if(view != null){
            TextView name = (TextView) view.findViewById(R.id.bt_name);
            TextView connecting = (TextView) view.findViewById(R.id.connecting);
            connecting.setVisibility(View.GONE);
            ImageView tick = (ImageView) view.findViewById(R.id.tick);

            FontHelper.setFonts(mContext, name, FontHelper.ROBOTO_REGULAR);
            name.setTextColor(ContextCompat.getColor(mContext, android.R.color.black));
            tick.setVisibility(View.GONE);
        }
    }

    public void selectItem(View view) {
        if (view != null) {
            TextView name = (TextView) view.findViewById(R.id.bt_name);
            TextView connecting = (TextView) view.findViewById(R.id.connecting);
            connecting.setVisibility(View.GONE);
            ImageView tick = (ImageView) view.findViewById(R.id.tick);

            FontHelper.setFonts(mContext, name, FontHelper.ROBOTO_BOLD);
            name.setTextColor(ContextCompat.getColor(mContext, R.color.bt_list_selected_color));
            tick.setVisibility(View.VISIBLE);
        }
    }

    public void showConnecting(View view) {
        if (view != null) {
            TextView connecting = (TextView) view.findViewById(R.id.connecting);
            connecting.setVisibility(View.VISIBLE);
        }
    }
}

