package com.exploride.droid.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.model.RouteSummary;

import java.util.ArrayList;

/**
 * Class to load maneuvers for each route
 * Author: Mayank(4/13/2017).
 */

public class RoutesManeuversAdapter extends RecyclerView.Adapter<RoutesManeuversAdapter.ViewHolder> {

    private ArrayList<RouteSummary.Maneuvers> mRouteManeuvers;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivTurn;
        public TextView tvTurnName;
        public TextView tvManeuverDistance;

        public ViewHolder(View view) {
            super(view);
            ivTurn = (ImageView) view.findViewById(R.id.iv_turn);
            tvTurnName = (TextView) view.findViewById(R.id.tv_turn_name);
            tvManeuverDistance = (TextView) view.findViewById(R.id.tv_maneuver_distance);
        }
    }

    public RoutesManeuversAdapter(ArrayList<RouteSummary.Maneuvers> routeManeuvers/*, RoutesAdapter.OnItemClickListener itemClickListener*/) {
        this.mRouteManeuvers = routeManeuvers;
    }

    @Override
    public RoutesManeuversAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_route_maneuvers, parent, false);

        return new RoutesManeuversAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RoutesManeuversAdapter.ViewHolder holder, int position) {
        RouteSummary.Maneuvers maneuver = mRouteManeuvers.get(position);
        holder.tvTurnName.setText(maneuver.getTurnName());
        holder.ivTurn.setImageResource(maneuver.getTurnImage());
        holder.tvManeuverDistance.setText(maneuver.getDistance());
    }

    @Override
    public int getItemCount() {
        return mRouteManeuvers.size();
    }

}
