package com.exploride.droid.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.exploride.droid.R;
import com.exploride.droid.model.RecentLocation;

import java.util.List;

/**
 * Class to load auto saved locations fetched from the cloudant into a listview
 * <p>
 * Author Abhijeet.
 */

public class RecentLocationsAdapter extends RecyclerView.Adapter<RecentLocationsAdapter.ViewHolder> {

    private List<RecentLocation> mRecentLocations;

    private OnLocationItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.location_name);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.setOnItemClick(mRecentLocations.get(getAdapterPosition()));
                }
            });
        }
    }

    public RecentLocationsAdapter(List<RecentLocation> recentLocations, OnLocationItemClickListener itemClickListener) {
        this.mRecentLocations = recentLocations;
        onItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recent_location_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RecentLocation recentLocation = mRecentLocations.get(position);
        holder.name.setText(recentLocation.getName());
    }

    @Override
    public int getItemCount() {
        return mRecentLocations.size();
    }

    /**
     * Listener for the ListView's items click
     */
    public interface OnLocationItemClickListener {

        /**
         * sets the place user selected from auto suggest places displayed in a listview
         *
         * @param location
         */
        void setOnItemClick(RecentLocation location);

    }
}

