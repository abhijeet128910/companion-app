package com.exploride.droid.location;

import android.os.Looper;
import android.util.Log;

import com.exploride.droid.logger.ExpLogger;

import static com.cloudant.android.ContentValues.TAG;

/**
 * Created on 05/01/17.
 */

public class HandlerThread extends Thread {

        Looper mThreadLooper;
        HandlerThread(){
            ExpLogger.logI(TAG, "HandlerThread()");
            setName("HandlerThread");
        }

        @Override
        public void run(){
            ExpLogger.logI(TAG, "HandlerThread.run()");
            if(mThreadLooper==null) {
                Looper.prepare();
                synchronized (this) {
                    mThreadLooper = Looper.myLooper();
                    notifyAll();

                }
                Looper.loop();
            }
        }

        public Looper getLooper() {
            synchronized (this) {
                while (mThreadLooper == null) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        ExpLogger.logI(TAG, "InterruptedException");
                    }
                }
            }
            return mThreadLooper;
        }
        void exitHandlerThread(){
            ExpLogger.logI(TAG, "HandlerThread.exitHandlerThread()");
            if(mThreadLooper!=null) {
                mThreadLooper.quitSafely();
                mThreadLooper = null;
            }
        }
}
