package com.exploride.droid.location;

import android.os.Looper;
import android.util.Log;

import com.exploride.droid.logger.ExpLogger;

import static com.cloudant.android.ContentValues.TAG;

/**
 * Created on 05/01/17.
 */

public class LocationThread extends Thread {

        Looper mLocationThreadLooper;
        LocationThread(){
            ExpLogger.logI(TAG, "LocationThread()");
            setName("LocationThread");
        }

        @Override
        public void run(){
            ExpLogger.logI(TAG, "LocationThread.run()");
            if(mLocationThreadLooper==null) {
                Looper.prepare();
                synchronized (this) {
                    mLocationThreadLooper = Looper.myLooper();
                    notifyAll();

                }
                Looper.loop();
            }
        }

        public Looper getLooper() {
            synchronized (this) {
                while (mLocationThreadLooper == null) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        ExpLogger.logI(TAG, "InterruptedException");
                    }
                }
            }
            return mLocationThreadLooper;
        }
        void exitLocationThread(){
            ExpLogger.logI(TAG, "LocationThread.exitLocationThread()");
            if(mLocationThreadLooper!=null) {
                mLocationThreadLooper.quitSafely();
                mLocationThreadLooper = null;
            }
        }
}
