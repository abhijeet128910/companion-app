package com.exploride.droid.location;

import com.exploride.droid.presenters.MapsPresenter;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;

/**
 * Reverse GeoCode coordinates to a location
 *
 * Author Mayank
 */

public class ReverseGeocodeListener implements ResultListener<com.here.android.mpa.search.Location> {
    @Override
    public void onCompleted(com.here.android.mpa.search.Location data, ErrorCode error) {
        if (data != null) {
            if (error != ErrorCode.NONE) {
                // Handle error
                MapsPresenter.getInstance().onReverseGeoCodingFailed(data.getCoordinate());
            } else {
                // sets current location that's reverse geo coded
                if (data.getAddress() != null)
                    MapsPresenter.getInstance().onLocationReverseGeoCoded(data.getAddress().toString(), data.getCoordinate());
            }
        }
    }
}