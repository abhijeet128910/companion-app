package com.exploride.droid.location;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.Coordinate;
import com.cloudant.exploridemobile.framework.Utils;
import com.exploride.droid.MainApplication;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.handlers.HUDMessenger;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * Created by peeyushupadhyay on 03/12/16.
 */

public class CustomLocationListener implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener
{

    public static final String TAG = "CustomLocationListener";
    private GoogleApiClient mGoogleApiClient;
    static CustomLocationListener customLocationListener;
    private static Object sLock = new Object();
    boolean sendLocation = false;

    private CustomLocationListener(){
        ExpLogger.logI(TAG, "gpsLoc:init GoogleApiClient");
        sendLocation = MainApplication.getInstance().shouldSendLocationFromPhone;
        mGoogleApiClient = new GoogleApiClient.Builder(MainApplication.getInstance().getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public static CustomLocationListener getInstance(){

        if(customLocationListener ==null) {
            synchronized (sLock) {
                if (customLocationListener == null) {
                    customLocationListener = new CustomLocationListener();
                }
            }
        }
        return customLocationListener;
    }
    CustomLocationProviderDefault mCustomLPD;
    Handler mLocationHandler;
    public void startListeningForLocation(){
        if(mLocationThread==null) {
            mLocationThread = new LocationThread();
            mLocationThread.start();
            mLocationHandler = new Handler(mLocationThread.getLooper());
            mCustomLPD = CustomLocationProviderDefault.getInstance(MainApplication.getInstance());
        }
        if(mGoogleApiClient!= null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }
    public void stopListeningForLocation(){
        if(mGoogleApiClient!= null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        if (mLocationThread != null) {
            mLocationThread.exitLocationThread();
        }
        mLocationThread = null;
    }
    public void startSendingLocation(){
        sendLocation = true;
    }
    public void stopSendingLocation(){
        sendLocation = false;
    }
    LocationRequest mLocationRequest;
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        ExpLogger.logI(TAG, "gpsLoc:onConnected:");
        mLastlocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocationThread!=null) {
            if(mLastlocation != null) {
                ExpLogger.logI(TAG, "gpsLoc:onConnected:" + mLastlocation.getLatitude() + "," + mLastlocation.getLongitude());
                //            Coordinate coordinate = new Coordinate(mLastlocation.getLatitude(), mLastlocation.getLongitude());
                Coordinate coordinate = new Coordinate(mLastlocation);
                MainPresenter.getInstance().setCurrentLocation(mLastlocation);
                if (sendLocation) {
                    sendLocationToHUD(coordinate);
                }
                mCustomLPD.getLocationListenerGPS().onLocationChanged(mLastlocation);
            }else
                ExpLogger.logI(TAG, "gpsLoc:onConnected: Location not detected");
            requestLocationUpdates();
        }
    }

    private void requestLocationUpdates(int interval, int disp){

        try {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(interval);
            mLocationRequest.setFastestInterval(1000);
//            if (disp != 0)//in debug mode, disable displacement for testing
//                mLocationRequest.setSmallestDisplacement(disp);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this, mLocationThread.getLooper());
        }catch (Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    public void requestLocationUpdates(){
        requestLocationUpdates(1000, MainApplication.getInstance().isDebugModeOn()?0:2);
    }

    Location mLastlocation;
    private Coordinate mPrevCoordinate;
    LocationThread mLocationThread;
    @Override
    public void onLocationChanged(Location location) {
        if (location != null && location.getLatitude() != 0 && location.getLongitude() != 0){
            ExpLogger.logI(TAG, "******gpsLoc:onLocationChanged******:" + location.getLatitude() + "," + location.getLongitude());
            if (getPrevCoordinate() != null) {
                if (getPrevCoordinate().mLat == location.getLatitude() && getPrevCoordinate().mLon == location.getLongitude())
                    return;
            }

            MainPresenter.getInstance().setCurrentLocation(location);
            mCustomLPD.getLocationListenerGPS().onLocationChanged(location);
            Coordinate coordinate = new Coordinate(location);
            setPrevCoordinate(coordinate);
            ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:time:" + location.getTime());
            ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Alt:" + location.getAltitude());
            ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Bearing:" + location.getBearing());
            ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Accu:" + location.getAccuracy());
            ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:ElapsedTime:" + location.getElapsedRealtimeNanos());
            ExpLogger.logI(TAG, "gpsLoc:------------------");

            if (sendLocation) {
                sendLocationToHUD(coordinate);
            }
        }
    }

    public void setMockLocation(final Location mockLocation) {

        if(mGoogleApiClient == null ||  !mGoogleApiClient.isConnected())
            return;

        // We use a CountDownLatch to ensure that all asynchronous tasks complete within setUp. We
        // set the CountDownLatch count to 1 and decrement this count only when we are certain that
        // mock location has been set.
        final CountDownLatch lock = new CountDownLatch(1);

        // First, ensure that the location provider is in mock mode. Using setMockMode() ensures
        // that only locations specified in setMockLocation(GoogleApiClient, Location) are used.
        LocationServices.FusedLocationApi.setMockMode(mGoogleApiClient, true)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            ExpLogger.logI(TAG, "gpsLoc: Mock mode set");
                            // Set the mock location to be used for the location provider. This
                            // location is used in place of any actual locations from the underlying
                            // providers (network or gps).
                            LocationServices.FusedLocationApi.setMockLocation(
                                    mGoogleApiClient,
                                    mockLocation
                            ).setResultCallback(new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    if (status.isSuccess()) {
                                        ExpLogger.logI(TAG, "Mock location set");
                                        // Decrement the count of the latch, releasing the waiting
                                        // thread. This permits lock.await() to return.
                                        ExpLogger.logI(TAG, "Decrementing latch count");
                                        lock.countDown();
                                    } else {
                                        ExpLogger.logI(TAG, "****Mock location not set");
                                    }
                                }
                            });
                        } else {
                            ExpLogger.logI(TAG, "++++Mock mode not set");
                        }
                    }
                });

        try {
            // Make the current thread wait until the latch has counted down to zero.
            ExpLogger.logI(TAG, "Waiting until the latch has counted down to zero");
            lock.await(2000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException exception) {
            ExpLogger.logException(TAG, exception);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        ExpLogger.logI(TAG, "gpsLoc:onConnectionSuspended");


        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(-1);location.setLongitude(-1);
        Coordinate coordinate = new Coordinate(location);
        setPrevCoordinate(coordinate);
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:time:" + location.getTime());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Alt:" + location.getAltitude());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Bearing:" + location.getBearing());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Accu:" + location.getAccuracy());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:ElapsedTime:" + location.getElapsedRealtimeNanos());
        ExpLogger.logI(TAG, "gpsLoc:------------------");

        mCustomLPD.locationLost();
        if(sendLocation) {
            sendLocationToHUD(coordinate);
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        ExpLogger.logI(TAG, "gpsLoc:onConnectionFailed. Error: " + connectionResult.getErrorCode());

        Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setLatitude(-1);location.setLongitude(-1);
        Coordinate coordinate = new Coordinate(location);
        setPrevCoordinate(coordinate);
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:time:" + location.getTime());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Alt:" + location.getAltitude());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Bearing:" + location.getBearing());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:Accu:" + location.getAccuracy());
        ExpLogger.logI(TAG, "gpsLoc:onLocationChanged:ElapsedTime:" + location.getElapsedRealtimeNanos());
        ExpLogger.logI(TAG, "gpsLoc:------------------");

        mCustomLPD.locationLost();
        if(sendLocation) {
            sendLocationToHUD(coordinate);
        }
    }
    void sendLocationToHUD(Coordinate coordinate){
        try {
            ExpLogger.logI(TAG, "sendLocationToHUD");
            BTComm btComm = new BTComm();
            btComm.setPriority(Utils.TaskPriority.Low.getVal());
            btComm.setCoordinate(coordinate);
            ConnectionHandler.getInstance().sendInfo(btComm);
            if (MapsPresenter.getInstance().isNavigationStarted() && (mGoogleApiClient!= null && mGoogleApiClient.isConnected())) //and trace enabled.
//            if (MapsPresenter.getInstance().isNavigationStarted())
                LocationTracer.getInstance().LogLocation(coordinate.mLoc);
        }catch (Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    public Coordinate getPrevCoordinate() {
        return mPrevCoordinate;
    }

    public void setPrevCoordinate(Coordinate mPrevCoordinate) {
        this.mPrevCoordinate = mPrevCoordinate;
    }
    public Location getLastLocation(){
        if(mPrevCoordinate!=null && mPrevCoordinate.mLoc != null &&
                mPrevCoordinate.mLoc.getLatitude()!=-1 && mPrevCoordinate.mLoc.getLongitude()!=-1){
            return mPrevCoordinate.mLoc;
        }
        return mLastlocation;
    }
}
