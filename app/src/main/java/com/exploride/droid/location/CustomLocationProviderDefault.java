package com.exploride.droid.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MainPresenter;
import com.here.android.mpa.common.PositioningManager;

import static com.cloudant.android.ContentValues.TAG;

/**
 * Created on 26/12/16.
 */

public class CustomLocationProviderDefault extends com.here.android.mpa.common.LocationDataSource{
    private LocationManager lm;
    private int gps_status= LocationProvider.OUT_OF_SERVICE;
    private static CustomLocationProviderDefault mCustomLocationProviderDefault;
    static boolean isStarted;
    private static Object sLock = new Object();
    public PositioningManager.LocationMethod mLocationMethod= PositioningManager.LocationMethod.GPS_NETWORK;
    public CustomLocationProviderDefault(Context context){
        lm = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
    }
    public static CustomLocationProviderDefault getInstance(Context context){
        if(mCustomLocationProviderDefault ==null) {
            synchronized (sLock) {
                if (mCustomLocationProviderDefault == null) {
                    mCustomLocationProviderDefault = new CustomLocationProviderDefault(context);
                }
            }
        }
        return mCustomLocationProviderDefault;
    }

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 1 meter

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 2000; // 5 secs
    @Override
    public boolean start(PositioningManager.LocationMethod locationMethod) {
        // implement type of position required
        boolean returnValue=false;
        synchronized (sLock) {
            try {
                if (locationMethod.equals(mLocationMethod)) {

                    if (!isStarted) {
                        isStarted = true;
                        ExpLogger.logI("start: ", "starting position updates");
                    }
                    returnValue = true;
                }
                return returnValue;
            }catch (Exception e){
                ExpLogger.logException(TAG, e);
                return returnValue;
            }
        }
    }

    //do nothing method as we need location beyond maps purpose
    @Override
    public void stop() {
    }

    public void stopLocationUpdates() {
        synchronized (sLock) {
            ExpLogger.logI(TAG, "gpsMock:CustomLocationProvider stopLocationUpdates");
            if (isStarted) {
                isStarted = false;
                mCustomLocationProviderDefault = null;
            }
        }
    }

    @Override
    public int getGpsStatus() {
        return gps_status;
    }

    @Override
    public int getNetworkStatus() {
        return 0;
    }

    @Override
    public int getIndoorStatus() {
        return 0;
    }

    @Override
    public Location getLastKnownLocation() {
        return CustomLocationListener.getInstance().getPrevCoordinate().mLoc;
    }

    public boolean isStarted(){
        return isStarted;
    }
    public LocationListener getLocationListenerGPS(){
        return locationListenerGPS;
    }

    public void locationLost(){
        locationListenerGPS.onStatusChanged(mLocationMethod.toString(),LocationProvider.TEMPORARILY_UNAVAILABLE,null);
    }
    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            synchronized (sLock) {
                ExpLogger.logI("onLocationChanged: ", "onLocationChanged");
                if (!isStarted())
                    return;
                ExpLogger.logI("onLocationChanged: ", "position updated passing to onLocationUpdated");
                if(gps_status!=LocationProvider.AVAILABLE)
                    onStatusChanged(mLocationMethod.toString(),LocationProvider.AVAILABLE,null);
                // Call LocationDataSource.onPostionUpdate
                onLocationUpdated(mLocationMethod,location);
            }
        }
        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            // call LocationDataSource.onStatusUpdated
            onStatusUpdated(mLocationMethod,i);
            gps_status=i;
        }
        @Override
        public void onProviderEnabled(String s) {
            ExpLogger.logI("onProviderEnabled: ", "onProviderEnabled");
        }
        @Override
        public void onProviderDisabled(String s) {
            ExpLogger.logI("onProviderDisabled: ", "onProviderDisabled");
        }
    };
}

