package com.exploride.droid.location;

import android.location.Location;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;

import com.cloudant.exploridemobile.framework.Coordinate;
import com.exploride.droid.MainApplication;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.services.HandlerManager;
import com.exploride.droid.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by peeyushupadhyay on 09/03/17.
 */

public class LocationTracer{
    Looper mLooper;
    Runnable mRunnable;
    private Handler mHandler;
    public static final String TAG = "LocationTracer";
    static LocationTracer mLocationTracer;
    final String mGpxPath = Environment
            .getExternalStorageDirectory().toString()+"/gpx/";
    final String mTraceFileName = "trace.txt";
    final String mTracePrevFileName = "trace-prev.txt";

    public static LocationTracer getInstance(){
        if(mLocationTracer == null){
            mLocationTracer = new LocationTracer();
            new Thread(mLocationTracer.mRunnable).start();
        }
        return mLocationTracer;
    }
    HandlerThread mHandlerThread;
    LocationTracer(){

        if(mHandlerThread==null) {
            mHandlerThread = new HandlerThread();
            mHandlerThread.start();
            mHandler = new Handler(mHandlerThread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case Constants.LocationTracer.MESSAGE_NEW:
                            startTracing();
                            break;
                        case Constants.LocationTracer.MESSAGE_WRITE:
                            Coordinate coord = (Coordinate) msg.obj;
                            if(coord!=null)
                                writeTrace(coord);
                            break;
                        case Constants.LocationTracer.MESSAGE_CLOSE:
                            stopTracing();
                            break;
                    }
                }
            };
        }

    }

    FileWrite mFileWrite;
    int mCoordSerialId = 0;
    private void startTracing(){
        try {
            mCoordSerialId = 0;
            //create a gpx file
            AddUpdateGpxFile();
            File traceFile = new File(mGpxPath + mTraceFileName);
            mFileWrite = new FileWt(traceFile);
            mFileWrite.open();
        }catch(Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }
    private void writeTrace(Coordinate coordinate){
        try {
            coordinate.setSerialId(mCoordSerialId++);
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            String command = gson.toJson(coordinate);
            mFileWrite.append(command);
            mFileWrite.append("\n"+mLocTraceDel+"\n");
        }catch(Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    private void stopTracing(){
        try {
            mFileWrite.close();
        }catch(Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }
    public String getLastTracePath(){
        return (mGpxPath + mTraceFileName);
    }

    private void AddUpdateGpxFile() {
        try {
            //rename existing mapstogpx.gpx to current date time file. /sdcard/gpx/mapstogpx.gpx
            File gpxPath = new File(mGpxPath);
            if (!gpxPath.exists())
                gpxPath.mkdirs();
            File gpxFile = new File(mGpxPath + mTraceFileName);
            if (gpxFile.exists() && gpxFile.length()>0) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String currentDateandTime = sdf.format(new Date());
                //create a simple backup of last trace for mocking
                File prevTraceFile = new File(mGpxPath + mTracePrevFileName);
                if(prevTraceFile.exists())
                    prevTraceFile.delete();
                FileUtils.copyFile(gpxFile, prevTraceFile);
                gpxFile.renameTo(new File(mGpxPath + currentDateandTime+ "-" + mTraceFileName));
            }
            gpxFile.createNewFile();
        }catch (Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    public Handler getHandler() {
        return mHandler;
    }

    boolean isLoggingLocation = false;
    public void startLoggingLocation(){
        isLoggingLocation = true;
        LocationTracer locationTracer = LocationTracer.getInstance();
        locationTracer.getHandler().obtainMessage(Constants.LocationTracer.MESSAGE_NEW).sendToTarget();;
    }
    public void stopLoggingLocation(){

        isLoggingLocation = false;
        LocationTracer locationTracer = LocationTracer.getInstance();
        locationTracer.getHandler().obtainMessage(Constants.LocationTracer.MESSAGE_CLOSE).sendToTarget();;
    }
    public void LogLocation(Location location){

        if(!isLoggingLocation)
            return;
        LocationTracer locationTracer = LocationTracer.getInstance();
        Coordinate coordinate = new Coordinate(location);
        locationTracer.getHandler().obtainMessage(Constants.LocationTracer.MESSAGE_WRITE, coordinate)
                .sendToTarget();;
    }

    String mLocTraceDel = "----";
    public String readLocationTraces() {
        StringBuffer locationTrace = new StringBuffer();
        File traceFile = new File(mGpxPath + mTracePrevFileName);

        BufferedReader br = null;
        FileReader fr = null;

        try {

            fr = new FileReader(traceFile);
            br = new BufferedReader(fr);

            String sCurrentLine;
            locationTrace.append("[");
            br = new BufferedReader(new FileReader(traceFile));

            while ((sCurrentLine = br.readLine()) != null) {

                System.out.println(sCurrentLine);
                if(!sCurrentLine.endsWith("}"))
                    continue;
                if (!mLocTraceDel.equals(sCurrentLine)) {
                    if(locationTrace.length()!=1)
                        locationTrace.append(",");
                    locationTrace.append(sCurrentLine);
                }
            }

            locationTrace.append("]");
        } catch (IOException e) {

            ExpLogger.logE(TAG, e.getMessage());

        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
        return locationTrace.toString();
    }
}
