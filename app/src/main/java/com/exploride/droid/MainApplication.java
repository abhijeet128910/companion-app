package com.exploride.droid;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.cloudant.exploridemobile.Helper;
import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.CallInfo;
import com.cloudant.exploridemobile.framework.CannedMsg;
import com.cloudant.exploridemobile.framework.DevOptions;
import com.cloudant.exploridemobile.framework.NotificationReq;
import com.cloudant.exploridemobile.framework.NotificationsInfo;
import com.cloudant.exploridemobile.framework.RequestDataOffline;
import com.cloudant.exploridemobile.framework.TTSInfo;
import com.cloudant.exploridemobile.framework.WifiConnect;
import com.cloudant.exploridemobile.framework.WifiScanResult;
import com.crashlytics.android.Crashlytics;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.location.CustomLocationListener;
import com.exploride.droid.location.LocationTracer;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.network.VolleySingleton;
import com.exploride.droid.presenters.CallManager;
import com.exploride.droid.presenters.DownloadMapsPresenter;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.presenters.MapsPresenter;
import com.exploride.droid.presenters.MusicPresenter;
import com.exploride.droid.presenters.NotificationHelper;
import com.exploride.droid.presenters.WifiConnectionPresenter;
import com.exploride.droid.services.BluetoothClientService;
import com.exploride.droid.services.OnClearFromRecentService;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.presenters.ContactsHelper;
import com.exploride.droid.utils.LocationMockerPhone;
import com.exploride.droid.utils.SMSHelper;
import com.exploride.droid.utils.Utils;
import com.exploride.droid.utils.appupdater.ExplorideUpdateChecker;
import com.exploride.droid.views.NotificationsMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.support.multidex.*;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

import static android.bluetooth.BluetoothDevice.BOND_NONE;
import static android.bluetooth.BluetoothDevice.EXTRA_BOND_STATE;
import static android.bluetooth.BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.GET_BT_NAME;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.INACTIVE_NAVIGATION;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MAP_ENGINE_OFFLINE_ERROR;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MUSIC_PLAY;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MapEngineInitializationError;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MapInstallError;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MapInstallSuccess;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MapPackageError;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MAP_INIT_FAILED;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MapUninstallError;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MapUninstallSuccess;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.OBD_CONN_STATE_RESP;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.ReqCoordinates;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.RequestUpdatesCheck;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.SEARCH_CONTACT_TERM;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.SC_NAME;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.SPEECH_TXT;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.UpdatesDownloadFinished;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.VOL_DECREASE;
import static com.exploride.droid.utils.Constants.CHANNEL.BT;

/**
 * Main Application Class!
 */
public class MainApplication extends MultiDexApplication implements TextToSpeech.OnInitListener, BluetoothHandler.BTState
{
    public static final String TAG = MainApplication.class.getSimpleName();
    static MainApplication mainApplication;
    PresenterManager mPresenterManager;
    private WeakReference<Activity> mCurrentActivity;
    static boolean isVoiceInitialized = false;
    public boolean shouldSendLocationFromPhone = true;

    // Volley related fields
    public static VolleySingleton volleyQueueInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private boolean isDebugMode;
    //would get unset in Launcher Activity.OnCreate
    public static boolean mFirstLaunch = true;
    private TextToSpeech mTTS;
    Handler mUIHandler;
    private final static Object sTTS_lock = new Object();

    private int mCurrentTTSId = -1;

    public static class BuildV{
        public static boolean IS_VANILLA = false;
        public static boolean IS_DEFAULTF = false;
    }
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onCreate()
    {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        if(BuildConfig.FLAVOR.equals("vanilla"))
        {
            BuildV.IS_DEFAULTF = false;
            BuildV.IS_VANILLA = true;
        }
        else if(BuildConfig.FLAVOR.equals("defaultF"))
        {
            BuildV.IS_VANILLA = false;
            BuildV.IS_DEFAULTF = true;
        }
        ExpLogger.logI(TAG, "git branch=" + BuildConfig.GIT_BRANCH);
        ExpLogger.logI(TAG, "git commit=" + BuildConfig.GIT_HASH);
        ExpLogger.logI(TAG, "build flav=" + BuildConfig.FLAVOR);
        Crashlytics.setString("devInfo", Utils.getDeviceName());
        String crashTag = BuildConfig.GIT_BRANCH+"-"+BuildConfig.GIT_HASH+"-"+BuildConfig.FLAVOR;
        Crashlytics.setString("crashTagM",crashTag);
        // Initialize the singletons so their instances
        // are bound to the application process.
        initSingletons();

        // Initialize volley request queue to make network calls
        instantiateVolleyQueue();
        CustomLocationListener.getInstance().startListeningForLocation();

        isDebugMode = SettingsUtils.readPreferenceValueAsBoolean(this, Constants.KEY_BASE + Constants.KEY_ENABLE_DEBUG_MODE);
    }


    AudioManager am;
    /**
     * method to instantiate singleton classes, services
     */
    protected void initSingletons()
    {

        mainApplication = this;
        mPresenterManager = PresenterManager.getInstance();
        BluetoothHandler.getInstance();
        final MainApplication ref = this;
        startService(new Intent(getBaseContext(), OnClearFromRecentService.class));
        new Thread(new Runnable() {
            @Override
            public void run() {

                boolean isTCP = SettingsUtils.readPreferenceValueAsBoolean(MainApplication.this,
                        Constants.SharedPrefKeys.KEY_DEVOPTIONS_STATE_TCP, true);
                LocationMockerPhone.getInstance();
                mTTS = new TextToSpeech(ref, ref);
                mTTS.setOnUtteranceProgressListener(utteranceProgressListener);
                am = (AudioManager) ref.getSystemService(Context.AUDIO_SERVICE);
                ExplorideUpdateChecker.getInstance();
                NotificationsMgr.getInstance();
                LocationTracer.getInstance();
                int val = SettingsUtils.readPreferenceValueAsInt(mainApplication, Constants.SharedPrefKeys.KEY_USER_COMM_CHANNEL);
                String host = SettingsUtils.readPreferenceValueAsString(mainApplication, Constants.SharedPrefKeys.KEY_DEVOPTIONS_TCP_HOST);
                ConnectionHandler.getInstance().setUserCommunicationChannelType(val == Constants.CHANNEL.BT.getValue()?Constants.CHANNEL.BT:Constants.CHANNEL.TCP);
                if(val != Constants.CHANNEL.BT.getValue())
                    ConnectionHandler.getInstance().startTCPClient(host);

            }
        }).start();
    }
    AudioManager.OnAudioFocusChangeListener afChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                        // Permanent loss of audio focus
                        // Pause playback immediately
//                        mediaController.getTransportControls().pause();
//                        // Wait 30 seconds before stopping playback
//                        mHandler.postDelayed(mDelayedStopRunnable, TimeUnit.toMillis(30));
                    } else if (focusChange == AUDIOFOCUS_LOSS_TRANSIENT) {
                        // Pause playback
                    } else if (focusChange == AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                        // Lower the volume, keep playing
                    } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                        // Your app has been granted audio focus again
                        // Raise volume to normal, restart playback if necessary
                    }
                }

            };
    public void sendCommand(){

    }
    public void setUpBT(){


    }
    public void cleanUpOnExit(){

        if (mTTS != null) {
            mTTS.stop();
            mTTS.shutdown();
            mTTS = null;
        }

    }
    public void exit(){

        Activity activity = (FragmentActivity)getCurrentActivity();
        if(isDebugMode)
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show();
        activity.finish();
        //TODO: exit app
    }

    public static MainApplication getInstance(){
        return mainApplication;
    }

    public void setCurrentActivity(Activity act){
        mCurrentActivity = new WeakReference<Activity>(act);
    }

    public Activity getCurrentActivity(){
        if(mCurrentActivity==null) return null;
        return mCurrentActivity.get();
    }

    /**
    * Method to startBTServer services that depend on the activity context, otherwise {@link MainApplication#initSingletons}
    * should be used.
    * @param mHandler handler to post the results back on UI thread
     */
    public void initServices(Handler mHandler){

        BluetoothHandler.getInstance();
        registerBtBinder();
    }

    @Override
    public void onInit(int status) {
        try {
            if (mTTS != null) {
                if (status == TextToSpeech.SUCCESS) {

                    int result = mTTS.setLanguage(Locale.US);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        ExpLogger.logException(TAG, new Exception("TTS onInit failed"));
                    } else
                        ExpLogger.logI(TAG, "TTS initialization done");
                }else {
                    ExpLogger.logException(TAG, new Exception("TextToSpeech engine initialization Failed!"));
                }
            } else {
                ExpLogger.logException(TAG, new Exception("TextToSpeech is null"));
            }
        }catch (Exception e){
            ExpLogger.logException(TAG, e);
        }
    }
    void playBeep(){
        try {
            AssetFileDescriptor afd = (getCurrentActivity()).getAssets().openFd("beep.mp3");
            final MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    player.stop();
                    player.release();
                }
            });
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }
    /**
     * call from bg thread only
     * @param text
     */
    public void speakText(String text, int currentTTSId) {

        synchronized (sTTS_lock) {
            try {
                assertBGThread();
                if(text.equals("beep")){
                    playBeep();
                    return;
                }
                if (mTTS != null) {
                    while (mTTS.isSpeaking())
                        Thread.sleep(1000);

                    mCurrentTTSId = currentTTSId;

                    ExpLogger.logI(TAG, "requesting audio focus");
                    int result = am.requestAudioFocus(afChangeListener,
                            // Use the music stream.
                            AudioManager.STREAM_MUSIC,
                            // Request permanent focus.
                            AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);

                    if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                        MainApplication.getInstance().showToast("AUDIOFOCUS_REQUEST_GRANTED refused");
                        ExpLogger.logI(TAG, "AUDIOFOCUS_REQUEST_GRANTED refused");
                    }
                        //we will speak irrespective of
                        mTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null, text);
                }
            } catch (Exception e) {
                ExpLogger.logE(TAG, e.getMessage());
            } finally {
                // Abandon audio focus

                while (mTTS.isSpeaking())
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        ExpLogger.logE(TAG, e.getMessage());
                    }
                ExpLogger.logI(TAG, "abandoning audio focus");
                am.abandonAudioFocus(afChangeListener);
            }
        }
    }

    public void stopTTS(){
        try {
            if (mTTS != null)
                mTTS.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Listener for speech completion
     */
    private UtteranceProgressListener utteranceProgressListener = new UtteranceProgressListener() {
        @Override
        public void onStart(String utteranceId) {

        }

        @Override
        public void onDone(String utteranceId) {
            if (isCommunicationChannelOpen())
            {
                BTComm btComm = new BTComm();
                TTSInfo ttsInfo = new TTSInfo();
                ttsInfo.setId(mCurrentTTSId);
                ttsInfo.setText(utteranceId);
                btComm.setTTSInfo(ttsInfo);
                ConnectionHandler.getInstance().sendInfo(btComm);
            }
        }

        @Override
        public void onError(String utteranceId) {

        }
    };

    public void assertBGThread() throws Exception{

        if(Looper.getMainLooper()==Looper.myLooper()) {
            Exception e = new Exception("Called from main thread");
            ExpLogger.logException(TAG, e);
            throw e;
        }
    }

    BroadcastReceiver mBondReceiver;
    public void registerBtBinder(){

        if(mBondReceiver==null) {
            mBondReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, final Intent intent) {
                    ExpLogger.logI(TAG, "mBondReceiver: onReceive");

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                BluetoothDevice mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                                if (mDevice != null) {
                                    if (!mDevice.getAddress().equals(BluetoothHandler.getInstance().getCurrentDevice().getAddress())) {
                                        ExpLogger.logI(TAG, "mBondReceiver: -------wrong device: " + mDevice.getAddress() + "--;;--" + BluetoothHandler.getInstance().getCurrentDevice().getAddress());
                                        return;
                                    }
                                    if (intent != null) {
                                        String action = intent.getAction();
                                        if (BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action)) {
                                            ExpLogger.logI(TAG, "mBondReceiver: onReceive " + "ACTION_PAIRING_REQUEST");
                                            ConnectionHandler.getInstance().handlePairingRequest(mDevice, intent);
                                        }
//                                    if (bondState == BOND_BONDED)
//                                        BluetoothHandler.getInstance().connectToService(mDevice);
                                    }
                                }

                            }catch(Exception e){
                                ExpLogger.logException(TAG, new Exception("mBondReceiver:\n" +e.getMessage()));
                            }
                        }
                    }).start();
                }
            };
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
            filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
            MainApplication.getInstance().registerReceiver(mBondReceiver, filter);

        }
    }

    public Handler getUIHandler(){
        return mUIHandler = new Handler(Looper.getMainLooper());
    }


    ArrayList<BluetoothHandler.BTState> mBTStateListeners = new ArrayList<>();
    static Object mBTStateListenerLock = new Object();
    public void registerBTStateListener(BluetoothHandler.BTState stateListener){
        synchronized (mBTStateListenerLock) {
            mBTStateListeners.add(stateListener);
        }
    }
    public void unRegisterBTStateListener(BluetoothHandler.BTState stateListener){
        synchronized (mBTStateListenerLock) {
            mBTStateListeners.remove(stateListener);
        }
    }
    private static Object sConnectionWaitObj = new Object();
    public Object getConnectionWaitObj(){return sConnectionWaitObj;};
    @Override
    public void BTConnected(Constants.CHANNEL channel) {
        try {
            if (channel == BT)
                setStatus(R.string.title_connected);
            //TODO: send only if changed.
            synchronized (sConnectionWaitObj) {
                sConnectionWaitObj.notifyAll();
            }

            //TODO: introduce a delay/pr mechanism
            if ((!ConnectionHandler.getInstance().useTCP() && channel == BT)
                    || (ConnectionHandler.getInstance().useTCP() && channel == Constants.CHANNEL.TCP)) {
                BTComm btComm = new BTComm();
                btComm.setCredentials(Helper.getCredential());
                MainPresenter.getInstance().setDevOptions(btComm);
                setLocale(btComm);
                ConnectionHandler.getInstance().sendInfo(btComm);
                MainPresenter.getInstance().sendSyncPrefsToHUD();
                MainPresenter.getInstance().checkNavigationStateOnHUD();
                NotificationHelper.getInstance().getAllNotifications();
                ContactsHelper.getInstance().syncContactsWithHUD();
            }
            synchronized (mBTStateListenerLock) {
                for (BluetoothHandler.BTState mBTStateListener : mBTStateListeners) {
                    mBTStateListener.BTConnected(channel);
                }
            }
        }catch (Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }
    private void setLocale(BTComm btcom){

        final Locale locale = Locale.getDefault();
        btcom.setDevLocale(locale);
    }


    public void sendNotificationInfo(NotificationsInfo notificationsInfo) {
        waitForChannel();
        BTComm btComm = new BTComm();
        btComm.setNotificationsInfo(notificationsInfo);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }
    void waitForChannel() {
        if(!MainApplication.getInstance().isCommunicationChannelOpen()) {
            try {
                Object waitObj = MainApplication.getInstance().getConnectionWaitObj();
                synchronized (waitObj) {
                    waitObj.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isCommunicationChannelOpen(){
        return (ConnectionHandler.getInstance().useTCP() || (BluetoothHandler.getInstance() != null && BluetoothHandler.getInstance().getBluetoothClientService() != null &&
                BluetoothHandler.getInstance().getBluetoothClientService().getState()== BluetoothClientService.STATE_CONNECTED));
    }
    @Override
    public void BTConnectedToDevice(final String deviceName, Constants.CHANNEL channel) {
        if(channel== ConnectionHandler.getInstance().getUserCommunicationChannelType())
            if (null != getCurrentActivity() && isDebugMode) {
                getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getCurrentActivity(), "Connected to " + deviceName, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        synchronized (mBTStateListenerLock) {
            for (BluetoothHandler.BTState mBTStateListener : mBTStateListeners) {
                mBTStateListener.BTConnectedToDevice(deviceName, channel);
            }
        }
    }

    @Override
    public void BTConnecting(Constants.CHANNEL channel) {
        if(channel== ConnectionHandler.getInstance().getUserCommunicationChannelType())
            setStatus(R.string.title_connecting);
        synchronized (mBTStateListenerLock) {
            for (BluetoothHandler.BTState mBTStateListener : mBTStateListeners) {
                mBTStateListener.BTConnecting(channel);
            }
        }
    }

    @Override
    public void BTNotConnected(Constants.CHANNEL channel) {
        if(channel== ConnectionHandler.getInstance().getUserCommunicationChannelType())
            setStatus(R.string.title_not_connected);
        synchronized (mBTStateListenerLock) {
            for (BluetoothHandler.BTState mBTStateListener : mBTStateListeners) {
                mBTStateListener.BTNotConnected(channel);
            }
        }
    }

    @Override
    public void toastToShow(final String msg, Constants.CHANNEL channel) {
        if (null != getCurrentActivity() && isDebugMode) {
            getCurrentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getCurrentActivity(), msg, Toast.LENGTH_SHORT).show();
                }
            });

        }
        synchronized (mBTStateListenerLock) {
            for (BluetoothHandler.BTState mBTStateListener : mBTStateListeners) {
                mBTStateListener.toastToShow(msg, channel);
            }
        }
    }

    @Override
    public void BTMsgReceived(String msg, Constants.CHANNEL channel){
        onBTCommandReceived(msg);
        synchronized (mBTStateListenerLock) {
            for (BluetoothHandler.BTState mBTStateListener : mBTStateListeners) {
                mBTStateListener.BTMsgReceived(msg, channel);
            }
        }
//        toastToShow(msg);
    }

    @Override
    public void BTMsgSent(String msg, Constants.CHANNEL channel){
        synchronized (mBTStateListenerLock) {
            for (BluetoothHandler.BTState mBTStateListener : mBTStateListeners) {
                mBTStateListener.BTMsgSent(msg, channel);
            }
        }
//        toastToShow("sent-\n"+msg);
    }
    public void onBTCommandReceived(String command){

        //TODO: temp code for testing connection
//        if(command.equals("this is a test message")){
//            mainView.get().showToast2(command);
//            return;
//        }
        ExpLogger.logI(TAG, "onBTCommandReceived: " + command);
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            BTComm btComm = gson.fromJson(command, BTComm.class);
            boolean res = resolveBTMsg(btComm);
            NotificationReq notificationReq = btComm.getNotificationReq();
            if(notificationReq!=null){
                NotificationsMgr.getInstance().handleNotificationRequest(notificationReq);
            }
            List<WifiScanResult> wifiScanResult = btComm.getWifiScanResult();
            if(wifiScanResult!=null){
                WifiConnectionPresenter.getInstance().scanResults(wifiScanResult);
            }
            CallInfo callInfo = btComm.getCallInfo();
            if(callInfo!=null){
                CallManager.getInstance().handleCallInfo(callInfo);
            }

            RequestDataOffline requestDataOffline = btComm.getRequestDataOffline();
            if (requestDataOffline != null) {
                MainPresenter.getInstance().handleOfflineDataRequest(requestDataOffline);
            }
            CannedMsg cannedMsg = btComm.getCannedMsg();
            if (cannedMsg != null){
                SMSHelper smsHelper = new SMSHelper(this);
                smsHelper.sendSMS(cannedMsg.getNumber(), cannedMsg.getMessage());
            }

            if(btComm.getActionCode() == ActionConstants.ActionCode.LOCATION_SRC_RESP){
                DevOptions.LocationSource locationSource = btComm.getDevOptions().getLocationSource();
                setShouldSendLocationFromPhone(locationSource.getIsLocationFromPhone());
                Utils.toggleLocPhone(MainApplication.getInstance().shouldSendLocationFromPhone);
            }

        }catch(Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }
    public void showToast(final String msg) {
        isDebugMode = true;
        if (null != getCurrentActivity() && isDebugMode){
            getCurrentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getCurrentActivity(), msg, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    boolean resolveBTMsg(BTComm btComm){
        int actionCode = btComm.getActionCode();

        final WifiConnect wifiConnect = btComm.getWifiConnect();
        if(actionCode<=0)
            return false;

        if(actionCode>=RequestUpdatesCheck && actionCode<=UpdatesDownloadFinished){
            ExplorideUpdateChecker.getInstance().handleStatusCodes(actionCode);
            return true;
        }

        if (actionCode >= MUSIC_PLAY && actionCode <= VOL_DECREASE) {
            MusicPresenter.getInstance().handleStatusCodes(actionCode);
            return true;
        }
        switch (actionCode){

            case ActionConstants.ActionCode.TCP_IN:
                try {
                    //lets check if mob app had tcp disabled though. In that case connect attempt
                    //should not be made and disable request should be sent.
                    if (!ConnectionHandler.getInstance().isTCPEnabled()) {
                        ConnectionHandler.getInstance().sendTCPStateReq(ActionConstants.ActionCode.TCP_DS);
                        showToast("Rejecting TCP configuration, TCP not enabled in mobile app");
                        break;
                    }
                    String port = btComm.getData();
                    if (port.equals(""))
                        port = "192.168.2.16";
                    final String finalPort = port;
                    ConnectionHandler.getInstance().putAddressEntryForBT(finalPort, BluetoothHandler.getInstance().getLastConnectedDeviceAddress());
                    ConnectionHandler.getInstance().startTCPClient(finalPort);
                    showToast("Received TCP configuration");
                }catch (Exception e){
                    ExpLogger.logE(TAG, e.getMessage());
                }
                break;
            case ActionConstants.ActionCode.WiFiConnectionCompleted:
                MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainPresenter.getInstance().setWifiName(wifiConnect.getSSID());
                        WifiConnectionPresenter.getInstance().connectionCompleted(wifiConnect.getSSID());
                    }
                });
                break;
            case ActionConstants.ActionCode.WifiPWD:
                MainPresenter.getInstance().showDialogForPassword(wifiConnect.getSSID());
                break;
            case ActionConstants.ActionCode.WifiConnectivityChanged:
                MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainPresenter.getInstance().setWifiName(wifiConnect.getSSID());
                        WifiConnectionPresenter.getInstance().connectionCompleted(wifiConnect.getSSID());
                    }
                });
                break;
            case ActionConstants.ActionCode.InternetAvailable:
                showToast("You are connected to Internet!");
                MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainPresenter.getInstance().setWifiName(wifiConnect.getSSID());
                    }
                });
                break;
            case ActionConstants.ActionCode.InternetNotAvailable:
                showToast("Internet is not available");
                MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainPresenter.getInstance().setWifiName(wifiConnect.getSSID());
                    }
                });
                break;
            case ReqCoordinates:
                CustomLocationListener customLocationListener = CustomLocationListener.getInstance();
                customLocationListener.startSendingLocation();
                break;
            case MapPackageError:
                DownloadMapsPresenter.getInstance().onMapPackageError();
                break;
            case MAP_ENGINE_OFFLINE_ERROR:
                DownloadMapsPresenter.getInstance().handleMapEngineOfflineError();
                break;
            case MAP_INIT_FAILED:
                MainPresenter.getInstance().notifyMapInitFailed();
                break;
            case INACTIVE_NAVIGATION:
                MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //checks if post trip summary is shown then redirect user to maps screen
                        if (MainPresenter.getInstance().isPostTripSummaryShown())
                            MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
                        else if (MapsPresenter.getInstance().isNavigationStarted())// checks if navigation is running on mobile app then reset it
                            MapsPresenter.getInstance().resetNavigation();
                    }
                });
                break;
            case MapInstallSuccess:
                DownloadMapsPresenter.getInstance().onMapInstallSuccess();
                break;
            case MapInstallError:
                DownloadMapsPresenter.getInstance().onMapInstallError();
                break;
            case MapUninstallSuccess:
                DownloadMapsPresenter.getInstance().onMapUninstallSuccess();
                break;
            case MapUninstallError:
                DownloadMapsPresenter.getInstance().onMapUninstallError();
                break;
            case MapEngineInitializationError:
                DownloadMapsPresenter.getInstance().onMapEngineInitializationError();
                break;
            case SPEECH_TXT:
                String txt = btComm.getData();
                TTSInfo ttsInfo = btComm.getTTSInfo();
                if (ttsInfo.getCommand() == TTSInfo.PLAY)
                    speakText(txt, ttsInfo.getId());
                else if (ttsInfo.getCommand() == TTSInfo.STOP)
                    stopTTS();
                break;
            case GET_BT_NAME:
                MainPresenter.getInstance().sendBtNameToHUD();
                break;
            case OBD_CONN_STATE_RESP:
                MainPresenter.getInstance().showDialog(getString(R.string.alert), getString(R.string.obd_chn_changed), new MainPresenter.DialogCallback() {
                    @Override
                    public void onUserOK() {}
                    @Override
                    public void onUserCancel() {}
                }, false);
                break;
            case SC_NAME:
                MainPresenter.getInstance().setScName(btComm.getData());
                break;
            case SEARCH_CONTACT_TERM:
                ContactsHelper.getInstance().searchContactByName(this, btComm.getData());
                break;
        }
        return true;
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(final int resId) {
        final Activity activity = getCurrentActivity();
        if (null == activity) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ActionBar actionBar = activity.getActionBar();
                if (null == actionBar) {
                    return;
                }
                actionBar.setSubtitle(resId);
            }
        });
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(CharSequence subTitle) {
        Activity activity = getCurrentActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    /**
     * Initializes volley singleton class to make network calls
     */
    public void instantiateVolleyQueue() {
        volleyQueueInstance = VolleySingleton.getInstance(getApplicationContext());
    }

    /**
     * gets Request queue for the network requests
     * @return RequestQueue: queue of network requests made
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {

            mRequestQueue = Volley.newRequestQueue(getApplicationContext(),
                    new HurlStack());
        }

        return mRequestQueue;
    }

    /**
     * Adds a Request to the dispatch queue.
     * @param req: The request to service
     * @param tag: tag for each network request
     * @return The passed-in request
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        getRequestQueue().add(req);
    }

    /**
     * gets Image Loader to load images
     * @return ImageLoader: Helper that handles loading and caching images from remote URLs.
     */
    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    /**
     * returns whether debug mode is on
     * @return boolean true if debug mode is on else false
     */
    public boolean isDebugModeOn(){
        return isDebugMode;
    }

    /**
     * sets  whether debug mode is on or not
     * @param isDebugMode: boolean true if debug mode is on else false
     */
    public void setDebugModeOn(boolean isDebugMode){
        this.isDebugMode = isDebugMode;
        CustomLocationListener.getInstance().requestLocationUpdates();
    }

    public void setShouldSendLocationFromPhone(boolean sendLocationFromPhone) {
        this.shouldSendLocationFromPhone = sendLocationFromPhone;
    }
}

