package com.exploride.droid.presenters;

import android.net.wifi.ScanResult;

import com.cloudant.exploridemobile.framework.WifiScanResult;
import com.exploride.droid.PresenterManager;
import com.exploride.droid.WifiConnectionContract;
import com.exploride.droid.utils.Constants;

import java.util.List;

/**
 * Created by Abhijeet on 11/15/2016.
 */

public class WifiConnectionPresenter implements WifiConnectionContract.WifiConnectionOps {

    static WifiConnectionPresenter wifiConnectionPresenter;
    private WifiConnectionContract.WifiConnectionView wifiConnectionView;

    public static WifiConnectionPresenter getInstance() {
        if (wifiConnectionPresenter == null) {
            wifiConnectionPresenter = new WifiConnectionPresenter();
        }
        return wifiConnectionPresenter;
    }


    @Override
    public void init(WifiConnectionContract.WifiConnectionView view) {
        if (this.wifiConnectionView != null)
            return;

        this.wifiConnectionView = view;
    }

    public boolean isViewValid() {

        if (wifiConnectionView == null)
            return false;

        return true;
    }
    public void scanResults(List<WifiScanResult> scanResults){
        if(isViewValid())
            this.wifiConnectionView.onScanResultsReceived(scanResults);
    }

    @Override
    public void onStartView() {
        PresenterManager.getInstance().addActivePresenter(this);
    }

    @Override
    public void onStopView() {
        PresenterManager.getInstance().removeActivePresenter(this);
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {
        wifiConnectionView = null;
    }

    @Override
    public void connectionCompleted(String ssid) {
        if(isViewValid()){
            wifiConnectionView.onConnectionCompleted(ssid);
        }
    }
}
