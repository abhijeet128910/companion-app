package com.exploride.droid.presenters;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.cloudant.exploridemobile.ExpDocument;
import com.cloudant.exploridemobile.LocationsMgr;
import com.exploride.droid.PresenterManager;
import com.exploride.droid.R;
import com.exploride.droid.SearchLocationContract;
import com.exploride.droid.geocoding.GeoCodingProvider;
import com.exploride.droid.geocoding.GeoCodingProviderFactory;
import com.exploride.droid.geocoding.GoogleMaps;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.MapsStore;
import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.model.RecentLocation;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.model.SearchLocationStore;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.DateFormatter;
import com.exploride.droid.utils.Utils;
import com.exploride.droid.views.fragments.BaseFragment;
import com.exploride.droid.views.fragments.MapsDestinationFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteTta;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Author Mayank
 */

public class SearchLocationPresenter implements SearchLocationContract.SearchLocationOps, LocationsMgr.LocationsUpdateListener {

    static SearchLocationPresenter searchLocationPresenter;
    Handler mHandler = new Handler(Looper.getMainLooper());
    private Activity mActivity;
    private String TAG = SearchLocationPresenter.class.getName();
    private WeakReference<SearchLocationContract.SearchLocationView> searchLocationView;
    private SearchLocationContract.LocationResolverListener mLocationResolverListener;
    private List<ExpDocument> mSavedLocationsList = new ArrayList<>();

    // To select unit as Kilometers/Miles to display speed and distance
    private String distanceUnits = Constants.Distance.MILES;

    private GeoCodingProvider geoCodingProvider;

    private ArrayList<Double> mCoordinatesList;
    private String unitInMeters;
    private String unitInKms;
    private String unitInFeet;
    private String unitInMiles;

    private List<RecentLocation> mRecentLocationsList;
    private final int MAX_RECENT_LOCATIONS = 3;

    public static SearchLocationPresenter getInstance() {
        if (searchLocationPresenter == null) {
            searchLocationPresenter = new SearchLocationPresenter();
        }
        return searchLocationPresenter;
    }

    public void initLocationMgr() {
        LocationsMgr.getLocationsMgr().getInstance(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                mSavedLocationsList.addAll(LocationsMgr.getLocationsMgr().reloadLocationsFromModel());
            }
        }).start();

    }

    @Override
    public void init(final SearchLocationContract.SearchLocationView view) {
        mActivity = ((BaseFragment) view).getActivity();
        if (this.searchLocationView != null)
            return;
        this.searchLocationView = new WeakReference<>(view);

        //get an object of GoogleMaps or HereMaps class instance to perform geo coding request.
        geoCodingProvider = new GeoCodingProviderFactory().getGeoCodingProvider(Constants.ProviderType.GOOGLE_MAPS);

        unitInFeet = mActivity.getResources().getString(R.string.turn_distance_feet);
        unitInMiles = mActivity.getResources().getString(R.string.turn_distance_miles);
        unitInMeters = mActivity.getResources().getString(R.string.turn_distance_meter);
        unitInKms = mActivity.getResources().getString(R.string.turn_distance_kms);

        try {
            if (mRecentLocationsList == null) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String recentLocationText = SettingsUtils.readPreferenceValueAsString(mActivity,
                                Constants.SharedPrefKeys.KEY_RECENT_LOCATIONS);
                        if (recentLocationText != null) {

                            Type type = new TypeToken<List<RecentLocation>>() {
                            }.getType();
                            mRecentLocationsList = new Gson().fromJson(recentLocationText, type);
                            view.showRecentLocations(mRecentLocationsList);
                        }
                    }
                });
                thread.start();

            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    public boolean isViewValid() {

        if (searchLocationView == null || searchLocationView.get() == null)
            return false;
        return true;
    }

    @Override
    public void onStartView() {
        PresenterManager.getInstance().addActivePresenter(this);
    }

    @Override
    public void onStopView() {
        PresenterManager.getInstance().removeActivePresenter(this);

    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {
        searchLocationView = null;
    }

    /**
     * gets location results based on user's query
     *
     * @param queryLocation: The user's query used for Geo Coding
     */
    @Override
    public void fetchAutoSuggestLocations(String queryLocation) {

        geoCodingProvider.getAutoSuggestPredictions(mActivity, queryLocation);
    }

    /**
     * sets error returned while making network request
     *
     * @param error: error occurred while making a network request
     */
    @Override
    public void onError(final String error) {
        if (isViewValid()) {
            if (Looper.getMainLooper() != Looper.myLooper())
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        searchLocationView.get().showErrorMessage(error);
                    }
                });
            else
                searchLocationView.get().showErrorMessage(error);
        }
    }

    /**
     * parses response for the network request and passes it to @class {@link MapsDestinationFragment}
     * to be rendered on the UI
     *
     * @param placeAutoComplete: network response
     */
    @Override
    public void onResponse(final PlaceAutoComplete placeAutoComplete) {
        if (isViewValid()) {
            if (Looper.getMainLooper() != Looper.myLooper())
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        searchLocationView.get().showGeocodeLocations(placeAutoComplete);
                    }
                });
            else
                searchLocationView.get().showGeocodeLocations(placeAutoComplete);
        }

    }

    /**
     * sets source location
     *
     * @param position source location
     */
    @Override
    public void setSourceLocation(String placeName, ArrayList<Double> position) {

        //set source location place name
        MapsStore.getInstance().setSourceLocation(placeName);

        // set source location parameters
        MapsStore.getInstance().setSourceLatitude(position.get(0));
        MapsStore.getInstance().setSourceLongitude(position.get(1));
    }

    /**
     * sets current location as source location
     */
    @Override
    public void setSourceLocation() {

        //set source location place name
        MapsStore.getInstance().setSourceLocation(MapsPresenter.getInstance().mReverseGeoCodedLocation);

        Location location = MapsPresenter.getInstance().mCurrentSourceLocation;
        //check to verify if location is not null
        if(location != null && location.getLatitude() != 0 && location.getLongitude() != 0) {
            // set source location parameters
            MapsStore.getInstance().setSourceLatitude(location.getLatitude());
            MapsStore.getInstance().setSourceLongitude(location.getLongitude());
        }
    }

    /**
     * sets destination location
     *
     * @param position: destination location
     */
    @Override
    public void setDestinationLocation(String placeName, ArrayList<Double> position) {

        //set destination location place name
        MapsStore.getInstance().setDestinationLocation(placeName);

        // set destination location parameters
        MapsStore.getInstance().setDestinationLatitude(position.get(0));
        MapsStore.getInstance().setDestinationLongitude(position.get(1));
    }

    /**
     * gets source location
     *
     * @return source location
     */
    @Override
    public Location getSourceLocation() {

        Location sourceLocation = new Location("");
        sourceLocation.setLatitude(MapsStore.getInstance().getSourceLatitude());
        sourceLocation.setLongitude(MapsStore.getInstance().getSourceLongitude());
        return sourceLocation;
    }

    /**
     * gets destination location
     *
     * @return destination location
     */
    @Override
    public Location getDestinationLocation() {
        Location destinationLocation = new Location("");
        destinationLocation.setLatitude(MapsStore.getInstance().getDestinationLatitude());
        destinationLocation.setLongitude(MapsStore.getInstance().getDestinationLongitude());
        return destinationLocation;
    }

    /**
     * Calculate estimated time of arrival. Delta between estimated time and current time.
     *
     * @param mapRoute: route from source to destination
     */
    @Override
    public void getETA(RouteSummary routeSummary, MapRoute mapRoute) {
        try {
            RouteTta routeTta = mapRoute.getRoute().getTta(Route.TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE);
            int routeDuration = routeTta.getDuration();
            if (routeDuration > 0) {

                routeSummary.setDuration(routeDuration);

                String reachTime = DateFormatter.getReachingTime(routeDuration);

                String eta = DateFormatter.getDurationInText(routeDuration) + reachTime;

                if (eta != null && !TextUtils.isEmpty(eta))
                    routeSummary.setEta("ETA " + eta);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * Get via place to reach the destination.
     *
     * @param mapRoute: route from source to destination
     */
    @Override
    public void getViaPoint(RouteSummary routeSummary, MapRoute mapRoute) {
        try {
            if (mapRoute != null && mapRoute.getRoute() != null && mapRoute.getRoute().getManeuvers() != null) {
                String viaPoint = calculateMaxManeuver(mapRoute);

                if (viaPoint != null && !TextUtils.isEmpty(viaPoint))
                    routeSummary.setViaPoint("via " + viaPoint);

            }
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * Calculate distance of route in meters and then decide to show as Miles or Kms
     *
     * @param mapRoute: route from source to destination
     */
    @Override
    public void getDistance(RouteSummary routeSummary, MapRoute mapRoute) {
        try {
            if (mapRoute.getRoute().getLength() > 0) {
                DecimalFormat decimalFormat = new DecimalFormat("#.##");

                double conversionRate = (distanceUnits == Constants.Distance.KILOMETERS) ? Constants.Distance.KMS_CONVERSION_UNIT :
                        Constants.Distance.MILES_CONVERSION_UNIT;

                double distanceMiles = mapRoute.getRoute().getLength() / conversionRate;
                String distance = decimalFormat.format(distanceMiles) + " " + distanceUnits;

                if (distance != null && !TextUtils.isEmpty(distance))
                    routeSummary.setDistance(distance);

            }
        } catch (Exception e) {
            e.printStackTrace();
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * sets summary for the route to be shown to the user
     *
     * @param routeSummaries : summary for routes like eta, via point, distance
     */
    @Override
    public void setRouteSummary(ArrayList<RouteSummary> routeSummaries) {

        //sets route summary
        MapsStore.getInstance().setRouteSummary(routeSummaries);
    }

    /**
     * sets map route selected by the user
     *
     * @param mapRoute: route from source to destination
     */
    @Override
    public void setMapRoute(MapRoute mapRoute) {

        //sets map route selected by the user
        MapsStore.getInstance().setMapRoute(mapRoute);
    }

    /**
     * sets way point selected by the user in the current route
     *
     * @param coordinates: way point coordinates
     */
    @Override
    public void setWayPoint(ArrayList<Double> coordinates) {
        Location location = new Location("");
        location.setLatitude(coordinates.get(0));
        location.setLongitude(coordinates.get(1));

        // add way points location parameters
        SearchLocationStore.getInstance().addWayPoints(location);
    }

    /**
     * gets way points added by the user
     *
     * @return list of way points
     */
    @Override
    public ArrayList<Location> getWayPoints() {
        return SearchLocationStore.getInstance().getWayPoints();

    }

    @Override
    public void removeAllWayPoints() {
        SearchLocationStore.getInstance().removeAllWayPoints();
    }

    /**
     * Get maneuver road name which has maximum distance out of all maneuvers.
     *
     * @param mapRoute
     * @return
     */
    private String calculateMaxManeuver(MapRoute mapRoute) {
        int distance = 0;
        String viaRoad = "", lastRoadName= "";
        List<Maneuver> maneuvers = mapRoute.getRoute().getManeuvers();
        if (maneuvers.size() > 1) {
            for (int i = 0; i < maneuvers.size() - 1; i++) {
                // Getting longest distance out of all maneuvers
                int thisDistance = maneuvers.get(i).getDistanceToNextManeuver();
                // Saving lastRoadName which may not have longest distance but in case road name of expected maneuver
                // is blank then this road name would be used
                if (maneuvers.get(i).getRoadName() != null && !maneuvers.get(i).getRoadName().isEmpty())
                    lastRoadName = maneuvers.get(i).getRoadName();
                ExpLogger.logInfo("ViaRoad", maneuvers.get(i).getRoadName());
                if (distance <= thisDistance || viaRoad.isEmpty()) {
                    distance = thisDistance;
                    if(maneuvers.get(i).getRoadName() != null && !maneuvers.get(i).getRoadName().isEmpty())
                        viaRoad = maneuvers.get(i).getRoadName();
                }
            }
            if (viaRoad.isEmpty() && !lastRoadName.isEmpty())
                viaRoad = lastRoadName;
        }
        return viaRoad;
    }

    /**
     * gets user's current location reverse geo coded
     */
    @Override
    public void getReverseGeoCodedLocation() {
        Thread thread = new Thread() {
            @Override
            public void run() {

                try {
                    //checks if current location has not been reverse geo coded
                    if (MapsPresenter.getInstance().mReverseGeoCodedLocation == null && isViewValid())
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                //call to set current Lat Long till reverse geo coding completes
                                Location currentLocation = MainPresenter.getInstance().getCurrentLocation();
                                searchLocationView.get().showSourceLocation(currentLocation.getLatitude() + ", " + currentLocation.getLongitude() + mActivity.getResources().getString(R.string.my_location));
                            }
                        });

                    while (MapsPresenter.getInstance().mReverseGeoCodedLocation == null)
                        Thread.sleep(1000);

                    if (isViewValid())
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                //call to set reverse geo ooded location
                                searchLocationView.get().showSourceLocation(MapsPresenter.getInstance().mReverseGeoCodedLocation);
                            }
                        });

                } catch (Exception e) {
                    e.printStackTrace();
                    ExpLogger.logException(TAG, e);
                }

            }
        };
        thread.start();
    }

    /**
     * gets source location place name
     *
     * @return String place name
     */
    @Override
    public String getSourceLocationName() {
        return MapsStore.getInstance().getSourceLocation();
    }

    /**
     * gets destination location place name
     *
     * @return String place name
     */
    @Override
    public String getDestinationLocationName() {
        return MapsStore.getInstance().getDestinationLocation();
    }

    @Override
    public void setSavedLocations(String locationName, Location location) {
            MapsStore.getInstance().setDestinationLocation(locationName);
            MapsStore.getInstance().setDestinationLatitude(location.getLatitude());
            MapsStore.getInstance().setDestinationLongitude(location.getLongitude());
    }

    @Override
    public void getSavedLocations() {
        if (isViewValid() && mSavedLocationsList != null) {
            searchLocationView.get().showSavedLocations(mSavedLocationsList);
        }
    }

    @Override
    public void switchToMapsScreen() {
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    @Override
    public void getRecentLocations() {
        if (!isViewValid() || mRecentLocationsList == null)
            return;

        searchLocationView.get().showRecentLocations(mRecentLocationsList);
    }

    /**
     * call to reset the destination
     */
    @Override
    public void resetDestination() {
        // resets map route calculated
        MapsStore.getInstance().setMapRoute(null);

        //resets route summary for the calculated route
        MapsStore.getInstance().setRouteSummary(null);

        //resets source location name
        MapsStore.getInstance().setSourceLocation(null);

        //resets destination location name
        MapsStore.getInstance().setDestinationLocation(null);

        // resets destination location parameters
        MapsStore.getInstance().setDestinationLatitude(0);
        MapsStore.getInstance().setDestinationLongitude(0);

        //removes all the way points added
        removeAllWayPoints();
    }

    /**
     * Save location as recent location that the user has searched for.
     *
     * If already exist in list at index 0 then return.
     * If already exist in list at index other than 0 then remove it and insert at index 0.
     *
     * @param recentLocation
     */
    @Override
    public void saveRecentLocation(final RecentLocation recentLocation) {
        if (isViewValid()) {

            if (mRecentLocationsList == null)
                mRecentLocationsList = new ArrayList<>();

            // If recent location is already at index 0 then return
            if (mRecentLocationsList.size() > 0 && recentLocation.getName().equalsIgnoreCase(mRecentLocationsList.get(0).getName()))
                return;

            boolean locationExists = false;
            for (int i = 0; i < mRecentLocationsList.size(); i++) {
                if (recentLocation.getName().equalsIgnoreCase(mRecentLocationsList.get(i).getName()) && i > 0) {
                    mRecentLocationsList.remove(i);
                    locationExists = true;
                    break;
                }
            }

            // If current search key is not present, then remove last element
            if (!locationExists) {
                if (mRecentLocationsList.size() == MAX_RECENT_LOCATIONS) {
                    mRecentLocationsList.remove(MAX_RECENT_LOCATIONS - 1);
                }
            }
            mRecentLocationsList.add(0, recentLocation);
        }
    }

    public void persistRecentLocations(final Context context) {
        if (mRecentLocationsList == null)
            return;

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String recentLocationsText = new Gson().toJson(mRecentLocationsList);
                    SettingsUtils.writePreferenceValue(context, Constants.SharedPrefKeys.KEY_RECENT_LOCATIONS,
                            recentLocationsText);
                } catch (Exception e) {
                    ExpLogger.logException(TAG, e);
                }
            }
        });
        thread.start();
    }

    /**
     * call to set/unset as favorite for the place that user selected
     */
    @Override
    public void removePlaceAsFavorite(String placeId) {
        LocationsMgr.getLocationsMgr().deleteLocation(placeId);
        ExpLogger.logI("SearchLocationPresenter", "removePlaceAsFavorite: "+ placeId);
    }

    /**
     * call to set/unset as favorite for the place that user selected
     */
    @Override
    public void addPlaceAsFavorite(String placeId, String title, String alias, String lat, String lng) {
        if(alias.isEmpty()){
            alias = title;
        }

        LocationsMgr.getLocationsMgr().addFavouriteLocation(placeId, alias, title, lat, lng);
        ExpLogger.logI("SearchLocationPresenter", "addPlaceAsFavorite: "+ placeId);
    }

    /**
     * call to find coordinates for the place that user selected
     */
    @Override
    public void findPlaceCoordinates(String placeId, SearchLocationContract.LocationResolverListener locationResolverListener) {
        mLocationResolverListener = locationResolverListener;

        if (geoCodingProvider instanceof GoogleMaps)
            ((GoogleMaps) geoCodingProvider).findCoordinatesByPlaceId(placeId);
    }

    /**
     * sets the user selected place coordinates
     */
    @Override
    public void setPlaceCoordinates(ArrayList<Double> coordinates) {
        mCoordinatesList = coordinates;
        mLocationResolverListener.onLocationResolved(coordinates);
    }

    /**
     * gets the user selected place coordinates
     */
    @Override
    public ArrayList<Double> getPlaceCoordinates() {
        return mCoordinatesList;
    }

    /**
     * call to set the currently selected route index by swiping route summary
     * @param selectedRouteIndex: int route index to se route summary view pager item
     */
    @Override
    public void setSelectedRouteIndex(int selectedRouteIndex) {
        MapsStore.getInstance().setSelectedRouteIndex(selectedRouteIndex);
    }

    /**
     * gets route maneuvers info to be shown when user pulls route summary
     * @param maneuver: {@link Maneuver} instance
     * @param routeSummaryManeuver {@link RouteSummary.Maneuvers} instance
     */
    @Override
    public void getManeuversInfo(Maneuver maneuver, RouteSummary.Maneuvers routeSummaryManeuver) {
        Maneuver.Icon icon = maneuver.getIcon();
        String imageName = "maneuver_icon_" + icon.value();
        int turnImage = mActivity.getResources().getIdentifier(imageName,
                "drawable", mActivity.getPackageName());
        routeSummaryManeuver.setTurnImage(turnImage);
        if(maneuver.getRoadName() != null && !TextUtils.isEmpty(maneuver.getRoadName()))
            routeSummaryManeuver.setTurnName(getTurnName(maneuver) + " onto " + maneuver.getRoadName());
        else
            routeSummaryManeuver.setTurnName(getTurnName(maneuver));
        routeSummaryManeuver.setDistance(computeDistance(maneuver.getDistanceToNextManeuver()));
    }

    /**
     * gets turn name from maneuver
     * @param maneuver: {@link Maneuver} instance
     * @return String turn name for the given maneuver
     */
    private String getTurnName(Maneuver maneuver){
        String turnName = "";
        if (maneuver != null && maneuver.getIcon() != null)
            turnName = Constants.Turns[maneuver.getIcon().value()];
        return turnName;
    }

    /**
     * formats distance from current maneuver to next maneuver
     * @param distance: distance to next maneuver
     * @return String formatted distance with unit
     */
    private String computeDistance(long distance) {
        // SDK returns Integer.MAX_VALUE if an error occurred or if the next maneuver is not available yet
        // Valid when value is less than Integer.MAX_VALUE

        if (distance < Integer.MAX_VALUE) {
            //checks if distance is greater than 0.1 mile or Km then show distance in miles or Kms else feet or meter

            StringBuilder formattedDistance = new StringBuilder();
            if(distance > 160.934){
//                if(presenter.isLocaleUnitKms())
                    formattedDistance.append(new DecimalFormat("#.0").format(Utils.meterToKms(distance))).append(unitInKms);
//                else
//                    formattedDistance.append(new DecimalFormat("#.0").format(Utils.meterToMiles(distance))).append(unitInMiles);
            }else {
//                if (presenter.isLocaleUnitKms())
                    formattedDistance.append(Integer.toString((int)distance)).append(unitInMeters);
//                else {
//                    long distanceInFeet = (long) Utils.meterToFeet(distance);
//                    formattedDistance.append(distanceInFeet).append(unitInFeet);
//                }
            }

            return formattedDistance.toString();

        }else
            return "";
    }

    /**
     * sorts Array List based on the ETA
     * @param routeSummaries: Route Summary list for the calculated routes
     */
    @Override
    public void sortRoutesList(ArrayList<RouteSummary> routeSummaries) {
        Collections.sort(routeSummaries, new Comparator<RouteSummary>() {
            @Override
            public int compare(RouteSummary routeSummary1, RouteSummary routeSummary2) {
                return routeSummary1.getDuration() - routeSummary2.getDuration();
            }
        });


    }

    @Override
    public void onLocationsUpdated(List<ExpDocument> locations) {
        try {
            if(locations == null)
                return;

            if(mSavedLocationsList == null)
                mSavedLocationsList = new ArrayList<>();
            else
                mSavedLocationsList.clear();

            mSavedLocationsList.addAll(locations);
            if (isViewValid())
                searchLocationView.get().showSavedLocations(mSavedLocationsList);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    public boolean isFavoriteLocation(String placeId) {
        if(mSavedLocationsList == null)
            return false;

        for (int i = 0; i < mSavedLocationsList.size(); i++) {
            com.cloudant.exploridemobile.Location location = (com.cloudant.exploridemobile.Location) mSavedLocationsList.get(i);
            if (location == null || location.getPlaceId() == null || location.getPlaceId().isEmpty())
                continue;

            if (location.getPlaceId().equalsIgnoreCase(placeId))
                return true;
        }
        return false;
    }
}