package com.exploride.droid.presenters;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.MapsContract;
import com.exploride.droid.PresenterManager;
import com.exploride.droid.R;
import com.exploride.droid.location.ReverseGeocodeListener;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.MapsStore;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.views.fragments.BaseFragment;
import com.exploride.droid.views.fragments.MapsDestinationFragment;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest2;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Author Mayank
 */
public class MapsPresenter implements MapsContract.MapsOps {

    static MapsPresenter mMapsPresenter;
    Handler mHandler = new Handler(Looper.getMainLooper());
    private Activity mActivity;
    private String TAG = MapsPresenter.class.getName();
    private WeakReference<MapsContract.MapsView> mMapsView;
    String mReverseGeoCodedLocation;
    public Location mCurrentSourceLocation, mDestinationLocation;
    public boolean mNavigationStarted = false, mRouteDrawn = false;
    private String mLocationType = Constants.LocationType.SOURCE;
    private String mSourceTitle, mDestinationTitle;

    public static MapsPresenter getInstance() {
        if (mMapsPresenter == null) {
            mMapsPresenter = new MapsPresenter();
        }
        return mMapsPresenter;
    }

    @Override
    public void init(MapsContract.MapsView view) {
        mActivity = ((BaseFragment) view).getActivity();
        if (this.mMapsView != null)
            return;
        this.mMapsView = new WeakReference<>(view);

    }

    public boolean isViewValid() {

        if (mMapsView == null || mMapsView.get() == null)
            return false;
        return true;
    }

    @Override
    public void onStartView() {
        PresenterManager.getInstance().addActivePresenter(this);
    }

    @Override
    public void onStopView() {
        PresenterManager.getInstance().removeActivePresenter(this);
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {
        mMapsView = null;
    }

    /**
     * gets source location
     *
     * @return source location
     */
    @Override
    public Location getSourceLocation() {

        Location sourceLocation = new Location("");
        sourceLocation.setLatitude(MapsStore.getInstance().getSourceLatitude());
        sourceLocation.setLongitude(MapsStore.getInstance().getSourceLongitude());
        return sourceLocation;
    }

    /**
     * sets source location selected by the user
     * @param location : source location
     */
    @Override
    public void setSourceLocation(Location location) {
        // set source location parameters
        MapsStore.getInstance().setSourceLatitude(location.getLatitude());
        MapsStore.getInstance().setSourceLongitude(location.getLongitude());
    }

    /**
     * gets destination location selected by the user
     *
     * @return destination location
     */
    @Override
    public Location getDestinationLocation() {
        Location destinationLocation = new Location("");
        destinationLocation.setLatitude(MapsStore.getInstance().getDestinationLatitude());
        destinationLocation.setLongitude(MapsStore.getInstance().getDestinationLongitude());
        return destinationLocation;
    }

    /**
     * callback for reverse geo coded current location
     *
     * @param reverseGeoCodedLocation: user's current location
     */
    @Override
    public void onLocationReverseGeoCoded(final String reverseGeoCodedLocation, final GeoCoordinate geoCoordinate) {

        if (mLocationType.equalsIgnoreCase(Constants.LocationType.SOURCE))
            mReverseGeoCodedLocation = reverseGeoCodedLocation;
        if (isViewValid()) {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    String formattedAddress = reverseGeoCodedLocation;
                    if (formattedAddress != null && !formattedAddress.isEmpty()) {
                        if (formattedAddress.contains("\n"))
                            formattedAddress = formattedAddress.replaceAll("\n", ",");
                        formattedAddress = formattedAddress.trim();

                        if (isViewValid()) {
                            if (mLocationType.equalsIgnoreCase(Constants.LocationType.SOURCE)) {
                                mSourceTitle = formattedAddress;
                                mMapsView.get().showSourceLocation(formattedAddress + mActivity.getResources().getString(R.string.my_location));
                            } else {
                                MainPresenter.getInstance().hideLoading();
                                destinationGeocodeCompleted(formattedAddress, geoCoordinate);
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onReverseGeoCodingFailed(GeoCoordinate geoCoordinate) {
        if(mLocationType.equalsIgnoreCase(Constants.LocationType.DESTINATION)) {
            MainPresenter.getInstance().hideLoading();
            String destinationLocation = geoCoordinate.getLatitude() + "," + geoCoordinate.getLongitude();
            destinationGeocodeCompleted(destinationLocation, geoCoordinate);
        }
    }

    /**
     * When destination location is geocoded, set details in Store and launch SearchLocationFragment
     * If reverse geocoding failed, set lat, long
     *
     * @param locationName
     * @param geoCoordinate
     */
    private void destinationGeocodeCompleted(String locationName, GeoCoordinate geoCoordinate){
        if (mDestinationLocation == null)
            mDestinationLocation = new Location("");
        mDestinationLocation.setLatitude(geoCoordinate.getLatitude());
        mDestinationLocation.setLongitude(geoCoordinate.getLongitude());
        mDestinationTitle = locationName;

        mMapsView.get().showDestinationLocation(locationName);
    }

    /**
     * call to reverse geo code user's current location
     */
    @Override
    public void requestCurrentLocToGeocode() {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                Location currentLocation = MainPresenter.getInstance().getCurrentLocation();

                while (currentLocation == null) {
                    try {
                        Thread.sleep(1000);

                        currentLocation = MainPresenter.getInstance().getCurrentLocation();
                    } catch (Exception e) {
                        e.printStackTrace();
                        ExpLogger.logException(TAG, e);
                    }
                }

                //sets current location as source
                mCurrentSourceLocation = currentLocation;

                if (isViewValid()) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (isViewValid()) {
                                //call to show if location has already been reverse geo coded
                                if (mReverseGeoCodedLocation != null && !TextUtils.isEmpty(mReverseGeoCodedLocation))
                                    mMapsView.get().showSourceLocation(mReverseGeoCodedLocation + mActivity.getResources().getString(R.string.my_location));
                                else//call to set current Lat Long till reverse geo coding completes
                                    mMapsView.get().showSourceLocation(mCurrentSourceLocation.getLatitude() + ", " + mCurrentSourceLocation.getLongitude() + mActivity.getResources().getString(R.string.my_location));
                            }
                        }
                    });

                    reverseGeoCodeLocation(currentLocation, Constants.LocationType.SOURCE);
                }

            }
        });
        t.start();

    }

    @Override
    public void reverseGeoCodeLocation(final Location selectedLocation, final String locationType) {
        //checks if map is not initialized then reverse geo coding won't be performed
        if(!mMapsView.get().isMapInitialized())
            return;

        mLocationType = locationType;

        try {
            if (isViewValid() && locationType.equalsIgnoreCase(Constants.LocationType.DESTINATION)) {
                ((MapsDestinationFragment) mMapsView.get()).getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainPresenter.getInstance().showLoading(((MapsDestinationFragment) mMapsView.get())
                                .getString(R.string.please_wait));
                    }
                });

            }
            // Instantiate a Navigation object
            GeoCoordinate vancouver = new GeoCoordinate(selectedLocation.getLatitude(), selectedLocation.getLongitude());

            // Example code for creating ReverseGeocodeRequest2
            ResultListener<com.here.android.mpa.search.Location> listener = new ReverseGeocodeListener();
            ReverseGeocodeRequest2 request = new ReverseGeocodeRequest2(vancouver);

            if (request.execute(listener) != ErrorCode.NONE) {
                // Handle request error
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * gets source location place name
     * @return String place name
     */
    @Override
    public String getSourceLocationName() {
        return MapsStore.getInstance().getSourceLocation();
    }

    /**
     * gets destination location place name
     * @return String place name
     */
    @Override
    public String getDestinationLocationName() {
        return MapsStore.getInstance().getDestinationLocation();
    }

    /**
     * gets route summary
     * @return RouteSummary summary for the rote
     */
    @Override
    public ArrayList<RouteSummary> getRouteSummary() {
        return MapsStore.getInstance().getRouteSummary();
    }

    /**
     * gets map route
     * @return MapRoute to be rendered on the map
     */
    @Override
    public MapRoute getMapRoute() {
        return MapsStore.getInstance().getMapRoute();
    }

    /**
     * sets summary for the route to be shown to the user
     * @param routeSummaries : summary for routes like eta, via point, distance
     */
    @Override
    public void setRouteSummary(ArrayList<RouteSummary> routeSummaries) {
        //sets route summary
        MapsStore.getInstance().setRouteSummary(routeSummaries);
    }

    /**
     * sets map route selected by the user
     * @param mapRoute: route from source to destination
     */
    @Override
    public void setMapRoute(MapRoute mapRoute) {
        //sets map route selected by the user
        MapsStore.getInstance().setMapRoute(mapRoute);
    }

    @Override
    public void resetNavigation() {
        setNavigationStarted(false);

        // resets previous map route calculated
        MapsStore.getInstance().setMapRoute(null);

        //resets route summary for the calculated route
        MapsStore.getInstance().setRouteSummary(null);

        //resets source location
        resetSourceLocationName();
        MapsStore.getInstance().setSourceLatitude(0);
        MapsStore.getInstance().setSourceLongitude(0);

        //resets destination location
        resetDestinationLocationName();
        MapsStore.getInstance().setDestinationLatitude(0);
        MapsStore.getInstance().setDestinationLongitude(0);

        //resets the route index for the calculated route
        setSelectedRouteIndex(-1);

        //call to remove way points
        removeWayPoints();

        //request user's current location reverse geo coded to be shown in the source field in the UI
        requestCurrentLocToGeocode();



        if(isViewValid()){
            mMapsView.get().resetNavigation();
        }
    }

    @Override
    public void setNavigationStarted(boolean navigationStarted) {
        mNavigationStarted = navigationStarted;
    }

    @Override
    public boolean isNavigationStarted() {
        return mNavigationStarted;
    }

    @Override
    public void setRouteDrawn(boolean routeDrawn) {
        mRouteDrawn = routeDrawn;
    }

    @Override
    public boolean isRouteDrawn() {
        return mRouteDrawn;
    }

    /**
     * resets source location name
     */
    @Override
    public void resetSourceLocationName() {
        MapsStore.getInstance().setSourceLocation(null);
    }

    /**
     * resets destination location name
     */
    @Override
    public void resetDestinationLocationName() {
        MapsStore.getInstance().setDestinationLocation(null);
    }

    /**
     * call to remove the way point added by the user
     */
    @Override
    public void removeWayPoints() {
        SearchLocationPresenter.getInstance().removeAllWayPoints();
    }

    @Override
    public void getRouteForLocationsSel() {
        try {
            if (mCurrentSourceLocation != null && mDestinationLocation != null) {
                MapsStore.getInstance().setSourceLocation(mSourceTitle);
                MapsStore.getInstance().setSourceLatitude(mCurrentSourceLocation.getLatitude());
                MapsStore.getInstance().setSourceLongitude(mCurrentSourceLocation.getLongitude());

                MapsStore.getInstance().setDestinationLocation(mDestinationTitle);
                MapsStore.getInstance().setDestinationLatitude(mDestinationLocation.getLatitude());
                MapsStore.getInstance().setDestinationLongitude(mDestinationLocation.getLongitude());

                Bundle bundle = new Bundle();
                bundle.putBoolean(Constants.IntentExtraKeys.DEST_SELECTED_ON_MAP, true);
                MainPresenter.getInstance().loadFragment(Constants.Fragments.SearchLocationFragment, bundle);
            }
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    /**
     * call to update the views on Route Calculation error occurred on HUD
     */
    @Override
    public void onRouteCalculationError() {
        setNavigationStarted(false);
        if (isViewValid())
            //updates the view on route calculation error on HUD
            mMapsView.get().updateViewsOnRouteCalculationError();

    }

    /**
     * syncs HUD navigation on mobile app
     * @param destination: destination name
     * @param destinationLatitude: destination latitude
     * @param destinationLongitude: destination longitude
     */
    @Override
    public void syncHudNavigationState(String destination, double destinationLatitude, double destinationLongitude) {
        if (MapsStore.getInstance().getDestinationLatitude() == destinationLatitude ||
                MapsStore.getInstance().getDestinationLongitude() == destinationLongitude)
            return;

        mNavigationStarted = true;

        MapsStore.getInstance().setDestinationLocation(destination);
        MapsStore.getInstance().setDestinationLatitude(destinationLatitude);
        MapsStore.getInstance().setDestinationLongitude(destinationLongitude);

        if (isViewValid())
            mMapsView.get().updateNavigationState();
    }

    /** call to get currently selected route index
     * @return: int index of route summary view pager
     */
    @Override
    public int getSelectedRouteIndex() {
        return MapsStore.getInstance().getSelectedRouteIndex();
    }

    /**
     * call to set the currently selected route index by swiping route summary
     * @param selectedRouteIndex: int route index to se route summary view pager item
     */
    @Override
    public void setSelectedRouteIndex(int selectedRouteIndex) {
        MapsStore.getInstance().setSelectedRouteIndex(selectedRouteIndex);
    }
}
