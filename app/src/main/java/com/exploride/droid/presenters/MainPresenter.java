package com.exploride.droid.presenters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.cloudant.exploridemobile.SettingsMgr;
import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.AppVersion;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.DevOptions;
import com.cloudant.exploridemobile.framework.DownloadMaps;
import com.cloudant.exploridemobile.framework.Navigation;
import com.cloudant.exploridemobile.framework.PhoneInfo;
import com.cloudant.exploridemobile.framework.RequestDataOffline;
import com.cloudant.exploridemobile.framework.ScreenScaling;
import com.cloudant.exploridemobile.framework.SyncPreferences;
import com.cloudant.exploridemobile.framework.WifiConnect;
import com.exploride.droid.MainApplication;
import com.exploride.droid.MainContract;
import com.exploride.droid.R;
import com.exploride.droid.bluetooth.BluetoothHandler;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.location.GPSLocationListener;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.MainStore;
import com.exploride.droid.services.BluetoothClientService;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.Utils;
import com.exploride.droid.views.activities.MainActivity;
import com.exploride.droid.views.fragments.DeveloperOptionsFragment;
import com.exploride.droid.views.fragments.ReachedLocationFragment;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * class to handle Main Activities
 */
public class MainPresenter implements MainContract.MainOps {

    private Activity activity;
    private static final String TAG = "MainPresenter";
    private WeakReference<MainContract.MainView> mainView;
    static MainPresenter mMainPresenter;
    String mWifiName = null;

    private Location mCurrentLocation = null;

    //provides gps location updates
    private GPSLocationListener gpsLocationListener;

    //Navigation request permission code
    private static final int MY_PERMISSIONS_REQUEST_LOC = 30;

    private long exactTime;
    private boolean settingSynced = false;

    private boolean mShowRouteOnly = true;
    private PhoneInfo mPhoneInfo;
    private String scName = Constants.SC_NAME;

    public static MainPresenter getInstance() {
        if (mMainPresenter == null) {
            mMainPresenter = new MainPresenter();
        }
        return mMainPresenter;
    }

    @Override
    public void init(MainContract.MainView view) {

        this.mainView = new WeakReference<MainContract.MainView>(view);

        activity = ((MainActivity) view);

        //init MainStore
        MainStore.getInstance();
        startLocationListener();
    }

    public boolean isViewValid() {
        return mainView != null && mainView.get() != null;
    }
    public void setWifiName(String wifiName){
        if(wifiName!=null && wifiName.equals(""))
            wifiName = null;
        mWifiName = wifiName;
        if(isViewValid()) {
            mainView.get().updateFragments();
            MainPresenter.getInstance().updateNavigationView();
        }
    }
    public String getWifiName(){
        return mWifiName;
    }

    public void showDialogForPassword(final String ssid){
        if(!isViewValid()) {
            ExpLogger.logException(TAG, new Exception("cannot show Password Dialog, no view valid"));
            return;
        }
        final EditText input = new EditText(MainApplication.getInstance().getCurrentActivity());
        input.setTextColor(Color.BLACK);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainApplication.getInstance().getCurrentActivity());
        alertDialog.setTitle("PASSWORD");
        alertDialog.setMessage("Enter Password for "+ssid);
        alertDialog.setIcon(R.drawable.mr_ic_pause_dark).setCancelable(false);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String password = input.getText().toString();
                        BTComm btComm = new BTComm();
                        WifiConnect wifiConnect = new WifiConnect();
                        wifiConnect.setSSID(ssid);
                        wifiConnect.setPwd(password);
                        btComm.setWifiConnect(wifiConnect);
                        ConnectionHandler.getInstance().sendInfo(btComm);

                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        MainPresenter.getInstance().hideLoading();
                    }
                });
        MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog.show();
            }
        });

    }
    public interface DialogCallback{

        void onUserOK();
        void onUserCancel();

    }
    public void showDialog(final String title, final String message, final DialogCallback dialogCallback, boolean hasCancelButton ){
        if(!isViewValid()) {
            ExpLogger.logException(TAG, new Exception("cannot show Dialog, no view valid"));
            return;
        }
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainApplication.getInstance().getCurrentActivity());
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon(R.drawable.mr_ic_pause_dark).setCancelable(false);

        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialogCallback.onUserOK();
                    }
                });

        if(hasCancelButton)
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            dialogCallback.onUserCancel();
                        }
                    });
        MainApplication.getInstance().getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog.show();
            }
        });

    }

    public String getWifiStatusText(){

        String wifiName = MainPresenter.getInstance().getWifiName();
        String statusText = wifiName==null?"Wifi not connected":"Wifi: "+wifiName;
        return statusText;
    }
    public void loadFragment(String fragName, Bundle args) {
        if (fragName != null)
            mainView.get().loadFragment(fragName, args);
    }

    @Override
    public void onStartView() {

        //call to location listener to start location updates when activity gets started
        if (gpsLocationListener != null) {
            gpsLocationListener.onStart();
        }

        // checks whether location is on or not
        locationChecker(gpsLocationListener.getGoogleApiClient(), activity);

    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {

    }

    @Override
    public void onStopView() {

        if (isViewValid()) {
            Context context = activity;
            // Save recent searched locations
            SearchLocationPresenter.getInstance().persistRecentLocations(context);
        }
        //call to stop location updates when activity gets stopped
        if (gpsLocationListener != null) {
            gpsLocationListener.onStop();
        }

    }

    /**
     * sets current location passed by @class {@link com.here.android.mpa.guidance.NavigationManager.GpsSignalListener}
     * @param currentLocation: user's current location
     */
    @Override
    public void setCurrentLocation(Location currentLocation) {

        this.mCurrentLocation = currentLocation;

    }

    /**
     * checks whether user has granted location permission or not
     */
    public void startLocationListener() {
        if (isViewValid()) {

            //get permission for Android M
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                fetchLocation();
            } else {

                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(activity,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(activity,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOC);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                } else {
                    fetchLocation();
                }
            }

        }
    }

    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    /**
     * call to get user's current location updates
     */
    private void fetchLocation() {

        gpsLocationListener = new GPSLocationListener(activity);
    }

    @Override
    public void setBluetoothState(int bluetoothState) {
        if (isViewValid()) {
            if (bluetoothState == BluetoothClientService.STATE_CONNECTED)
                mainView.get().showBluetoothState(true);
            else
                mainView.get().showBluetoothState(false);
        }
    }

    public void updateNavigationView(){
        if (isViewValid()){
            mainView.get().updateNavigationViews();
        }

    }

    /**
     * checks whether location is on or not. If not then shows user a prompt to grant location permission
     * @param googleApiClient: Google Api Client used to fetch location
     * @param activity: current activity's context
     */
    public void locationChecker(GoogleApiClient googleApiClient, final Activity activity) {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                                     @Override
                                     public void onResult(LocationSettingsResult result) {
                                         final Status status = result.getStatus();
                                         final LocationSettingsStates state = result.getLocationSettingsStates();
                                         switch (status.getStatusCode()) {
                                             case LocationSettingsStatusCodes.SUCCESS:
                                                 // All location settings are satisfied. The client can initialize location
                                                 // requests here.
                                                 break;
                                             case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                                 // Navigation settings are not satisfied. But could be fixed by showing the user
                                                 // a dialog.
                                                 try {

                                                     // Show the dialog by calling startResolutionForResult(),
                                                     // and check the result in onActivityResult().
                                                     status.startResolutionForResult(
                                                             activity, 1000);
                                                 } catch (IntentSender.SendIntentException e) {
                                                     // Ignore the error.
                                                 }
                                                 break;
                                             case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                                 // Navigation settings are not satisfied. However, we have no way to fix the
                                                 // settings so we won't show the dialog.
                                                 break;
                                         }
                                     }
                                 }


        );
    }

    /**
     * set download maps information received from HUD app
     * @param downloadMaps: {@link DownloadMaps} class instance holding the information sent by HUD
     */
    @Override
    public void setDownloadMapsInformation(DownloadMaps downloadMaps) {

        //checks if HUD sent a progress for Map Package that's downloading
        if (downloadMaps.isInstall()) {
            //call to set Map Package install progress on HUD
            DownloadMapsPresenter.getInstance().onMapInstallProgress(downloadMaps.getProgress());
            return;
        }

        //checks if HUD sent the Installed Map Package Id list
        if (downloadMaps.getMapPackageIdList() != null) {
            DownloadMapsPresenter.getInstance().onGetMapPackagesId(downloadMaps.getMapPackageIdList(), downloadMaps.isInstalledMapPackageId());
        }

    }

    /**
     * sets HUD brightness value
     * @param value: brightness value (range 0-255)
     */
    @Override
    public void setHudBrightness(int value) {
        MainStore.getInstance().setHudBrightness(value);
    }

    /**
     * sets automatic brightness mode is on fo HUD
     */
    @Override
    public void setHudAutomaticBrightnessOn(boolean automaticBrightnessOn) {
        MainStore.getInstance().setHudAutomaticBrightnessOn(automaticBrightnessOn);
    }

    /**
     * gets Hud Automatic brightness mode status
     * @return boolean true if automatic brightness mode is on for HUD else false
     */
    @Override
    public boolean getHudAutomaticBrightnessOn() {
        return MainStore.getInstance().getHudAutomaticBrightnessOn();
    }

    @Override
    public void showLoading(String text) {
        if(isViewValid())
            mainView.get().showLoading(text);
    }

    @Override
    public void hideLoading() {
        if(isViewValid())
            mainView.get().hideLoading();
    }

    @Override
    public void sendSyncPrefsToHUD() {
        try {
            if (BluetoothHandler.getInstance() != null) {
                if (!settingSynced && BluetoothHandler.getInstance().getBluetoothClientService() != null) {
                    if (BluetoothHandler.getInstance().getBluetoothClientService().getState() == BluetoothClientService.STATE_CONNECTED) {
                        if (SettingsMgr.getSettings() != null && SettingsMgr.getSettings().getExpSettings() != null
                                && SettingsMgr.getSettings().getExpSettings().getSyncPreferences() != null) {
                            final SyncPreferences syncPreferences = SettingsMgr.getSettings().getExpSettings().getSyncPreferences();

                            syncPreferences.getVoicePref().setIsVoiceOnline(SettingsUtils.readPreferenceValueAsBoolean(
                                    MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_VOICE_ONLINE, true));
                            syncPreferences.getOfflineNavigation().setOffline(SettingsUtils.readPreferenceValueAsBoolean(
                                    MainApplication.getInstance(), Constants.SharedPrefKeys.KEY_ENABLE_OFFLINE_NAVIGATION,
                                    false));
                            BTComm btComm = new BTComm();
                            btComm.setSyncPreferences(syncPreferences);
                            ConnectionHandler.getInstance().sendInfo(btComm);
                            settingSynced = true;
                        }
                    }
                }
            }
        }catch (Exception e){
            ExpLogger.logException(TAG, e);
        }

    }

    /**
     * Get device timezone and set to btcomm object. Called when HUD connected and timezone changed.
     */
    @Override
    public void getDeviceTimezone(BTComm btComm){
        TimeZone tz = TimeZone.getDefault();
        Calendar calendar = Calendar.getInstance();
        com.cloudant.exploridemobile.framework.TimeZone timeZone = new com.cloudant.exploridemobile.framework.TimeZone();
        timeZone.setTimeZoneId(tz.getID());
        timeZone.setTimeMillis("" + calendar.getTimeInMillis());
        ExpLogger.logInfo("Time set", "Timezone: " + tz.getID() + ", time:" + calendar.getTime() + ", time millis"
                + calendar.getTimeInMillis());
        btComm.setTimeZone(timeZone);
    }
    public void setPhoneInfo(String btName){
        if(mPhoneInfo == null)
        mPhoneInfo = new PhoneInfo();

        mPhoneInfo.setBtName(btName);
        if(MainApplication.getInstance().isCommunicationChannelOpen())
            sendBtNameToHUD();
    }

    public void sendBtNameToHUD(){
        if(mPhoneInfo == null)
            return;

        try {
            BTComm btComm = new BTComm();
            btComm.setPhoneInfo(mPhoneInfo);
            ConnectionHandler.getInstance().sendInfo(btComm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setDevOptions(BTComm btComm) {
        if(isViewValid()) {
            getDeviceTimezone(btComm);

            DevOptions devOptions = new DevOptions();
            if (SettingsUtils.readPreferenceValueAsInt(activity,
                    Constants.SharedPrefKeys.KEY_DEVOPTIONS_TILT_LEVEL) == -1) {
                devOptions.setTiltLevel(Constants.DEFAULT_TILT);
            }
            if (SettingsUtils.readPreferenceValueAsInt(activity,
                    Constants.SharedPrefKeys.KEY_DEVOPTIONS_ZOOM_LEVEL) == -1) {
                devOptions.setZoomLevel(Constants.DEFAULT_ZOOM);
            }

            ScreenScaling screenScaling = new ScreenScaling();
            if (SettingsUtils.readPreferenceValueAsInt(activity,
                    Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_Y_OFFSET) == -1) {
                screenScaling.setOffsetY("" + Constants.DEFAULT_Y_OFFSET);
            }
            else
                screenScaling.setOffsetY(DeveloperOptionsFragment.offsetRange[SettingsUtils.
                        readPreferenceValueAsInt(activity,
                        Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_Y_OFFSET)]);

            if (SettingsUtils.readPreferenceValueAsInt(activity,
                    Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_SCREEN_SCALE) == -1) {
                screenScaling.setScaleRate("" + Constants.DEFAULT_SCALE);
            }
            else
                screenScaling.setScaleRate(DeveloperOptionsFragment.screenScaleRange[SettingsUtils.
                        readPreferenceValueAsInt(activity,
                        Constants.SharedPrefKeys.KEY_DEVOPTIONS_PHONE_SCREEN_SCALE)]);

            btComm.setScreenScaling(screenScaling);

            DevOptions.TopMapView topMapView = devOptions.new TopMapView();
            topMapView.setEnable(SettingsUtils.readPreferenceValueAsBoolean(activity,
                    Constants.KEY_BASE + Constants.KEY_TOP_NAVIGATION_VIEW_HUD));
            devOptions.setTopView(topMapView);

            DevOptions.AllowMockLocation allowMockLocation = devOptions.new AllowMockLocation();
            allowMockLocation.setEnable(SettingsUtils.readPreferenceValueAsBoolean(activity,
                    Constants.SharedPrefKeys.KEY_DEVOPTIONS_ALLOW_MOCK));
            devOptions.setAllowMockLocations(allowMockLocation);

            DevOptions.SCType scType = devOptions.new SCType();
            scType.setBluClick(SettingsUtils.readPreferenceValueAsBoolean(activity,
                    Constants.SharedPrefKeys.KEY_SC_TYPE));
            devOptions.setScType(scType);

            btComm.setActionCode(ActionConstants.ActionCode.LOCATION_SRC_REQ);

            btComm.setDevOptions(devOptions);
        }
    }

    @Override
    public void provideOfflineTimeInfo() {
        BTComm btComm = new BTComm();

        TimeZone tz = TimeZone.getDefault();
        com.cloudant.exploridemobile.framework.TimeZone timeZone = new com.cloudant.exploridemobile.framework.TimeZone();
        timeZone.setTimeZoneId(tz.getID());

        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        timeZone.setTimeMillis("" + timeInMillis);

        btComm.setTimeZone(timeZone);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }


    @Override
    public void provideOfflineWeatherInfo() {
        //call to fetch weather info to be passed to the HUD
        Utils.getWeatherInfo(activity,
                mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
    }

    @Override
    public void handleOfflineDataRequest(RequestDataOffline requestDataOffline) {
        if (requestDataOffline.isRequestWeather())
            provideOfflineWeatherInfo();
        if (requestDataOffline.isRequestTime())
            provideOfflineTimeInfo();
    }

    /**
     * gets HUD brightness value
     * @return int brightness value (range 0-255)
     */
    @Override
    public int getHudBrightness() {
        return MainStore.getInstance().getHudBrightness();
    }

    /**
     * notifies {@link MapsPresenter} class for the route calculation occurred on HUD
     */
    @Override
    public void notifyRouteCalculationError() {
        MapsPresenter.getInstance().onRouteCalculationError();
    }

    /** sets the HUD app version to {@link AppVersionPresenter} to be shown to the user
     * gets google api client from {@link GPSLocationListener}
     * @return GoogleApiClient instance
     */
    @Override
    public GoogleApiClient getGoogleApiClient() {
        return gpsLocationListener != null ? gpsLocationListener.getGoogleApiClient() : null;
    }

     /** sets the HUD app version to {@link AppVersionPresenter} to be shown to the user
     * @param hudAppVersion: HUD app version info
     */
    @Override
    public void setHudAppVersion(AppVersion hudAppVersion) {
        AppVersionPresenter.getInstance().showHudAppVersion(hudAppVersion.getVersionName());
    }

    /**
     * call to sync HUD navigation on mobile app
     * @param status: {@link com.cloudant.exploridemobile.framework.Navigation.Status} instance containing navigation info
     */
    @Override
    public void syncHudNavigationState(Navigation.Status status) {
        MapsPresenter.getInstance().syncHudNavigationState(status.getDestination(), status.getDestLat(), status.getDestLng());
    }

    public boolean assertBGThread(){

        if(Looper.getMainLooper()==Looper.myLooper()) {
            Exception e = new Exception("Called from main thread");
            ExpLogger.logException(TAG, e);
            return true;
        }
        return false;
    }

    public boolean isBGThread(){

        if(Looper.getMainLooper()==Looper.myLooper()) {
            Exception e = new Exception("Called from main thread");
            ExpLogger.logException(TAG, e);
            return false;
        }
        return true;
    }

    /**
     * notifies user that Map Init Failed on HUD
     */
    public void notifyMapInitFailed(){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(isViewValid())
                    mainView.get().showErrorAlert(activity.getResources().getString(R.string.map_init_error_on_hud));

                //call to notify that route couldn't be calculated at HUD end since Map Initialization failed
                notifyRouteCalculationError();
            }
        });

    }

    /**
     * call to HUD to check the navigation state at HUD
     */
    @Override
    public void checkNavigationStateOnHUD() {
        BTComm btComm = new BTComm();
        btComm.setActionCode(ActionConstants.ActionCode.CHECK_NAVIGATION_STATE);
        ConnectionHandler.getInstance().sendInfo(btComm);

    }

    @Override
    public boolean isShowRouteOnly() {
        return mShowRouteOnly;
    }

    public void handleShowRouteOnly(boolean showRouteOnly){
        mShowRouteOnly = showRouteOnly;

        BTComm btComm = new BTComm();
        DevOptions devOptions = new DevOptions();
        DevOptions.RouteOnlyMode routeOnlyMode = devOptions.new RouteOnlyMode();
        routeOnlyMode.setRouteOnlyModeEnabled(mShowRouteOnly);
        devOptions.setRouteOnlyMode(routeOnlyMode);
        btComm.setDevOptions(devOptions);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    /**
     * checks if Post trip summary is shown
     * @return boolean true is post trip summary is shown else false
     */
    public boolean isPostTripSummaryShown(){
        if (isViewValid())
            return mainView.get().getActiveFragment() instanceof ReachedLocationFragment;

        return false;
    }

    public String getStringResource(int resId) {
        return activity.getResources().getString(resId);
    }

    public String getScName() {
        return scName != null ? scName : Constants.SC_NAME;
    }

    public void setScName(String scName) {
        if (scName != null)
            this.scName = scName;
    }
}
