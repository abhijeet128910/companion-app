package com.exploride.droid.presenters;

import com.cloudant.exploridemobile.ExpDocument;
import com.cloudant.exploridemobile.Location;
import com.cloudant.exploridemobile.LocationsMgr;
import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.PresenterManager;
import com.exploride.droid.ReachedLocationContract;
import com.exploride.droid.model.SavedTripCollection;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.DateFormatter;

import java.util.List;

/**
 * Created by Abhijeet on 11/15/2016.
 */

public class ReachedLocationPresenter implements ReachedLocationContract.ReachedLocationOps {

    private Navigation mNavigation;
    static ReachedLocationPresenter reachedLocationPresenter;
    private ReachedLocationContract.ReachedLocationView reachedLocationView;


    public static ReachedLocationPresenter getInstance() {
        if (reachedLocationPresenter == null) {
            reachedLocationPresenter = new ReachedLocationPresenter();
        }
        return reachedLocationPresenter;
    }

    /**
     * To accept trip summary when destination is reached. Called from Main Activity.
     *
     * @param navigation
     */
    public void setNavigationCompleted(Navigation navigation) {
        mNavigation = navigation;
        if (navigation.getDestinationReached() != null) {
            if (isViewValid())
                reachedLocationView.showNavigationDetails(navigation);
            else {
                MainPresenter.getInstance().loadFragment(Constants.Fragments.ReachedLocationFragment, null);
            }
        }
    }

    @Override
    public Navigation getNavigationDetails() {
        return mNavigation;
    }

    @Override
    public void saveTrip(String tripLabel) {
        SavedTripCollection savedTripCollection = SavedTripsPresenter.getInstance().getSavedTripCollection();
        if (savedTripCollection.getTrips() != null) {
            List<Navigation> trips = SavedTripsPresenter.getInstance().getSavedTripCollection().getTrips();
            for (Navigation navigation : trips) {
                if (navigation != null && navigation.getStatus().getDescription() != null &&
                        navigation.getStatus().getDescription().equalsIgnoreCase(tripLabel)) {
                    if (isViewValid()) {
                        reachedLocationView.onSavedTripResponse(tripLabel, Constants.SAVE_DATA_RESPONSE.Exists);
                        return;
                    }
                }
            }
            mNavigation.getStatus().setDescription(tripLabel);
            SavedTripsPresenter.getInstance().addSavedTrip(mNavigation);
            reachedLocationView.onSavedTripResponse(tripLabel, Constants.SAVE_DATA_RESPONSE.Success);
        }
        else{
            mNavigation.getStatus().setDescription(tripLabel);
            SavedTripsPresenter.getInstance().addSavedTrip(mNavigation);
            reachedLocationView.onSavedTripResponse(tripLabel, Constants.SAVE_DATA_RESPONSE.Success);
        }
    }

    /**
     * Location is saved to Cloudant using LocationMgr.
     *
     * @param locationLabel
     */
    @Override
    public void saveLocation(final String locationLabel) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                LocationsMgr locationsMgr = LocationsMgr.getLocationsMgr();
                List<ExpDocument> expDocuments = locationsMgr.reloadLocationsFromModel();
                if (expDocuments != null) {
                    for (ExpDocument expDocument : expDocuments) {
                        Location location = (Location) expDocument;
                        if (location.getName().equalsIgnoreCase(locationLabel)) {
                            //
                            if (isViewValid()) {
                                reachedLocationView.onSavedLocationResponse(locationLabel, Constants.SAVE_DATA_RESPONSE.Exists);
                                return;
                            }
                        }
                    }
                    if (mNavigation.getDestinationReached() != null) {
                        locationsMgr.addFavouriteLocation(locationLabel, mNavigation.getDestinationReached().getDestinationName(),
                                DateFormatter.getDate(), mNavigation.getDestinationReached().getLat(),
                                mNavigation.getDestinationReached().getLng());
                        reachedLocationView.onSavedLocationResponse(locationLabel, Constants.SAVE_DATA_RESPONSE.Success);
                    }
                }
            }
        }).start();
    }


    @Override
    public void init(ReachedLocationContract.ReachedLocationView view) {
        if (this.reachedLocationView != null)
            return;

        this.reachedLocationView = view;
    }

    public boolean isViewValid() {
        if (reachedLocationView == null)
            return false;

        return true;
    }

    @Override
    public void onStartView() {
        PresenterManager.getInstance().addActivePresenter(this);
    }

    @Override
    public void onStopView() {
        PresenterManager.getInstance().removeActivePresenter(this);
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {
        reachedLocationView = null;
    }
}
