package com.exploride.droid.presenters;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;

import com.android.internal.telephony.ITelephony;
import com.cloudant.exploridemobile.framework.CallInfo;
import com.exploride.droid.MainApplication;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.services.NotifService;
import com.exploride.droid.utils.Constants;

import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by peeyushupadhyay on 08/02/17.
 */

public class CallManager {

    private static String TAG = "CallManager";
    private static Object sLock = new Object();
    static CallManager sCallManager;

    public static CallManager getInstance(){
        if(sCallManager ==null) {
            synchronized (sLock) {
                if (sCallManager == null) {
                    sCallManager = new CallManager();
                }
            }
        }
        return sCallManager;
    }

    /**
     *
     * @param callInfo callInfo to process
     * @return whether event was handled or not
     */
    public boolean handleCallInfo(CallInfo callInfo){
        int callAction = callInfo.getCallAction();
        switch (callAction){
            case CallInfo.CallActions.AcceptCall:
                acceptCall();
                break;
            case CallInfo.CallActions.EndCall:
                endCall();
                break;
            case CallInfo.CallActions.RejectCall:
                endCall();
                break;
            case CallInfo.CallActions.Call:
                startCall(callInfo);
                break;
        }
        return true;
    }
    Context mCtx = MainApplication.getInstance();
    private synchronized void startCall(CallInfo callInfo) {

        try {

            String lastCallNumber;
            if (callInfo != null && callInfo.getNumber() != null)
                lastCallNumber = callInfo.getNumber();
            else
                lastCallNumber = CallLog.Calls.getLastOutgoingCall(mCtx);
//            Cursor cur = mCtx.getContentResolver().query( CallLog.Calls.CONTENT_URI,
//                    null, null, null, CallLog.Calls.DATE +" desc");
//            cur.moveToFirst();
//            String lastCallnumber = cur.getString(0);

            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("tel:" + lastCallNumber));
            if (ActivityCompat.checkSelfPermission(mCtx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainApplication.getInstance().getCurrentActivity(), new String[]{Manifest.permission.CALL_PHONE}, Constants.Permissions.CALL_PHONE_1);
                return;
            }
            MainApplication.getInstance().getCurrentActivity().startActivity(intent);

//            TelephonyManager telephonyManager = (TelephonyManager)mCtx.getSystemService(Context.TELEPHONY_SERVICE);
//            Class clazz = Class.forName(telephonyManager.getClass().getName());
//            Method method = clazz.getDeclaredMethod("getITelephony");
//            method.setAccessible(true);
//            ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);
//            telephonyService.call();
        }catch (Exception e){
            String a = "";
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){

        if(requestCode== Constants.Permissions.CALL_PHONE_1 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
            startCall(null);
    };

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void acceptCall() {
        MediaSessionManager mediaSessionManager = (MediaSessionManager) mCtx.getApplicationContext().getSystemService(Context.MEDIA_SESSION_SERVICE);

        try {
            List<MediaController> mediaControllerList = mediaSessionManager.getActiveSessions
                    (new ComponentName(mCtx.getApplicationContext(), NotifService.class));

            for (MediaController m : mediaControllerList) {
                if ("com.android.server.telecom".equals(m.getPackageName())) {
                    m.dispatchMediaButtonEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
                    ExpLogger.logI(TAG, "HEADSETHOOK sent to telecom server");
                    break;
                }
            }
        } catch (SecurityException e) {
            ExpLogger.logE(TAG, "Permission error. Access to notification not granted to the app.");
        }
    }
//    private void acceptCall() {
//
//        try {
//
//            TelephonyManager telephonyManager = (TelephonyManager)mCtx.getSystemService(Context.TELEPHONY_SERVICE);
//            Class clazz = Class.forName(telephonyManager.getClass().getName());
//            Method method = clazz.getDeclaredMethod("getITelephony");
//            method.setAccessible(true);
//            ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);
//            telephonyService.answerRingingCall();
//        }catch (Exception e){
//            String a = "";
//        }
//    }
    private void endCall() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager)mCtx.getSystemService(Context.TELEPHONY_SERVICE);
            Class clazz = Class.forName(telephonyManager.getClass().getName());
            Method method = clazz.getDeclaredMethod("getITelephony");
            method.setAccessible(true);
            ITelephony telephonyService = (ITelephony) method.invoke(telephonyManager);
            telephonyService.endCall();
        }catch (Exception e){
            String a = "";
        }
    }


}
