package com.exploride.droid.presenters;

public interface BasePresenter<T> {
    void init(T view);

    void onStopView();

    void onStartView();

    void onCreateView();

    void onDestroyView();

}
