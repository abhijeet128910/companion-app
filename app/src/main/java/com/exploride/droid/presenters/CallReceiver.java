package com.exploride.droid.presenters;

import android.content.Context;
import android.util.Log;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.CallInfo;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.receivers.PhonecallReceiver;

import java.util.Date;

import static com.exploride.droid.R.string.send;

/**
 * Created on 06/02/17.
 */

/**
 * the methods in this class are called on bg thread.
 */
public class CallReceiver extends PhonecallReceiver {

    private static final String TAG = "CallReceiver";
    static CallInfo mLastCallInfo;

    @Override
    protected void onIncomingCallReceived(Context ctx, String name, String number, Date start, String photo)
    {
        ExpLogger.logI(TAG, "onIncomingCallReceived");
        CallInfo callInfo = new CallInfo(name, number, start, null, CallInfo.CallConstants.IncomingCallReceived, photo);
        sendCallInfo(callInfo);
    }

    @Override
    protected void onIncomingCallAnswered(Context ctx,String name, String number, Date start, String photo)
    {
        ExpLogger.logI(TAG, "onIncomingCallAnswered");
        CallInfo callInfo = new CallInfo(name, number, start, null, CallInfo.CallConstants.IncomingCallAnswered, photo);
        sendCallInfo(callInfo);
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String name, String number, Date start, Date end, String photo)
    {
        ExpLogger.logI(TAG, "onIncomingCallEnded");
        CallInfo callInfo = new CallInfo(name, number, start, end, CallInfo.CallConstants.IncomingCallEnded, photo);
        sendCallInfo(callInfo);
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String name, String number, Date start, String photo)
    {
        ExpLogger.logI(TAG, "onOutgoingCallStarted");
        CallInfo callInfo = new CallInfo(name, number, start, null, CallInfo.CallConstants.OutgoingCallStarted, photo);
        sendCallInfo(callInfo);
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String name, String number, Date start, Date end, String photo)
    {
        ExpLogger.logI(TAG, "onOutgoingCallEnded");
        CallInfo callInfo = new CallInfo(name, number, start, end, CallInfo.CallConstants.OutgoingCallEnded, photo);
        sendCallInfo(callInfo);
    }

    @Override
    protected void onMissedCall(Context ctx, String name, String number, Date start, String photo)
    {
        ExpLogger.logI(TAG, "onMissedCall");
        CallInfo callInfo = new CallInfo(name, number, start, null, CallInfo.CallConstants.MissedCall, photo);
        sendCallInfo(callInfo);
    }

    private void sendCallInfo(CallInfo callInfo){
        ExpLogger.logI(TAG, "sendCallInfo");
        mLastCallInfo = callInfo;
        BTComm btComm = new BTComm();
        btComm.setCallInfo(callInfo);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }

    public static void addLastCallInfo(BTComm btComm){
        if(mLastCallInfo==null)
            return;
        btComm.setCallInfo(mLastCallInfo);
    }

}
