package com.exploride.droid.presenters;

import android.util.Log;

import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.PresenterManager;
import com.exploride.droid.SavedTripsContract;
import com.exploride.droid.SearchLocationContract;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.MapsStore;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.model.SavedTripCollection;
import com.exploride.droid.utils.Constants;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.RoutingError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhijeet on 11/15/2016.
 */

public class SavedTripsPresenter implements SavedTripsContract.SavedTripsOps {

    static SavedTripsPresenter savedTripsPresenter;
    private SavedTripsContract.SavedTripsView savedTripsView;
    private SavedTripCollection savedTripCollection;

    public static SavedTripsPresenter getInstance() {
        if (savedTripsPresenter == null) {
            savedTripsPresenter = new SavedTripsPresenter();
        }
        return savedTripsPresenter;
    }

    @Override
    public void fetchSavedTrips() {
        if (isViewValid())
            savedTripsView.showSavedTrips(getSavedTripCollection().getTrips());
    }

    @Override
    public void setSavedTrip(Navigation navigation) {
        if (navigation.getStatus().getDestination() != null && navigation.getStatus().getSource() != null &&
                navigation.getGeoCoordinateList() != null) {

            MapsStore.getInstance().setSourceLocation(navigation.getStatus().getSource());
            MapsStore.getInstance().setDestinationLocation(navigation.getStatus().getDestination());

            MapsStore.getInstance().setSourceLatitude(navigation.getStatus().getSourceLat());
            MapsStore.getInstance().setSourceLongitude(navigation.getStatus().getSourceLng());

            MapsStore.getInstance().setDestinationLatitude(navigation.getStatus().getDestLat());
            MapsStore.getInstance().setDestinationLongitude(navigation.getStatus().getDestLng());

            CoreRouter coreRouter = new CoreRouter();

            // Select routing options via RoutingMode
            RoutePlan routePlan = new RoutePlan();
            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);
            routeOptions.setRouteCount(10);
            routePlan.setRouteOptions(routeOptions);

            // If a saved trip is set
            ArrayList<Navigation.GeoCoordinate> savedPointList = navigation.getGeoCoordinateList();
            if (savedPointList != null) {

                if (savedPointList != null && savedPointList.size() > 0) {

                    for (Navigation.GeoCoordinate geoCoordinate : savedPointList) {
                        // adds way points for your routes
                        routePlan.addWaypoint(new RouteWaypoint(new GeoCoordinate(geoCoordinate.getLatitude(),
                                geoCoordinate.getLongitude())));
                    }
                }
                // Retrieve Routing information via CoreRouter.Listener
                coreRouter.calculateRoute(routePlan, savedRouterListener);
            }
        }
    }

    @Override
    public void init(SavedTripsContract.SavedTripsView view) {
        if (this.savedTripsView != null)
            return;

        this.savedTripsView = view;
    }

    public boolean isViewValid() {

        if (savedTripsView == null)
            return false;

        return true;
    }

    @Override
    public void onStartView() {
        PresenterManager.getInstance().addActivePresenter(this);
    }

    @Override
    public void onStopView() {
        PresenterManager.getInstance().removeActivePresenter(this);
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {
        savedTripsView = null;

        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    @Override
    public SavedTripCollection getSavedTripCollection() {
        if (savedTripCollection == null) {
            savedTripCollection = new SavedTripCollection();
        }
        return savedTripCollection;
    }

    @Override
    public void addSavedTrip(Navigation trip) {
        if (trip != null) {
            try {
                if (savedTripCollection == null) {
                    savedTripCollection = new SavedTripCollection();
                }
                savedTripCollection.addSavedTrip(trip);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private CoreRouter.Listener savedRouterListener =
            new CoreRouter.Listener() {
                public void onCalculateRouteFinished(List<RouteResult> routeResults,
                                                     RoutingError errorCode) {
                    if (errorCode == RoutingError.NONE) {

                        //holds various routes for a destination
                        ArrayList<RouteSummary> routeSummaries = new ArrayList<>();
                        SearchLocationPresenter presenter = SearchLocationPresenter.getInstance();

                        for (RouteResult routeResult : routeResults) {

                            if (routeResult.getRoute() != null) {

                                MapRoute mapRoute = new MapRoute(routeResult.getRoute());

                                RouteSummary routeSummary = new RouteSummary();

                                presenter.getETA(routeSummary, mapRoute);

                                presenter.getViaPoint(routeSummary, mapRoute);

                                presenter.getDistance(routeSummary, mapRoute);

                                routeSummary.setRoute(routeResult.getRoute());

                                routeSummaries.add(routeSummary);
                            }

                        }

                        MapRoute mapRoute = new MapRoute(routeResults.get(0).getRoute());
                        presenter.setMapRoute(mapRoute);
                        presenter.setRouteSummary(routeSummaries);

                        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
                    }
                }

                public void onProgress(int percentage) {
                    ExpLogger.logD("pylogs:", String.format("... %d percent done ...", percentage));
                }
            };
}
