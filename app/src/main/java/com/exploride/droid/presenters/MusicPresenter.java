package com.exploride.droid.presenters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;

import com.exploride.droid.MainApplication;
import com.exploride.droid.MusicContract;
import com.exploride.droid.logger.ExpLogger;

import java.lang.ref.WeakReference;

import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MUSIC_NEXT;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MUSIC_PAUSE;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MUSIC_PLAY;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MUSIC_PREV;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.MUSIC_STOP;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.TOGGLE_PLAY_PAUSE;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.VOL_DECREASE;
import static com.cloudant.exploridemobile.framework.ActionConstants.ActionCode.VOL_INCREASE;

/**
 * class to handle Music Activities
 */
public class MusicPresenter implements MusicContract.MusicOps {

    private static final String TAG = "MusicPresenter";
    private WeakReference<MusicContract.MusicView> mMusicView;
    static MusicPresenter presenter;
    private AudioManager mAudioManager;

    public static final String SERVICECMD = "com.android.music.musicservicecommand";

    public static final String CMD_PLAY = "play";
    public static final String CMD_PAUSE = "pause";
    public static final String CMD_PREVIOUS = "previous";
    public static final String CMD_NEXT = "next";
    public static final String CMD_NAME = "command";
    public static final String CMD_STOP = "stop";


    public static MusicPresenter getInstance() {
        if (presenter == null) {
            presenter = new MusicPresenter();
        }
        return presenter;
    }

    public void handleStatusCodes(int requestStatusCode){
        switch(requestStatusCode) {
            case MUSIC_PLAY:
                play();
                break;
            case MUSIC_PAUSE:
                pause();
                break;
            case MUSIC_STOP:
                stop();
                break;
            case MUSIC_NEXT:
                next();
                break;
            case MUSIC_PREV:
                prev();
                break;
            case TOGGLE_PLAY_PAUSE:
                togglePlayPause();
                break;
            case VOL_INCREASE:
                increaseVolume();
                break;
            case VOL_DECREASE:
                decreaseVolume();
                break;
        }
    }

    public MusicPresenter() {
        try {
            mAudioManager = (AudioManager) MainApplication.getInstance().getCurrentActivity()
                    .getSystemService(Context.AUDIO_SERVICE);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    public boolean isViewValid() {
        return mMusicView != null && mMusicView.get() != null;
    }

    @Override
    public void play() {
        sendBroadCast(CMD_PLAY);
    }

    @Override
    public void pause() {
        sendBroadCast(CMD_PAUSE);
    }

    @Override
    public void stop() {
        sendBroadCast(CMD_STOP);
    }

    @Override
    public void next() {
        sendBroadCast(CMD_NEXT);
    }

    @Override
    public void prev() {
        sendBroadCast(CMD_PREVIOUS);
    }

    @Override
    public void togglePlayPause() {
        sendBroadCast(mAudioManager.isMusicActive() ? CMD_PAUSE : CMD_PLAY);
    }

    @Override
    public void increaseVolume() {
        if (mAudioManager == null)
            return;

        try {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    @Override
    public void decreaseVolume() {
        if (mAudioManager == null)
            return;

        try {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    @Override
    public void init(MusicContract.MusicView view) {
        this.mMusicView = new WeakReference<>(view);

        // TODO: init MusicStore
    }

    @Override
    public void onStopView() {

    }

    @Override
    public void onStartView() {

    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {

    }

    private void sendBroadCast(String command) {
        try {
            Intent i = new Intent(SERVICECMD);
            i.putExtra(CMD_NAME, command);
            MainApplication.getInstance().getCurrentActivity().sendBroadcast(i);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }
}
