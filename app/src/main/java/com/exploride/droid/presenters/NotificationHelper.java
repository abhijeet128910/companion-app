package com.exploride.droid.presenters;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.provider.Settings;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.cloudant.exploridemobile.framework.NotificationData;
import com.cloudant.exploridemobile.framework.NotificationsInfo;
import com.exploride.droid.MainApplication;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.services.NotifService;
import com.exploride.droid.views.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by peeyushupadhyay on 30/03/17.
 */

public class NotificationHelper extends BroadcastReceiver{

    private final String TAG = this.getClass().getName();
    private static NotificationHelper sNotificationHelper;
    private static Object sLock = new Object();
    public static final String Action_Toast = "action-toast";
    public static final String Action_Notifications = "notificationhelper";
    public static final String Extra_Notificationtype = "notification_type";//fresh list 1, new 2, del3
    public static final String Extra_NotificationData = "notififcation_data";
    ArrayList<NotificationData> mNotificationData;
    int mNotificationType;

    public NotificationHelper(){

    }

    public static NotificationHelper getInstance(){

        if(sNotificationHelper ==null) {
            synchronized (sLock) {
                if (sNotificationHelper == null) {
                    sNotificationHelper = new NotificationHelper();
                }
            }
        }
        return sNotificationHelper;
    }

    @Override
    public void onReceive(Context context, final Intent intent) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        MainApplication.getInstance().assertBGThread();
                        String action = intent.getAction();
                        if(action.equals(Action_Notifications)){
                            mNotificationData = intent.getParcelableArrayListExtra(Extra_NotificationData);
                            mNotificationType = intent.getIntExtra(Extra_Notificationtype, 1);
                            if (mNotificationData != null)
                                sendNotificationList();
                        }else
                        if(action.equals(Action_Toast)) {
                            MainApplication.getInstance().showToast("Notif service started");
                        }
                    }catch (Exception e){
                        ExpLogger.logE(TAG, e.getMessage());
                    }
                }
            }).start();
    }

    void sendNotificationList() {

        NotificationsInfo notificationsInfo = new NotificationsInfo();
        notificationsInfo.setNotificationData(mNotificationData);
        notificationsInfo.setNotificationType(mNotificationType);
        MainApplication.getInstance().sendNotificationInfo(notificationsInfo);
    }

    void removeNotification(){

    }
    public void getAllNotifications(){

        Intent intentNotif = new  Intent(NotifService.NOTIF_SVC);
        intentNotif.putExtra(NotifService.EXTRA_COMMAND, NotifService.EXTRA_LIST);
        MainApplication.getInstance().sendBroadcast(intentNotif);
    }

    public boolean checkNotifAccess(Context ctx) {
        Set<String> packgs = NotificationManagerCompat.from(MainApplication.getInstance()).
                getEnabledListenerPackages(MainApplication.getInstance());
        if (!packgs.contains(ctx.getPackageName()))
            return false;
        else return true;
    }
    public void askNotifAccess(Context ctx) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
        intent.putExtra("app_package", ctx.getPackageName());
        intent.putExtra("app_uid", ctx.getApplicationInfo().uid);

        PackageManager manager = ctx.getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            //Then there is an Application(s) can handle your intent
            String a = ";";
            MainApplication.getInstance().getCurrentActivity().startActivityForResult(intent, MainActivity.RC_NOTIF_ACCESS);
        } else {
            //No Application can handle your intent
            String a = ";";
        }
    }

    private void markNotificationAsDone(NotificationData notificationData){
        if(notificationData.getPkg().equalsIgnoreCase(NotifService.CALL_PACKAGE)){
            ContactsHelper.getInstance().markCallLogRead(notificationData.getId());
        }
    }

    public void toggleNotificationListenerService(Context context) {
        if(!checkNotifAccess(context))
            return;

        try {
            PackageManager pm = context.getPackageManager();
            pm.setComponentEnabledSetting(new ComponentName(context, com.exploride.droid.services.NotifService.class),
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

            pm.setComponentEnabledSetting(new ComponentName(context, com.exploride.droid.services.NotifService.class),
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }
}
