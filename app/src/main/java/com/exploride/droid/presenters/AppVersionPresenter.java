package com.exploride.droid.presenters;

import android.app.Activity;

import com.exploride.droid.AppVersionContract;
import com.exploride.droid.PresenterManager;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.views.fragments.AppVersionFragment;
import com.exploride.droid.views.fragments.BaseFragment;

import java.lang.ref.WeakReference;

/**
 * Created by Mayank on 1/24/2017.
 */

public class AppVersionPresenter implements AppVersionContract.VersionOps {

    static AppVersionPresenter appAppVersionPresenter;
    private Activity mActivity;
    private WeakReference<AppVersionContract.VersionView> appVersionView;
    
    public static AppVersionPresenter getInstance() {
        if (appAppVersionPresenter == null) {
            appAppVersionPresenter = new AppVersionPresenter();
        }
        return appAppVersionPresenter;
    }

    @Override
    public void init(AppVersionContract.VersionView view) {
        mActivity = ((BaseFragment) view).getActivity();
        if (this.appVersionView != null)
            return;
        this.appVersionView = new WeakReference<>(view);

    }

    public boolean isViewValid() {
        return appVersionView != null || appVersionView.get() != null;
    }

    @Override
    public void onStartView() {
        PresenterManager.getInstance().addActivePresenter(this);
    }

    @Override
    public void onStopView() {
        PresenterManager.getInstance().removeActivePresenter(this);
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroyView() {
        appVersionView = null;
    }

    /**
     * call to {@link AppVersionFragment} to show HUD app version info to the user
     * @param versionName: HUD app version name
     */
    @Override
    public void showHudAppVersion(String versionName) {
        if(isViewValid())
            appVersionView.get().showHudAppVersionInfo(versionName);
    }

    /**
     * call to switch to Maps Fragment screen
     */
    @Override
    public void switchToMapsScreen() {
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }
}
