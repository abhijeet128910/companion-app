package com.exploride.droid.presenters;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.ContactInfo;
import com.exploride.droid.MainApplication;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Abhijeet
 */

public class ContactsHelper {

    private final String TAG = "ContactsHelper";
    private final int RECENT_LIST_MAX_COUNT = 20;
    private Context mContext;
    private static ContactsHelper sContactsHelper;
    private List<ContactInfo> mContacts;

    public static ContactsHelper getInstance() {

        if (sContactsHelper == null) {
            if (sContactsHelper == null) {
                sContactsHelper = new ContactsHelper();
            }
        }
        return sContactsHelper;
    }

    private ContactsHelper() {

    }

    /**
     * This is called when bluetooth is connected
     */
    public void syncContactsWithHUD() {
        if (mContacts != null && MainApplication.getInstance().isCommunicationChannelOpen()) {
            BTComm btComm = new BTComm();
            btComm.setActionCode(ActionConstants.ActionCode.CONTACT_LIST_RESP);
            btComm.setContactList(mContacts);
            ConnectionHandler.getInstance().sendInfo(btComm);
        }
    }

    /**
     * Get recent and favourite contacts.
     * Called on app start and when call receiver is called.
     */
    public void getContacts(final Context context) {
        mContext = context;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                if (mContacts == null)
                    mContacts = new ArrayList<>();
                else
                    mContacts.clear();

                LinkedHashMap<String, ContactInfo> contactInfoMap = getRecentContacts(RECENT_LIST_MAX_COUNT);
                contactInfoMap.putAll(getFavoriteContacts());
                mContacts.addAll(getContactList(contactInfoMap));
                syncContactsWithHUD();
            }
        });
        thread.start();
    }

    /**
     * Get recent contact and pass max count
     *
     * @param maxCount
     * @return
     */
    public LinkedHashMap<String, ContactInfo> getRecentContacts(int maxCount) {

        LinkedHashMap<String, ContactInfo> map = new LinkedHashMap<>();

        try {
            Uri queryUri = CallLog.Calls.CONTENT_URI;

            String[] projection = new String[]{
                    CallLog.Calls.NUMBER,
                    CallLog.Calls.DATE};

//        String sortOrder = String.format("%s limit " + maxCount, CallLog.Calls.DATE + " DESC");
            String sortOrder = String.format(CallLog.Calls.DATE + " DESC");

            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            Cursor cursor = mContext.getContentResolver().query(queryUri, projection, null, null, sortOrder);

            if(cursor == null) {
                return map;
            }

            while (cursor.moveToNext()) {
                String phoneNumber = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                if (phoneNumber == null || phoneNumber.isEmpty())
                    continue;

                ContactInfo contactInfo = getDetailsByNumber(mContext, phoneNumber);
                if (contactInfo == null)
                    contactInfo = new ContactInfo("", "", phoneNumber);

                map.put(phoneNumber, contactInfo);
                if (map.size() == maxCount) {
                    break;
                }
            }
            cursor.close();
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
        return map;
    }

    /**
     * Get favourite contacts
     */
    public LinkedHashMap<String, ContactInfo> getFavoriteContacts() {
        LinkedHashMap<String, ContactInfo> map = new LinkedHashMap<>();
        try {
            Cursor cursor = mContext.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
                    "starred=?", new String[]{"1"}, null);

            if(cursor == null) {
                return map;
            }

            while (cursor.moveToNext()) {
                String contactID = cursor.getString(cursor
                        .getColumnIndex(ContactsContract.Contacts._ID));

                ContactInfo contactInfo = getDetailsById(mContext, contactID);
                if(contactInfo == null || contactInfo.getNumber() == null)
                    continue;

                map.put(contactInfo.getNumber().replace(" ", ""), contactInfo);
            }

            cursor.close();
        } catch (Exception e) {
           ExpLogger.logException(TAG, e);
        }
        return map;
    }

    /**
     * Get details such as name and number by Id.
     * Used for favourites as cursor does not return Contact number.
     * @param context
     * @param id
     * @return
     */
    public ContactInfo getDetailsById(Context context, String id) {
        try {
            Cursor cursor = context.getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                    new String[]{id}, null);

            if(cursor == null)
                return null;

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                String name, phoneNumber;
                name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
//                phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
//
                phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if (phoneNumber != null)
                    phoneNumber = phoneNumber.trim();

                cursor.close();
                return new ContactInfo(id, name, phoneNumber);
            }

            cursor.close();
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
        return null;
    }

    /**
     * Get details such as name and Id by number.
     * Used for recent call logs as cursor returns CacheName which is mostly null.
     * @param context
     * @param phoneNumber
     * @return
     */
    public ContactInfo getDetailsByNumber(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup._ID,ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup.NUMBER,ContactsContract.PhoneLookup.NORMALIZED_NUMBER}, null, null, null);
        if (cursor == null) {
            return null;
        }
        if (cursor.moveToFirst()) {
            String id, contactName, contactNumber;
            id = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup._ID));
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
//            Normalized number gives unformatted number (without spaces). Commented as not used currently
//            phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.NORMALIZED_NUMBER));
//            if (phoneNumber == null)
            // Fetching number from cursor again in case the save number and to search has country extension or 0 appended
            contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.NUMBER));
            if (contactNumber == null)
                contactNumber = phoneNumber.trim();

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            return new ContactInfo(id, contactName, contactNumber);
        }
        return null;
    }

    /**
     * Converts map to list
     * @param contactInfoMap
     * @return
     */
    private List<ContactInfo> getContactList(LinkedHashMap<String, ContactInfo> contactInfoMap) {

        List<ContactInfo> contactList = new ArrayList<>();

        for (Map.Entry<String, ContactInfo> entry : contactInfoMap.entrySet()) {
            contactList.add(entry.getValue());
        }
        return contactList;
    }

    /**
     * Search contacts for contact name
     * @param context
     * @param contactName
     * @return
     */
    public void searchContactByName(Context context, String contactName){
        Cursor cursor = null;
        try {
            List<ContactInfo> contactList = new ArrayList<>();
            if (contactName == null)
                return;

            Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " like'%" + contactName + "%'";

            String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone._ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.NUMBER};

            cursor = context.getContentResolver().query(uri, projection, selection, null, null, null);

            int _id = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID);
            int indexName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int indexNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                LinkedHashMap<String, ContactInfo> contactInfos = new LinkedHashMap<>();
                do {
                    String id = cursor.getString(_id);
                    String name = cursor.getString(indexName);
                    String number = cursor.getString(indexNumber);
                    if (number != null)
                        number = number.replace("-", "");
                    contactInfos.put(number.replace(" ", ""), new ContactInfo(id, name, number));
                } while (cursor.moveToNext());

                if (cursor != null)
                    cursor.close();

                if (contactInfos != null)
                    contactList = new ArrayList(contactInfos.values());
            }

            BTComm btComm = new BTComm();
            btComm.setActionCode(ActionConstants.ActionCode.SEARCH_CONTACT_RESP);
            btComm.setContactList(contactList);
            ConnectionHandler.getInstance().sendInfo(btComm);
        } catch (Exception e) {
           ExpLogger.logException(TAG, e);
        }
        finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public void markCallLogRead(int id) {

        Uri callLogUri = CallLog.Calls.CONTENT_URI;
        ContentValues values = new ContentValues();
        values.put("is_read", true);
        try{
            mContext.getContentResolver().update(callLogUri, values, "_id=?",
                    new String[] { String.valueOf(id) });
        }catch(Exception e){
            ExpLogger.logException(TAG, e);
        }
    }
}