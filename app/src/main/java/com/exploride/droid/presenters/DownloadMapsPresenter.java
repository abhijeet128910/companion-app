package com.exploride.droid.presenters;


import android.util.Log;

import com.exploride.droid.DownloadMapsContract;
import com.exploride.droid.PresenterManager;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.model.DownloadMapsStore;
import com.exploride.droid.utils.Constants;
import com.here.android.mpa.odml.MapLoader;
import com.here.android.mpa.odml.MapPackage;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Author Mayank
 */

public class DownloadMapsPresenter implements DownloadMapsContract.DownloadMapsOps {

    private String TAG = DownloadMapsPresenter.class.getName();
    private WeakReference<DownloadMapsContract.DownloadMapsView> downloadMapsView;
    private static DownloadMapsPresenter downloadMapsPresenter;
    private ArrayList<MapPackage> currentInstalledMapsList;
    private MapPackage currentInstalledMapPackage;
    private MapPackage worldMapPackage;

    public static DownloadMapsPresenter getInstance() {
        if (downloadMapsPresenter == null) {
            downloadMapsPresenter = new DownloadMapsPresenter();
        }
        return downloadMapsPresenter;
    }

    @Override
    public void init(DownloadMapsContract.DownloadMapsView view) {
        if (this.downloadMapsView != null)
            return;
        this.downloadMapsView = new WeakReference<>(view);

        worldMapPackage = null;

    }

    private boolean isViewValid() {
        return downloadMapsView != null && downloadMapsView.get() != null;
    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onStartView() {
        PresenterManager.getInstance().addActivePresenter(this);
    }

    @Override
    public void onStopView() {
        PresenterManager.getInstance().removeActivePresenter(this);
    }

    @Override
    public void onDestroyView() {

        downloadMapsView = null;

    }

    /**
     * gets {@link MapPackage} Id from HUD
     *
     * @param packageIdList: list of map package id
     */
    @Override
    public void onGetMapPackagesId(List<Integer> packageIdList, boolean isInstalledMapPackageId) {

        ExpLogger.logD(TAG, "DownloadMapsInformation : " + packageIdList + " isInstalledMapPackageId : " + isInstalledMapPackageId);

        DownloadMapsStore.getInstance().setPackageIdList(packageIdList);

        if (isInstalledMapPackageId) {//shows downloaded Maps list

            currentInstalledMapsList = new ArrayList<>();
            getDownloadedMapsList(worldMapPackage, getDownloadedMapPackageIdList());
            if (isViewValid())
                downloadMapsView.get().showInstalledMapsList(currentInstalledMapsList);

        } else if (worldMapPackage == null) {//shows complete Maps List starting with continents

//            MapLoader.getInstance().cancelCurrentOperation();
            MapLoader.getInstance().addListener(mapLoaderListener);
            MapLoader.getInstance().getMapPackages();

        } else {//shows complete Maps List

            if (isViewValid())
                downloadMapsView.get().showMapsList(null);

        }

    }

    /**
     * gets Downloaded {@link MapPackage} List from HUD to show into listview
     *
     * @param mapPackage: {@link MapPackage} used to check Installation state of each Map.
     * @param mPackageIdList: list of downloaded Map Packages on HUD
     */
    private void getDownloadedMapsList(MapPackage mapPackage, List<Integer> mPackageIdList) {
        // only take installed root package, so if e.g. Germany is installed, don't check for children states
        if (mPackageIdList.contains(mapPackage.getId())) {
            ExpLogger.logI(TAG, "DownloadMapsInformation : Installed package found: " + mapPackage.getTitle() + " id " + mapPackage.getId());
            currentInstalledMapsList.add(mapPackage);
        } else if (mapPackage.getChildren() != null && mapPackage.getChildren().size() > 0) {
            for (MapPackage mapPackage1 : mapPackage.getChildren()) {
                getDownloadedMapsList(mapPackage1, mPackageIdList);
            }
        }
    }

    /**
     * gets Downloaded {@link MapPackage} from HUD to show into listview
     *
     * @param mapPackage: {@link MapPackage} used to check Installation state of each Map.
     * @param mapPackageId: {@link MapPackage} id downloaded on HUD
     */
    private void getDownloadedMapsPackage(MapPackage mapPackage, int mapPackageId) {

        if(currentInstalledMapPackage != null)
            return;
        
        if (mapPackageId == mapPackage.getId()) {
            ExpLogger.logI(TAG, "DownloadMapsInformation : Installed package found: " + mapPackage.getTitle() + " id " + mapPackage.getId());
            currentInstalledMapPackage = mapPackage;
        } else if (mapPackage.getChildren() != null && mapPackage.getChildren().size() > 0) {
            for (MapPackage mapPackage1 : mapPackage.getChildren()) {
                getDownloadedMapsPackage(mapPackage1, mapPackageId);
            }
        }
    }

    /**
     * call to filter {@link MapPackage} List as per user's query
     *
     * @param query:          user entered text
     * @param mapPackageList: List of {@link MapPackage} class instance containing maps continent, country, state or city maps
     */
    @Override
    public void onQueryTextChange(String query, List<MapPackage> mapPackageList) {
        final List<MapPackage> filteredMapPackageList = filter(mapPackageList, query);
        if (isViewValid())
            downloadMapsView.get().onSearchCompleted(filteredMapPackageList);

    }

    /**
     * filters the {@link MapPackage} list as per user entered query
     *
     * @param mapPackageList: List of {@link MapPackage} class instance containing maps continent, country, state or city maps
     * @param query:          user entered text
     * @return List<MapPackage> filtered list
     */
    private List<MapPackage> filter(List<MapPackage> mapPackageList, String query) {
        query = query.toLowerCase();

        final List<MapPackage> filteredMapPackageList = new ArrayList<>();
        for (MapPackage mapPackage : mapPackageList) {

            if (mapPackage.getTitle().toLowerCase().contains(query)) {
                filteredMapPackageList.add(mapPackage);
            }
        }
        return filteredMapPackageList;
    }

    /**
     * sets Here Map Package Id which would be sent to HUD to install or uninstall the respective Maps
     *
     * @param packageId: int Maps Package Id
     */
    @Override
    public void setMapPackageId(int packageId) {

        DownloadMapsStore.getInstance().setMapPackageId(packageId);
    }

    /**
     * gets Here Map Package Id used to show or hide items in the listview
     *
     * @return int Here Map Package Id
     */
    @Override
    public int getMapPackageId() {
        return DownloadMapsStore.getInstance().getMapPackageId();
    }

    /**
     * Map Loader Lister for various events
     * <p>
     * getMapPackages() - To retrieve the state of the map data on the device
     * installMapPackages(List<Integer> packageIdList) - To download and install new country or region data
     * uninstallMapPackages(List<Integer> packageIdList) - To uninstall and delete country or region data that is no longer desired
     * checkForMapDataUpdate() - To check whether a new map data version is available
     * performMapDataUpdate() - To perform a map data version update, if available
     * cancelCurrentOperation() - To cancel the running MapLoader operation
     */
    private MapLoader.Listener mapLoaderListener = new MapLoader.Listener() {
        public void onUninstallMapPackagesComplete(MapPackage rootMapPackage,
                                                   MapLoader.ResultCode mapLoaderResultCode) {
        }

        public void onProgress(int progressPercentage) {

        }

        public void onPerformMapDataUpdateComplete(MapPackage rootMapPackage,
                                                   MapLoader.ResultCode mapLoaderResultCode) {
        }

        public void onInstallationSize(long diskSize, long networkSize) {
        }

        public void onInstallMapPackagesComplete(MapPackage rootMapPackage,
                                                 MapLoader.ResultCode mapLoaderResultCode) {

        }

        public void onGetMapPackagesComplete(MapPackage rootMapPackage,
                                             MapLoader.ResultCode resultCode) {

            if (resultCode == MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
                ExpLogger.logD(TAG, "DownloadMapsInformation : " + rootMapPackage + " ResultCode : " + resultCode);
                worldMapPackage = rootMapPackage;

                //shows complete Maps List
                if (isViewValid())
                    downloadMapsView.get().showMapsList(rootMapPackage);
            } else {
                ExpLogger.logD(TAG, "DownloadMapsInformation : " + rootMapPackage + " ResultCode : " + resultCode);
            }

        }

        public void onCheckForUpdateComplete(boolean updateAvailable,
                                             String currentMapVersion, String newestMapVersion,
                                             MapLoader.ResultCode mapLoaderResultCode) {
        }
    };

    /**
     * resets Map package Id to default value
     */
    private void resetMapPackageId() {
        DownloadMapsStore.getInstance().setMapPackageId(-1);
    }

    /**
     * call to switch to Maps Fragment screen
     */
    @Override
    public void switchToMapsScreen() {
        MainPresenter.getInstance().loadFragment(Constants.Fragments.MapsDestinationFragment, null);
    }

    /**
     * initializes list od installed map package id's
     */
    @Override
    public void initInstalledPackageIdList() {
        DownloadMapsStore.getInstance().initPackageIdList();
    }

    /**
     * call to notify of the error occurred while getting Map Package on HUD
     */
    @Override
    public void onMapPackageError() {
        ExpLogger.logD(TAG, "DownloadMapsInformation : " + "onMapPackageError");
        //notify user of the error occurred
        if (isViewValid())
            downloadMapsView.get().showMapPackageError();
    }

    /**
     * call to notify Map Engine being offline while getting Map Package on HUD
     */
    public void handleMapEngineOfflineError(){
        if (isViewValid())
            downloadMapsView.get().showMapEngineOfflineError();
    }

    /**
     * gets map install progress from HUD
     *
     * @param progress: install percentage (0-100)
     */
    @Override
    public void onMapInstallProgress(int progress) {

        ExpLogger.logD(TAG, "DownloadMapsInformation : " + "Progress: " + progress);
        DownloadMapsStore.getInstance().setProgress(progress);

        if (isViewValid())
            downloadMapsView.get().showMapInstallProgress();
    }

    /**
     * gets map install progress to be shown in the mobile app
     *
     * @return int map install progress
     */
    @Override
    public int getMapInstallProgress() {
        return DownloadMapsStore.getInstance().getProgress();
    }

    /**
     * call to notify user of the success while installing maps
     */
    @Override
    public void onMapInstallSuccess() {
        ExpLogger.logD(TAG, "DownloadMapsInformation : " + "onMapInstallSuccess");
        getDownloadedMapPackageIdList().add(getMapPackageId());

        //call to get the downloaded MapPackage
        getDownloadedMapsPackage(worldMapPackage, getMapPackageId());

        resetMapPackageId();

        //call to reset the progress on map install complete
        DownloadMapsStore.getInstance().setProgress(0);

        //notify user of the map install success
        if (isViewValid()) {
            downloadMapsView.get().showMapInstallSuccess(currentInstalledMapPackage);
            currentInstalledMapPackage = null;
        }

    }

    /**
     * call to notify user of the error occurred while installing maps
     */
    @Override
    public void onMapInstallError() {
        ExpLogger.logE(TAG, "DownloadMapsInformation : " + "onMapInstallError");
        resetMapPackageId();

        //call to reset the progress on map install complete
        DownloadMapsStore.getInstance().setProgress(0);

        //notify user of the error occurred
        if (isViewValid())
            downloadMapsView.get().showMapInstallError();
    }

    /**
     * call to notify user of the success while uninstalling maps
     */
    @Override
    public void onMapUninstallSuccess() {
        ExpLogger.logD(TAG, "DownloadMapsInformation : " + "onMapUninstallSuccess");
        getDownloadedMapPackageIdList().remove((Integer) getMapPackageId());
        resetMapPackageId();

        //notify user of the map install success
        if (isViewValid())
            downloadMapsView.get().showMapUninstallSuccess();
    }

    /**
     * call to notify user of the error occurred while uninstalling maps
     */
    @Override
    public void onMapUninstallError() {
        ExpLogger.logE(TAG, "DownloadMapsInformation : " + "onMapUninstallError");
        resetMapPackageId();

        //notify user of the error occurred
        if (isViewValid())
            downloadMapsView.get().showMapUninstallError();
    }

    /**
     * call to notify user of the error occurred while Map Engine Initialization
     */
    @Override
    public void onMapEngineInitializationError() {
        //notify user of the error occurred
        if (isViewValid())
            downloadMapsView.get().showMapEngineInitializationError();
    }

    /**
     * gets list downloaded map package id's on HUD
     * @return list of downloaded map package id
     */
    @Override
    public List<Integer> getDownloadedMapPackageIdList() {
        return DownloadMapsStore.getInstance().getPackageIdList();
    }
}