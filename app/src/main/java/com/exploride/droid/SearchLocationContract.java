package com.exploride.droid;

import android.content.Context;
import android.location.Location;
import com.cloudant.exploridemobile.ExpDocument;
import com.exploride.droid.model.PlaceAutoComplete;
import com.exploride.droid.model.RecentLocation;
import com.exploride.droid.model.RouteSummary;
import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.Maneuver;

import java.util.ArrayList;
import java.util.List;

/**
 * Author Mayank
 */

public interface SearchLocationContract {
    /**
     * methods for Presenter. Available to View
     */
    interface SearchLocationOps extends BasePresenter<SearchLocationContract.SearchLocationView> {

        void fetchAutoSuggestLocations(String queryLocation);

        void setSourceLocation(String placeName, ArrayList<Double> position);

        void setSourceLocation();

        void setDestinationLocation(String placeName, ArrayList<Double> position);

        Location getSourceLocation();

        Location getDestinationLocation();

        void getETA(RouteSummary routeSummary, MapRoute mapRoute);

        void getViaPoint(RouteSummary routeSummary, MapRoute mapRoute);

        void getDistance(RouteSummary routeSummary, MapRoute mapRoute);

        void setRouteSummary(ArrayList<RouteSummary> routeSummaries);

        void setMapRoute(MapRoute mapRoute);

        void setWayPoint(ArrayList<Double> coordinates);

        ArrayList<Location> getWayPoints();

        void removeAllWayPoints();

        void getReverseGeoCodedLocation();

        String getSourceLocationName();

        String getDestinationLocationName();

        void setSavedLocations(String locationName, Location location);

        void getSavedLocations();

        void switchToMapsScreen();

        void getRecentLocations();

        void resetDestination();

        void saveRecentLocation(RecentLocation recentLocation);

        void onResponse(PlaceAutoComplete placeAutoComplete);

        void onError(String error);

        void findPlaceCoordinates(String placeId, LocationResolverListener locationResolverListener);
        void addPlaceAsFavorite(String placeId,String title, String alias, String lat, String lng);
        void removePlaceAsFavorite(String placeId);
        void setPlaceCoordinates(ArrayList<Double> coordinates);

        ArrayList<Double> getPlaceCoordinates();

        void setSelectedRouteIndex(int selectedRouteIndex);

        void getManeuversInfo(Maneuver maneuver, RouteSummary.Maneuvers routeSummaryManeuver);

        void sortRoutesList(ArrayList<RouteSummary> routeSummaries);
    }


    /**
     * methods for View. Available to Presenter
     */

    interface SearchLocationView extends BaseView {

        void showGeocodeLocations(PlaceAutoComplete placeAutoComplete);

        void showErrorMessage(String error);

        void showSourceLocation(String Location);

        void showSavedLocations(List<ExpDocument> locations);

        void showRecentLocations(List<RecentLocation> locations);

    }

    /**
     * methods for Model. Available to Presenter
     */
    interface SearchLocationStore {

        void addWayPoints(Location location);

        ArrayList<Location> getWayPoints();

        void removeAllWayPoints();
    }

    /**
     * methods for Presenter. Available to Model
     */
    interface SearchLocationPresenterOps {

    }

    interface LocationResolverListener{
        void onLocationResolved(ArrayList<Double> coordinates);
    }
}
