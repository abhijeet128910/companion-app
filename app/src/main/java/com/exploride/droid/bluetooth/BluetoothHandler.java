package com.exploride.droid.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.cloudant.exploridemobile.framework.ActionConstants;
import com.cloudant.exploridemobile.framework.BTComm;
import com.exploride.droid.MainApplication;
import com.exploride.droid.R;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;
import com.exploride.droid.presenters.MainPresenter;
import com.exploride.droid.services.BluetoothClientService;
import com.exploride.droid.settings.SettingsUtils;
import com.exploride.droid.utils.CommonUtils;
import com.exploride.droid.utils.Constants;
import com.exploride.droid.utils.Utils;
import com.exploride.droid.utils.appupdater.ExplorideUpdateChecker;
import com.exploride.droid.views.activities.DeviceListActivity;

import java.util.Set;

import static com.exploride.droid.services.BluetoothClientService.STATE_CONNECTED;
import static com.exploride.droid.utils.Constants.CHANNEL.BT;
import static com.exploride.droid.utils.Constants.CHANNEL.TCP;
import static com.exploride.droid.utils.Constants.SharedPrefKeys.KEY_BT_DEV_ADDRESS;
import static com.exploride.droid.utils.Constants.SharedPrefKeys.KEY_BT_DEV_NAME;

/**
 * Helper class to help initiate and maintain Bluetooth connection
 */
public class BluetoothHandler {

    private static final String TAG = "BluetoothHandler";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    public static final int REQUEST_ENABLE_BT = 3;

    /**
     * Name of the BTConnected device
     */
    private String mConnectedDeviceName = null;

    /**
     * String buffer for outgoing messages
     */
    private StringBuffer mOutStringBuffer;

    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;

    /**
     * Member object for the BT service
     */
    private static volatile BluetoothClientService mBluetoothClientService = null;

    String mLastBTAddress = "24:DF:6A:AD:6E:50";

    static volatile BluetoothDevice mDevice;
    DiscoveryHandler.DiscoveryHandlerCallback mDiscoveryHandlerCallback;
    static volatile BluetoothHandler bluetoothHandler;

    public static BluetoothHandler getInstance(){
        if (bluetoothHandler == null) {
            bluetoothHandler = new BluetoothHandler();
            bluetoothHandler.init();
        }
        return bluetoothHandler;
    }
    public BluetoothDevice getCurrentDevice(){
        return mDevice;
    }
    public void setCurrentDevice(BluetoothDevice device){
        mDevice = device;
    }
    public void setCurrentDevice(String deviceAddress){
        mDevice = mBluetoothAdapter.getRemoteDevice(deviceAddress);;
    }

    public BluetoothAdapter getBluetoothAdapter(){
        if (mBluetoothAdapter == null) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        return mBluetoothAdapter;
    }

    public BluetoothClientService getBluetoothClientService(){
        return mBluetoothClientService;
    }

    public void init() {
        try {
            // Get local Bluetooth adapter
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            // If the adapter is null, then Bluetooth is not supported
            if (mBluetoothAdapter == null) {
                MainApplication.getInstance().exit();
                return;
            }
            // If BT is not on, request that it be enabled.
            // setUpService() will then be called during onActivityResult
            if (!mBluetoothAdapter.isEnabled()) {
                if(MainApplication.getInstance().getCurrentActivity()==null) {
                    ExpLogger.logE(TAG, "MainApplication.getInstance().getCurrentActivity() is null, cannot show dialog for enable BT");
                    return;
                }
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                MainApplication.getInstance().getCurrentActivity().startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            } else if (mBluetoothClientService == null) {
                setUpService(true);
            }
        }catch (Exception e){
            bluetoothHandler = null;
            ExpLogger.logE(TAG, e.getMessage());
        }
    }

    public void connectToService(BluetoothDevice device){
        mDevice = device;
        if(mBluetoothClientService==null)
            setUpService(false);
        else
            mBluetoothClientService.connect(mDevice, false);
    }


    public void runStressTest(){
        ExpLogger.logI("AutoBTTest", "running for the count = "+btStressTestCount);
        Runnable r = new Runnable()
        {
            @Override
            public void run() {
                try{
                    StringBuffer output = new StringBuffer();
                    isStressTestRunning = true;
                    int successCount = 0;
                    for(int i =0; i<btStressTestCount; i++){
                        output.append("\n\n---");
                        output.append("Iteration# "+(i+1)+"\n");
                        ExpLogger.logI("AutoBTTest", "Iteration# "+(i+1));
                        mBluetoothClientService.start();
                        Thread.sleep(500); //allow to wind up
                        mBluetoothClientService.connect(mDevice, false);
                        synchronized(BTTestLock){
                            try {
                                BTTestLock.wait();
                            } catch (InterruptedException e) {
                                ExpLogger.logI(TAG, e.getMessage());
                            }
                        }
                        if(statusForBTTest==BluetoothClientService.STATE_CONNECTED) {
                            output.append("connected");
                            ExpLogger.logI("AutoBTTest", "connected");
                            successCount++;
                        }
                        else if(statusForBTTest==BluetoothClientService.STATE_LISTEN ||
                                statusForBTTest==BluetoothClientService.STATE_NONE){
                            output.append("STATE_LISTEN/STATE_NONE");
                            ExpLogger.logI("AutoBTTest", "STATE_LISTEN/STATE_NONE");
                        }
                    }
                    output.append("\n\n passed tests = " + successCount);
                    output.append("\n\n\n---------\n\n");
                    final String res = output.toString();
                    ExpLogger.logI("AutoBTTest", "passed tests = " + successCount);
                    MainApplication.getInstance().getUIHandler().post(new Runnable() {
                        @Override
                        public void run() {
                            CommonUtils.showAlert(MainApplication.getInstance().getCurrentActivity(),
                                    "test output", res, (DialogInterface.OnDismissListener) null
                            );
                        }
                    });
                } catch (InterruptedException e) {
                    ExpLogger.logI(TAG, e.getMessage());
                }finally {
                    isStressTestRunning = false;
                }
            }
        };
        Thread t = new Thread(r);
        t.start();
    }

    final int btStressTestCount = 100;
    public boolean isStressTestRunning = false;
    private int statusForBTTest;
    private final Object BTTestLock = new Object();
    private void notifyTestRunner(int status) {
        statusForBTTest = status;
        switch (status){
            case BluetoothClientService.STATE_CONNECTED:
                synchronized(BTTestLock){
                    BTTestLock.notifyAll();
                }
                break;
            case BluetoothClientService.STATE_CONNECTING:

                break;
            case BluetoothClientService.STATE_LISTEN:
            case BluetoothClientService.STATE_NONE:
                synchronized(BTTestLock){
                    BTTestLock.notifyAll();
                }
                break;
        }
    }

    /**
     * Connects to the Exp HUD. Starts discovery if device is not previously paired.
     */
    public void tryBTConnect(final DiscoveryHandler.DiscoveryHandlerCallback discoveryHandlerCallback){

        try {
            {
//            if(mBTAddress!=null){
//                mDevice = mBluetoothAdapter.getRemoteDevice(mBTAddress);
//                mBluetoothClientService.connect(mDevice, false);
//                Toast.makeText(this.getActivity(), "BTConnecting to BT address", Toast.LENGTH_SHORT).show();
//                foundDevice = true;
//                return;
//            }
            }
            boolean foundDevice = false;

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            mLastBTAddress = SettingsUtils.readPreferenceValueAsString(MainApplication.getInstance().getApplicationContext(), KEY_BT_DEV_ADDRESS);
            mDevice = getBluetoothAdapter().getRemoteDevice(mLastBTAddress);

            if (mLastBTAddress != null) {
                if (pairedDevices.size() > 0) {
                    // Loop through paired devices
                    for (BluetoothDevice device : pairedDevices) {
                        String deviceAddress = device.getAddress();
                        if (deviceAddress.equals(mLastBTAddress)) {
                            // Add the name and address to an array adapter to show in a ListView
                            connectToService(device);
                            MainApplication.getInstance().toastToShow(MainApplication.getInstance().getString(R.string.bt_connecting_to_device) + device.getName(), BT);
                            foundDevice = true;
                            return;
                        }
                    }
                }
            } else {
                if (pairedDevices.size() > 0) {
                    // Loop through paired devices
                    for (BluetoothDevice device : pairedDevices) {
                        String deviceName = device.getName();
                        if (device.getName().startsWith(BluetoothClientService.BTEXPNAME)) {
                            // Add the name and address to an array adapter to show in a ListView
                            connectToService(device);
                            MainApplication.getInstance().toastToShow(MainApplication.getInstance().
                                    getString(R.string.bt_connecting_to_device) + device.getName(), BT);
                            foundDevice = true;
                            return;
                        }
                    }
                }
            }

            if (!foundDevice) {

                DiscoveryHandler discoveryHandler = new DiscoveryHandler(new DiscoveryHandler.DiscoveryHandlerCallback() {
                    @Override
                    public void BTDiscoveryDeviceFound() {
                        if (discoveryHandlerCallback != null)
                            discoveryHandlerCallback.BTDiscoveryDeviceFound();
                    }

                    @Override
                    public void BTDiscoveryStarted() {
                        if (discoveryHandlerCallback != null)
                            discoveryHandlerCallback.BTDiscoveryStarted();
                    }

                    @Override
                    public void BTDiscoveryFinished() {
                        if (discoveryHandlerCallback != null)
                            discoveryHandlerCallback.BTDiscoveryFinished();
                    }

                    @Override
                    public void BTDiscoveryPairingRequestReceived() {
                        if (discoveryHandlerCallback != null)
                            discoveryHandlerCallback.BTDiscoveryPairingRequestReceived();
                    }
                });
                boolean val = mBluetoothAdapter.startDiscovery();
            }
        }catch (Exception e){
            ExpLogger.logE(TAG, e.getMessage());
        }
    }
    public String getLastConnectedDeviceName(){
        String devName = SettingsUtils.readPreferenceValueAsString(MainApplication.getInstance().getApplicationContext(), KEY_BT_DEV_NAME);
        if(devName == null)
            devName = "Exploride";
        return devName;
    }

    public String getLastConnectedDeviceAddress(){
        String devAddress = SettingsUtils.readPreferenceValueAsString(MainApplication.getInstance().getApplicationContext(),
                KEY_BT_DEV_ADDRESS);
        return devAddress;
    }


    public void stopBTService() {
        if (mBluetoothClientService != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mBluetoothClientService.stopBTClient();
                }
            }).start();
        }
    }

    public void onActivityResume() {

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mBluetoothClientService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mBluetoothClientService.getState() == BluetoothClientService.STATE_NONE) {
                // Start the Bluetooth services
                ExpLogger.logD(TAG, "mBluetoothClientService.start()");
                mBluetoothClientService.start();
            }
        }
    }

    /**
     * Set up the UI and background operations for BT connection.
     */
    private void setUpService(final boolean autoConnect) {
        ExpLogger.logD(TAG, "setUpService()");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        new Thread(new Runnable() {
            @Override
            public void run() {
                // Initialize the BluetoothClientService to perform bluetooth connections
                mBluetoothClientService = new BluetoothClientService(mHandler, mBluetoothAdapter);

                // Initialize the buffer for outgoing messages
                mOutStringBuffer = new StringBuffer("");
//                if(!ConnectionHandler.getInstance().isTCPEnabled())
                if(autoConnect)
                    tryBTConnect(null);
                else
                    mBluetoothClientService.connect(mDevice, false);
            }
        }).start();
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    public void sendMessage(String message) {
        // Check that we're actually BTConnected before trying anything
        if(mBluetoothClientService==null) {
            ExpLogger.logE(TAG, "sendMessage() : mBluetoothClientService is null");
            return;
        }
        if (mBluetoothClientService.getState() != STATE_CONNECTED) {
//            MainApplication.getInstance().getCurrentActivity().runOnUiThread(
//                    new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(MainApplication.getInstance().getCurrentActivity(), R.string.not_connected, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//            );
            ExpLogger.logI(TAG, "sendMessage() : Bluetooth is not connected");
            return;
        }
        synchronized (mLockObject) {
            // Check that there's actually something to send
            if (message.length() > 0) {
                // Get the message bytes and tell the BluetoothClientService to write
                message += "$#^";

                byte[] send = message.getBytes(java.nio.charset.StandardCharsets.UTF_8);
                ExpLogger.logI(TAG, "comm: bt: text to write:\n" + message + "\n");
                mBluetoothClientService.write(send);

                // Reset out string buffer to zero and clear the edit text field
                mOutStringBuffer.setLength(0);
            }
        }
        ExpLogger.logI(TAG, "sendMessage() : Bluetooth message sent\n\n" + message);
    }


    final static Object mLockObject = new Object();

    public interface BTState{
        void BTConnected(Constants.CHANNEL channel);
        void BTConnectedToDevice(String deviceName, Constants.CHANNEL channel);
        void BTConnecting(Constants.CHANNEL channel);
        void BTNotConnected(Constants.CHANNEL channel);
        void toastToShow(String msg, Constants.CHANNEL channel);
        void BTMsgReceived(String msg, Constants.CHANNEL channel);
        void BTMsgSent(String msg, Constants.CHANNEL channel);
    }
    /**
     * The Handler that gets information back from the BluetoothClientService
     */
    private final Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            final BTState btStateCallback = MainApplication.getInstance();
            switch (msg.what) {
                case Constants.Bluetooth.MESSAGE_STATE_CHANGE:
                    notifyTestRunner(msg.arg1);
                    switch (msg.arg1) {
                        case STATE_CONNECTED:

                            if(ConnectionHandler.getInstance().isTCPEnabled()&& ConnectionHandler.getInstance().useTCP()){
                                ConnectionHandler.getInstance().disconnectBT();
                                return;
                            }
                            btStateCallback.BTConnected(BT);
//                            if(!MainApplication.getInstance().isTCPEnabled()){
                            BTComm btCommResp = new BTComm();
                            btCommResp.setActionCode(ActionConstants.ActionCode.CheckInternet);
                            ConnectionHandler.getInstance().sendInfo(btCommResp, BT);
                            MainPresenter.getInstance().setBluetoothState(STATE_CONNECTED);
                            Utils.toggleLocPhone(MainApplication.getInstance().shouldSendLocationFromPhone);
                            ExpLogger.logI(TAG, "handleMessage(): BT Connected");
//                            }
                            break;
                        case BluetoothClientService.STATE_CONNECTING:
                            btStateCallback.BTConnecting(BT);
                            ExpLogger.logI(TAG, "handleMessage(): BT connecting");
                            break;
                        case BluetoothClientService.STATE_LISTEN:
                        case BluetoothClientService.STATE_NONE:
                            btStateCallback.BTNotConnected(BT);
                            MainPresenter.getInstance().setBluetoothState(BluetoothClientService.STATE_NONE);
                            ExpLogger.logI(TAG, "handleMessage(): BT not connected");
                            break;
                    }
                    break;
                case Constants.Bluetooth.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    ExpLogger.logI(TAG, "comm: bt: handler to write");
                    btStateCallback.BTMsgSent(writeMessage, BT);
                    break;
                case Constants.Bluetooth.MESSAGE_READ:
                    try {


                        byte[] readBuf = (byte[]) msg.obj;
                        // construct a string from the valid bytes in the buffer
                        String readMessage = new String(readBuf, 0, readBuf.length, java.nio.charset.StandardCharsets.UTF_8);
                        ExpLogger.logI(TAG, "comm: bt: handler to read:\n" + readMessage + "\n");
                        final String[] msgs = readMessage.split("\\$\\#");
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                for (String msgS : msgs) {
                                    if (msgS.length() > 0) {
                                        ExpLogger.logI(TAG, "comm: bt: command:\n" + msgS + "\n");
                                        btStateCallback.BTMsgReceived(msgS, BT);
                                    }
                                }
                            }
                        }).start();


                    }catch(Exception e2){
                        ExpLogger.logException(TAG, new Exception(e2.getMessage()));
                    }
                    break;
                case Constants.Bluetooth.MESSAGE_DEVICE_NAME:
                    // save the BTConnected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.Bluetooth.DEVICE_NAME);
                    final String devAddress =  msg.getData().getString(Constants.Bluetooth.DEVICE_ADDR);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SettingsUtils.writePreferenceValue(MainApplication.getInstance().getApplicationContext(),
                                    KEY_BT_DEV_ADDRESS, devAddress);
                            SettingsUtils.writePreferenceValue(MainApplication.getInstance().getApplicationContext(),
                                    KEY_BT_DEV_NAME, mConnectedDeviceName);
                            btStateCallback.BTConnectedToDevice(mConnectedDeviceName, BT);
                        }
                    }).start();
                    ExpLogger.logI(TAG, "Connected device: "+ mConnectedDeviceName);
                    break;
                case Constants.Bluetooth.MESSAGE_TOAST:
                    btStateCallback.toastToShow(msg.getData().getString(Constants.Bluetooth.TOAST), BT);
                    break;
            }
        }
    };

    private void requestConnectionInfo() {

        BTComm btCommResp = new BTComm();
        btCommResp.setActionCode(ActionConstants.ActionCode.CheckInternet);
        ConnectionHandler.getInstance().sendInfo(btCommResp, BT);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    setUpService(true);
                } else {
                    // User did not enable Bluetooth or an error occurred
                    ExpLogger.logD(TAG, "BT not enabled");
                }
        }
    }

    /**
     * Establish connection with other device
     *
     * @param data   An {@link Intent} with {@link DeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    public void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        mDevice = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        connectToService(mDevice);
    }

}
