package com.exploride.droid.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.exploride.droid.MainApplication;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.services.BluetoothClientService;

import static android.R.attr.filter;

/**
 * Created by peeyushupadhyay on 22/10/16.
 */

public class DiscoveryHandler {

    BluetoothDevice mDevice;
    BroadcastReceiver mReceiver;
    DiscoveryHandlerCallback mDiscoveryHandlerCallback;

    public DiscoveryHandler(DiscoveryHandlerCallback discoveryHandlerCallback){
        mDiscoveryHandlerCallback = discoveryHandlerCallback;
        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();
                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    // Get the BluetoothDevice object from the Intent
                    mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    String deviceName = mDevice.getName();
                    if(deviceName!=null && (deviceName.startsWith(BluetoothClientService.BTEXPNAME))) {
                        BluetoothHandler.getInstance().connectToService(mDevice);
                    }
                    if(mDiscoveryHandlerCallback!=null)
                        mDiscoveryHandlerCallback.BTDiscoveryDeviceFound();
                }else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {

                    //show progress
                    if(mDiscoveryHandlerCallback!=null)
                        mDiscoveryHandlerCallback.BTDiscoveryStarted();
                }else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                    //hide progress

                    if(mDiscoveryHandlerCallback!=null)
                        mDiscoveryHandlerCallback.BTDiscoveryFinished();
                    cleanUp();
                }

                if (BluetoothDevice.ACTION_PAIRING_REQUEST.equals(action)) {
                    mDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    ConnectionHandler.getInstance().handlePairingRequest(mDevice, intent);
                    if(mDiscoveryHandlerCallback!=null)
                        mDiscoveryHandlerCallback.BTDiscoveryPairingRequestReceived();
                }
            }
        };
        init();
    }

    /**
     * must be called when activity is finishing or you are done with the discovery task.
     */
    public void cleanUp() {
        if(mReceiver!=null) {
            MainApplication.getInstance().getCurrentActivity().unregisterReceiver(mReceiver);
            mReceiver=null;
        }
    }

    /**
     * register for callbacks.
     */
    private void init() {

        if(mReceiver!=null)
            return;
        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        MainApplication.getInstance().getCurrentActivity().registerReceiver(mReceiver, filter);

    }
    public void onPause(){
        MainApplication.getInstance().getCurrentActivity().unregisterReceiver(mReceiver);
    }
    public interface DiscoveryHandlerCallback{
        public void BTDiscoveryDeviceFound();
        public void BTDiscoveryStarted();
        public void BTDiscoveryFinished();
        public void BTDiscoveryPairingRequestReceived();

    }

}
