package com.exploride.droid;

import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;

/**
 * Created by Mayank on 1/24/2017.
 */

public interface AppVersionContract {

    /**
     * methods for Presenter. Available to View
     */
    interface VersionOps extends BasePresenter<VersionView> {
        void showHudAppVersion(String versionName);
        void switchToMapsScreen();
    }

    /**
     * methods for View. Available to Presenter
     */

    interface VersionView extends BaseView {
        void showHudAppVersionInfo(String versionName);
    }

    /**
     * methods for Model. Available to Presenter
     */
    interface VersionStore {

    }

    /**
     * methods for Presenter. Available to Model
     */
    interface VersionPresenterOps {

    }
}
