package com.exploride.droid;

import com.cloudant.exploridemobile.framework.Navigation;
import com.exploride.droid.model.SavedTripCollection;
import com.exploride.droid.presenters.BasePresenter;
import com.exploride.droid.views.BaseView;

import java.util.List;

/**
 * @author Abhijeet
 */

public interface SavedTripsContract {
    /**
     * methods for Presenter. Available to View
     */
    interface SavedTripsOps extends BasePresenter<SavedTripsContract.SavedTripsView> {

        void fetchSavedTrips();

        void setSavedTrip(Navigation savedTrip);

        SavedTripCollection getSavedTripCollection();

        void addSavedTrip(Navigation navigation);
    }


    /**
     * methods for View. Available to Presenter
     */

    interface SavedTripsView extends BaseView {

        void showSavedTrips(List<Navigation> expTrips);
    }
}
