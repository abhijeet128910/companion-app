package com.exploride.droid.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.cloudant.exploridemobile.framework.BTComm;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.presenters.MainPresenter;

/**
 * Created by Abhijeet on 6/2/17.
 */

public class TimeZoneChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        BTComm btComm = new BTComm();
        MainPresenter.getInstance().getDeviceTimezone(btComm);
        ConnectionHandler.getInstance().sendInfo(btComm);
    }
}