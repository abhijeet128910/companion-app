package com.exploride.droid.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.cloudant.exploridemobile.framework.BTComm;
import com.cloudant.exploridemobile.framework.MusicInfo;
import com.exploride.droid.handlers.ConnectionHandler;
import com.exploride.droid.logger.ExpLogger;

import java.util.Set;

/**
 * Created by abhijeet on 24/7/17.
 */

public class MusicUpdateReceiver extends BroadcastReceiver {

    public static final String TAG = "MusicUpdateReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            MusicInfo musicInfo = null;
            Bundle b = intent.getExtras();
            // Set of keys as StringExtra
            Set<String> set = b.keySet();

            String track, album, artist, genre;
            boolean isPlaying;
            int duration = intent.getIntExtra("length", 0);
            int progress = intent.getIntExtra("playbackPosition", 0);

            //
            String action = intent.getAction();
            String cmd = intent.getStringExtra("command");

            // for sony phones
            artist = intent.getStringExtra("ARTIST_NAME");
            album = intent.getStringExtra("ALBUM_NAME");
            track = intent.getStringExtra("TRACK_NAME");
            genre = intent.getStringExtra("GENRE");

            if (artist == null && album == null && track == null) {
                artist = intent.getStringExtra("artist");
                album = intent.getStringExtra("album");
                track = intent.getStringExtra("track");
                genre = intent.getStringExtra("genre");
            }

            isPlaying = intent.getBooleanExtra("playing", false);

            if (musicInfo == null)
                musicInfo = new MusicInfo();

            musicInfo.setArtist(artist);
            musicInfo.setAlbum(album);
            musicInfo.setTrack(track);
            musicInfo.setGenre(genre);
            musicInfo.setDuration(duration);
            musicInfo.setProgress(progress);
            musicInfo.setPlayingState(isPlaying ? MusicInfo.PLAYING : MusicInfo.PAUSED);

            BTComm btComm = new BTComm();
            btComm.setMusicInfo(musicInfo);
            ConnectionHandler.getInstance().sendInfo(btComm);
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }
}
