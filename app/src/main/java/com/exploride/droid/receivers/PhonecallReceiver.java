package com.exploride.droid.receivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Base64;

import com.exploride.droid.presenters.ContactsHelper;
import com.exploride.droid.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * Created on 06/02/17.
 */

public abstract class PhonecallReceiver extends BroadcastReceiver {
//The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations

    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static boolean isIncoming;
    private static String savedNumber, savedName;  //because the passed incoming is only valid in ringing


    @Override
    public void onReceive(final Context context, final Intent intent) {

        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
            savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
        }
        else{
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            final String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                state = TelephonyManager.CALL_STATE_IDLE;
                // When call is disconnected get recent call logs
                ContactsHelper.getInstance().getContacts(context);
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                state = TelephonyManager.CALL_STATE_RINGING;
            }
            final int finalSt = state;
            if (number != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        String name = Utils.getContactName(context, number);
                        String encodedImage = Utils.getContactPhoto(context, number);

                        onCallStateChanged(context, finalSt, number, name, encodedImage);
                    }
                }).start();
            }
        }
    }

    //Derived classes should override these to respond to specific events of interest
    protected abstract void onIncomingCallReceived(Context ctx, String name, String number, Date start, String photo);
    protected abstract void onIncomingCallAnswered(Context ctx, String name, String number, Date start, String  photo);
    protected abstract void onIncomingCallEnded(Context ctx, String name, String number, Date start, Date end, String photo);

    protected abstract void onOutgoingCallStarted(Context ctx, String name, String number, Date start, String photo);
    protected abstract void onOutgoingCallEnded(Context ctx, String name, String number, Date start, Date end, String photo);

    protected abstract void onMissedCall(Context ctx, String name, String number, Date start, String photo);

    //Deals with actual events

    //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    public void onCallStateChanged(Context context, int state, String number, String name, String photo) {
        if(lastState == state){
            //No change, debounce extras
            return;
        }
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = new Date();
                savedNumber = number;
                savedName = name;
                onIncomingCallReceived(context, name, number, callStartTime, photo);
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                savedName = name;
                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                if(lastState != TelephonyManager.CALL_STATE_RINGING){
                    isIncoming = false;
                    callStartTime = new Date();
                    onOutgoingCallStarted(context, savedName, savedNumber, callStartTime, photo);
                }
                else
                {
                    isIncoming = true;
                    callStartTime = new Date();
                    onIncomingCallAnswered(context, savedName, savedNumber, callStartTime, photo);
                }

                break;
            case TelephonyManager.CALL_STATE_IDLE:
                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                if(lastState == TelephonyManager.CALL_STATE_RINGING){
                    //Ring but no pickup-  a miss
                    onMissedCall(context, savedName, savedNumber, callStartTime, photo);
                }
                else if(isIncoming){
                    onIncomingCallEnded(context, savedName, savedNumber, callStartTime, new Date(), photo);
                }
                else{
                    onOutgoingCallEnded(context, savedName, savedNumber, callStartTime, new Date(), photo);
                }
                break;
        }
        lastState = state;
    }
}
