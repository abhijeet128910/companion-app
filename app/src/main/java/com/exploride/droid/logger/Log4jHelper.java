package com.exploride.droid.logger;

import android.os.Environment;

import org.apache.log4j.Logger;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Created by abhijeet on 13/11/17.
 */

public class Log4jHelper {
    private static String TAG = "Log4jHelper";

    private final static LogConfigurator mLogConfigurator = new LogConfigurator();

    /**
     * To be called
     */
    public static void configureLogger() {
        try {
            File logDirectory = new File(Environment.getExternalStorageDirectory() + "/ExplorideLogs");
            if (!logDirectory.exists()) {
                logDirectory.mkdir();
            }

            String fileDateFormat = "d_MMM_yyyy_H:mm:ss";
            SimpleDateFormat formatter = new SimpleDateFormat(fileDateFormat);
            Calendar calendar = Calendar.getInstance();

            String fileName = logDirectory + "/" + formatter.format(calendar.getTime()) + ".txt";
            String filePattern = "%d - [%c] - %p : %m%n";
            int maxBackupSize = 10;
            long maxFileSize = 1024 * 1024;

            // set the name of the log file
            mLogConfigurator.setFileName(fileName);
            // set output format of the log line
            mLogConfigurator.setFilePattern(filePattern);
            // Maximum number of backed up log files
            mLogConfigurator.setMaxBackupSize(maxBackupSize);
            // Maximum size of log file until rolling
            mLogConfigurator.setMaxFileSize(maxFileSize);

            // configure
            mLogConfigurator.configure();
        } catch (Exception e) {
            ExpLogger.logException(TAG, e);
        }
    }

    public static Logger getLogger(String name) {
        Logger logger = Logger.getLogger(name);
        return logger;
    }
}