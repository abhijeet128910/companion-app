package com.exploride.droid.logger;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

public class ExpLogger {


    /**
    log exceptions
     */
    public static void logException(String tag, Exception e){

        Log.e(tag, e.getMessage() == null ? "-----" : e.getMessage());
        Crashlytics.logException(e);
        Log4jHelper.getLogger(tag).error(e.getMessage());
    }
    /**
    log errors
     */
    public static void logE(String tag, String message){
        Log.e(tag, message == null ? "-----" : message);
        Log4jHelper.getLogger(tag).error(message);
    }

    /**
     log debug
     */
    public static void logD(String tag, String message){
        Log.d(tag, message==null?"+++---+++":message);
        Log4jHelper.getLogger(tag).debug(message);
    }

    /**
    log info
     */
    public static void logI(String tag, String message){
        Log.i(tag, message==null?"message is null":message);
        Log4jHelper.getLogger(tag).info(message);
    }

    /**
     log Info
     */
    public static void logInfo(String tag, String message){
        Log.i(tag, message == null ? "-----" : message);
        Crashlytics.log(tag + "-" + message);
        Log4jHelper.getLogger(tag).info(message);
    }
}
