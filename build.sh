#!/bin/sh

echo "...Building Android..."
echo y | android update sdk --filter "extra-android-m2repository" --no-ui -a # Grab the Android Support Repo which isn't included in the container
mkdir "${ANDROID_HOME}/licenses" || true
echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > "${ANDROID_HOME}/licenses/android-sdk-license"
echo "...Starting Android build..."
echo `pwd`
./gradlew clean
./gradlew  -d assembleDevDebug
echo "...Uploading apk..."
now="$(date)"
buildType="DroidBuilds"
curl -H "Authorization: Bearer OWGS5TYdJmAAAAAAAAACFPa_r1jQxvJ6mW_0sZJg2-4kRwbAFTeX1LzqscpO0XqI" "https://api-content.dropbox.com/1/files_put/auto/$buildType/$now/" -T ./app/build/outputs/apk/app-dev-debug.apk
echo `....`
echo `ls `
echo "...Finished Building Android...."